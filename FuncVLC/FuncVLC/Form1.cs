﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FuncVLC
{
    public partial class Form1 : Form
    {
    }

    

        public class StateIndicate
        {
            private PictureBox pbIndicateByte;
            private Image imgStateActive;
            private Image imgStateNonactive;
            short sTimeIndicate;


            private System.Threading.Timer tmShowByte;

            public StateIndicate(PictureBox pbByte, Image imgActive, Image imgNonactive, short sTime)
            {
                pbIndicateByte = pbByte;
                imgStateActive = imgActive;
                imgStateNonactive = imgNonactive;

                sTimeIndicate = sTime;
            }

            public void SetIndicateOn()
            {
                if (pbIndicateByte.InvokeRequired)
                {
                    pbIndicateByte.Invoke((MethodInvoker)(delegate()
                    {
                        pbIndicateByte.Image = imgStateActive;
                        tmShowByte = new System.Threading.Timer(TimeShowByte, null, sTimeIndicate, 0);
                    }));

                }
                else
                {
                    pbIndicateByte.Image = imgStateActive;
                    tmShowByte = new System.Threading.Timer(TimeShowByte, null, sTimeIndicate, 0);
                }
            }

            private void TimeShowByte(object o)
            {
                SetIndicateOff();
            }

            private void SetIndicateOff()
            {
                if (pbIndicateByte.InvokeRequired)
                {
                    pbIndicateByte.Invoke((MethodInvoker)(delegate()
                    {
                        pbIndicateByte.Image = imgStateNonactive;
                    }));

                }
                else
                {
                    pbIndicateByte.Image = imgStateNonactive;
                }
            }
        }


        public class StateConnection
        {
            private Button bConnection;
            private Color colorConnect;
            private Color colorDisconnect;


            public StateConnection(Button bConnect, Color clConnect, Color clDisconnect)
            {
                bConnection = bConnect;
                colorConnect = clConnect;
                colorDisconnect = clDisconnect;
            }


            public void SetConnectOn()
            {
                if (bConnection.InvokeRequired)
                {
                    bConnection.Invoke((MethodInvoker)(delegate()
                    {
                        bConnection.BackColor = colorConnect;

                    }));

                }
                else
                {
                    bConnection.BackColor = colorConnect;

                }

            }

            public void SetConnectOff()
            {
                if (bConnection.InvokeRequired)
                {
                    bConnection.Invoke((MethodInvoker)(delegate()
                    {
                        bConnection.BackColor = colorDisconnect;

                    }));

                }
                else
                {
                    bConnection.BackColor = colorDisconnect;

                }

            }

        }


        public class ShowLog
        {

            public delegate void dlgShowMessage(RichTextBox rtbLog, byte bType, string strTimeCodeAdr, string strTypeCmd, string strDetailCmd);
            public dlgShowMessage _ShowMessage;

            

            public ShowLog()
            {
                InitDelegateVLC();
            }

            private void InitDelegateVLC()
            {
                _ShowMessage = new dlgShowMessage(ShowMessage);
            }
            
            
            // show write byte
            public string DecodeHexByte(byte[] bByte)
            {
                string hex = BitConverter.ToString(bByte);
                hex = hex.Replace("-", " ");
                hex += "\n";

                return hex;
            }


            // show read/write command message
            public void ShowMessage(RichTextBox rtbLog, byte bType, string strTimeCodeAdr, string strTypeCmd, string strDetailCmd)
            {
                if (rtbLog.InvokeRequired)
                {
                    rtbLog.BeginInvoke(_ShowMessage, new object[] { rtbLog, bType, strTimeCodeAdr, strTypeCmd, strDetailCmd });
                }
                else
                {
                    try
                    {
                        if (rtbLog.TextLength == rtbLog.MaxLength - 100)
                            rtbLog.Clear();

                        rtbLog.AppendText("\n");
                        switch (bType)
                        {
                            case 0:
                                rtbLog.SelectionColor = Color.Red;
                                break;

                            case 1:
                                rtbLog.SelectionColor = Color.Blue;
                                break;

                            default:
                                rtbLog.SelectionColor = Color.Black;
                                break;
                        }

                        rtbLog.AppendText(strTimeCodeAdr);
                        //rtbLog.SelectionColor = Color.Black;
                        rtbLog.AppendText(strTypeCmd);
                        rtbLog.SelectionColor = Color.Black;
                        rtbLog.AppendText(strDetailCmd);

                        rtbLog.SelectionStart = rtbLog.TextLength;
                        rtbLog.ScrollToCaret();
                    }
                    catch (System.Exception)
                    {
                        //GlobalVar._LogFile.LogWrite("   " + this.Name + ":  " + ex.TargetSite + "   " + ex.Message); 

                    }
                }
            }


        }

        public class InitComEth
        {
            public InitComEth()
            { }

            public void InitPort(ComboBox comboBox)
            {

                string[] strPortSystem = System.IO.Ports.SerialPort.GetPortNames();

                try
                {
                    for (int i = 0; i < strPortSystem.Length; i++)
                        comboBox.Items.Add(strPortSystem[i]);

                    if (comboBox.Items.Count == 0)
                        comboBox.Items.Add("");

                    comboBox.SelectedIndex = 0;
                }
                catch (SystemException)
                { }

            }
        }
}
