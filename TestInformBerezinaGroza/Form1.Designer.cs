﻿namespace TestODClient
{
    partial class TestInformExchangeGRZ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pState = new System.Windows.Forms.Panel();
            this.pTypeConnection = new System.Windows.Forms.Panel();
            this.label55 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.mtbServerPort = new System.Windows.Forms.MaskedTextBox();
            this.mtbServerIp = new System.Windows.Forms.MaskedTextBox();
            this.btnConnectServer = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.mtbDbPort = new System.Windows.Forms.MaskedTextBox();
            this.mtbDbIp = new System.Windows.Forms.MaskedTextBox();
            this.btnConnectDB = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.nudAddressJammer = new System.Windows.Forms.NumericUpDown();
            this.label48 = new System.Windows.Forms.Label();
            this.mtbAddressIP_ = new System.Windows.Forms.MaskedTextBox();
            this.mtbPortIP_ = new System.Windows.Forms.MaskedTextBox();
            this.cmbComRRD1 = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pbWriteByteRRD = new System.Windows.Forms.PictureBox();
            this.pbReadByteRRD = new System.Windows.Forms.PictureBox();
            this.pOpenClosePort = new System.Windows.Forms.Panel();
            this.lOpenClosePort = new System.Windows.Forms.Label();
            this.bConnection = new System.Windows.Forms.Button();
            this.rbEth = new System.Windows.Forms.RadioButton();
            this.rbCom = new System.Windows.Forms.RadioButton();
            this.pLog = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.rtbLogJammer = new System.Windows.Forms.RichTextBox();
            this.ctmClearLog = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pShowLog = new System.Windows.Forms.Panel();
            this.chbAutoAnswer = new System.Windows.Forms.CheckBox();
            this.chbByteHex = new System.Windows.Forms.CheckBox();
            this.chbDetail = new System.Windows.Forms.CheckBox();
            this.chbName = new System.Windows.Forms.CheckBox();
            this.tbpSimultanBearing = new System.Windows.Forms.TabPage();
            this.bSendSimultanBearing = new System.Windows.Forms.Button();
            this.pSimultanBearing = new System.Windows.Forms.Panel();
            this.nudSimulBearCodeError = new System.Windows.Forms.NumericUpDown();
            this.label35 = new System.Windows.Forms.Label();
            this.nudSimulBearFreq = new System.Windows.Forms.NumericUpDown();
            this.label28 = new System.Windows.Forms.Label();
            this.nudSimulBearID = new System.Windows.Forms.NumericUpDown();
            this.label21 = new System.Windows.Forms.Label();
            this.nudBearingAddSB = new System.Windows.Forms.NumericUpDown();
            this.label29 = new System.Windows.Forms.Label();
            this.nudBearingMainSB = new System.Windows.Forms.NumericUpDown();
            this.label30 = new System.Windows.Forms.Label();
            this.tbpExecuteBearing = new System.Windows.Forms.TabPage();
            this.bSendExecuteBearing = new System.Windows.Forms.Button();
            this.pExecuteBearing = new System.Windows.Forms.Panel();
            this.nudExecBearCodeError = new System.Windows.Forms.NumericUpDown();
            this.label37 = new System.Windows.Forms.Label();
            this.nudExecBearFreq = new System.Windows.Forms.NumericUpDown();
            this.label27 = new System.Windows.Forms.Label();
            this.nudExecBearID = new System.Windows.Forms.NumericUpDown();
            this.lIDExecBear = new System.Windows.Forms.Label();
            this.nudBearingEB = new System.Windows.Forms.NumericUpDown();
            this.label32 = new System.Windows.Forms.Label();
            this.tbpControlSignal = new System.Windows.Forms.TabPage();
            this.bSendStateSupressFreq = new System.Windows.Forms.Button();
            this.pStateSupressFreq = new System.Windows.Forms.Panel();
            this.nudSupprCtrlCodeError = new System.Windows.Forms.NumericUpDown();
            this.label51 = new System.Windows.Forms.Label();
            this.nudSupprCtrlCountSource = new System.Windows.Forms.NumericUpDown();
            this.label52 = new System.Windows.Forms.Label();
            this.cmbTypeFreqState = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.tbpDataFHSS = new System.Windows.Forms.TabPage();
            this.rtbMemoReconFHSS = new System.Windows.Forms.RichTextBox();
            this.chbAutoReconFHSS = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.bSendDataFHSS = new System.Windows.Forms.Button();
            this.pDataFHSS = new System.Windows.Forms.Panel();
            this.chbExistAudioFHSS = new System.Windows.Forms.CheckBox();
            this.nudLevelLinkedReconFHSS = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.nudLevelOwnReconFHSS = new System.Windows.Forms.NumericUpDown();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.nudBearLinkedReconFHSS = new System.Windows.Forms.NumericUpDown();
            this.label18 = new System.Windows.Forms.Label();
            this.nudWidthReconFHSS = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.cmbModeReconFHSS = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.nudBearOwnReconFHSS = new System.Windows.Forms.NumericUpDown();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.nudSecondReconFHSS = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.nudMinuteReconFHSS = new System.Windows.Forms.NumericUpDown();
            this.label14 = new System.Windows.Forms.Label();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.nudCodeErrorSourceFHSS = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.nudFreqMaxReconFHSS = new System.Windows.Forms.NumericUpDown();
            this.label45 = new System.Windows.Forms.Label();
            this.nudFreqMinReconFHSS = new System.Windows.Forms.NumericUpDown();
            this.label44 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.tbpDataFWS = new System.Windows.Forms.TabPage();
            this.rtbMemoReconFWS = new System.Windows.Forms.RichTextBox();
            this.chbAutoReconFWS = new System.Windows.Forms.CheckBox();
            this.bRangeReconClear = new System.Windows.Forms.Button();
            this.bRangeReconAdd = new System.Windows.Forms.Button();
            this.bSendDataFWS = new System.Windows.Forms.Button();
            this.pDataFWS = new System.Windows.Forms.Panel();
            this.nudCodeErrorSourceFWS = new System.Windows.Forms.NumericUpDown();
            this.label49 = new System.Windows.Forms.Label();
            this.nudIDSReconFWS = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.nudFreqReconFWS = new System.Windows.Forms.NumericUpDown();
            this.chbExistAudioFWS = new System.Windows.Forms.CheckBox();
            this.nudLevelLinkedReconFWS = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.nudLevelOwnReconFWS = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.tbFWSDegB = new System.Windows.Forms.TextBox();
            this.nudBearLinkedReconFWS = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.nudWidthReconFWS = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.dtpTimeFindFWS = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbModeReconFWS = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chSignLatFWS = new System.Windows.Forms.CheckBox();
            this.chSignLongFWS = new System.Windows.Forms.CheckBox();
            this.nudBearOwnReconFWS = new System.Windows.Forms.NumericUpDown();
            this.tbFWSDegL = new System.Windows.Forms.TextBox();
            this.tbFWSMinL = new System.Windows.Forms.TextBox();
            this.tbFWSSecB = new System.Windows.Forms.TextBox();
            this.tbFWSSecL = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbFWSMinB = new System.Windows.Forms.TextBox();
            this.tbpReceiptJ = new System.Windows.Forms.TabPage();
            this.bSendReceiptJ = new System.Windows.Forms.Button();
            this.pReceiptJ = new System.Windows.Forms.Panel();
            this.cmbActForbid = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.cmbTypeConfirm = new System.Windows.Forms.ComboBox();
            this.nudCodeError = new System.Windows.Forms.NumericUpDown();
            this.label73 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.tbpTextJ = new System.Windows.Forms.TabPage();
            this.bSendTextMessageJammer = new System.Windows.Forms.Button();
            this.pTextMessageJammer = new System.Windows.Forms.Panel();
            this.rtbMesJam = new System.Windows.Forms.RichTextBox();
            this.tbpCoordinates = new System.Windows.Forms.TabPage();
            this.bSendCoordJammer = new System.Windows.Forms.Button();
            this.pCoordJammer = new System.Windows.Forms.Panel();
            this.nudCodeErrorCoord = new System.Windows.Forms.NumericUpDown();
            this.label46 = new System.Windows.Forms.Label();
            this.chSignLatCoord = new System.Windows.Forms.CheckBox();
            this.chSignLongCoord = new System.Windows.Forms.CheckBox();
            this.tbJamDegL = new System.Windows.Forms.TextBox();
            this.tbJamMinL = new System.Windows.Forms.TextBox();
            this.tbJamSecL = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.tbJamDegB = new System.Windows.Forms.TextBox();
            this.tbJamMinB = new System.Windows.Forms.TextBox();
            this.tbJamSecB = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tbcCmdJammer = new System.Windows.Forms.TabControl();
            this.tbpSynchronizeJ = new System.Windows.Forms.TabPage();
            this.pSynchronizeJ = new System.Windows.Forms.Panel();
            this.nudCodeErrorSynchTime = new System.Windows.Forms.NumericUpDown();
            this.label47 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label23 = new System.Windows.Forms.Label();
            this.bSendSynchronizeJ = new System.Windows.Forms.Button();
            this.tbpTestJ = new System.Windows.Forms.TabPage();
            this.bSendTestChannelJ = new System.Windows.Forms.Button();
            this.pTestChannelJ = new System.Windows.Forms.Panel();
            this.cmbStateRole = new System.Windows.Forms.ComboBox();
            this.label43 = new System.Windows.Forms.Label();
            this.cmbStateType = new System.Windows.Forms.ComboBox();
            this.label42 = new System.Windows.Forms.Label();
            this.nudStateNumber = new System.Windows.Forms.NumericUpDown();
            this.label41 = new System.Windows.Forms.Label();
            this.cmbStateRegime = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.nudStateCodeError = new System.Windows.Forms.NumericUpDown();
            this.label39 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.cmbMainRegimeTest = new System.Windows.Forms.ComboBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.chMainL1 = new System.Windows.Forms.CheckBox();
            this.chMainL2 = new System.Windows.Forms.CheckBox();
            this.chMainL3 = new System.Windows.Forms.CheckBox();
            this.chMainL4 = new System.Windows.Forms.CheckBox();
            this.chMainL5 = new System.Windows.Forms.CheckBox();
            this.chMainL6 = new System.Windows.Forms.CheckBox();
            this.chMainL7 = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.chAddL1 = new System.Windows.Forms.CheckBox();
            this.chAddL2 = new System.Windows.Forms.CheckBox();
            this.chAddL3 = new System.Windows.Forms.CheckBox();
            this.chAddL4 = new System.Windows.Forms.CheckBox();
            this.chAddL5 = new System.Windows.Forms.CheckBox();
            this.chAddL6 = new System.Windows.Forms.CheckBox();
            this.chAddL7 = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.cmbAddRegimeTest = new System.Windows.Forms.ComboBox();
            this.nudSignTest = new System.Windows.Forms.NumericUpDown();
            this.label36 = new System.Windows.Forms.Label();
            this.dtpTimeSinchro = new System.Windows.Forms.DateTimePicker();
            this.pState.SuspendLayout();
            this.pTypeConnection.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAddressJammer)).BeginInit();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteRRD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteRRD)).BeginInit();
            this.pOpenClosePort.SuspendLayout();
            this.pLog.SuspendLayout();
            this.ctmClearLog.SuspendLayout();
            this.pShowLog.SuspendLayout();
            this.tbpSimultanBearing.SuspendLayout();
            this.pSimultanBearing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSimulBearCodeError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSimulBearFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSimulBearID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingAddSB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMainSB)).BeginInit();
            this.tbpExecuteBearing.SuspendLayout();
            this.pExecuteBearing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExecBearCodeError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExecBearFreq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExecBearID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingEB)).BeginInit();
            this.tbpControlSignal.SuspendLayout();
            this.pStateSupressFreq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupprCtrlCodeError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupprCtrlCountSource)).BeginInit();
            this.tbpDataFHSS.SuspendLayout();
            this.pDataFHSS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevelLinkedReconFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevelOwnReconFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearLinkedReconFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidthReconFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearOwnReconFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSecondReconFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinuteReconFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorSourceFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMaxReconFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMinReconFHSS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tbpDataFWS.SuspendLayout();
            this.pDataFWS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorSourceFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIDSReconFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqReconFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevelLinkedReconFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevelOwnReconFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearLinkedReconFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidthReconFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearOwnReconFWS)).BeginInit();
            this.tbpReceiptJ.SuspendLayout();
            this.pReceiptJ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeError)).BeginInit();
            this.tbpTextJ.SuspendLayout();
            this.pTextMessageJammer.SuspendLayout();
            this.tbpCoordinates.SuspendLayout();
            this.pCoordJammer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorCoord)).BeginInit();
            this.tbcCmdJammer.SuspendLayout();
            this.tbpSynchronizeJ.SuspendLayout();
            this.pSynchronizeJ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorSynchTime)).BeginInit();
            this.tbpTestJ.SuspendLayout();
            this.pTestChannelJ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudStateNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStateCodeError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSignTest)).BeginInit();
            this.SuspendLayout();
            // 
            // pState
            // 
            this.pState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pState.Controls.Add(this.pTypeConnection);
            this.pState.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pState.Location = new System.Drawing.Point(0, 603);
            this.pState.Name = "pState";
            this.pState.Size = new System.Drawing.Size(826, 57);
            this.pState.TabIndex = 2;
            // 
            // pTypeConnection
            // 
            this.pTypeConnection.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pTypeConnection.Controls.Add(this.label55);
            this.pTypeConnection.Controls.Add(this.button4);
            this.pTypeConnection.Controls.Add(this.mtbServerPort);
            this.pTypeConnection.Controls.Add(this.mtbServerIp);
            this.pTypeConnection.Controls.Add(this.btnConnectServer);
            this.pTypeConnection.Controls.Add(this.label54);
            this.pTypeConnection.Controls.Add(this.mtbDbPort);
            this.pTypeConnection.Controls.Add(this.mtbDbIp);
            this.pTypeConnection.Controls.Add(this.btnConnectDB);
            this.pTypeConnection.Controls.Add(this.label53);
            this.pTypeConnection.Controls.Add(this.nudAddressJammer);
            this.pTypeConnection.Controls.Add(this.label48);
            this.pTypeConnection.Controls.Add(this.mtbAddressIP_);
            this.pTypeConnection.Controls.Add(this.mtbPortIP_);
            this.pTypeConnection.Controls.Add(this.cmbComRRD1);
            this.pTypeConnection.Controls.Add(this.panel4);
            this.pTypeConnection.Controls.Add(this.pOpenClosePort);
            this.pTypeConnection.Controls.Add(this.rbEth);
            this.pTypeConnection.Controls.Add(this.rbCom);
            this.pTypeConnection.Dock = System.Windows.Forms.DockStyle.Left;
            this.pTypeConnection.Location = new System.Drawing.Point(0, 0);
            this.pTypeConnection.Name = "pTypeConnection";
            this.pTypeConnection.Size = new System.Drawing.Size(1096, 53);
            this.pTypeConnection.TabIndex = 91;
            this.pTypeConnection.Paint += new System.Windows.Forms.PaintEventHandler(this.pTypeConnection_Paint);
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 7);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(24, 13);
            this.label55.TabIndex = 111;
            this.label55.Text = "ОД";
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(8, 24);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(20, 20);
            this.button4.TabIndex = 110;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // mtbServerPort
            // 
            this.mtbServerPort.Location = new System.Drawing.Point(769, 26);
            this.mtbServerPort.Name = "mtbServerPort";
            this.mtbServerPort.Size = new System.Drawing.Size(48, 20);
            this.mtbServerPort.TabIndex = 109;
            this.mtbServerPort.Text = "5900";
            // 
            // mtbServerIp
            // 
            this.mtbServerIp.Location = new System.Drawing.Point(671, 26);
            this.mtbServerIp.Name = "mtbServerIp";
            this.mtbServerIp.Size = new System.Drawing.Size(92, 20);
            this.mtbServerIp.TabIndex = 108;
            this.mtbServerIp.Text = "127.0.0.1";
            // 
            // btnConnectServer
            // 
            this.btnConnectServer.BackColor = System.Drawing.Color.Red;
            this.btnConnectServer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnConnectServer.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnConnectServer.FlatAppearance.BorderSize = 0;
            this.btnConnectServer.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnConnectServer.Location = new System.Drawing.Point(643, 24);
            this.btnConnectServer.Name = "btnConnectServer";
            this.btnConnectServer.Size = new System.Drawing.Size(22, 22);
            this.btnConnectServer.TabIndex = 107;
            this.btnConnectServer.UseVisualStyleBackColor = false;
            this.btnConnectServer.Click += new System.EventHandler(this.btnConnectServer_Click);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(597, 31);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(44, 13);
            this.label54.TabIndex = 106;
            this.label54.Text = "Сервер";
            // 
            // mtbDbPort
            // 
            this.mtbDbPort.Location = new System.Drawing.Point(769, 3);
            this.mtbDbPort.Name = "mtbDbPort";
            this.mtbDbPort.Size = new System.Drawing.Size(48, 20);
            this.mtbDbPort.TabIndex = 105;
            this.mtbDbPort.Text = "30051";
            // 
            // mtbDbIp
            // 
            this.mtbDbIp.Location = new System.Drawing.Point(671, 3);
            this.mtbDbIp.Name = "mtbDbIp";
            this.mtbDbIp.Size = new System.Drawing.Size(92, 20);
            this.mtbDbIp.TabIndex = 104;
            this.mtbDbIp.Text = "127.0.0.1";
            // 
            // btnConnectDB
            // 
            this.btnConnectDB.BackColor = System.Drawing.Color.Red;
            this.btnConnectDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnConnectDB.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnConnectDB.FlatAppearance.BorderSize = 0;
            this.btnConnectDB.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnConnectDB.Location = new System.Drawing.Point(643, 1);
            this.btnConnectDB.Name = "btnConnectDB";
            this.btnConnectDB.Size = new System.Drawing.Size(22, 22);
            this.btnConnectDB.TabIndex = 103;
            this.btnConnectDB.UseVisualStyleBackColor = false;
            this.btnConnectDB.Click += new System.EventHandler(this.btnConnectDB_Click);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(614, 8);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(23, 13);
            this.label53.TabIndex = 102;
            this.label53.Text = "БД";
            // 
            // nudAddressJammer
            // 
            this.nudAddressJammer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudAddressJammer.Location = new System.Drawing.Point(546, 3);
            this.nudAddressJammer.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.nudAddressJammer.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudAddressJammer.Name = "nudAddressJammer";
            this.nudAddressJammer.Size = new System.Drawing.Size(47, 20);
            this.nudAddressJammer.TabIndex = 101;
            this.nudAddressJammer.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudAddressJammer.ValueChanged += new System.EventHandler(this.nudAddressJammer_ValueChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(494, 9);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(59, 13);
            this.label48.TabIndex = 100;
            this.label48.Text = "Адрес.......";
            // 
            // mtbAddressIP_
            // 
            this.mtbAddressIP_.Location = new System.Drawing.Point(331, 4);
            this.mtbAddressIP_.Name = "mtbAddressIP_";
            this.mtbAddressIP_.Size = new System.Drawing.Size(92, 20);
            this.mtbAddressIP_.TabIndex = 98;
            this.mtbAddressIP_.Text = "192.168.150.160";
            // 
            // mtbPortIP_
            // 
            this.mtbPortIP_.Location = new System.Drawing.Point(429, 4);
            this.mtbPortIP_.Name = "mtbPortIP_";
            this.mtbPortIP_.Size = new System.Drawing.Size(48, 20);
            this.mtbPortIP_.TabIndex = 97;
            this.mtbPortIP_.Text = "9102";
            // 
            // cmbComRRD1
            // 
            this.cmbComRRD1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbComRRD1.FormattingEnabled = true;
            this.cmbComRRD1.Location = new System.Drawing.Point(193, 2);
            this.cmbComRRD1.Name = "cmbComRRD1";
            this.cmbComRRD1.Size = new System.Drawing.Size(61, 21);
            this.cmbComRRD1.TabIndex = 96;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.pbWriteByteRRD);
            this.panel4.Controls.Add(this.pbReadByteRRD);
            this.panel4.Location = new System.Drawing.Point(59, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(42, 21);
            this.panel4.TabIndex = 95;
            // 
            // pbWriteByteRRD
            // 
            this.pbWriteByteRRD.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteRRD.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteRRD.Name = "pbWriteByteRRD";
            this.pbWriteByteRRD.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteRRD.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteRRD.TabIndex = 24;
            this.pbWriteByteRRD.TabStop = false;
            // 
            // pbReadByteRRD
            // 
            this.pbReadByteRRD.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteRRD.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteRRD.Name = "pbReadByteRRD";
            this.pbReadByteRRD.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteRRD.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteRRD.TabIndex = 23;
            this.pbReadByteRRD.TabStop = false;
            // 
            // pOpenClosePort
            // 
            this.pOpenClosePort.Controls.Add(this.lOpenClosePort);
            this.pOpenClosePort.Controls.Add(this.bConnection);
            this.pOpenClosePort.Location = new System.Drawing.Point(29, 1);
            this.pOpenClosePort.Name = "pOpenClosePort";
            this.pOpenClosePort.Size = new System.Drawing.Size(30, 21);
            this.pOpenClosePort.TabIndex = 87;
            // 
            // lOpenClosePort
            // 
            this.lOpenClosePort.AutoSize = true;
            this.lOpenClosePort.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lOpenClosePort.Location = new System.Drawing.Point(28, 2);
            this.lOpenClosePort.Name = "lOpenClosePort";
            this.lOpenClosePort.Size = new System.Drawing.Size(0, 15);
            this.lOpenClosePort.TabIndex = 84;
            // 
            // bConnection
            // 
            this.bConnection.BackColor = System.Drawing.Color.Red;
            this.bConnection.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnection.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnection.FlatAppearance.BorderSize = 0;
            this.bConnection.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnection.Location = new System.Drawing.Point(4, 1);
            this.bConnection.Name = "bConnection";
            this.bConnection.Size = new System.Drawing.Size(22, 22);
            this.bConnection.TabIndex = 83;
            this.bConnection.UseVisualStyleBackColor = false;
            this.bConnection.Click += new System.EventHandler(this.bConnection_Click);
            // 
            // rbEth
            // 
            this.rbEth.AutoSize = true;
            this.rbEth.BackColor = System.Drawing.SystemColors.Control;
            this.rbEth.Location = new System.Drawing.Point(265, 5);
            this.rbEth.Name = "rbEth";
            this.rbEth.Size = new System.Drawing.Size(65, 17);
            this.rbEth.TabIndex = 92;
            this.rbEth.Text = "Ethernet";
            this.rbEth.UseVisualStyleBackColor = false;
            // 
            // rbCom
            // 
            this.rbCom.AutoSize = true;
            this.rbCom.Location = new System.Drawing.Point(129, 4);
            this.rbCom.Name = "rbCom";
            this.rbCom.Size = new System.Drawing.Size(61, 17);
            this.rbCom.TabIndex = 91;
            this.rbCom.Text = "RS-232";
            this.rbCom.UseVisualStyleBackColor = true;
            this.rbCom.CheckedChanged += new System.EventHandler(this.rbCom_CheckedChanged);
            // 
            // pLog
            // 
            this.pLog.Controls.Add(this.button3);
            this.pLog.Controls.Add(this.rtbLogJammer);
            this.pLog.Controls.Add(this.pShowLog);
            this.pLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pLog.Location = new System.Drawing.Point(300, 0);
            this.pLog.Name = "pLog";
            this.pLog.Size = new System.Drawing.Size(526, 603);
            this.pLog.TabIndex = 13;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(446, 574);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 10;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // rtbLogJammer
            // 
            this.rtbLogJammer.BackColor = System.Drawing.Color.Tan;
            this.rtbLogJammer.ContextMenuStrip = this.ctmClearLog;
            this.rtbLogJammer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLogJammer.Location = new System.Drawing.Point(0, 28);
            this.rtbLogJammer.Name = "rtbLogJammer";
            this.rtbLogJammer.Size = new System.Drawing.Size(526, 575);
            this.rtbLogJammer.TabIndex = 9;
            this.rtbLogJammer.Text = "";
            // 
            // ctmClearLog
            // 
            this.ctmClearLog.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.ctmClearLog.Name = "ctmClear";
            this.ctmClearLog.Size = new System.Drawing.Size(102, 26);
            this.ctmClearLog.Text = "Clear";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(101, 22);
            this.toolStripMenuItem1.Text = "Clear";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // pShowLog
            // 
            this.pShowLog.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pShowLog.Controls.Add(this.chbAutoAnswer);
            this.pShowLog.Controls.Add(this.chbByteHex);
            this.pShowLog.Controls.Add(this.chbDetail);
            this.pShowLog.Controls.Add(this.chbName);
            this.pShowLog.Dock = System.Windows.Forms.DockStyle.Top;
            this.pShowLog.Location = new System.Drawing.Point(0, 0);
            this.pShowLog.Name = "pShowLog";
            this.pShowLog.Size = new System.Drawing.Size(526, 28);
            this.pShowLog.TabIndex = 6;
            // 
            // chbAutoAnswer
            // 
            this.chbAutoAnswer.AutoSize = true;
            this.chbAutoAnswer.Checked = true;
            this.chbAutoAnswer.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbAutoAnswer.Location = new System.Drawing.Point(222, 4);
            this.chbAutoAnswer.Name = "chbAutoAnswer";
            this.chbAutoAnswer.Size = new System.Drawing.Size(81, 17);
            this.chbAutoAnswer.TabIndex = 70;
            this.chbAutoAnswer.Text = "Авто ответ";
            this.chbAutoAnswer.UseVisualStyleBackColor = true;
            this.chbAutoAnswer.CheckedChanged += new System.EventHandler(this.chbAutoAnswer_CheckedChanged);
            // 
            // chbByteHex
            // 
            this.chbByteHex.AutoSize = true;
            this.chbByteHex.Checked = true;
            this.chbByteHex.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbByteHex.Location = new System.Drawing.Point(9, 4);
            this.chbByteHex.Name = "chbByteHex";
            this.chbByteHex.Size = new System.Drawing.Size(48, 17);
            this.chbByteHex.TabIndex = 69;
            this.chbByteHex.Text = "HEX";
            this.chbByteHex.UseVisualStyleBackColor = true;
            // 
            // chbDetail
            // 
            this.chbDetail.AutoSize = true;
            this.chbDetail.Checked = true;
            this.chbDetail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbDetail.Location = new System.Drawing.Point(140, 3);
            this.chbDetail.Name = "chbDetail";
            this.chbDetail.Size = new System.Drawing.Size(45, 17);
            this.chbDetail.TabIndex = 67;
            this.chbDetail.Text = "Код";
            this.chbDetail.UseVisualStyleBackColor = true;
            // 
            // chbName
            // 
            this.chbName.AutoSize = true;
            this.chbName.Checked = true;
            this.chbName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbName.Location = new System.Drawing.Point(63, 3);
            this.chbName.Name = "chbName";
            this.chbName.Size = new System.Drawing.Size(76, 17);
            this.chbName.TabIndex = 68;
            this.chbName.Text = "Название";
            this.chbName.UseVisualStyleBackColor = true;
            // 
            // tbpSimultanBearing
            // 
            this.tbpSimultanBearing.BackColor = System.Drawing.SystemColors.Control;
            this.tbpSimultanBearing.Controls.Add(this.bSendSimultanBearing);
            this.tbpSimultanBearing.Controls.Add(this.pSimultanBearing);
            this.tbpSimultanBearing.Location = new System.Drawing.Point(4, 58);
            this.tbpSimultanBearing.Name = "tbpSimultanBearing";
            this.tbpSimultanBearing.Padding = new System.Windows.Forms.Padding(3);
            this.tbpSimultanBearing.Size = new System.Drawing.Size(292, 541);
            this.tbpSimultanBearing.TabIndex = 8;
            this.tbpSimultanBearing.Text = "КвП";
            // 
            // bSendSimultanBearing
            // 
            this.bSendSimultanBearing.Location = new System.Drawing.Point(195, 144);
            this.bSendSimultanBearing.Name = "bSendSimultanBearing";
            this.bSendSimultanBearing.Size = new System.Drawing.Size(75, 23);
            this.bSendSimultanBearing.TabIndex = 16;
            this.bSendSimultanBearing.Text = "Отправить";
            this.bSendSimultanBearing.UseVisualStyleBackColor = true;
            this.bSendSimultanBearing.Click += new System.EventHandler(this.bSendSimultanBearing_Click);
            // 
            // pSimultanBearing
            // 
            this.pSimultanBearing.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pSimultanBearing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pSimultanBearing.Controls.Add(this.nudSimulBearCodeError);
            this.pSimultanBearing.Controls.Add(this.label35);
            this.pSimultanBearing.Controls.Add(this.nudSimulBearFreq);
            this.pSimultanBearing.Controls.Add(this.label28);
            this.pSimultanBearing.Controls.Add(this.nudSimulBearID);
            this.pSimultanBearing.Controls.Add(this.label21);
            this.pSimultanBearing.Controls.Add(this.nudBearingAddSB);
            this.pSimultanBearing.Controls.Add(this.label29);
            this.pSimultanBearing.Controls.Add(this.nudBearingMainSB);
            this.pSimultanBearing.Controls.Add(this.label30);
            this.pSimultanBearing.Location = new System.Drawing.Point(3, 3);
            this.pSimultanBearing.Name = "pSimultanBearing";
            this.pSimultanBearing.Size = new System.Drawing.Size(271, 126);
            this.pSimultanBearing.TabIndex = 9;
            // 
            // nudSimulBearCodeError
            // 
            this.nudSimulBearCodeError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudSimulBearCodeError.Location = new System.Drawing.Point(193, 3);
            this.nudSimulBearCodeError.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudSimulBearCodeError.Name = "nudSimulBearCodeError";
            this.nudSimulBearCodeError.Size = new System.Drawing.Size(72, 20);
            this.nudSimulBearCodeError.TabIndex = 57;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(8, 11);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(208, 13);
            this.label35.TabIndex = 56;
            this.label35.Text = "Код ошибки...............................................";
            // 
            // nudSimulBearFreq
            // 
            this.nudSimulBearFreq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudSimulBearFreq.DecimalPlaces = 1;
            this.nudSimulBearFreq.Location = new System.Drawing.Point(193, 49);
            this.nudSimulBearFreq.Maximum = new decimal(new int[] {
            30000000,
            0,
            0,
            65536});
            this.nudSimulBearFreq.Name = "nudSimulBearFreq";
            this.nudSimulBearFreq.Size = new System.Drawing.Size(73, 20);
            this.nudSimulBearFreq.TabIndex = 55;
            this.nudSimulBearFreq.Value = new decimal(new int[] {
            2480009,
            0,
            0,
            65536});
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(6, 56);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(219, 13);
            this.label28.TabIndex = 54;
            this.label28.Text = "Частота,кГц................................................\\";
            // 
            // nudSimulBearID
            // 
            this.nudSimulBearID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudSimulBearID.Location = new System.Drawing.Point(192, 26);
            this.nudSimulBearID.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudSimulBearID.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSimulBearID.Name = "nudSimulBearID";
            this.nudSimulBearID.Size = new System.Drawing.Size(73, 20);
            this.nudSimulBearID.TabIndex = 47;
            this.nudSimulBearID.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(8, 33);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(195, 13);
            this.label21.TabIndex = 46;
            this.label21.Text = "ID...........................................................";
            // 
            // nudBearingAddSB
            // 
            this.nudBearingAddSB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearingAddSB.Location = new System.Drawing.Point(192, 97);
            this.nudBearingAddSB.Maximum = new decimal(new int[] {
            361,
            0,
            0,
            0});
            this.nudBearingAddSB.Name = "nudBearingAddSB";
            this.nudBearingAddSB.Size = new System.Drawing.Size(74, 20);
            this.nudBearingAddSB.TabIndex = 45;
            this.nudBearingAddSB.Value = new decimal(new int[] {
            37,
            0,
            0,
            0});
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 103);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(219, 13);
            this.label29.TabIndex = 44;
            this.label29.Text = "Пеленг ведомая, гр.....................................";
            // 
            // nudBearingMainSB
            // 
            this.nudBearingMainSB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearingMainSB.Location = new System.Drawing.Point(193, 74);
            this.nudBearingMainSB.Maximum = new decimal(new int[] {
            361,
            0,
            0,
            0});
            this.nudBearingMainSB.Name = "nudBearingMainSB";
            this.nudBearingMainSB.Size = new System.Drawing.Size(73, 20);
            this.nudBearingMainSB.TabIndex = 43;
            this.nudBearingMainSB.Value = new decimal(new int[] {
            235,
            0,
            0,
            0});
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 81);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(228, 13);
            this.label30.TabIndex = 42;
            this.label30.Text = "Пеленг ведущая, гр........................................";
            // 
            // tbpExecuteBearing
            // 
            this.tbpExecuteBearing.BackColor = System.Drawing.SystemColors.Control;
            this.tbpExecuteBearing.Controls.Add(this.bSendExecuteBearing);
            this.tbpExecuteBearing.Controls.Add(this.pExecuteBearing);
            this.tbpExecuteBearing.Location = new System.Drawing.Point(4, 58);
            this.tbpExecuteBearing.Name = "tbpExecuteBearing";
            this.tbpExecuteBearing.Padding = new System.Windows.Forms.Padding(3);
            this.tbpExecuteBearing.Size = new System.Drawing.Size(292, 541);
            this.tbpExecuteBearing.TabIndex = 7;
            this.tbpExecuteBearing.Text = "ИП";
            // 
            // bSendExecuteBearing
            // 
            this.bSendExecuteBearing.Location = new System.Drawing.Point(198, 128);
            this.bSendExecuteBearing.Name = "bSendExecuteBearing";
            this.bSendExecuteBearing.Size = new System.Drawing.Size(75, 23);
            this.bSendExecuteBearing.TabIndex = 17;
            this.bSendExecuteBearing.Text = "Отправить";
            this.bSendExecuteBearing.UseVisualStyleBackColor = true;
            this.bSendExecuteBearing.Click += new System.EventHandler(this.bSendExecuteBearing_Click);
            // 
            // pExecuteBearing
            // 
            this.pExecuteBearing.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pExecuteBearing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pExecuteBearing.Controls.Add(this.nudExecBearCodeError);
            this.pExecuteBearing.Controls.Add(this.label37);
            this.pExecuteBearing.Controls.Add(this.nudExecBearFreq);
            this.pExecuteBearing.Controls.Add(this.label27);
            this.pExecuteBearing.Controls.Add(this.nudExecBearID);
            this.pExecuteBearing.Controls.Add(this.lIDExecBear);
            this.pExecuteBearing.Controls.Add(this.nudBearingEB);
            this.pExecuteBearing.Controls.Add(this.label32);
            this.pExecuteBearing.Location = new System.Drawing.Point(3, 3);
            this.pExecuteBearing.Name = "pExecuteBearing";
            this.pExecuteBearing.Size = new System.Drawing.Size(271, 107);
            this.pExecuteBearing.TabIndex = 13;
            // 
            // nudExecBearCodeError
            // 
            this.nudExecBearCodeError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudExecBearCodeError.Location = new System.Drawing.Point(194, 3);
            this.nudExecBearCodeError.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudExecBearCodeError.Name = "nudExecBearCodeError";
            this.nudExecBearCodeError.Size = new System.Drawing.Size(72, 20);
            this.nudExecBearCodeError.TabIndex = 59;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(4, 11);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(208, 13);
            this.label37.TabIndex = 58;
            this.label37.Text = "Код ошибки...............................................";
            // 
            // nudExecBearFreq
            // 
            this.nudExecBearFreq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudExecBearFreq.DecimalPlaces = 1;
            this.nudExecBearFreq.Location = new System.Drawing.Point(193, 49);
            this.nudExecBearFreq.Maximum = new decimal(new int[] {
            300010,
            0,
            0,
            65536});
            this.nudExecBearFreq.Minimum = new decimal(new int[] {
            15000,
            0,
            0,
            65536});
            this.nudExecBearFreq.Name = "nudExecBearFreq";
            this.nudExecBearFreq.Size = new System.Drawing.Size(73, 20);
            this.nudExecBearFreq.TabIndex = 53;
            this.nudExecBearFreq.Value = new decimal(new int[] {
            15000,
            0,
            0,
            65536});
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(4, 56);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(208, 13);
            this.label27.TabIndex = 52;
            this.label27.Text = "Частота,кГц..............................................";
            // 
            // nudExecBearID
            // 
            this.nudExecBearID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudExecBearID.Location = new System.Drawing.Point(194, 26);
            this.nudExecBearID.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudExecBearID.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudExecBearID.Name = "nudExecBearID";
            this.nudExecBearID.Size = new System.Drawing.Size(73, 20);
            this.nudExecBearID.TabIndex = 45;
            this.nudExecBearID.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lIDExecBear
            // 
            this.lIDExecBear.AutoSize = true;
            this.lIDExecBear.Location = new System.Drawing.Point(4, 33);
            this.lIDExecBear.Name = "lIDExecBear";
            this.lIDExecBear.Size = new System.Drawing.Size(228, 13);
            this.lIDExecBear.TabIndex = 44;
            this.lIDExecBear.Text = "ID......................................................................";
            // 
            // nudBearingEB
            // 
            this.nudBearingEB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearingEB.Location = new System.Drawing.Point(193, 73);
            this.nudBearingEB.Maximum = new decimal(new int[] {
            361,
            0,
            0,
            0});
            this.nudBearingEB.Name = "nudBearingEB";
            this.nudBearingEB.Size = new System.Drawing.Size(73, 20);
            this.nudBearingEB.TabIndex = 43;
            this.nudBearingEB.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(4, 80);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(223, 13);
            this.label32.TabIndex = 42;
            this.label32.Text = "Пеленг , гр.....................................................";
            // 
            // tbpControlSignal
            // 
            this.tbpControlSignal.BackColor = System.Drawing.SystemColors.Control;
            this.tbpControlSignal.Controls.Add(this.bSendStateSupressFreq);
            this.tbpControlSignal.Controls.Add(this.pStateSupressFreq);
            this.tbpControlSignal.Location = new System.Drawing.Point(4, 58);
            this.tbpControlSignal.Name = "tbpControlSignal";
            this.tbpControlSignal.Padding = new System.Windows.Forms.Padding(3);
            this.tbpControlSignal.Size = new System.Drawing.Size(292, 541);
            this.tbpControlSignal.TabIndex = 6;
            this.tbpControlSignal.Text = "Контроль ИРИ";
            // 
            // bSendStateSupressFreq
            // 
            this.bSendStateSupressFreq.Location = new System.Drawing.Point(199, 93);
            this.bSendStateSupressFreq.Name = "bSendStateSupressFreq";
            this.bSendStateSupressFreq.Size = new System.Drawing.Size(75, 23);
            this.bSendStateSupressFreq.TabIndex = 18;
            this.bSendStateSupressFreq.Text = "Отправить";
            this.bSendStateSupressFreq.UseVisualStyleBackColor = true;
            this.bSendStateSupressFreq.Click += new System.EventHandler(this.bSendStateSupressFreq_Click);
            // 
            // pStateSupressFreq
            // 
            this.pStateSupressFreq.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pStateSupressFreq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pStateSupressFreq.Controls.Add(this.nudSupprCtrlCodeError);
            this.pStateSupressFreq.Controls.Add(this.label51);
            this.pStateSupressFreq.Controls.Add(this.nudSupprCtrlCountSource);
            this.pStateSupressFreq.Controls.Add(this.label52);
            this.pStateSupressFreq.Controls.Add(this.cmbTypeFreqState);
            this.pStateSupressFreq.Controls.Add(this.label22);
            this.pStateSupressFreq.Location = new System.Drawing.Point(2, 3);
            this.pStateSupressFreq.Name = "pStateSupressFreq";
            this.pStateSupressFreq.Size = new System.Drawing.Size(272, 84);
            this.pStateSupressFreq.TabIndex = 10;
            // 
            // nudSupprCtrlCodeError
            // 
            this.nudSupprCtrlCodeError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudSupprCtrlCodeError.Location = new System.Drawing.Point(183, 2);
            this.nudSupprCtrlCodeError.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudSupprCtrlCodeError.Name = "nudSupprCtrlCodeError";
            this.nudSupprCtrlCodeError.Size = new System.Drawing.Size(84, 20);
            this.nudSupprCtrlCodeError.TabIndex = 63;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(5, 10);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(208, 13);
            this.label51.TabIndex = 62;
            this.label51.Text = "Код ошибки...............................................";
            // 
            // nudSupprCtrlCountSource
            // 
            this.nudSupprCtrlCountSource.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudSupprCtrlCountSource.Location = new System.Drawing.Point(183, 54);
            this.nudSupprCtrlCountSource.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudSupprCtrlCountSource.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSupprCtrlCountSource.Name = "nudSupprCtrlCountSource";
            this.nudSupprCtrlCountSource.Size = new System.Drawing.Size(84, 20);
            this.nudSupprCtrlCountSource.TabIndex = 61;
            this.nudSupprCtrlCountSource.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(3, 61);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(277, 13);
            this.label52.TabIndex = 60;
            this.label52.Text = "Кол-во ИРИ......................................................................";
            // 
            // cmbTypeFreqState
            // 
            this.cmbTypeFreqState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeFreqState.FormattingEnabled = true;
            this.cmbTypeFreqState.Location = new System.Drawing.Point(183, 27);
            this.cmbTypeFreqState.Name = "cmbTypeFreqState";
            this.cmbTypeFreqState.Size = new System.Drawing.Size(84, 21);
            this.cmbTypeFreqState.TabIndex = 27;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(3, 36);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(185, 13);
            this.label22.TabIndex = 26;
            this.label22.Text = "Тип.....................................................";
            // 
            // tbpDataFHSS
            // 
            this.tbpDataFHSS.BackColor = System.Drawing.SystemColors.Control;
            this.tbpDataFHSS.Controls.Add(this.rtbMemoReconFHSS);
            this.tbpDataFHSS.Controls.Add(this.chbAutoReconFHSS);
            this.tbpDataFHSS.Controls.Add(this.button1);
            this.tbpDataFHSS.Controls.Add(this.button2);
            this.tbpDataFHSS.Controls.Add(this.bSendDataFHSS);
            this.tbpDataFHSS.Controls.Add(this.pDataFHSS);
            this.tbpDataFHSS.Location = new System.Drawing.Point(4, 58);
            this.tbpDataFHSS.Name = "tbpDataFHSS";
            this.tbpDataFHSS.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDataFHSS.Size = new System.Drawing.Size(292, 541);
            this.tbpDataFHSS.TabIndex = 5;
            this.tbpDataFHSS.Text = "ИРИ ППРЧ";
            // 
            // rtbMemoReconFHSS
            // 
            this.rtbMemoReconFHSS.BackColor = System.Drawing.SystemColors.Control;
            this.rtbMemoReconFHSS.Location = new System.Drawing.Point(3, 442);
            this.rtbMemoReconFHSS.Name = "rtbMemoReconFHSS";
            this.rtbMemoReconFHSS.ReadOnly = true;
            this.rtbMemoReconFHSS.Size = new System.Drawing.Size(270, 66);
            this.rtbMemoReconFHSS.TabIndex = 48;
            this.rtbMemoReconFHSS.Text = "";
            // 
            // chbAutoReconFHSS
            // 
            this.chbAutoReconFHSS.AutoSize = true;
            this.chbAutoReconFHSS.Location = new System.Drawing.Point(14, 419);
            this.chbAutoReconFHSS.Name = "chbAutoReconFHSS";
            this.chbAutoReconFHSS.Size = new System.Drawing.Size(50, 17);
            this.chbAutoReconFHSS.TabIndex = 47;
            this.chbAutoReconFHSS.Text = "Авто";
            this.chbAutoReconFHSS.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(115, 415);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 46;
            this.button1.Text = "Очистить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(196, 415);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 45;
            this.button2.Text = "Добавить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // bSendDataFHSS
            // 
            this.bSendDataFHSS.Location = new System.Drawing.Point(198, 514);
            this.bSendDataFHSS.Name = "bSendDataFHSS";
            this.bSendDataFHSS.Size = new System.Drawing.Size(75, 23);
            this.bSendDataFHSS.TabIndex = 7;
            this.bSendDataFHSS.Text = "Отправить";
            this.bSendDataFHSS.UseVisualStyleBackColor = true;
            this.bSendDataFHSS.Click += new System.EventHandler(this.bSendDataFHSS_Click);
            // 
            // pDataFHSS
            // 
            this.pDataFHSS.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pDataFHSS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pDataFHSS.Controls.Add(this.chbExistAudioFHSS);
            this.pDataFHSS.Controls.Add(this.nudLevelLinkedReconFHSS);
            this.pDataFHSS.Controls.Add(this.label16);
            this.pDataFHSS.Controls.Add(this.nudLevelOwnReconFHSS);
            this.pDataFHSS.Controls.Add(this.label17);
            this.pDataFHSS.Controls.Add(this.textBox1);
            this.pDataFHSS.Controls.Add(this.nudBearLinkedReconFHSS);
            this.pDataFHSS.Controls.Add(this.label18);
            this.pDataFHSS.Controls.Add(this.nudWidthReconFHSS);
            this.pDataFHSS.Controls.Add(this.label19);
            this.pDataFHSS.Controls.Add(this.cmbModeReconFHSS);
            this.pDataFHSS.Controls.Add(this.label20);
            this.pDataFHSS.Controls.Add(this.checkBox2);
            this.pDataFHSS.Controls.Add(this.checkBox3);
            this.pDataFHSS.Controls.Add(this.nudBearOwnReconFHSS);
            this.pDataFHSS.Controls.Add(this.textBox2);
            this.pDataFHSS.Controls.Add(this.textBox3);
            this.pDataFHSS.Controls.Add(this.textBox4);
            this.pDataFHSS.Controls.Add(this.textBox5);
            this.pDataFHSS.Controls.Add(this.label50);
            this.pDataFHSS.Controls.Add(this.textBox6);
            this.pDataFHSS.Controls.Add(this.nudSecondReconFHSS);
            this.pDataFHSS.Controls.Add(this.label15);
            this.pDataFHSS.Controls.Add(this.nudMinuteReconFHSS);
            this.pDataFHSS.Controls.Add(this.label14);
            this.pDataFHSS.Controls.Add(this.numericUpDown6);
            this.pDataFHSS.Controls.Add(this.label13);
            this.pDataFHSS.Controls.Add(this.numericUpDown5);
            this.pDataFHSS.Controls.Add(this.label12);
            this.pDataFHSS.Controls.Add(this.nudCodeErrorSourceFHSS);
            this.pDataFHSS.Controls.Add(this.label8);
            this.pDataFHSS.Controls.Add(this.nudFreqMaxReconFHSS);
            this.pDataFHSS.Controls.Add(this.label45);
            this.pDataFHSS.Controls.Add(this.nudFreqMinReconFHSS);
            this.pDataFHSS.Controls.Add(this.label44);
            this.pDataFHSS.Controls.Add(this.numericUpDown1);
            this.pDataFHSS.Controls.Add(this.label11);
            this.pDataFHSS.Location = new System.Drawing.Point(3, 3);
            this.pDataFHSS.Name = "pDataFHSS";
            this.pDataFHSS.Size = new System.Drawing.Size(274, 406);
            this.pDataFHSS.TabIndex = 6;
            // 
            // chbExistAudioFHSS
            // 
            this.chbExistAudioFHSS.AutoSize = true;
            this.chbExistAudioFHSS.Location = new System.Drawing.Point(10, 382);
            this.chbExistAudioFHSS.Name = "chbExistAudioFHSS";
            this.chbExistAudioFHSS.Size = new System.Drawing.Size(137, 17);
            this.chbExistAudioFHSS.TabIndex = 106;
            this.chbExistAudioFHSS.Text = "наличие аудиофайлов";
            this.chbExistAudioFHSS.UseVisualStyleBackColor = true;
            // 
            // nudLevelLinkedReconFHSS
            // 
            this.nudLevelLinkedReconFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudLevelLinkedReconFHSS.Location = new System.Drawing.Point(194, 357);
            this.nudLevelLinkedReconFHSS.Maximum = new decimal(new int[] {
            140,
            0,
            0,
            0});
            this.nudLevelLinkedReconFHSS.Name = "nudLevelLinkedReconFHSS";
            this.nudLevelLinkedReconFHSS.Size = new System.Drawing.Size(74, 20);
            this.nudLevelLinkedReconFHSS.TabIndex = 105;
            this.nudLevelLinkedReconFHSS.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(9, 363);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(186, 13);
            this.label16.TabIndex = 104;
            this.label16.Text = "Уровень ведомая, дБ.......................";
            // 
            // nudLevelOwnReconFHSS
            // 
            this.nudLevelOwnReconFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudLevelOwnReconFHSS.Location = new System.Drawing.Point(194, 334);
            this.nudLevelOwnReconFHSS.Maximum = new decimal(new int[] {
            140,
            0,
            0,
            0});
            this.nudLevelOwnReconFHSS.Name = "nudLevelOwnReconFHSS";
            this.nudLevelOwnReconFHSS.Size = new System.Drawing.Size(74, 20);
            this.nudLevelOwnReconFHSS.TabIndex = 103;
            this.nudLevelOwnReconFHSS.Value = new decimal(new int[] {
            56,
            0,
            0,
            0});
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(9, 341);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(192, 13);
            this.label17.TabIndex = 102;
            this.label17.Text = "Уровень ведущая, дБ.........................";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox1.Location = new System.Drawing.Point(154, 287);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(32, 20);
            this.textBox1.TabIndex = 101;
            this.textBox1.Text = "23";
            // 
            // nudBearLinkedReconFHSS
            // 
            this.nudBearLinkedReconFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearLinkedReconFHSS.Location = new System.Drawing.Point(194, 264);
            this.nudBearLinkedReconFHSS.Maximum = new decimal(new int[] {
            361,
            0,
            0,
            0});
            this.nudBearLinkedReconFHSS.Name = "nudBearLinkedReconFHSS";
            this.nudBearLinkedReconFHSS.Size = new System.Drawing.Size(74, 20);
            this.nudBearLinkedReconFHSS.TabIndex = 100;
            this.nudBearLinkedReconFHSS.Value = new decimal(new int[] {
            326,
            0,
            0,
            0});
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(9, 270);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(177, 13);
            this.label18.TabIndex = 89;
            this.label18.Text = "Пеленг ведомая, гр.......................";
            // 
            // nudWidthReconFHSS
            // 
            this.nudWidthReconFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudWidthReconFHSS.DecimalPlaces = 1;
            this.nudWidthReconFHSS.Location = new System.Drawing.Point(194, 218);
            this.nudWidthReconFHSS.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudWidthReconFHSS.Name = "nudWidthReconFHSS";
            this.nudWidthReconFHSS.Size = new System.Drawing.Size(74, 20);
            this.nudWidthReconFHSS.TabIndex = 99;
            this.nudWidthReconFHSS.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(8, 220);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(208, 13);
            this.label19.TabIndex = 98;
            this.label19.Text = "Ширина, кГц..............................................";
            // 
            // cmbModeReconFHSS
            // 
            this.cmbModeReconFHSS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModeReconFHSS.FormattingEnabled = true;
            this.cmbModeReconFHSS.Location = new System.Drawing.Point(194, 194);
            this.cmbModeReconFHSS.Name = "cmbModeReconFHSS";
            this.cmbModeReconFHSS.Size = new System.Drawing.Size(74, 21);
            this.cmbModeReconFHSS.TabIndex = 97;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(8, 197);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(180, 13);
            this.label20.TabIndex = 96;
            this.label20.Text = "Модуляция.......................................";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(12, 316);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(142, 17);
            this.checkBox2.TabIndex = 95;
            this.checkBox2.Text = "Широта..........................";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(12, 293);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(141, 17);
            this.checkBox3.TabIndex = 94;
            this.checkBox3.Text = "Долгота........................";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // nudBearOwnReconFHSS
            // 
            this.nudBearOwnReconFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearOwnReconFHSS.Location = new System.Drawing.Point(194, 241);
            this.nudBearOwnReconFHSS.Maximum = new decimal(new int[] {
            361,
            0,
            0,
            0});
            this.nudBearOwnReconFHSS.Name = "nudBearOwnReconFHSS";
            this.nudBearOwnReconFHSS.Size = new System.Drawing.Size(74, 20);
            this.nudBearOwnReconFHSS.TabIndex = 88;
            this.nudBearOwnReconFHSS.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox2.Location = new System.Drawing.Point(154, 308);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(32, 20);
            this.textBox2.TabIndex = 93;
            this.textBox2.Text = "55";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox3.Location = new System.Drawing.Point(192, 308);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(32, 20);
            this.textBox3.TabIndex = 92;
            this.textBox3.Text = "31";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox4.Location = new System.Drawing.Point(229, 286);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(40, 20);
            this.textBox4.TabIndex = 86;
            this.textBox4.Text = "44";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox5.Location = new System.Drawing.Point(229, 308);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(40, 20);
            this.textBox5.TabIndex = 91;
            this.textBox5.Text = "12";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(9, 248);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(183, 13);
            this.label50.TabIndex = 87;
            this.label50.Text = "Пеленг ведущая, гр.........................";
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.textBox6.Location = new System.Drawing.Point(192, 286);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(32, 20);
            this.textBox6.TabIndex = 90;
            this.textBox6.Text = "15";
            // 
            // nudSecondReconFHSS
            // 
            this.nudSecondReconFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudSecondReconFHSS.Location = new System.Drawing.Point(194, 171);
            this.nudSecondReconFHSS.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nudSecondReconFHSS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSecondReconFHSS.Name = "nudSecondReconFHSS";
            this.nudSecondReconFHSS.Size = new System.Drawing.Size(74, 20);
            this.nudSecondReconFHSS.TabIndex = 85;
            this.nudSecondReconFHSS.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 178);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(216, 13);
            this.label15.TabIndex = 84;
            this.label15.Text = "Секунды.......................................................";
            // 
            // nudMinuteReconFHSS
            // 
            this.nudMinuteReconFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudMinuteReconFHSS.Location = new System.Drawing.Point(194, 145);
            this.nudMinuteReconFHSS.Maximum = new decimal(new int[] {
            59,
            0,
            0,
            0});
            this.nudMinuteReconFHSS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudMinuteReconFHSS.Name = "nudMinuteReconFHSS";
            this.nudMinuteReconFHSS.Size = new System.Drawing.Size(74, 20);
            this.nudMinuteReconFHSS.TabIndex = 83;
            this.nudMinuteReconFHSS.Value = new decimal(new int[] {
            23,
            0,
            0,
            0});
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 152);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(187, 13);
            this.label14.TabIndex = 82;
            this.label14.Text = "Минуты...............................................";
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown6.Location = new System.Drawing.Point(194, 122);
            this.numericUpDown6.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown6.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(74, 20);
            this.numericUpDown6.TabIndex = 81;
            this.numericUpDown6.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(7, 129);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(188, 13);
            this.label13.TabIndex = 80;
            this.label13.Text = "Длительность....................................";
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown5.Location = new System.Drawing.Point(195, 99);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(74, 20);
            this.numericUpDown5.TabIndex = 79;
            this.numericUpDown5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 106);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(191, 13);
            this.label12.TabIndex = 78;
            this.label12.Text = "Шаг сетки,кГц.....................................";
            // 
            // nudCodeErrorSourceFHSS
            // 
            this.nudCodeErrorSourceFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudCodeErrorSourceFHSS.Location = new System.Drawing.Point(194, 2);
            this.nudCodeErrorSourceFHSS.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudCodeErrorSourceFHSS.Name = "nudCodeErrorSourceFHSS";
            this.nudCodeErrorSourceFHSS.Size = new System.Drawing.Size(75, 20);
            this.nudCodeErrorSourceFHSS.TabIndex = 77;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 9);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(208, 13);
            this.label8.TabIndex = 76;
            this.label8.Text = "Код ошибки...............................................";
            // 
            // nudFreqMaxReconFHSS
            // 
            this.nudFreqMaxReconFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudFreqMaxReconFHSS.Location = new System.Drawing.Point(195, 73);
            this.nudFreqMaxReconFHSS.Maximum = new decimal(new int[] {
            30000000,
            0,
            0,
            65536});
            this.nudFreqMaxReconFHSS.Name = "nudFreqMaxReconFHSS";
            this.nudFreqMaxReconFHSS.Size = new System.Drawing.Size(74, 20);
            this.nudFreqMaxReconFHSS.TabIndex = 75;
            this.nudFreqMaxReconFHSS.Value = new decimal(new int[] {
            2150004,
            0,
            0,
            65536});
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(8, 80);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(192, 13);
            this.label45.TabIndex = 74;
            this.label45.Text = "Частота макс,кГц...............................";
            // 
            // nudFreqMinReconFHSS
            // 
            this.nudFreqMinReconFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudFreqMinReconFHSS.Location = new System.Drawing.Point(195, 50);
            this.nudFreqMinReconFHSS.Maximum = new decimal(new int[] {
            30000000,
            0,
            0,
            65536});
            this.nudFreqMinReconFHSS.Name = "nudFreqMinReconFHSS";
            this.nudFreqMinReconFHSS.Size = new System.Drawing.Size(74, 20);
            this.nudFreqMinReconFHSS.TabIndex = 73;
            this.nudFreqMinReconFHSS.Value = new decimal(new int[] {
            1760002,
            0,
            0,
            65536});
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(8, 57);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(186, 13);
            this.label44.TabIndex = 72;
            this.label44.Text = "Частота мин,кГц...............................";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.numericUpDown1.Location = new System.Drawing.Point(195, 24);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(74, 20);
            this.numericUpDown1.TabIndex = 71;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(189, 13);
            this.label11.TabIndex = 70;
            this.label11.Text = "ID.........................................................";
            // 
            // tbpDataFWS
            // 
            this.tbpDataFWS.BackColor = System.Drawing.SystemColors.Control;
            this.tbpDataFWS.Controls.Add(this.rtbMemoReconFWS);
            this.tbpDataFWS.Controls.Add(this.chbAutoReconFWS);
            this.tbpDataFWS.Controls.Add(this.bRangeReconClear);
            this.tbpDataFWS.Controls.Add(this.bRangeReconAdd);
            this.tbpDataFWS.Controls.Add(this.bSendDataFWS);
            this.tbpDataFWS.Controls.Add(this.pDataFWS);
            this.tbpDataFWS.Location = new System.Drawing.Point(4, 58);
            this.tbpDataFWS.Name = "tbpDataFWS";
            this.tbpDataFWS.Padding = new System.Windows.Forms.Padding(3);
            this.tbpDataFWS.Size = new System.Drawing.Size(292, 541);
            this.tbpDataFWS.TabIndex = 4;
            this.tbpDataFWS.Text = "ИРИ ФРЧ";
            // 
            // rtbMemoReconFWS
            // 
            this.rtbMemoReconFWS.BackColor = System.Drawing.SystemColors.Control;
            this.rtbMemoReconFWS.Location = new System.Drawing.Point(3, 351);
            this.rtbMemoReconFWS.Name = "rtbMemoReconFWS";
            this.rtbMemoReconFWS.ReadOnly = true;
            this.rtbMemoReconFWS.Size = new System.Drawing.Size(271, 157);
            this.rtbMemoReconFWS.TabIndex = 45;
            this.rtbMemoReconFWS.Text = "";
            // 
            // chbAutoReconFWS
            // 
            this.chbAutoReconFWS.AutoSize = true;
            this.chbAutoReconFWS.Location = new System.Drawing.Point(8, 326);
            this.chbAutoReconFWS.Name = "chbAutoReconFWS";
            this.chbAutoReconFWS.Size = new System.Drawing.Size(50, 17);
            this.chbAutoReconFWS.TabIndex = 44;
            this.chbAutoReconFWS.Text = "Авто";
            this.chbAutoReconFWS.UseVisualStyleBackColor = true;
            // 
            // bRangeReconClear
            // 
            this.bRangeReconClear.Location = new System.Drawing.Point(116, 322);
            this.bRangeReconClear.Name = "bRangeReconClear";
            this.bRangeReconClear.Size = new System.Drawing.Size(75, 23);
            this.bRangeReconClear.TabIndex = 43;
            this.bRangeReconClear.Text = "Очистить";
            this.bRangeReconClear.UseVisualStyleBackColor = true;
            this.bRangeReconClear.Click += new System.EventHandler(this.bRangeReconClear_Click);
            // 
            // bRangeReconAdd
            // 
            this.bRangeReconAdd.Location = new System.Drawing.Point(197, 322);
            this.bRangeReconAdd.Name = "bRangeReconAdd";
            this.bRangeReconAdd.Size = new System.Drawing.Size(75, 23);
            this.bRangeReconAdd.TabIndex = 42;
            this.bRangeReconAdd.Text = "Добавить";
            this.bRangeReconAdd.UseVisualStyleBackColor = true;
            this.bRangeReconAdd.Click += new System.EventHandler(this.bRangeReconAdd_Click);
            // 
            // bSendDataFWS
            // 
            this.bSendDataFWS.Location = new System.Drawing.Point(199, 514);
            this.bSendDataFWS.Name = "bSendDataFWS";
            this.bSendDataFWS.Size = new System.Drawing.Size(75, 23);
            this.bSendDataFWS.TabIndex = 9;
            this.bSendDataFWS.Text = "Отправить";
            this.bSendDataFWS.UseVisualStyleBackColor = true;
            this.bSendDataFWS.Click += new System.EventHandler(this.bSendDataFWS_Click);
            // 
            // pDataFWS
            // 
            this.pDataFWS.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pDataFWS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pDataFWS.Controls.Add(this.nudCodeErrorSourceFWS);
            this.pDataFWS.Controls.Add(this.label49);
            this.pDataFWS.Controls.Add(this.nudIDSReconFWS);
            this.pDataFWS.Controls.Add(this.label9);
            this.pDataFWS.Controls.Add(this.nudFreqReconFWS);
            this.pDataFWS.Controls.Add(this.chbExistAudioFWS);
            this.pDataFWS.Controls.Add(this.nudLevelLinkedReconFWS);
            this.pDataFWS.Controls.Add(this.label1);
            this.pDataFWS.Controls.Add(this.nudLevelOwnReconFWS);
            this.pDataFWS.Controls.Add(this.label10);
            this.pDataFWS.Controls.Add(this.tbFWSDegB);
            this.pDataFWS.Controls.Add(this.nudBearLinkedReconFWS);
            this.pDataFWS.Controls.Add(this.label7);
            this.pDataFWS.Controls.Add(this.nudWidthReconFWS);
            this.pDataFWS.Controls.Add(this.label5);
            this.pDataFWS.Controls.Add(this.dtpTimeFindFWS);
            this.pDataFWS.Controls.Add(this.label4);
            this.pDataFWS.Controls.Add(this.cmbModeReconFWS);
            this.pDataFWS.Controls.Add(this.label3);
            this.pDataFWS.Controls.Add(this.label2);
            this.pDataFWS.Controls.Add(this.chSignLatFWS);
            this.pDataFWS.Controls.Add(this.chSignLongFWS);
            this.pDataFWS.Controls.Add(this.nudBearOwnReconFWS);
            this.pDataFWS.Controls.Add(this.tbFWSDegL);
            this.pDataFWS.Controls.Add(this.tbFWSMinL);
            this.pDataFWS.Controls.Add(this.tbFWSSecB);
            this.pDataFWS.Controls.Add(this.tbFWSSecL);
            this.pDataFWS.Controls.Add(this.label6);
            this.pDataFWS.Controls.Add(this.tbFWSMinB);
            this.pDataFWS.Location = new System.Drawing.Point(3, 3);
            this.pDataFWS.Name = "pDataFWS";
            this.pDataFWS.Size = new System.Drawing.Size(271, 313);
            this.pDataFWS.TabIndex = 8;
            // 
            // nudCodeErrorSourceFWS
            // 
            this.nudCodeErrorSourceFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudCodeErrorSourceFWS.Location = new System.Drawing.Point(189, 2);
            this.nudCodeErrorSourceFWS.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudCodeErrorSourceFWS.Name = "nudCodeErrorSourceFWS";
            this.nudCodeErrorSourceFWS.Size = new System.Drawing.Size(75, 20);
            this.nudCodeErrorSourceFWS.TabIndex = 71;
            this.nudCodeErrorSourceFWS.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(5, 9);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(208, 13);
            this.label49.TabIndex = 70;
            this.label49.Text = "Код ошибки...............................................";
            // 
            // nudIDSReconFWS
            // 
            this.nudIDSReconFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudIDSReconFWS.Location = new System.Drawing.Point(189, 29);
            this.nudIDSReconFWS.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.nudIDSReconFWS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudIDSReconFWS.Name = "nudIDSReconFWS";
            this.nudIDSReconFWS.Size = new System.Drawing.Size(74, 20);
            this.nudIDSReconFWS.TabIndex = 69;
            this.nudIDSReconFWS.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(4, 36);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(189, 13);
            this.label9.TabIndex = 68;
            this.label9.Text = "ID.........................................................";
            // 
            // nudFreqReconFWS
            // 
            this.nudFreqReconFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudFreqReconFWS.DecimalPlaces = 1;
            this.nudFreqReconFWS.Location = new System.Drawing.Point(190, 52);
            this.nudFreqReconFWS.Maximum = new decimal(new int[] {
            30000000,
            0,
            0,
            65536});
            this.nudFreqReconFWS.Name = "nudFreqReconFWS";
            this.nudFreqReconFWS.Size = new System.Drawing.Size(74, 20);
            this.nudFreqReconFWS.TabIndex = 67;
            this.nudFreqReconFWS.Value = new decimal(new int[] {
            5620000,
            0,
            0,
            65536});
            // 
            // chbExistAudioFWS
            // 
            this.chbExistAudioFWS.AutoSize = true;
            this.chbExistAudioFWS.Location = new System.Drawing.Point(5, 290);
            this.chbExistAudioFWS.Name = "chbExistAudioFWS";
            this.chbExistAudioFWS.Size = new System.Drawing.Size(137, 17);
            this.chbExistAudioFWS.TabIndex = 66;
            this.chbExistAudioFWS.Text = "наличие аудиофайлов";
            this.chbExistAudioFWS.UseVisualStyleBackColor = true;
            // 
            // nudLevelLinkedReconFWS
            // 
            this.nudLevelLinkedReconFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudLevelLinkedReconFWS.Location = new System.Drawing.Point(189, 265);
            this.nudLevelLinkedReconFWS.Maximum = new decimal(new int[] {
            140,
            0,
            0,
            0});
            this.nudLevelLinkedReconFWS.Name = "nudLevelLinkedReconFWS";
            this.nudLevelLinkedReconFWS.Size = new System.Drawing.Size(74, 20);
            this.nudLevelLinkedReconFWS.TabIndex = 65;
            this.nudLevelLinkedReconFWS.Value = new decimal(new int[] {
            90,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 271);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 13);
            this.label1.TabIndex = 64;
            this.label1.Text = "Уровень ведомая, дБ.......................";
            // 
            // nudLevelOwnReconFWS
            // 
            this.nudLevelOwnReconFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudLevelOwnReconFWS.Location = new System.Drawing.Point(189, 242);
            this.nudLevelOwnReconFWS.Maximum = new decimal(new int[] {
            140,
            0,
            0,
            0});
            this.nudLevelOwnReconFWS.Name = "nudLevelOwnReconFWS";
            this.nudLevelOwnReconFWS.Size = new System.Drawing.Size(74, 20);
            this.nudLevelOwnReconFWS.TabIndex = 63;
            this.nudLevelOwnReconFWS.Value = new decimal(new int[] {
            56,
            0,
            0,
            0});
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(4, 249);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(192, 13);
            this.label10.TabIndex = 62;
            this.label10.Text = "Уровень ведущая, дБ.........................";
            // 
            // tbFWSDegB
            // 
            this.tbFWSDegB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFWSDegB.Location = new System.Drawing.Point(149, 195);
            this.tbFWSDegB.Name = "tbFWSDegB";
            this.tbFWSDegB.Size = new System.Drawing.Size(32, 20);
            this.tbFWSDegB.TabIndex = 61;
            this.tbFWSDegB.Text = "23";
            // 
            // nudBearLinkedReconFWS
            // 
            this.nudBearLinkedReconFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearLinkedReconFWS.Location = new System.Drawing.Point(189, 172);
            this.nudBearLinkedReconFWS.Maximum = new decimal(new int[] {
            361,
            0,
            0,
            0});
            this.nudBearLinkedReconFWS.Name = "nudBearLinkedReconFWS";
            this.nudBearLinkedReconFWS.Size = new System.Drawing.Size(74, 20);
            this.nudBearLinkedReconFWS.TabIndex = 60;
            this.nudBearLinkedReconFWS.Value = new decimal(new int[] {
            326,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(4, 178);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(177, 13);
            this.label7.TabIndex = 41;
            this.label7.Text = "Пеленг ведомая, гр.......................";
            // 
            // nudWidthReconFWS
            // 
            this.nudWidthReconFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudWidthReconFWS.DecimalPlaces = 1;
            this.nudWidthReconFWS.Location = new System.Drawing.Point(190, 102);
            this.nudWidthReconFWS.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudWidthReconFWS.Name = "nudWidthReconFWS";
            this.nudWidthReconFWS.Size = new System.Drawing.Size(74, 20);
            this.nudWidthReconFWS.TabIndex = 57;
            this.nudWidthReconFWS.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(184, 13);
            this.label5.TabIndex = 56;
            this.label5.Text = "Время................................................";
            // 
            // dtpTimeFindFWS
            // 
            this.dtpTimeFindFWS.CalendarMonthBackground = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dtpTimeFindFWS.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpTimeFindFWS.Location = new System.Drawing.Point(190, 125);
            this.dtpTimeFindFWS.Name = "dtpTimeFindFWS";
            this.dtpTimeFindFWS.ShowUpDown = true;
            this.dtpTimeFindFWS.Size = new System.Drawing.Size(74, 20);
            this.dtpTimeFindFWS.TabIndex = 55;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(208, 13);
            this.label4.TabIndex = 54;
            this.label4.Text = "Ширина, кГц..............................................";
            // 
            // cmbModeReconFWS
            // 
            this.cmbModeReconFWS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbModeReconFWS.FormattingEnabled = true;
            this.cmbModeReconFWS.Location = new System.Drawing.Point(190, 78);
            this.cmbModeReconFWS.Name = "cmbModeReconFWS";
            this.cmbModeReconFWS.Size = new System.Drawing.Size(74, 21);
            this.cmbModeReconFWS.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(180, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Модуляция.......................................";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(184, 13);
            this.label2.TabIndex = 50;
            this.label2.Text = "Частота,кГц......................................";
            // 
            // chSignLatFWS
            // 
            this.chSignLatFWS.AutoSize = true;
            this.chSignLatFWS.Location = new System.Drawing.Point(7, 224);
            this.chSignLatFWS.Name = "chSignLatFWS";
            this.chSignLatFWS.Size = new System.Drawing.Size(142, 17);
            this.chSignLatFWS.TabIndex = 49;
            this.chSignLatFWS.Text = "Широта..........................";
            this.chSignLatFWS.UseVisualStyleBackColor = true;
            // 
            // chSignLongFWS
            // 
            this.chSignLongFWS.AutoSize = true;
            this.chSignLongFWS.Location = new System.Drawing.Point(7, 201);
            this.chSignLongFWS.Name = "chSignLongFWS";
            this.chSignLongFWS.Size = new System.Drawing.Size(141, 17);
            this.chSignLongFWS.TabIndex = 48;
            this.chSignLongFWS.Text = "Долгота........................";
            this.chSignLongFWS.UseVisualStyleBackColor = true;
            // 
            // nudBearOwnReconFWS
            // 
            this.nudBearOwnReconFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearOwnReconFWS.Location = new System.Drawing.Point(189, 149);
            this.nudBearOwnReconFWS.Maximum = new decimal(new int[] {
            361,
            0,
            0,
            0});
            this.nudBearOwnReconFWS.Name = "nudBearOwnReconFWS";
            this.nudBearOwnReconFWS.Size = new System.Drawing.Size(74, 20);
            this.nudBearOwnReconFWS.TabIndex = 39;
            this.nudBearOwnReconFWS.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // tbFWSDegL
            // 
            this.tbFWSDegL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFWSDegL.Location = new System.Drawing.Point(149, 216);
            this.tbFWSDegL.Name = "tbFWSDegL";
            this.tbFWSDegL.Size = new System.Drawing.Size(32, 20);
            this.tbFWSDegL.TabIndex = 47;
            this.tbFWSDegL.Text = "55";
            // 
            // tbFWSMinL
            // 
            this.tbFWSMinL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFWSMinL.Location = new System.Drawing.Point(187, 216);
            this.tbFWSMinL.Name = "tbFWSMinL";
            this.tbFWSMinL.Size = new System.Drawing.Size(32, 20);
            this.tbFWSMinL.TabIndex = 46;
            this.tbFWSMinL.Text = "31";
            // 
            // tbFWSSecB
            // 
            this.tbFWSSecB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFWSSecB.Location = new System.Drawing.Point(224, 194);
            this.tbFWSSecB.Name = "tbFWSSecB";
            this.tbFWSSecB.Size = new System.Drawing.Size(40, 20);
            this.tbFWSSecB.TabIndex = 37;
            this.tbFWSSecB.Text = "44";
            // 
            // tbFWSSecL
            // 
            this.tbFWSSecL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFWSSecL.Location = new System.Drawing.Point(224, 216);
            this.tbFWSSecL.Name = "tbFWSSecL";
            this.tbFWSSecL.Size = new System.Drawing.Size(40, 20);
            this.tbFWSSecL.TabIndex = 45;
            this.tbFWSSecL.Text = "12";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 156);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(183, 13);
            this.label6.TabIndex = 38;
            this.label6.Text = "Пеленг ведущая, гр.........................";
            // 
            // tbFWSMinB
            // 
            this.tbFWSMinB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFWSMinB.Location = new System.Drawing.Point(187, 194);
            this.tbFWSMinB.Name = "tbFWSMinB";
            this.tbFWSMinB.Size = new System.Drawing.Size(32, 20);
            this.tbFWSMinB.TabIndex = 42;
            this.tbFWSMinB.Text = "15";
            // 
            // tbpReceiptJ
            // 
            this.tbpReceiptJ.BackColor = System.Drawing.SystemColors.Control;
            this.tbpReceiptJ.Controls.Add(this.bSendReceiptJ);
            this.tbpReceiptJ.Controls.Add(this.pReceiptJ);
            this.tbpReceiptJ.Location = new System.Drawing.Point(4, 58);
            this.tbpReceiptJ.Name = "tbpReceiptJ";
            this.tbpReceiptJ.Padding = new System.Windows.Forms.Padding(3);
            this.tbpReceiptJ.Size = new System.Drawing.Size(292, 541);
            this.tbpReceiptJ.TabIndex = 3;
            this.tbpReceiptJ.Text = "Квитанция";
            // 
            // bSendReceiptJ
            // 
            this.bSendReceiptJ.Location = new System.Drawing.Point(195, 108);
            this.bSendReceiptJ.Name = "bSendReceiptJ";
            this.bSendReceiptJ.Size = new System.Drawing.Size(75, 23);
            this.bSendReceiptJ.TabIndex = 12;
            this.bSendReceiptJ.Text = "Отправить";
            this.bSendReceiptJ.UseVisualStyleBackColor = true;
            this.bSendReceiptJ.Click += new System.EventHandler(this.bSendReceiptJ_Click);
            // 
            // pReceiptJ
            // 
            this.pReceiptJ.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pReceiptJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pReceiptJ.Controls.Add(this.cmbActForbid);
            this.pReceiptJ.Controls.Add(this.label38);
            this.pReceiptJ.Controls.Add(this.cmbTypeConfirm);
            this.pReceiptJ.Controls.Add(this.nudCodeError);
            this.pReceiptJ.Controls.Add(this.label73);
            this.pReceiptJ.Controls.Add(this.label24);
            this.pReceiptJ.Location = new System.Drawing.Point(3, 6);
            this.pReceiptJ.Name = "pReceiptJ";
            this.pReceiptJ.Size = new System.Drawing.Size(271, 84);
            this.pReceiptJ.TabIndex = 11;
            // 
            // cmbActForbid
            // 
            this.cmbActForbid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbActForbid.FormattingEnabled = true;
            this.cmbActForbid.Location = new System.Drawing.Point(87, 53);
            this.cmbActForbid.Name = "cmbActForbid";
            this.cmbActForbid.Size = new System.Drawing.Size(179, 21);
            this.cmbActForbid.TabIndex = 24;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(2, 61);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(83, 13);
            this.label38.TabIndex = 21;
            this.label38.Text = "Тип...................";
            // 
            // cmbTypeConfirm
            // 
            this.cmbTypeConfirm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeConfirm.FormattingEnabled = true;
            this.cmbTypeConfirm.Location = new System.Drawing.Point(87, 2);
            this.cmbTypeConfirm.Name = "cmbTypeConfirm";
            this.cmbTypeConfirm.Size = new System.Drawing.Size(179, 21);
            this.cmbTypeConfirm.TabIndex = 20;
            this.cmbTypeConfirm.SelectedIndexChanged += new System.EventHandler(this.cmbTypeConfirm_SelectedIndexChanged);
            // 
            // nudCodeError
            // 
            this.nudCodeError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudCodeError.Location = new System.Drawing.Point(87, 29);
            this.nudCodeError.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudCodeError.Name = "nudCodeError";
            this.nudCodeError.Size = new System.Drawing.Size(179, 20);
            this.nudCodeError.TabIndex = 17;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(3, 37);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(208, 13);
            this.label73.TabIndex = 16;
            this.label73.Text = "Код ошибки...............................................";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(4, 10);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(154, 13);
            this.label24.TabIndex = 12;
            this.label24.Text = "Тип кдг....................................";
            // 
            // tbpTextJ
            // 
            this.tbpTextJ.BackColor = System.Drawing.SystemColors.Control;
            this.tbpTextJ.Controls.Add(this.bSendTextMessageJammer);
            this.tbpTextJ.Controls.Add(this.pTextMessageJammer);
            this.tbpTextJ.Location = new System.Drawing.Point(4, 58);
            this.tbpTextJ.Name = "tbpTextJ";
            this.tbpTextJ.Padding = new System.Windows.Forms.Padding(3);
            this.tbpTextJ.Size = new System.Drawing.Size(292, 541);
            this.tbpTextJ.TabIndex = 2;
            this.tbpTextJ.Text = "Текст";
            // 
            // bSendTextMessageJammer
            // 
            this.bSendTextMessageJammer.Location = new System.Drawing.Point(199, 122);
            this.bSendTextMessageJammer.Name = "bSendTextMessageJammer";
            this.bSendTextMessageJammer.Size = new System.Drawing.Size(75, 23);
            this.bSendTextMessageJammer.TabIndex = 21;
            this.bSendTextMessageJammer.Text = "Отправить";
            this.bSendTextMessageJammer.UseVisualStyleBackColor = true;
            this.bSendTextMessageJammer.Click += new System.EventHandler(this.bSendTextMessageJammer_Click);
            // 
            // pTextMessageJammer
            // 
            this.pTextMessageJammer.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pTextMessageJammer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTextMessageJammer.Controls.Add(this.rtbMesJam);
            this.pTextMessageJammer.Location = new System.Drawing.Point(5, 6);
            this.pTextMessageJammer.Name = "pTextMessageJammer";
            this.pTextMessageJammer.Size = new System.Drawing.Size(269, 110);
            this.pTextMessageJammer.TabIndex = 12;
            // 
            // rtbMesJam
            // 
            this.rtbMesJam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbMesJam.Location = new System.Drawing.Point(0, 0);
            this.rtbMesJam.MaxLength = 125;
            this.rtbMesJam.Name = "rtbMesJam";
            this.rtbMesJam.Size = new System.Drawing.Size(267, 108);
            this.rtbMesJam.TabIndex = 0;
            this.rtbMesJam.Text = "";
            // 
            // tbpCoordinates
            // 
            this.tbpCoordinates.BackColor = System.Drawing.SystemColors.Control;
            this.tbpCoordinates.Controls.Add(this.bSendCoordJammer);
            this.tbpCoordinates.Controls.Add(this.pCoordJammer);
            this.tbpCoordinates.Location = new System.Drawing.Point(4, 58);
            this.tbpCoordinates.Name = "tbpCoordinates";
            this.tbpCoordinates.Padding = new System.Windows.Forms.Padding(3);
            this.tbpCoordinates.Size = new System.Drawing.Size(292, 541);
            this.tbpCoordinates.TabIndex = 0;
            this.tbpCoordinates.Text = "Координаты СП";
            // 
            // bSendCoordJammer
            // 
            this.bSendCoordJammer.Location = new System.Drawing.Point(194, 97);
            this.bSendCoordJammer.Name = "bSendCoordJammer";
            this.bSendCoordJammer.Size = new System.Drawing.Size(75, 23);
            this.bSendCoordJammer.TabIndex = 19;
            this.bSendCoordJammer.Text = "Отправить";
            this.bSendCoordJammer.UseVisualStyleBackColor = true;
            this.bSendCoordJammer.Click += new System.EventHandler(this.bSendCoordJammer_Click);
            // 
            // pCoordJammer
            // 
            this.pCoordJammer.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pCoordJammer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pCoordJammer.Controls.Add(this.nudCodeErrorCoord);
            this.pCoordJammer.Controls.Add(this.label46);
            this.pCoordJammer.Controls.Add(this.chSignLatCoord);
            this.pCoordJammer.Controls.Add(this.chSignLongCoord);
            this.pCoordJammer.Controls.Add(this.tbJamDegL);
            this.pCoordJammer.Controls.Add(this.tbJamMinL);
            this.pCoordJammer.Controls.Add(this.tbJamSecL);
            this.pCoordJammer.Controls.Add(this.label25);
            this.pCoordJammer.Controls.Add(this.tbJamDegB);
            this.pCoordJammer.Controls.Add(this.tbJamMinB);
            this.pCoordJammer.Controls.Add(this.tbJamSecB);
            this.pCoordJammer.Controls.Add(this.label26);
            this.pCoordJammer.Location = new System.Drawing.Point(6, 6);
            this.pCoordJammer.Name = "pCoordJammer";
            this.pCoordJammer.Size = new System.Drawing.Size(268, 85);
            this.pCoordJammer.TabIndex = 7;
            // 
            // nudCodeErrorCoord
            // 
            this.nudCodeErrorCoord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudCodeErrorCoord.Location = new System.Drawing.Point(187, 3);
            this.nudCodeErrorCoord.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudCodeErrorCoord.Name = "nudCodeErrorCoord";
            this.nudCodeErrorCoord.Size = new System.Drawing.Size(72, 20);
            this.nudCodeErrorCoord.TabIndex = 61;
            this.nudCodeErrorCoord.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(3, 10);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(208, 13);
            this.label46.TabIndex = 60;
            this.label46.Text = "Код ошибки...............................................";
            // 
            // chSignLatCoord
            // 
            this.chSignLatCoord.AutoSize = true;
            this.chSignLatCoord.Location = new System.Drawing.Point(5, 37);
            this.chSignLatCoord.Name = "chSignLatCoord";
            this.chSignLatCoord.Size = new System.Drawing.Size(15, 14);
            this.chSignLatCoord.TabIndex = 41;
            this.chSignLatCoord.UseVisualStyleBackColor = true;
            // 
            // chSignLongCoord
            // 
            this.chSignLongCoord.AutoSize = true;
            this.chSignLongCoord.Location = new System.Drawing.Point(5, 64);
            this.chSignLongCoord.Name = "chSignLongCoord";
            this.chSignLongCoord.Size = new System.Drawing.Size(15, 14);
            this.chSignLongCoord.TabIndex = 40;
            this.chSignLongCoord.UseVisualStyleBackColor = true;
            // 
            // tbJamDegL
            // 
            this.tbJamDegL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbJamDegL.Location = new System.Drawing.Point(144, 31);
            this.tbJamDegL.Name = "tbJamDegL";
            this.tbJamDegL.Size = new System.Drawing.Size(32, 20);
            this.tbJamDegL.TabIndex = 39;
            this.tbJamDegL.Text = "55";
            // 
            // tbJamMinL
            // 
            this.tbJamMinL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbJamMinL.Location = new System.Drawing.Point(182, 31);
            this.tbJamMinL.Name = "tbJamMinL";
            this.tbJamMinL.Size = new System.Drawing.Size(32, 20);
            this.tbJamMinL.TabIndex = 38;
            this.tbJamMinL.Text = "46";
            // 
            // tbJamSecL
            // 
            this.tbJamSecL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbJamSecL.Location = new System.Drawing.Point(219, 31);
            this.tbJamSecL.Name = "tbJamSecL";
            this.tbJamSecL.Size = new System.Drawing.Size(40, 20);
            this.tbJamSecL.TabIndex = 37;
            this.tbJamSecL.Text = "37";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(26, 39);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(147, 13);
            this.label25.TabIndex = 36;
            this.label25.Text = "Широта..................................";
            // 
            // tbJamDegB
            // 
            this.tbJamDegB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbJamDegB.Location = new System.Drawing.Point(144, 57);
            this.tbJamDegB.Name = "tbJamDegB";
            this.tbJamDegB.Size = new System.Drawing.Size(32, 20);
            this.tbJamDegB.TabIndex = 35;
            this.tbJamDegB.Text = "37";
            // 
            // tbJamMinB
            // 
            this.tbJamMinB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbJamMinB.Location = new System.Drawing.Point(182, 57);
            this.tbJamMinB.Name = "tbJamMinB";
            this.tbJamMinB.Size = new System.Drawing.Size(32, 20);
            this.tbJamMinB.TabIndex = 34;
            this.tbJamMinB.Text = "23";
            // 
            // tbJamSecB
            // 
            this.tbJamSecB.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbJamSecB.Location = new System.Drawing.Point(219, 57);
            this.tbJamSecB.Name = "tbJamSecB";
            this.tbJamSecB.Size = new System.Drawing.Size(40, 20);
            this.tbJamSecB.TabIndex = 33;
            this.tbJamSecB.Text = "22";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(26, 64);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(146, 13);
            this.label26.TabIndex = 32;
            this.label26.Text = "Долгота................................";
            // 
            // tbcCmdJammer
            // 
            this.tbcCmdJammer.Controls.Add(this.tbpCoordinates);
            this.tbcCmdJammer.Controls.Add(this.tbpSynchronizeJ);
            this.tbcCmdJammer.Controls.Add(this.tbpTextJ);
            this.tbcCmdJammer.Controls.Add(this.tbpReceiptJ);
            this.tbcCmdJammer.Controls.Add(this.tbpDataFWS);
            this.tbcCmdJammer.Controls.Add(this.tbpDataFHSS);
            this.tbcCmdJammer.Controls.Add(this.tbpControlSignal);
            this.tbcCmdJammer.Controls.Add(this.tbpExecuteBearing);
            this.tbcCmdJammer.Controls.Add(this.tbpSimultanBearing);
            this.tbcCmdJammer.Controls.Add(this.tbpTestJ);
            this.tbcCmdJammer.Dock = System.Windows.Forms.DockStyle.Left;
            this.tbcCmdJammer.Location = new System.Drawing.Point(0, 0);
            this.tbcCmdJammer.Multiline = true;
            this.tbcCmdJammer.Name = "tbcCmdJammer";
            this.tbcCmdJammer.SelectedIndex = 0;
            this.tbcCmdJammer.Size = new System.Drawing.Size(300, 603);
            this.tbcCmdJammer.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tbcCmdJammer.TabIndex = 12;
            this.tbcCmdJammer.Visible = false;
            // 
            // tbpSynchronizeJ
            // 
            this.tbpSynchronizeJ.BackColor = System.Drawing.SystemColors.Control;
            this.tbpSynchronizeJ.Controls.Add(this.pSynchronizeJ);
            this.tbpSynchronizeJ.Controls.Add(this.bSendSynchronizeJ);
            this.tbpSynchronizeJ.Location = new System.Drawing.Point(4, 58);
            this.tbpSynchronizeJ.Name = "tbpSynchronizeJ";
            this.tbpSynchronizeJ.Padding = new System.Windows.Forms.Padding(3);
            this.tbpSynchronizeJ.Size = new System.Drawing.Size(292, 541);
            this.tbpSynchronizeJ.TabIndex = 1;
            this.tbpSynchronizeJ.Text = "Синхронизация";
            // 
            // pSynchronizeJ
            // 
            this.pSynchronizeJ.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pSynchronizeJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pSynchronizeJ.Controls.Add(this.nudCodeErrorSynchTime);
            this.pSynchronizeJ.Controls.Add(this.label47);
            this.pSynchronizeJ.Controls.Add(this.dateTimePicker1);
            this.pSynchronizeJ.Controls.Add(this.label23);
            this.pSynchronizeJ.Location = new System.Drawing.Point(5, 6);
            this.pSynchronizeJ.Name = "pSynchronizeJ";
            this.pSynchronizeJ.Size = new System.Drawing.Size(269, 58);
            this.pSynchronizeJ.TabIndex = 21;
            // 
            // nudCodeErrorSynchTime
            // 
            this.nudCodeErrorSynchTime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudCodeErrorSynchTime.Location = new System.Drawing.Point(189, 3);
            this.nudCodeErrorSynchTime.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudCodeErrorSynchTime.Name = "nudCodeErrorSynchTime";
            this.nudCodeErrorSynchTime.Size = new System.Drawing.Size(74, 20);
            this.nudCodeErrorSynchTime.TabIndex = 63;
            this.nudCodeErrorSynchTime.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(5, 10);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(208, 13);
            this.label47.TabIndex = 62;
            this.label47.Text = "Код ошибки...............................................";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CalendarMonthBackground = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dateTimePicker1.Location = new System.Drawing.Point(189, 30);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.ShowUpDown = true;
            this.dateTimePicker1.Size = new System.Drawing.Size(74, 20);
            this.dateTimePicker1.TabIndex = 19;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 37);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(226, 13);
            this.label23.TabIndex = 18;
            this.label23.Text = "Время..............................................................";
            // 
            // bSendSynchronizeJ
            // 
            this.bSendSynchronizeJ.Location = new System.Drawing.Point(199, 70);
            this.bSendSynchronizeJ.Name = "bSendSynchronizeJ";
            this.bSendSynchronizeJ.Size = new System.Drawing.Size(75, 23);
            this.bSendSynchronizeJ.TabIndex = 20;
            this.bSendSynchronizeJ.Text = "Отправить";
            this.bSendSynchronizeJ.UseVisualStyleBackColor = true;
            this.bSendSynchronizeJ.Click += new System.EventHandler(this.bSendSynchronizeJ_Click);
            // 
            // tbpTestJ
            // 
            this.tbpTestJ.BackColor = System.Drawing.SystemColors.Control;
            this.tbpTestJ.Controls.Add(this.bSendTestChannelJ);
            this.tbpTestJ.Controls.Add(this.pTestChannelJ);
            this.tbpTestJ.Location = new System.Drawing.Point(4, 58);
            this.tbpTestJ.Name = "tbpTestJ";
            this.tbpTestJ.Padding = new System.Windows.Forms.Padding(3);
            this.tbpTestJ.Size = new System.Drawing.Size(292, 541);
            this.tbpTestJ.TabIndex = 9;
            this.tbpTestJ.Text = "Состояние";
            // 
            // bSendTestChannelJ
            // 
            this.bSendTestChannelJ.Location = new System.Drawing.Point(195, 149);
            this.bSendTestChannelJ.Name = "bSendTestChannelJ";
            this.bSendTestChannelJ.Size = new System.Drawing.Size(75, 23);
            this.bSendTestChannelJ.TabIndex = 15;
            this.bSendTestChannelJ.Text = "Отправить";
            this.bSendTestChannelJ.UseVisualStyleBackColor = true;
            this.bSendTestChannelJ.Click += new System.EventHandler(this.bSendTestChannelJ_Click);
            // 
            // pTestChannelJ
            // 
            this.pTestChannelJ.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pTestChannelJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTestChannelJ.Controls.Add(this.cmbStateRole);
            this.pTestChannelJ.Controls.Add(this.label43);
            this.pTestChannelJ.Controls.Add(this.cmbStateType);
            this.pTestChannelJ.Controls.Add(this.label42);
            this.pTestChannelJ.Controls.Add(this.nudStateNumber);
            this.pTestChannelJ.Controls.Add(this.label41);
            this.pTestChannelJ.Controls.Add(this.cmbStateRegime);
            this.pTestChannelJ.Controls.Add(this.label40);
            this.pTestChannelJ.Controls.Add(this.nudStateCodeError);
            this.pTestChannelJ.Controls.Add(this.label39);
            this.pTestChannelJ.Location = new System.Drawing.Point(3, 3);
            this.pTestChannelJ.Name = "pTestChannelJ";
            this.pTestChannelJ.Size = new System.Drawing.Size(271, 140);
            this.pTestChannelJ.TabIndex = 14;
            // 
            // cmbStateRole
            // 
            this.cmbStateRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStateRole.FormattingEnabled = true;
            this.cmbStateRole.Location = new System.Drawing.Point(162, 107);
            this.cmbStateRole.Name = "cmbStateRole";
            this.cmbStateRole.Size = new System.Drawing.Size(104, 21);
            this.cmbStateRole.TabIndex = 69;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 114);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(176, 13);
            this.label43.TabIndex = 68;
            this.label43.Text = "Роль................................................";
            // 
            // cmbStateType
            // 
            this.cmbStateType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStateType.FormattingEnabled = true;
            this.cmbStateType.Location = new System.Drawing.Point(162, 80);
            this.cmbStateType.Name = "cmbStateType";
            this.cmbStateType.Size = new System.Drawing.Size(104, 21);
            this.cmbStateType.TabIndex = 67;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(6, 90);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(182, 13);
            this.label42.TabIndex = 66;
            this.label42.Text = "Тип....................................................";
            // 
            // nudStateNumber
            // 
            this.nudStateNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudStateNumber.Location = new System.Drawing.Point(162, 56);
            this.nudStateNumber.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.nudStateNumber.Name = "nudStateNumber";
            this.nudStateNumber.Size = new System.Drawing.Size(104, 20);
            this.nudStateNumber.TabIndex = 65;
            this.nudStateNumber.Value = new decimal(new int[] {
            111,
            0,
            0,
            0});
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(6, 64);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(182, 13);
            this.label41.TabIndex = 64;
            this.label41.Text = "Номер...............................................";
            // 
            // cmbStateRegime
            // 
            this.cmbStateRegime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStateRegime.FormattingEnabled = true;
            this.cmbStateRegime.Location = new System.Drawing.Point(162, 31);
            this.cmbStateRegime.Name = "cmbStateRegime";
            this.cmbStateRegime.Size = new System.Drawing.Size(104, 21);
            this.cmbStateRegime.TabIndex = 63;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(6, 39);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(183, 13);
            this.label40.TabIndex = 62;
            this.label40.Text = "Режим...............................................";
            // 
            // nudStateCodeError
            // 
            this.nudStateCodeError.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudStateCodeError.Location = new System.Drawing.Point(162, 7);
            this.nudStateCodeError.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nudStateCodeError.Name = "nudStateCodeError";
            this.nudStateCodeError.Size = new System.Drawing.Size(104, 20);
            this.nudStateCodeError.TabIndex = 61;
            this.nudStateCodeError.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(6, 15);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(208, 13);
            this.label39.TabIndex = 60;
            this.label39.Text = "Код ошибки...............................................";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(4, 10);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(174, 13);
            this.label33.TabIndex = 12;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(4, 34);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(179, 13);
            this.label31.TabIndex = 26;
            // 
            // cmbMainRegimeTest
            // 
            this.cmbMainRegimeTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMainRegimeTest.FormattingEnabled = true;
            this.cmbMainRegimeTest.Location = new System.Drawing.Point(120, 26);
            this.cmbMainRegimeTest.Name = "cmbMainRegimeTest";
            this.cmbMainRegimeTest.Size = new System.Drawing.Size(116, 21);
            this.cmbMainRegimeTest.TabIndex = 27;
            // 
            // groupBox5
            // 
            this.groupBox5.Location = new System.Drawing.Point(13, 77);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(92, 149);
            this.groupBox5.TabIndex = 30;
            this.groupBox5.TabStop = false;
            // 
            // chMainL1
            // 
            this.chMainL1.AutoSize = true;
            this.chMainL1.Checked = true;
            this.chMainL1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chMainL1.Location = new System.Drawing.Point(7, 19);
            this.chMainL1.Name = "chMainL1";
            this.chMainL1.Size = new System.Drawing.Size(70, 17);
            this.chMainL1.TabIndex = 17;
            this.chMainL1.Text = "..........Л1";
            this.chMainL1.UseVisualStyleBackColor = true;
            // 
            // chMainL2
            // 
            this.chMainL2.AutoSize = true;
            this.chMainL2.Location = new System.Drawing.Point(7, 37);
            this.chMainL2.Name = "chMainL2";
            this.chMainL2.Size = new System.Drawing.Size(70, 17);
            this.chMainL2.TabIndex = 18;
            this.chMainL2.Text = "..........Л2";
            this.chMainL2.UseVisualStyleBackColor = true;
            // 
            // chMainL3
            // 
            this.chMainL3.AutoSize = true;
            this.chMainL3.Checked = true;
            this.chMainL3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chMainL3.Location = new System.Drawing.Point(7, 55);
            this.chMainL3.Name = "chMainL3";
            this.chMainL3.Size = new System.Drawing.Size(70, 17);
            this.chMainL3.TabIndex = 19;
            this.chMainL3.Text = "..........Л3";
            this.chMainL3.UseVisualStyleBackColor = true;
            // 
            // chMainL4
            // 
            this.chMainL4.AutoSize = true;
            this.chMainL4.Checked = true;
            this.chMainL4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chMainL4.Location = new System.Drawing.Point(7, 73);
            this.chMainL4.Name = "chMainL4";
            this.chMainL4.Size = new System.Drawing.Size(70, 17);
            this.chMainL4.TabIndex = 20;
            this.chMainL4.Text = "..........Л4";
            this.chMainL4.UseVisualStyleBackColor = true;
            // 
            // chMainL5
            // 
            this.chMainL5.AutoSize = true;
            this.chMainL5.Checked = true;
            this.chMainL5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chMainL5.Location = new System.Drawing.Point(7, 91);
            this.chMainL5.Name = "chMainL5";
            this.chMainL5.Size = new System.Drawing.Size(70, 17);
            this.chMainL5.TabIndex = 21;
            this.chMainL5.Text = "..........Л5";
            this.chMainL5.UseVisualStyleBackColor = true;
            // 
            // chMainL6
            // 
            this.chMainL6.AutoSize = true;
            this.chMainL6.Checked = true;
            this.chMainL6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chMainL6.Location = new System.Drawing.Point(7, 109);
            this.chMainL6.Name = "chMainL6";
            this.chMainL6.Size = new System.Drawing.Size(70, 17);
            this.chMainL6.TabIndex = 22;
            this.chMainL6.Text = "..........Л6";
            this.chMainL6.UseVisualStyleBackColor = true;
            // 
            // chMainL7
            // 
            this.chMainL7.AutoSize = true;
            this.chMainL7.Location = new System.Drawing.Point(7, 127);
            this.chMainL7.Name = "chMainL7";
            this.chMainL7.Size = new System.Drawing.Size(70, 17);
            this.chMainL7.TabIndex = 23;
            this.chMainL7.Text = "..........Л7";
            this.chMainL7.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Location = new System.Drawing.Point(127, 78);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(94, 149);
            this.groupBox6.TabIndex = 31;
            this.groupBox6.TabStop = false;
            // 
            // chAddL1
            // 
            this.chAddL1.AutoSize = true;
            this.chAddL1.Checked = true;
            this.chAddL1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chAddL1.Location = new System.Drawing.Point(7, 19);
            this.chAddL1.Name = "chAddL1";
            this.chAddL1.Size = new System.Drawing.Size(70, 17);
            this.chAddL1.TabIndex = 17;
            this.chAddL1.Text = "..........Л1";
            this.chAddL1.UseVisualStyleBackColor = true;
            // 
            // chAddL2
            // 
            this.chAddL2.AutoSize = true;
            this.chAddL2.Checked = true;
            this.chAddL2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chAddL2.Location = new System.Drawing.Point(7, 37);
            this.chAddL2.Name = "chAddL2";
            this.chAddL2.Size = new System.Drawing.Size(70, 17);
            this.chAddL2.TabIndex = 18;
            this.chAddL2.Text = "..........Л2";
            this.chAddL2.UseVisualStyleBackColor = true;
            // 
            // chAddL3
            // 
            this.chAddL3.AutoSize = true;
            this.chAddL3.Location = new System.Drawing.Point(7, 55);
            this.chAddL3.Name = "chAddL3";
            this.chAddL3.Size = new System.Drawing.Size(70, 17);
            this.chAddL3.TabIndex = 19;
            this.chAddL3.Text = "..........Л3";
            this.chAddL3.UseVisualStyleBackColor = true;
            // 
            // chAddL4
            // 
            this.chAddL4.AutoSize = true;
            this.chAddL4.Checked = true;
            this.chAddL4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chAddL4.Location = new System.Drawing.Point(7, 73);
            this.chAddL4.Name = "chAddL4";
            this.chAddL4.Size = new System.Drawing.Size(70, 17);
            this.chAddL4.TabIndex = 20;
            this.chAddL4.Text = "..........Л4";
            this.chAddL4.UseVisualStyleBackColor = true;
            // 
            // chAddL5
            // 
            this.chAddL5.AutoSize = true;
            this.chAddL5.Location = new System.Drawing.Point(7, 91);
            this.chAddL5.Name = "chAddL5";
            this.chAddL5.Size = new System.Drawing.Size(70, 17);
            this.chAddL5.TabIndex = 21;
            this.chAddL5.Text = "..........Л5";
            this.chAddL5.UseVisualStyleBackColor = true;
            // 
            // chAddL6
            // 
            this.chAddL6.AutoSize = true;
            this.chAddL6.Checked = true;
            this.chAddL6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chAddL6.Location = new System.Drawing.Point(7, 109);
            this.chAddL6.Name = "chAddL6";
            this.chAddL6.Size = new System.Drawing.Size(70, 17);
            this.chAddL6.TabIndex = 22;
            this.chAddL6.Text = "..........Л6";
            this.chAddL6.UseVisualStyleBackColor = true;
            // 
            // chAddL7
            // 
            this.chAddL7.AutoSize = true;
            this.chAddL7.Checked = true;
            this.chAddL7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chAddL7.Location = new System.Drawing.Point(7, 127);
            this.chAddL7.Name = "chAddL7";
            this.chAddL7.Size = new System.Drawing.Size(70, 17);
            this.chAddL7.TabIndex = 23;
            this.chAddL7.Text = "..........Л7";
            this.chAddL7.UseVisualStyleBackColor = true;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(4, 58);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(194, 13);
            this.label34.TabIndex = 32;
            // 
            // cmbAddRegimeTest
            // 
            this.cmbAddRegimeTest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAddRegimeTest.FormattingEnabled = true;
            this.cmbAddRegimeTest.Location = new System.Drawing.Point(120, 50);
            this.cmbAddRegimeTest.Name = "cmbAddRegimeTest";
            this.cmbAddRegimeTest.Size = new System.Drawing.Size(117, 21);
            this.cmbAddRegimeTest.TabIndex = 33;
            // 
            // nudSignTest
            // 
            this.nudSignTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudSignTest.Location = new System.Drawing.Point(168, 3);
            this.nudSignTest.Maximum = new decimal(new int[] {
            21,
            0,
            0,
            0});
            this.nudSignTest.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSignTest.Name = "nudSignTest";
            this.nudSignTest.Size = new System.Drawing.Size(70, 20);
            this.nudSignTest.TabIndex = 34;
            this.nudSignTest.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(2, 12);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(166, 13);
            this.label36.TabIndex = 18;
            // 
            // dtpTimeSinchro
            // 
            this.dtpTimeSinchro.CalendarMonthBackground = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dtpTimeSinchro.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpTimeSinchro.Location = new System.Drawing.Point(164, 3);
            this.dtpTimeSinchro.Name = "dtpTimeSinchro";
            this.dtpTimeSinchro.ShowUpDown = true;
            this.dtpTimeSinchro.Size = new System.Drawing.Size(74, 20);
            this.dtpTimeSinchro.TabIndex = 19;
            // 
            // TestInformExchangeGRZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(826, 660);
            this.Controls.Add(this.pLog);
            this.Controls.Add(this.tbcCmdJammer);
            this.Controls.Add(this.pState);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "TestInformExchangeGRZ";
            this.Text = "ОД клиент";
            this.pState.ResumeLayout(false);
            this.pTypeConnection.ResumeLayout(false);
            this.pTypeConnection.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAddressJammer)).EndInit();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteRRD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteRRD)).EndInit();
            this.pOpenClosePort.ResumeLayout(false);
            this.pOpenClosePort.PerformLayout();
            this.pLog.ResumeLayout(false);
            this.ctmClearLog.ResumeLayout(false);
            this.pShowLog.ResumeLayout(false);
            this.pShowLog.PerformLayout();
            this.tbpSimultanBearing.ResumeLayout(false);
            this.pSimultanBearing.ResumeLayout(false);
            this.pSimultanBearing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSimulBearCodeError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSimulBearFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSimulBearID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingAddSB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMainSB)).EndInit();
            this.tbpExecuteBearing.ResumeLayout(false);
            this.pExecuteBearing.ResumeLayout(false);
            this.pExecuteBearing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudExecBearCodeError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExecBearFreq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudExecBearID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingEB)).EndInit();
            this.tbpControlSignal.ResumeLayout(false);
            this.pStateSupressFreq.ResumeLayout(false);
            this.pStateSupressFreq.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupprCtrlCodeError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSupprCtrlCountSource)).EndInit();
            this.tbpDataFHSS.ResumeLayout(false);
            this.tbpDataFHSS.PerformLayout();
            this.pDataFHSS.ResumeLayout(false);
            this.pDataFHSS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevelLinkedReconFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevelOwnReconFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearLinkedReconFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidthReconFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearOwnReconFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSecondReconFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudMinuteReconFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorSourceFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMaxReconFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqMinReconFHSS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tbpDataFWS.ResumeLayout(false);
            this.tbpDataFWS.PerformLayout();
            this.pDataFWS.ResumeLayout(false);
            this.pDataFWS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorSourceFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudIDSReconFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqReconFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevelLinkedReconFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevelOwnReconFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearLinkedReconFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudWidthReconFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearOwnReconFWS)).EndInit();
            this.tbpReceiptJ.ResumeLayout(false);
            this.pReceiptJ.ResumeLayout(false);
            this.pReceiptJ.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeError)).EndInit();
            this.tbpTextJ.ResumeLayout(false);
            this.pTextMessageJammer.ResumeLayout(false);
            this.tbpCoordinates.ResumeLayout(false);
            this.pCoordJammer.ResumeLayout(false);
            this.pCoordJammer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorCoord)).EndInit();
            this.tbcCmdJammer.ResumeLayout(false);
            this.tbpSynchronizeJ.ResumeLayout(false);
            this.pSynchronizeJ.ResumeLayout(false);
            this.pSynchronizeJ.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorSynchTime)).EndInit();
            this.tbpTestJ.ResumeLayout(false);
            this.pTestChannelJ.ResumeLayout(false);
            this.pTestChannelJ.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudStateNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudStateCodeError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSignTest)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pState;
        private System.Windows.Forms.Panel pTypeConnection;
        private System.Windows.Forms.Panel pOpenClosePort;
        private System.Windows.Forms.Label lOpenClosePort;
        private System.Windows.Forms.Button bConnection;
        private System.Windows.Forms.RadioButton rbEth;
        private System.Windows.Forms.RadioButton rbCom;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.PictureBox pbWriteByteRRD;
        private System.Windows.Forms.PictureBox pbReadByteRRD;
        private System.Windows.Forms.ComboBox cmbComRRD1;
        private System.Windows.Forms.Panel pLog;
        private System.Windows.Forms.RichTextBox rtbLogJammer;
        private System.Windows.Forms.Panel pShowLog;
        private System.Windows.Forms.CheckBox chbByteHex;
        private System.Windows.Forms.CheckBox chbDetail;
        private System.Windows.Forms.CheckBox chbName;
        private System.Windows.Forms.TabPage tbpSimultanBearing;
        private System.Windows.Forms.Button bSendSimultanBearing;
        private System.Windows.Forms.Panel pSimultanBearing;
        private System.Windows.Forms.NumericUpDown nudBearingAddSB;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.NumericUpDown nudBearingMainSB;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TabPage tbpExecuteBearing;
        private System.Windows.Forms.Button bSendExecuteBearing;
        private System.Windows.Forms.Panel pExecuteBearing;
        private System.Windows.Forms.NumericUpDown nudBearingEB;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TabPage tbpControlSignal;
        private System.Windows.Forms.Button bSendStateSupressFreq;
        private System.Windows.Forms.Panel pStateSupressFreq;
        private System.Windows.Forms.ComboBox cmbTypeFreqState;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TabPage tbpDataFHSS;
        private System.Windows.Forms.Button bSendDataFHSS;
        private System.Windows.Forms.Panel pDataFHSS;
        private System.Windows.Forms.TabPage tbpDataFWS;
        private System.Windows.Forms.Button bSendDataFWS;
        private System.Windows.Forms.Panel pDataFWS;
        private System.Windows.Forms.TabPage tbpReceiptJ;
        private System.Windows.Forms.Button bSendReceiptJ;
        private System.Windows.Forms.Panel pReceiptJ;
        private System.Windows.Forms.NumericUpDown nudCodeError;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TabPage tbpTextJ;
        private System.Windows.Forms.Button bSendTextMessageJammer;
        private System.Windows.Forms.Panel pTextMessageJammer;
        private System.Windows.Forms.RichTextBox rtbMesJam;
        private System.Windows.Forms.TabPage tbpCoordinates;
        private System.Windows.Forms.Button bSendCoordJammer;
        private System.Windows.Forms.Panel pCoordJammer;
        private System.Windows.Forms.CheckBox chSignLatCoord;
        private System.Windows.Forms.CheckBox chSignLongCoord;
        private System.Windows.Forms.TextBox tbJamDegL;
        private System.Windows.Forms.TextBox tbJamMinL;
        private System.Windows.Forms.TextBox tbJamSecL;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox tbJamDegB;
        private System.Windows.Forms.TextBox tbJamMinB;
        private System.Windows.Forms.TextBox tbJamSecB;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TabControl tbcCmdJammer;
        private System.Windows.Forms.TabPage tbpSynchronizeJ;
        private System.Windows.Forms.Panel pSynchronizeJ;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Button bSendSynchronizeJ;
        private System.Windows.Forms.TabPage tbpTestJ;
        private System.Windows.Forms.Button bSendTestChannelJ;
        private System.Windows.Forms.Panel pTestChannelJ;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.ComboBox cmbMainRegimeTest;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox chMainL1;
        private System.Windows.Forms.CheckBox chMainL2;
        private System.Windows.Forms.CheckBox chMainL3;
        private System.Windows.Forms.CheckBox chMainL4;
        private System.Windows.Forms.CheckBox chMainL5;
        private System.Windows.Forms.CheckBox chMainL6;
        private System.Windows.Forms.CheckBox chMainL7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox chAddL1;
        private System.Windows.Forms.CheckBox chAddL2;
        private System.Windows.Forms.CheckBox chAddL3;
        private System.Windows.Forms.CheckBox chAddL4;
        private System.Windows.Forms.CheckBox chAddL5;
        private System.Windows.Forms.CheckBox chAddL6;
        private System.Windows.Forms.CheckBox chAddL7;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cmbAddRegimeTest;
        private System.Windows.Forms.NumericUpDown nudSignTest;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.DateTimePicker dtpTimeSinchro;
        private System.Windows.Forms.MaskedTextBox mtbPortIP_;
        private System.Windows.Forms.MaskedTextBox mtbAddressIP_;
        private System.Windows.Forms.NumericUpDown nudWidthReconFWS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DateTimePicker dtpTimeFindFWS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbModeReconFWS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox chSignLatFWS;
        private System.Windows.Forms.CheckBox chSignLongFWS;
        private System.Windows.Forms.NumericUpDown nudBearOwnReconFWS;
        private System.Windows.Forms.TextBox tbFWSDegL;
        private System.Windows.Forms.TextBox tbFWSMinL;
        private System.Windows.Forms.TextBox tbFWSSecB;
        private System.Windows.Forms.TextBox tbFWSSecL;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbFWSMinB;
        private System.Windows.Forms.TextBox tbFWSDegB;
        private System.Windows.Forms.NumericUpDown nudBearLinkedReconFWS;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox chbExistAudioFWS;
        private System.Windows.Forms.NumericUpDown nudLevelLinkedReconFWS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown nudLevelOwnReconFWS;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chbAutoReconFWS;
        private System.Windows.Forms.Button bRangeReconClear;
        private System.Windows.Forms.Button bRangeReconAdd;
        private System.Windows.Forms.RichTextBox rtbMemoReconFWS;
        private System.Windows.Forms.ComboBox cmbTypeConfirm;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.NumericUpDown nudExecBearID;
        private System.Windows.Forms.Label lIDExecBear;
        private System.Windows.Forms.NumericUpDown nudSimulBearID;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.NumericUpDown nudExecBearFreq;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.NumericUpDown nudSimulBearFreq;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown nudSimulBearCodeError;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.NumericUpDown nudExecBearCodeError;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox cmbStateRole;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.ComboBox cmbStateType;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.NumericUpDown nudStateNumber;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox cmbStateRegime;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.NumericUpDown nudStateCodeError;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox cmbActForbid;
        private System.Windows.Forms.NumericUpDown nudIDSReconFWS;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown nudFreqReconFWS;
        private System.Windows.Forms.NumericUpDown nudFreqMaxReconFHSS;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.NumericUpDown nudFreqMinReconFHSS;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ContextMenuStrip ctmClearLog;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.NumericUpDown nudCodeErrorCoord;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown nudCodeErrorSynchTime;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.NumericUpDown nudAddressJammer;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.NumericUpDown nudCodeErrorSourceFWS;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudCodeErrorSourceFHSS;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown nudSecondReconFHSS;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown nudMinuteReconFHSS;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.CheckBox chbExistAudioFHSS;
        private System.Windows.Forms.NumericUpDown nudLevelLinkedReconFHSS;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.NumericUpDown nudLevelOwnReconFHSS;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.NumericUpDown nudBearLinkedReconFHSS;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown nudWidthReconFHSS;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cmbModeReconFHSS;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.NumericUpDown nudBearOwnReconFHSS;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.RichTextBox rtbMemoReconFHSS;
        private System.Windows.Forms.CheckBox chbAutoReconFHSS;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NumericUpDown nudSupprCtrlCodeError;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.NumericUpDown nudSupprCtrlCountSource;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.CheckBox chbAutoAnswer;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.MaskedTextBox mtbDbPort;
        private System.Windows.Forms.MaskedTextBox mtbDbIp;
        private System.Windows.Forms.Button btnConnectDB;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.MaskedTextBox mtbServerPort;
        private System.Windows.Forms.MaskedTextBox mtbServerIp;
        private System.Windows.Forms.Button btnConnectServer;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label55;
    }
}

