﻿using FuncVLC;
using GrozaBerezinaDLL;
using StructTypeGR_BRZ;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GrpcClientLib;
using KvetkaModelsDBLib;
using InheritorsEventArgs;
using TransmissionPackageKvetka;
using Grpc.Core;
using System.Threading;

namespace TestODClient
{
    public partial class TestInformExchangeGRZ : Form
    {
        const string CMD_Text = "  Текстовое сообщение";
        const string CMD_ConfirmText = "  Текстовое сообщение (подтверждение)";
        const string CMD_SynchTime = "  Назначение времени (синхронизация)";
        const string CMD_Regime = "  Назначение режима работы";
        const string CMD_RangeSector = "  Назначение секторов и диапазонов";
        const string CMD_RangeSpec = "  Назначение специальных частот";
        const string CMD_SupprFWS = "  Назначение ФРЧ на РП";
        const string CMD_SupprFHSS = "  Назначение ППРЧ на РП";
        const string CMD_State = "  Запрос состояния";
        const string CMD_Coord = "  Запрос координат";
        const string CMD_ReconFWS = "  Запрос ИРИ ФРЧ";
        const string CMD_ReconFHSS = "  Запрос ИРИ ППРЧ";
        const string CMD_ExecBear = "  Запрос на ИП";
        const string CMD_SimulBear = "  Запрос на КвП";
        const string CMD_StateSupprFWS = "  Запрос состояния РП ИРИ ФРЧ";
        const string CMD_StateSupprFHSS = "  Запрос состояния РП ИРИ ППРЧ";
        
        
        short TIME_INDICATE = 300;
        byte bOwnAddress = 1;

        IExGRZ_BRZ iExGRZ_BRZ = null;
        
        StateIndicate stateIndicateRead = null;
        StateIndicate stateIndicateWrite = null;
        StateConnection stateIndicateConnection = null;
        StateConnection stateIndicateConnectionDB = null;
        StateConnection stateIndicateConnectionServer = null;

        Image imgReadByte = Properties.Resources.green;
        Image imgWriteByte = Properties.Resources.red;
        Image imgWaitByte = Properties.Resources.gray;

        Color clConnect = Color.Green;
        Color clDisconnect = Color.Red;


        ShowLog showLogJammer = null;
        InitComEth initComEth = null;

        TReconFWS[] tReconFWS = null;

        TReconFHSS[] tReconFHSS = null;

        bool blAutoAnswer = true;

       

        //TestClass testClass = null;

        public TestInformExchangeGRZ()
        {
            InitializeComponent();

            InitIndicate();

            InitParamVLC();

            InitConnection();
        }



        private void InitParamVLC()
        {

          //  testClass = new TestClass();

            showLogJammer = new ShowLog();

            initComEth = new InitComEth();

            initComEth.InitPort(cmbComRRD1);

            string strLocalHost = Dns.GetHostName();
            System.Net.IPAddress ip = System.Net.Dns.GetHostByName(strLocalHost).AddressList[0];

            mtbAddressIP_.Text = ip.ToString();
                
            
            rbEth.Checked = true;

            label38.Visible = false;
            cmbActForbid.Visible = false;

            cmbTypeConfirm.Items.Clear();
            cmbTypeConfirm.Items.Add("Текстовое сообщение");            
            cmbTypeConfirm.Items.Add("Назначение режима работы");
            cmbTypeConfirm.Items.Add("Назначение секторов и диапазонов");
            cmbTypeConfirm.Items.Add("Назначение специальных частот");
            cmbTypeConfirm.Items.Add("Назначение ФРЧ на РП");
            cmbTypeConfirm.Items.Add("Назначение ППРЧ на РП");
            cmbTypeConfirm.SelectedIndex = 0;


            cmbStateRegime.Items.Clear();
            cmbStateRegime.Items.Add("Подготовка");
            cmbStateRegime.Items.Add("Радиоразведка");
            cmbStateRegime.Items.Add("Радиоразведка с пеленгованием");
            cmbStateRegime.Items.Add("Радиоразведка ППРЧ");
            cmbStateRegime.Items.Add("Радиоподавление ФРЧ");
            cmbStateRegime.Items.Add("Радиоподавление ППРЧ");
            cmbStateRegime.SelectedIndex = 0;


            cmbStateType.Items.Clear();
            cmbStateType.Items.Add("Р-327");
            cmbStateType.Items.Add("Гроза");
            cmbStateType.Items.Add("Гроза-6 Апп1");
            cmbStateType.Items.Add("Гроза-6 Апп2");
            cmbStateType.Items.Add("Пурга");
            cmbStateType.Items.Add("Р-325Б");
            cmbStateType.Items.Add("Гриф");
            cmbStateType.Items.Add("Журавль");
            cmbStateType.SelectedIndex = 0;



            cmbStateRole.Items.Clear();
            cmbStateRole.Items.Add("Автономная");
            cmbStateRole.Items.Add("Ведущая");
            cmbStateRole.Items.Add("Ведомая");
            cmbStateRole.SelectedIndex = 0;

            cmbModeReconFWS.Items.Clear();
            cmbModeReconFWS.Items.Add("ЧМШ");
            cmbModeReconFWS.Items.Add("ЧМШ скан f.");
            cmbModeReconFWS.Items.Add("ЧМн-2");
            cmbModeReconFWS.Items.Add("ЧМн-4");
            cmbModeReconFWS.Items.Add("ЧМн-8");
            cmbModeReconFWS.Items.Add("ФМн-2");
            cmbModeReconFWS.Items.Add("ФМн-4");
            cmbModeReconFWS.Items.Add("ФМн-8");
            cmbModeReconFWS.Items.Add("ЗШ");
            cmbModeReconFWS.Items.Add("АФМн");
            //cmbModeReconFWS.Items.Add("АМ");
            //cmbModeReconFWS.Items.Add("ЧМ");
            //cmbModeReconFWS.Items.Add("ЧМ-2");
            //cmbModeReconFWS.Items.Add("НБП");
            //cmbModeReconFWS.Items.Add("ВБП");
            //cmbModeReconFWS.Items.Add("ШПС");
            //cmbModeReconFWS.Items.Add("Несущая");
            //cmbModeReconFWS.Items.Add("Неопр");
            //cmbModeReconFWS.Items.Add("АМ-2");
            //cmbModeReconFWS.Items.Add("ФМ-2");
            //cmbModeReconFWS.Items.Add("ФМ-4");
            //cmbModeReconFWS.Items.Add("ФМ-8");
            //cmbModeReconFWS.Items.Add("КАМ-16");
            //cmbModeReconFWS.Items.Add("КАМ-32");
            //cmbModeReconFWS.Items.Add("КАМ-64");
            //cmbModeReconFWS.Items.Add("КАМ-128");
            //cmbModeReconFWS.Items.Add("ППРЧ");         
            cmbModeReconFWS.SelectedIndex = 0;

            cmbModeReconFHSS.Items.Add("ППРЧ");
            cmbModeReconFHSS.SelectedIndex = 0;

            cmbTypeFreqState.Items.Add("ИРИ ФРЧ");
            cmbTypeFreqState.Items.Add("ИРИ ППРЧ");
            cmbTypeFreqState.SelectedIndex = 0;


        }


        private void InitConnection()
        {
            iExGRZ_BRZ = null;
            iExGRZ_BRZ = new IExGRZ_BRZ(bOwnAddress);

            InitEventConnectionJammer();

            try
            {

                if (rbCom.Checked)
                    iExGRZ_BRZ.Connect(cmbComRRD1.Items[cmbComRRD1.SelectedIndex].ToString(), 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
                if (rbEth.Checked)
                {                   
                    iExGRZ_BRZ.Connect(mtbAddressIP_.Text, Convert.ToInt32(mtbPortIP_.Text));
                }
            }
            catch(SystemException)
            {
            }

        }

        private void InitDisconnection()
        {
            if (iExGRZ_BRZ != null)
            {
                iExGRZ_BRZ.Disconnect();
                iExGRZ_BRZ = null; 
            }
        }
        

        private void InitEventConnectionJammer()
        {
            if (iExGRZ_BRZ != null)
            {
                try
                {
                    iExGRZ_BRZ.OnConnect += new IExGRZ_BRZ.ConnectEventHandler(Connect);
                    iExGRZ_BRZ.OnDisconnect += new IExGRZ_BRZ.ConnectEventHandler(Disconnect);

                    iExGRZ_BRZ.OnReadByte += new IExGRZ_BRZ.ByteEventHandler(ReadByte);
                    iExGRZ_BRZ.OnWriteByte += new IExGRZ_BRZ.ByteEventHandler(WriteByte);
                   

                    iExGRZ_BRZ.OnRequestCoord += new IExGRZ_BRZ.RequestCoordEventHandler(Receive_RequestCoord);
                    iExGRZ_BRZ.OnRequestExecBear += new IExGRZ_BRZ.RequestExecBearEventHandler(Receive_RequestExecBear);
                    iExGRZ_BRZ.OnRequestRangeSector += new IExGRZ_BRZ.RequestRangeSectorEventHandler(Receive_RequestRangeSector);
                    iExGRZ_BRZ.OnRequestRangeSpec += new IExGRZ_BRZ.RequestRangeSpecEventHandler(Receive_RequestRangeSpec);
                    iExGRZ_BRZ.OnRequestReconFHSS += new IExGRZ_BRZ.RequestReconFHSSEventHandler(Receive_RequestReconFHSS);
                    iExGRZ_BRZ.OnRequestReconFWS += new IExGRZ_BRZ.RequestReconFWSEventHandler(Receive_RequestReconFWS);
                    iExGRZ_BRZ.OnRequestRegime += new IExGRZ_BRZ.RequestRegimeEventHandler(Receive_RequestRegime);
                    iExGRZ_BRZ.OnRequestSimulBear += new IExGRZ_BRZ.RequestSimulBearEventHandler(Receive_RequestSimulBear);
                    iExGRZ_BRZ.OnRequestState += new IExGRZ_BRZ.RequestStateEventHandler(Receive_RequestState);
                    iExGRZ_BRZ.OnRequestStateSupprFHSS += new IExGRZ_BRZ.RequestStateSupprFHSSEventHandler(Receive_RequestStateSupprFHSS);
                    iExGRZ_BRZ.OnRequestStateSupprFWS += new IExGRZ_BRZ.RequestStateSupprFWSEventHandler(Receive_RequestStateSupprFWS);
                    iExGRZ_BRZ.OnRequestSupprFHSS += new IExGRZ_BRZ.RequestSupprFHSSEventHandler(Receive_RequestSupprFHSS);
                    iExGRZ_BRZ.OnRequestSupprFWS += new IExGRZ_BRZ.RequestSupprFWSEventHandler(Receive_RequestSupprFWS);
                    iExGRZ_BRZ.OnRequestSynchTime += new IExGRZ_BRZ.RequestSynchTimeEventHandler(Receive_RequestSynchTime);
                    iExGRZ_BRZ.OnReceiveTextCmd += new IExGRZ_BRZ.CmdTextEventHandler(Receive_TextCmd);
                    iExGRZ_BRZ.OnReceiveConfirmTextCmd += new IExGRZ_BRZ.ConfirmTextEventHandler(Receive_ConfirmTextCmd);

                    iExGRZ_BRZ.OnSendSynchTime += new IExGRZ_BRZ.SendSynchTimeEventHandler(Send_SynchTime);
                    iExGRZ_BRZ.OnSendRegime += new IExGRZ_BRZ.SendRegimeEventHandler(Send_Regime);
                    iExGRZ_BRZ.OnSendRangeSector += new IExGRZ_BRZ.SendRangeSectorEventHandler(Send_RangeSector);
                    iExGRZ_BRZ.OnSendRangeSpec += new IExGRZ_BRZ.SendRangeSpecEventHandler(Send_RangeSpec);
                    iExGRZ_BRZ.OnSendSupprFWS += new IExGRZ_BRZ.SendSupprFWSEventHandler(Send_SupprFWS);
                    iExGRZ_BRZ.OnSendSupprFHSS += new IExGRZ_BRZ.SendSupprFHSSEventHandler(Send_SupprFHSS);
                    iExGRZ_BRZ.OnSendState += new IExGRZ_BRZ.SendStateEventHandler(Send_State);
                    iExGRZ_BRZ.OnSendCoord += new IExGRZ_BRZ.SendCoordEventHandler(Send_Coord);
                    iExGRZ_BRZ.OnSendReconFWS += new IExGRZ_BRZ.SendReconFWSEventHandler(Send_ReconFWS);
                    iExGRZ_BRZ.OnSendReconFHSS += new IExGRZ_BRZ.SendReconFHSSEventHandler(Send_ReconFHSS);
                    iExGRZ_BRZ.OnSendExecBear += new IExGRZ_BRZ.SendExecBearEventHandler(Send_ExecBear);
                    iExGRZ_BRZ.OnSendSimulBear += new IExGRZ_BRZ.SendSimulBearEventHandler(Send_SimulBear);
                    iExGRZ_BRZ.OnSendStateSupprFWS += new IExGRZ_BRZ.SendStateSupprFWSEventHandler(Send_StateSupprFWS);
                    iExGRZ_BRZ.OnSendStateSupprFHSS += new IExGRZ_BRZ.SendStateSupprFHSSEventHandler(Send_StateSupprFHSS);
                    iExGRZ_BRZ.OnSendTextCmd += new IExGRZ_BRZ.CmdTextEventHandler(Send_TextCmd);
                    iExGRZ_BRZ.OnSendConfirmTextCmd += new IExGRZ_BRZ.ConfirmTextEventHandler(Send_ConfirmTextCmd);

                    
                }
                catch (SystemException)
                { }
                    

            }

        }

        # region Receive_Event

        private void Receive_RequestCoord(object sender)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_Coord + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                if (blAutoAnswer)
                {
                    TCoord tCoord = new TCoord();


                    tCoord.bSignLongitude = Convert.ToByte(chSignLongCoord.Checked);

                    try
                    {

                        byte bCodeError = (byte)(nudCodeErrorCoord.Value);

                        tCoord.bDegreeLongitude = Convert.ToByte(tbJamDegB.Text);
                        tCoord.bMinuteLongitude = Convert.ToByte(tbJamMinB.Text);
                        tCoord.bSecondLongitude = Convert.ToByte(tbJamSecB.Text);

                        tCoord.bSignLatitude = Convert.ToByte(chSignLatCoord.Checked);


                        tCoord.bDegreeLatitude = Convert.ToByte(tbJamDegL.Text);
                        tCoord.bMinuteLatitude = Convert.ToByte(tbJamMinL.Text);
                        tCoord.bSecondLatitude = Convert.ToByte(tbJamSecL.Text);

                        if (iExGRZ_BRZ != null)
                            iExGRZ_BRZ.SendCoord(bCodeError, tCoord);
                    }

                    catch
                    {
                        MessageBox.Show("Ошибка ввода данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    }
 
                }
            }
            catch (SystemException)
            {}
        }

        private void Receive_RequestExecBear(object sender, int iID, int iFreq)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_ExecBear + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "ID " + iID.ToString() + "\n";
                    strDetail += "Частота " + Convert.ToDouble((double)iFreq).ToString() + "\n";

                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }


            if (blAutoAnswer)
            {
                try
                {
                    if (btnConnectDB.BackColor == clConnect)
                    {
                        var record = lResFF.Where(t => Math.Abs(t.FrequencyKHz - iFreq) < 10).FirstOrDefault();
                        short sBear = record == null ? (short)-1 : (short)record.BearingOwn;
                        if (iExGRZ_BRZ != null)
                            iExGRZ_BRZ.SendExecBear(0, iID, iFreq, sBear);
                    }
                    else 
                    {
                        short sBearMain = (short)(nudBearingEB.Value);
                        if (iExGRZ_BRZ != null)
                            iExGRZ_BRZ.SendExecBear(0, iID, iFreq, sBearMain);
                    }

                    //byte bCodeError = (byte)(nudSimulBearCodeError.Value);
                    //int iId = (int)(nudSimulBearID.Value);
                    //int iFreq = (int)(nudSimulBearFreq.Value * 10);
                    //short sBearMain = (short)(nudBearingMainSB.Value);
                    //short sBearAdd = (short)(nudBearingAddSB.Value);
                }
                catch
                {

                }
            }
        }

        private void Receive_RequestRangeSector(object sender, byte bType, TRangeSector[] tRangeSector)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_RangeSector + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    switch (bType)
                    {
                        case 0:
                            strDetail = "\n Радиоразведка"  + "\n";
                            break;

                        case 1:
                            strDetail = "\n Радиоподавление"  + "\n";
                            break;

                        default:
                            strDetail = "\n -----" + "\n";
                            break;

                    }
                    if (tRangeSector != null)
                    {
                        for (int i = 0; i < tRangeSector.Length; i++)
                        {
                            strDetail += (i + 1).ToString() + ".  " + Convert.ToDouble(((double)tRangeSector[i].iFregMin / 10)).ToString() + " --- " +Convert.ToDouble(((double)tRangeSector[i].iFregMax / 10)).ToString();
                            strDetail += " , " + (tRangeSector[i].sAngleMin).ToString() + " --- " + (tRangeSector[i].sAngleMax).ToString() + "\n";
                        }
                    }
                
                    

                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                

                SendSectorsRangesToDB(bType, tRangeSector);


            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendRangeSector(0, bType);
            }

            
        }

        private void Receive_RequestRangeSpec(object sender, byte bType, TRangeSpec[] tRangeSpec)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_RangeSpec + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    switch (bType)
                    {
                        case 0:
                            strDetail = "Запрещенные" + "\n";
                            break;

                        case 1:
                            strDetail = "Известные" + "\n";
                            break;

                        case 2:
                            strDetail = "Важные" + "\n";
                            break;

                        default:
                            strDetail = "-----" + "\n";
                            break;

                    }
                    if (tRangeSpec != null)
                    {
                        for (int i = 0; i < tRangeSpec.Length; i++)
                        {
                            strDetail += (i + 1).ToString(); 
                            strDetail += ".  " + Convert.ToDouble(((double)tRangeSpec[i].iFregMin / 10)).ToString() + " --- " + Convert.ToDouble(((double)tRangeSpec[i].iFregMax / 10)).ToString() + "\n";
                           
                        }
                    }
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                SendFreqRangesToDB(bType, tRangeSpec);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendRangeSpec(0, bType);
            }
        }

        private void Receive_RequestReconFHSS(object sender)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName =  CMD_ReconFHSS+ "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                Random rnd = new Random();

                //int iCountPack = (int)(rnd.Next(0, 10));

                TReconFHSS[] tReconFHSSAuto = null;

                try
                {
                    tReconFHSSAuto = ConvertResFhssToLocal(lResFHSS);
                    //if (iCountPack > 0)
                    //{
                    //    tReconFHSSAuto = new TReconFHSS[iCountPack];

                    //    for (int i = 0; i < iCountPack; i++)
                    //    {
                    //        tReconFHSSAuto[i].iID = (int)(rnd.Next(1, 100));
                    //        tReconFHSSAuto[i].iFreqMin = (int)(rnd.Next(300000, 30000000));
                    //        tReconFHSSAuto[i].iFreqMax = (int)(rnd.Next(tReconFHSSAuto[i].iFreqMin, 30000000));

                    //        tReconFHSSAuto[i].sBearingOwn = Convert.ToInt16(rnd.Next(0, 359));
                    //        tReconFHSSAuto[i].sBearingLinked = Convert.ToInt16(rnd.Next(0, 359));

                    //        tReconFHSSAuto[i].bModulation = Convert.ToByte(rnd.Next(0, 7));

                    //        tReconFHSSAuto[i].bBandWidth = Convert.ToByte(rnd.Next(0, 7));

                    //        tReconFHSSAuto[i].bLevelOwn = Convert.ToByte(rnd.Next(0, 250));
                    //        tReconFHSSAuto[i].bLevelLinked = Convert.ToByte(rnd.Next(0, 250));

                    //        tReconFHSSAuto[i].bMinute = Convert.ToByte(rnd.Next(0, 59));
                    //        tReconFHSSAuto[i].bSecond = Convert.ToByte(rnd.Next(0, 59));

                    //        tReconFHSSAuto[i].tCoord.bSignLongitude = Convert.ToByte(rnd.Next(0, 1));
                    //        tReconFHSSAuto[i].tCoord.bDegreeLongitude = Convert.ToByte(rnd.Next(0, 180));
                    //        tReconFHSSAuto[i].tCoord.bMinuteLongitude = Convert.ToByte(rnd.Next(0, 60));
                    //        tReconFHSSAuto[i].tCoord.bSecondLongitude = Convert.ToByte(rnd.Next(0, 60));

                    //        tReconFHSSAuto[i].tCoord.bSignLatitude = Convert.ToByte(rnd.Next(0, 1));
                    //        tReconFHSSAuto[i].tCoord.bDegreeLatitude = Convert.ToByte(rnd.Next(0, 90));
                    //        tReconFHSSAuto[i].tCoord.bMinuteLatitude = Convert.ToByte(rnd.Next(0, 60));
                    //        tReconFHSSAuto[i].tCoord.bSecondLatitude = Convert.ToByte(rnd.Next(0, 60));

                    //    }
                    //}

                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendReconFHSS(0, tReconFHSSAuto);
                }

                catch (SystemException)
                { }
            }
        }

        private void Receive_RequestReconFWS(object sender)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_ReconFWS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {

                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                Random rnd = new Random();
                  
                //int iCountPack = (int)(rnd.Next(0, 10));

                TReconFWS[] tReconFWSAuto = null;
                
                try
                {
                    tReconFWSAuto = ConvertResFwsToLocal(lResFFDistribution);
                    //if (iCountPack > 0)
                    //{
                    //    tReconFWSAuto = new TReconFWS[iCountPack];

                    //    for (int i=0; i<iCountPack; i++)
                    //    {
                    //        tReconFWSAuto[i].iID = (int)(rnd.Next(1, 100));
                    //        tReconFWSAuto[i].iFreq = (int)(rnd.Next(300000, 30000000));

                    //        tReconFWSAuto[i].sBearingOwn = Convert.ToInt16(rnd.Next(0, 359));
                    //        tReconFWSAuto[i].sBearingLinked = Convert.ToInt16(rnd.Next(0, 359));

                    //        tReconFWSAuto[i].bModulation = Convert.ToByte(rnd.Next(0, 7));

                    //        tReconFWSAuto[i].bBandWidth = Convert.ToByte(rnd.Next(0, 7));

                    //        tReconFWSAuto[i].bLevelOwn = Convert.ToByte(rnd.Next(0, 250));
                    //        tReconFWSAuto[i].bLevelLinked = Convert.ToByte(rnd.Next(0, 250));


                    //        tReconFWSAuto[i].tTime.bHour = Convert.ToByte(rnd.Next(0, 23));
                    //        tReconFWSAuto[i].tTime.bMinute = Convert.ToByte(rnd.Next(0, 59));
                    //        tReconFWSAuto[i].tTime.bSecond = Convert.ToByte(rnd.Next(0, 59));

                    //        tReconFWSAuto[i].tCoord.bSignLongitude = Convert.ToByte(rnd.Next(0, 1));
                    //        tReconFWSAuto[i].tCoord.bDegreeLongitude = Convert.ToByte(rnd.Next(0, 180));
                    //        tReconFWSAuto[i].tCoord.bMinuteLongitude = Convert.ToByte(rnd.Next(0, 60));
                    //        tReconFWSAuto[i].tCoord.bSecondLongitude = Convert.ToByte(rnd.Next(0, 60));

                    //        tReconFWSAuto[i].tCoord.bSignLatitude = Convert.ToByte(rnd.Next(0, 1));
                    //        tReconFWSAuto[i].tCoord.bDegreeLatitude = Convert.ToByte(rnd.Next(0, 90));
                    //        tReconFWSAuto[i].tCoord.bMinuteLatitude = Convert.ToByte(rnd.Next(0, 60));
                    //        tReconFWSAuto[i].tCoord.bSecondLatitude = Convert.ToByte(rnd.Next(0, 60));

                    //    }
                    //}

                    if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendReconFWS(0, tReconFWSAuto);
                }

                catch (SystemException)
                {}
            }
        }

        private async void Receive_RequestRegime(object sender, byte bRegime)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_Regime + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                Task.Run(() => SetMode(bRegime));

                if (chbDetail.Checked == true)
                {
                    switch (bRegime)
                    {
                        case 0:
                            strDetail = "Подготовка" + "\n";
                            break;

                        case 1:
                            strDetail = "Радиоразведка" + "\n";
                            break;
                        case 2:
                            strDetail = "Радиоразведка с пеленгованием" + "\n";
                            break;
                        case 3:
                            strDetail = "Радиоразведка ППРЧ" + "\n";
                            break;
                        case 4:
                            strDetail = "Радиоподавление ФРЧ" + "\n";
                            break;
                        case 5:
                            strDetail = "Радиоподавление ППРЧ" + "\n";
                            break;

                        default:
                            strDetail = "-----" + "\n";
                            break;

                    }
                }

                if (cmbStateRegime.InvokeRequired)
                {
                    cmbStateRegime.Invoke((MethodInvoker)(delegate ()
                    {
                        cmbStateRegime.SelectedIndex = bRegime;

                    }));

                }
                else
                {
                    cmbStateRegime.SelectedIndex = bRegime;
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendRegime(0);
            }
            
        }

        private void Receive_RequestSimulBear(object sender, int iID, int iFreq)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_SimulBear + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "ID " + iID.ToString() + "\n";
                    strDetail += "Частота " + Convert.ToDouble((double)iFreq).ToString() + "\n";

                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }


            if (blAutoAnswer)
            {
                try
                {
                    Random rnd = new Random();

                    short sBearMain = (short)(rnd.Next(0, 359));
                    short sBearLinked = (short)(rnd.Next(0, 359));

                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendSimulBear(0, iID, iFreq, sBearMain, sBearLinked);
                }
                catch
                {

                }
            }
        }

        private void Receive_RequestState(object sender)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_State + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {

                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                try
                {
                    byte bCodeError = (byte)(nudStateCodeError.Value);

                    byte bRegime = 0;

                    if (cmbStateRegime.InvokeRequired)
                    {
                        cmbStateRegime.Invoke((MethodInvoker)(delegate()
                        {
                            bRegime = Convert.ToByte(cmbStateRegime.SelectedIndex);

                        }));

                    }
                    else
                    {
                        bRegime = Convert.ToByte(cmbStateRegime.SelectedIndex);

                    }
                    

                    //if (bRegime == 2)
                    //    bRegime++;

                    short sNum = (short)(nudStateNumber.Value);


                    byte bType = 0;

                    if (cmbStateType.InvokeRequired)
                    {
                        cmbStateType.Invoke((MethodInvoker)(delegate()
                        {
                            bType = Convert.ToByte(cmbStateType.SelectedIndex);

                        }));

                    }
                    else
                    {
                        bType = Convert.ToByte(cmbStateType.SelectedIndex);

                    }


                    
                    byte bRole = 0;

                    if (cmbStateRole.InvokeRequired)
                    {
                        cmbStateRole.Invoke((MethodInvoker)(delegate()
                        {
                            bRole = Convert.ToByte(cmbStateRole.SelectedIndex);

                        }));

                    }
                    else
                    {
                        bRole = Convert.ToByte(cmbStateRole.SelectedIndex);

                    }

                    byte[] bLetter = new byte[1] { (byte)1};

                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendState(bCodeError, bRegime, sNum, bType, bRole, bLetter);
                }
                catch
                {

                }
            }
        }

        private void Receive_RequestStateSupprFHSS(object sender)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_StateSupprFHSS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {

                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                try
                {
                   
                    byte bCountSource = (byte)(nudSupprCtrlCountSource.Value);

                    Random rnd = new Random();

                    if (iExGRZ_BRZ != null)
                    {
                        TResSupprFHSS[] tResSupprFHSS = null;

                        if (bCountSource > 0)
                        {
                            tResSupprFHSS = new TResSupprFHSS[bCountSource];

                            for (int i = 0; i < bCountSource; i++)
                            {
                                tResSupprFHSS[i].iID = Convert.ToInt32(rnd.Next(1, 1000));
                                tResSupprFHSS[i].iFreqMin = (int)(rnd.Next(1500, 30000));
                                tResSupprFHSS[i].iFreqMax = (int)(rnd.Next((int)tResSupprFHSS[i].iFreqMin, 30000));
                                tResSupprFHSS[i].bResCtrl = Convert.ToByte(rnd.Next(0, 4));
                                tResSupprFHSS[i].bResSuppr = Convert.ToByte(rnd.Next(0, 4));
                            }
                        }

                        iExGRZ_BRZ.SendStateSupprFHSS(0, tResSupprFHSS);
                    }


                }
                
                catch
                {

                }
 
            }
        }

        private void Receive_RequestStateSupprFWS(object sender)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_StateSupprFWS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {

                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                try
                {
                   
                    byte bCountSource = (byte)(nudSupprCtrlCountSource.Value);

                    Random rnd = new Random();

                    if (iExGRZ_BRZ != null)
                    {
                        TResSupprFWS[] tResSupprFWS = null;

                        if (bCountSource > 0)
                        {
                            tResSupprFWS = new TResSupprFWS[bCountSource];

                            for (int i = 0; i < bCountSource; i++)
                            {
                                tResSupprFWS[i].iID = Convert.ToInt32(rnd.Next(1, 1000));
                                tResSupprFWS[i].iFreq = Convert.ToInt32(rnd.Next(1500, 30000));
                                tResSupprFWS[i].bResCtrl = Convert.ToByte(rnd.Next(0, 4));
                                tResSupprFWS[i].bResSuppr = Convert.ToByte(rnd.Next(0, 4));
                            }
                        }

                        iExGRZ_BRZ.SendStateSupprFWS(0, tResSupprFWS);                      
                    }
                }
                catch
                {

                }
 
            }
        }

        private void Receive_RequestSupprFHSS(object sender, int iDuration, TSupprFHSS[] tSupprFHSS)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_SupprFHSS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Длительность " + iDuration.ToString()+ ",мс \n";


                    if (tSupprFHSS != null)
                    {
                        for (int i = 0; i < tSupprFHSS.Length; i++)
                        {
                            strDetail += (i + 1).ToString();
                            strDetail += ".  Частота "+tSupprFHSS[i].iFreqMin.ToString("#.#") + " --- " + tSupprFHSS[i].iFreqMax.ToString("#.#") + "\n";
                            strDetail += "  Код БПФ "+ tSupprFHSS[i].bCodeFFT.ToString() + "\n";
                            strDetail += "  Модуляция " + tSupprFHSS[i].bModulation.ToString() + "\n";
                            strDetail += "  Девиация " + tSupprFHSS[i].bDeviation.ToString() + "\n";
                            strDetail += "  Манипуляция " + tSupprFHSS[i].bManipulation.ToString() + "\n";                            

                        }
                    }
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                SendFHSSSuppress(iDuration, tSupprFHSS);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendSupprFHSS(0);
 
            }
        }

        private void Receive_RequestSupprFWS(object sender, int iDuration, TSupprFWS[] tSupprFWS)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_SupprFWS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Длительность " + iDuration.ToString() + ",мс \n";


                    if (tSupprFWS != null)
                    {
                        for (int i = 0; i < tSupprFWS.Length; i++)
                        {
                            strDetail += (i + 1).ToString();
                            strDetail += ". Частота " + tSupprFWS[i].iFreq.ToString("#.#") + "\n";                           
                            strDetail += "  Модуляция " + tSupprFWS[i].bModulation.ToString() + "\n";
                            strDetail += "  Девиация " + tSupprFWS[i].bDeviation.ToString() + "\n";
                            strDetail += "  Манипуляция " + tSupprFWS[i].bManipulation.ToString() + "\n";
                            strDetail += "  Приоритет " + tSupprFWS[i].bPrioritet.ToString() + "\n";
                            strDetail += "  Порог -" + tSupprFWS[i].bThreshold.ToString() + "\n";
                            strDetail += "  Пеленг " + tSupprFWS[i].sBearing.ToString() + "\n";

                        }
                    }
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                SendFWSSuppress(iDuration, tSupprFWS);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendSupprFWS(0);

            }
        }

        private void Receive_RequestSynchTime(object sender, TTimeMy tTime)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_SynchTime + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Время " + tTime.bHour.ToString() + ":" + tTime.bMinute.ToString() + ":" + tTime.bSecond.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }


            if (blAutoAnswer)
            {
                try
                {
                   
                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendSynchTime(0, tTime);
                }
                catch
                {

                }
 
            }
        }

        private void Receive_TextCmd(object sender, string strText)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_Text + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = strText + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }

            if (blAutoAnswer)
            {
                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendConfirmText(0);
            }
        }

        private void Receive_ConfirmTextCmd(object sender, byte bCodeError)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_ConfirmText + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        

        # endregion


        # region Send_Event

        private void Send_SynchTime(object sender, byte bCodeError, TTimeMy tTime)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_SynchTime + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";
                    strDetail += "Время " + tTime.bHour.ToString() + ":" + tTime.bMinute.ToString() + ":" + tTime.bSecond.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_Regime(object sender, byte bCodeError)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_Regime + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_RangeSector(object sender, byte bCodeError, byte bTypeRange)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_RangeSector + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                    switch (bTypeRange)
                    { 
                        case 0:
                            strDetail += "Тип РР  \n";
                            break;

                        case 1:
                            strDetail += "Тип РП  \n";
                            break;

                        default:
                            break;
                    }                    
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_RangeSpec(object sender, byte bCodeError, byte bTypeSpec)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_RangeSpec + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                    switch (bTypeSpec)
                    {
                        case 0:
                            strDetail += "Запрещенные  " + bCodeError.ToString() + "\n";
                            break;

                        case 1:
                            strDetail += "Известные  " + bCodeError.ToString() + "\n";
                            break;

                        case 2:
                            strDetail += "Важные  " + bCodeError.ToString() + "\n";
                            break;

                        default:
                            break;
                    }         
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_SupprFWS(object sender, byte bCodeError)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_SupprFWS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_SupprFHSS(object sender, byte bCodeError)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_SupprFHSS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_State(object sender, byte bCodeError, byte bRegime, short sNum, byte bType, byte bRole, byte[] bLetter)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_State + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                    switch (bRegime)
                    {
                        case 0:
                            strDetail += "Режим Подготовка  \n";
                            break;

                        case 1:
                            strDetail += "Режим Радиоразведка  \n";
                            break;

                        case 2:
                            strDetail += "Режим Радиоразведка с пеленгованием \n";
                            break;

                        case 3:
                            strDetail += "Режим Радиоразведка ППРЧ \n";
                            break;

                        case 4:
                            strDetail += "Режим Радиоподавление ФРЧ \n";
                            break;

                        case 5:
                            strDetail += "Режим Радиоподавление ППРЧ \n";
                            break;

                        default:
                            break;
                    }       
                    
                    strDetail += "Номер  " + sNum.ToString() + "\n";

                    switch (bType)
                    {
                        case 0:
                            strDetail += "Тип Р-327  \n";
                            break;

                        case 1:
                            strDetail += "Тип Гроза  \n";
                            break;

                        case 2:
                            strDetail += "Тип Гроза-6 Апп1  \n";
                            break;

                        case 3:
                            strDetail += "Тип Гроза-6 Апп2  \n";
                            break;

                        case 4:
                            strDetail += "Тип Пурга  \n";
                            break;

                        case 5:
                            strDetail += "Тип Р-325Б   \n";
                            break;

                        case 6:
                            strDetail += "Тип Гриф   \n";
                            break;

                        case 7:
                            strDetail += "Тип Журавль   \n";
                            break;

                        default:
                            break;
                    }

                    switch (bRole)
                    {
                        case 0:
                            strDetail += "Роль Автономная  \n";
                            break;

                        case 1:
                            strDetail += "Роль Ведущая  \n";
                            break;

                        case 2:
                            strDetail += "Роль Ведомая  \n";
                            break;

                        default:
                            break;
                    }

                    if (bLetter != null)
                    {
                        for (int i = 0; i < bLetter.Length; i++)
                        {
                            strDetail += "Литера "+(i+1).ToString()+" "+ bLetter[i].ToString()+"\n";
 
                        }
                    }
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_Coord(object sender, byte bCodeError, TCoord tCoord)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_Coord + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";
                    
                    if (tCoord.bSignLatitude == 0)
                        strDetail += "Широта (З) ";
                    else
                        strDetail += "Широта (В) ";
                    strDetail += tCoord.bDegreeLatitude.ToString() + "  " + tCoord.bMinuteLatitude.ToString() + "  " + tCoord.bSecondLatitude.ToString() + "\n";

                    if (tCoord.bSignLongitude == 0)
                        strDetail += "Долгота (С) ";
                    else
                        strDetail += "Долгота (Ю) ";
                    strDetail += tCoord.bDegreeLongitude.ToString() + "  " + tCoord.bMinuteLongitude.ToString() + "  " + tCoord.bSecondLongitude.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_ExecBear(object sender, byte bCodeError, int iID, int iFreq, short sOwnBearing)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_ExecBear + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                    strDetail += "ID " + iID.ToString() + "\n";
                    strDetail += "Частота " + Convert.ToDouble((double)iFreq).ToString() + "\n";
                    strDetail += "Пеленг " + sOwnBearing.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_SimulBear(object sender, byte bCodeError, int iID, int iFreq, short sOwnBearing, short sLinkedBearing)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_SimulBear + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                    strDetail += "ID " + iID.ToString() + "\n";
                    strDetail += "Частота " + Convert.ToDouble((double)iFreq).ToString() + "\n";
                    strDetail += "Пеленг ведущая " + sOwnBearing.ToString() + "\n";
                    strDetail += "Пеленг ведомая " + sLinkedBearing.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_StateSupprFWS(object sender, byte bCodeError, TResSupprFWS[] tResSupprFWS)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_StateSupprFWS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                    if  (tResSupprFWS != null)
                    {
                        for (int i=0; i<tResSupprFWS.Length; i++)
                        {
                            strDetail += (i+1).ToString() + "\n";
                            strDetail += "  ID " +tResSupprFWS[i].iID.ToString()+ "\n";
                            strDetail += "  Частота " + tResSupprFWS[i].iFreq.ToString("#.#") + "\n";
                            strDetail += "  Контроль " + tResSupprFWS[i].bResCtrl.ToString() + "\n";
                            strDetail += "  Подавление " + tResSupprFWS[i].bResSuppr.ToString() + "\n";                            
                        }                        
                    }                        
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_StateSupprFHSS(object sender, byte bCodeError, TResSupprFHSS[] tResSupprFHSS)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_StateSupprFHSS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                    if (tResSupprFHSS != null)
                    {
                        for (int i = 0; i < tResSupprFHSS.Length; i++)
                        {
                            strDetail += (i + 1).ToString() + "\n";
                            strDetail += "  ID " + tResSupprFHSS[i].iID.ToString() + "\n";
                            strDetail += "  Частота мин " + tResSupprFHSS[i].iFreqMin.ToString("#.#") + "\n";
                            strDetail += "  Частота макс " + tResSupprFHSS[i].iFreqMax.ToString("#.#") + "\n";
                            strDetail += "  Контроль " + tResSupprFHSS[i].bResCtrl.ToString() + "\n";
                            strDetail += "  Подавление " + tResSupprFHSS[i].bResSuppr.ToString() + "\n";
                        }
                    }
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_ReconFWS(object sender, byte bCodeError, TReconFWS[] tReconFWS)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_ReconFWS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                    if (tReconFWS != null)
                    {
                        for (int i = 0; i < tReconFWS.Length; i++)
                        {
                            strDetail += (i + 1).ToString() + "\n";
                            strDetail += "  ID " + tReconFWS[i].iID.ToString() + "\n";
                            strDetail += "  Частота " + tReconFWS[i].iFreq.ToString("#.#") + "\n";
                            strDetail += "  Время " + tReconFWS[i].tTime.bHour.ToString() + ":" + tReconFWS[i].tTime.bMinute.ToString() + ":" + tReconFWS[i].tTime.bSecond.ToString() + "\n";
                            strDetail += "  Пеленг ведущая " + tReconFWS[i].sBearingOwn.ToString() + "\n";
                            strDetail += "  Пеленг ведомая " + tReconFWS[i].sBearingLinked.ToString() + "\n";

                            if (tReconFWS[i].tCoord.bSignLatitude == 0)
                                strDetail += "  Широта (З) ";
                            else
                                strDetail += "  Широта (В) ";
                            strDetail += tReconFWS[i].tCoord.bDegreeLatitude.ToString() + "  " + tReconFWS[i].tCoord.bMinuteLatitude.ToString() + "  " + tReconFWS[i].tCoord.bSecondLatitude.ToString() + "\n";

                            if (tReconFWS[i].tCoord.bSignLongitude == 0)
                                strDetail += "  Долгота (С) ";
                            else
                                strDetail += "  Долгота (Ю) ";
                            strDetail += tReconFWS[i].tCoord.bDegreeLongitude.ToString() + "  " + tReconFWS[i].tCoord.bMinuteLongitude.ToString() + "  " + tReconFWS[i].tCoord.bSecondLongitude.ToString() + "\n";

                            strDetail += "  Ур. сигнала ведущая -" + tReconFWS[i].bLevelOwn.ToString() + "\n";
                            strDetail += "  Ур. сигнала ведомая -" + tReconFWS[i].bLevelLinked.ToString() + "\n";
                            strDetail += "  Ширина полосы " + tReconFWS[i].bBandWidth.ToString() + "\n";

                            strDetail += "  Вид модуляции " + tReconFWS[i].bModulation.ToString() + "\n";

                            //strDetail += "  Наличие аудиозаписи " + tReconFWS[i].bSignAudio.ToString() + "\n";

                        }
                    }
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_ReconFHSS(object sender, byte bCodeError, TReconFHSS[] tReconFHSS)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_ReconFHSS + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                    if (tReconFHSS != null)
                    {
                        for (int i = 0; i < tReconFHSS.Length; i++)
                        {
                            strDetail += (i + 1).ToString() + "\n";
                            strDetail += "  ID " + tReconFHSS[i].iID.ToString() + "\n";
                            strDetail += "  Частота мин " + tReconFHSS[i].iFreqMin.ToString("#.#") + "\n";
                            strDetail += "  Частота макс " +tReconFHSS[i].iFreqMax.ToString("#.#") + "\n";

                            strDetail += "  Шаг сетки " + tReconFHSS[i].sStep.ToString() + "\n";
                            strDetail += "  Длительность импульса " + tReconFHSS[i].iDuration.ToString() + "\n";

                            strDetail += "  Время " + tReconFHSS[i].bMinute.ToString() + ":" + tReconFHSS[i].bSecond.ToString() + "\n";
                            strDetail += "  Пеленг ведущая " + tReconFHSS[i].sBearingOwn.ToString() + "\n";
                            strDetail += "  Пеленг ведомая " + tReconFHSS[i].sBearingLinked.ToString() + "\n";

                            if (tReconFHSS[i].tCoord.bSignLatitude == 0)
                                strDetail += "  Широта (З) ";
                            else
                                strDetail += "  Широта (В) ";
                            strDetail += tReconFHSS[i].tCoord.bDegreeLatitude.ToString() + "  " + tReconFHSS[i].tCoord.bMinuteLatitude.ToString() + "  " + tReconFHSS[i].tCoord.bSecondLatitude.ToString() + "\n";

                            if (tReconFHSS[i].tCoord.bSignLongitude == 0)
                                strDetail += "  Долгота (С) ";
                            else
                                strDetail += "  Долгота (Ю) ";
                            strDetail += tReconFHSS[i].tCoord.bDegreeLongitude.ToString() + "  " + tReconFHSS[i].tCoord.bMinuteLongitude.ToString() + "  " + tReconFHSS[i].tCoord.bSecondLongitude.ToString() + "\n";

                            strDetail += "  Ур. сигнала ведущая " + tReconFHSS[i].bLevelOwn.ToString() + "\n";
                            strDetail += "  Ур. сигнала ведомая " + tReconFHSS[i].bLevelLinked.ToString() + "\n";
                            strDetail += "  Ширина полосы " + tReconFHSS[i].bBandWidth.ToString() + "\n";

                            strDetail += "  Вид модуляции " + tReconFHSS[i].bModulation.ToString() + "\n";

                            strDetail += "  Наличие аудиозаписи " + tReconFHSS[i].bSignAudio.ToString() + "\n";

                        }
                    }
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_TextCmd(object sender, string strText)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_Text + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = strText + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        private void Send_ConfirmTextCmd(object sender, byte bCodeError)
        {
            try
            {
                string strName = "";
                if (chbName.Checked)
                    strName = CMD_ConfirmText + "\n"; //+" ( Адр " + bAddressSend.ToString() + ")\n";

                string strDetail = "";

                if (chbDetail.Checked == true)
                {
                    strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";
                }

                if (chbName.Checked)
                    showLogJammer.ShowMessage(rtbLogJammer, 1, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
            }
            catch (SystemException)
            { }
        }

        # endregion




        private void Connect(object sender)
        {
            if (sender == iExGRZ_BRZ)
                stateIndicateConnection.SetConnectOn();

        }

        private void Disconnect(object sender)
        {
            if (sender == iExGRZ_BRZ)
                stateIndicateConnection.SetConnectOff();

        }



        private void ReadByte(object sender, byte[] bData)
        {
            if (sender == iExGRZ_BRZ)
                stateIndicateRead.SetIndicateOn();

            if (chbByteHex.Checked)
            {
                showLogJammer.ShowMessage(rtbLogJammer, 0, showLogJammer.DecodeHexByte(bData), "", "");
            }

        }

        private void WriteByte(object sender, byte[] bData)
        {
            if (sender == iExGRZ_BRZ)
                stateIndicateWrite.SetIndicateOn();

            if (chbByteHex.Checked)
            {
                showLogJammer.ShowMessage(rtbLogJammer, 1, showLogJammer.DecodeHexByte(bData), "", "");
            }
        }


        private void InitIndicate()
        {
            stateIndicateRead = new StateIndicate(pbReadByteRRD, imgReadByte, imgWaitByte, TIME_INDICATE);
            stateIndicateWrite = new StateIndicate(pbWriteByteRRD, imgWriteByte, imgWaitByte, TIME_INDICATE);
            stateIndicateConnection = new StateConnection(bConnection, clConnect, clDisconnect);
            stateIndicateConnectionDB = new StateConnection(btnConnectDB, clConnect, clDisconnect);
            stateIndicateConnectionServer = new StateConnection(btnConnectServer, clConnect, clDisconnect);

            stateIndicateRead.SetIndicateOn();
            stateIndicateWrite.SetIndicateOn();
        }

        private void bConnection_Click(object sender, EventArgs e)
        {
            if (bConnection.BackColor == clConnect)
            {
                InitDisconnection();
            }
            else
            {
                InitConnection();
            }
        }

        private void rbCom_CheckedChanged(object sender, EventArgs e)
        {
            if (bConnection.BackColor == clConnect)
            {
                InitDisconnection();
                InitConnection();
            }
           
        }

        private void bSendCoordJammer_Click(object sender, EventArgs e)
        {
            TCoord tCoord = new TCoord();


            tCoord.bSignLongitude = Convert.ToByte(chSignLongCoord.Checked);
               
            try
            {

                byte bCodeError = (byte)(nudCodeErrorCoord.Value);
                
                tCoord.bDegreeLongitude = Convert.ToByte(tbJamDegB.Text);
                tCoord.bMinuteLongitude = Convert.ToByte(tbJamMinB.Text);
                tCoord.bSecondLongitude = Convert.ToByte(tbJamSecB.Text);

                tCoord.bSignLatitude = Convert.ToByte(chSignLatCoord.Checked);


                tCoord.bDegreeLatitude = Convert.ToByte(tbJamDegL.Text);
                tCoord.bMinuteLatitude = Convert.ToByte(tbJamMinL.Text);
                tCoord.bSecondLatitude = Convert.ToByte(tbJamSecL.Text);

                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendCoord(bCodeError, tCoord);
            }

            catch
            {
                MessageBox.Show("Ошибка ввода данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
            }

        }

        private void bSendSynchronizeJ_Click(object sender, EventArgs e)
        {
            try
            {
                TTimeMy tTimeMy = new TTimeMy();

                DateTime dtTimeSynch = dtpTimeSinchro.Value;
                tTimeMy.bHour = Convert.ToByte(dtTimeSynch.Hour);
                tTimeMy.bMinute = Convert.ToByte(dtTimeSynch.Minute);
                tTimeMy.bSecond = Convert.ToByte(dtTimeSynch.Second);

                byte bCodeError = (byte)(nudCodeErrorSynchTime.Value);

                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendSynchTime(bCodeError, tTimeMy);
            }
            catch
            {
               
            }
        }

        private void bSendTextMessageJammer_Click(object sender, EventArgs e)
        {
            try
            {
                string strText = rtbMesJam.Text;

                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendText(strText);
            }
            catch
            {
               
            }
        }

        private void bSendExecuteBearing_Click(object sender, EventArgs e)
        {
            try
            {
                byte bCodeError = (byte)(nudExecBearCodeError.Value);
                int iId = (int)(nudExecBearID.Value);
                int iFreq = (int)(nudExecBearFreq.Value);
                short sBear = (short)(nudBearingEB.Value);
                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendExecBear(bCodeError, iId, iFreq, sBear);
            }
            catch
            {
             
            }
        }

        private void bSendSimultanBearing_Click(object sender, EventArgs e)
        {
            try
            {
                byte bCodeError = (byte)(nudSimulBearCodeError.Value);
                int iId = (int)(nudSimulBearID.Value);
                int iFreq = (int)(nudSimulBearFreq.Value * 10);
                short sBearMain = (short)(nudBearingMainSB.Value);
                short sBearAdd = (short)(nudBearingAddSB.Value);



                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendSimulBear(bCodeError, iId, iFreq, sBearMain, sBearAdd);
            }
            catch
            {
                
            }
        }

        private void bSendDataFWS_Click(object sender, EventArgs e)
        {

            try
            {
                byte bCodeError = (byte)(nudCodeErrorSourceFWS.Value);

                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendReconFWS(bCodeError, tReconFWS);
            }
            catch
            {

            }
        }

        private void bSendReceiptJ_Click(object sender, EventArgs e)
        {
            byte bCodeError = Convert.ToByte(nudCodeError.Value);
            byte bType = 0;

            try
            {
                bType = Convert.ToByte(cmbActForbid.SelectedIndex);
            }
            catch(SystemException)
            {}

            switch (cmbTypeConfirm.SelectedIndex)
            {
                case 0:
                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendConfirmText(bCodeError);

                    break;

                case 1:
                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendRegime(bCodeError);

                    break;

                case 2:
                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendRangeSector(bCodeError, bType);

                    break;

                case 3:
                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendRangeSpec(bCodeError, bType);

                    break;

                case 4:
                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendSupprFWS(bCodeError);

                    break;

                case 5:
                    if (iExGRZ_BRZ != null)
                        iExGRZ_BRZ.SendSupprFHSS(bCodeError);

                    break;

                default:
                    break;

            }
        }

        private void cmbTypeConfirm_SelectedIndexChanged(object sender, EventArgs e)
        {
            label38.Visible = false;
            cmbActForbid.Visible = false;

            if (cmbTypeConfirm.SelectedIndex == 2)
            {
                label38.Visible = true;
                cmbActForbid.Visible = true;

                cmbActForbid.Items.Clear();
                cmbActForbid.Items.Add("Радиоразведка");
                cmbActForbid.Items.Add("Радиоподавление");
                cmbActForbid.SelectedIndex = 0;
 
            }

            if (cmbTypeConfirm.SelectedIndex == 3)
            {
                label38.Visible = true;
                cmbActForbid.Visible = true;

                cmbActForbid.Items.Clear();
                cmbActForbid.Items.Add("Запрещенные");
                cmbActForbid.Items.Add("Известные");
                cmbActForbid.Items.Add("Важные");
                cmbActForbid.SelectedIndex = 0;

            }
        }

       

        private void bSendTestChannelJ_Click(object sender, EventArgs e)
        {
            try
            {
                byte bCodeError = (byte)(nudStateCodeError.Value);
                byte bRegime = (byte)(cmbStateRegime.SelectedIndex);

                //if (bRegime == 2)
                //    bRegime++;

                short sNum = (short)(nudStateNumber.Value);
                byte bType = (byte)(cmbStateType.SelectedIndex);
                byte bRole = (byte)(cmbStateRole.SelectedIndex);

                byte[] bLetter = new byte[1] { (byte)1};

                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendState(bCodeError, bRegime, sNum, bType, bRole, bLetter);
            }
            catch
            {

            }

        }

        

        private void bRangeReconAdd_Click(object sender, EventArgs e)
        {
            try
            {
                TReconFWS tReconFWSTemp = new TReconFWS();

                if (chbAutoReconFWS.Checked)
                {
                    Random rnd = new Random();

                    tReconFWSTemp.iID = (int)(rnd.Next(1, 100));
                    tReconFWSTemp.iFreq = (int)(rnd.Next(1500, 30000));

                    tReconFWSTemp.sBearingOwn = Convert.ToInt16(rnd.Next(0, 359));
                    tReconFWSTemp.sBearingLinked = Convert.ToInt16(rnd.Next(0, 359));
                    
                    tReconFWSTemp.bModulation = Convert.ToByte(rnd.Next(0, 7));

                    tReconFWSTemp.bBandWidth = Convert.ToByte(rnd.Next(0, 7));

                    tReconFWSTemp.bLevelOwn = Convert.ToByte(rnd.Next(0, 250));
                    tReconFWSTemp.bLevelLinked = Convert.ToByte(rnd.Next(0, 250));


                    tReconFWSTemp.tTime.bHour = Convert.ToByte(rnd.Next(0, 23));
                    tReconFWSTemp.tTime.bMinute = Convert.ToByte(rnd.Next(0, 59));
                    tReconFWSTemp.tTime.bSecond = Convert.ToByte(rnd.Next(0, 59));

                    tReconFWSTemp.tCoord.bSignLongitude = Convert.ToByte(rnd.Next(0, 1));
                    tReconFWSTemp.tCoord.bDegreeLongitude = Convert.ToByte(rnd.Next(0, 180));
                    tReconFWSTemp.tCoord.bMinuteLongitude = Convert.ToByte(rnd.Next(0, 60));
                    tReconFWSTemp.tCoord.bSecondLongitude = Convert.ToByte(rnd.Next(0, 60));

                    tReconFWSTemp.tCoord.bSignLatitude = Convert.ToByte(rnd.Next(0, 1));
                    tReconFWSTemp.tCoord.bDegreeLatitude = Convert.ToByte(rnd.Next(0, 90));
                    tReconFWSTemp.tCoord.bMinuteLatitude = Convert.ToByte(rnd.Next(0, 60));
                    tReconFWSTemp.tCoord.bSecondLatitude = Convert.ToByte(rnd.Next(0, 60));

                   
                }
                else
                {
                    tReconFWSTemp.iID = (int)(nudIDSReconFWS.Value);
                    tReconFWSTemp.iFreq = (float)(nudFreqReconFWS.Value);

                    tReconFWSTemp.sBearingOwn = Convert.ToInt16(nudBearOwnReconFWS.Value);
                    tReconFWSTemp.sBearingLinked = Convert.ToInt16(nudBearLinkedReconFWS.Value);

                    tReconFWSTemp.bModulation = Convert.ToByte(cmbModeReconFWS.SelectedIndex);


                    tReconFWSTemp.bBandWidth = 0;

                    int iWidth = (int)(nudWidthReconFWS.Value * 10);

                    if (iWidth <= 50)
                        tReconFWSTemp.bBandWidth = 0;

                    if (iWidth > 50 && iWidth <= 125)
                        tReconFWSTemp.bBandWidth = 1;

                    if (iWidth > 125 && iWidth <= 250)
                        tReconFWSTemp.bBandWidth = 2;

                    if (iWidth > 250 && iWidth <= 1250)
                        tReconFWSTemp.bBandWidth = 3;

                    if (iWidth > 1250 && iWidth <= 30000)
                        tReconFWSTemp.bBandWidth = 4;

                    if (iWidth > 30000 && iWidth <= 35000)
                        tReconFWSTemp.bBandWidth = 5;

                    if (iWidth > 35000 && iWidth <= 83300)
                        tReconFWSTemp.bBandWidth = 6;

                    if (iWidth > 83300)
                        tReconFWSTemp.bBandWidth = 7;


                    tReconFWSTemp.bLevelOwn = Convert.ToByte(nudLevelOwnReconFWS.Value);
                    tReconFWSTemp.bLevelLinked = Convert.ToByte(nudLevelLinkedReconFWS.Value);


                    try
                    {
                      
                        DateTime dtTimeSynch = dtpTimeSinchro.Value;
                        tReconFWSTemp.tTime.bHour = Convert.ToByte(dtTimeSynch.Hour);
                        tReconFWSTemp.tTime.bMinute = Convert.ToByte(dtTimeSynch.Minute);
                        tReconFWSTemp.tTime.bSecond = Convert.ToByte(dtTimeSynch.Second);

                    }
                    catch
                    {

                    }

                    
                    tReconFWSTemp.tCoord.bSignLongitude = Convert.ToByte(chSignLongCoord.Checked);

                    try
                    {
                        tReconFWSTemp.tCoord.bDegreeLongitude = Convert.ToByte(tbFWSDegB.Text);
                        tReconFWSTemp.tCoord.bMinuteLongitude = Convert.ToByte(tbFWSMinB.Text);
                        tReconFWSTemp.tCoord.bSecondLongitude = Convert.ToByte(tbFWSSecB.Text);

                        tReconFWSTemp.tCoord.bSignLatitude = Convert.ToByte(chSignLatCoord.Checked);


                        tReconFWSTemp.tCoord.bDegreeLatitude = Convert.ToByte(tbFWSDegL.Text);
                        tReconFWSTemp.tCoord.bMinuteLatitude = Convert.ToByte(tbFWSMinL.Text);
                        tReconFWSTemp.tCoord.bSecondLatitude = Convert.ToByte(tbFWSSecL.Text);

                    }

                    catch
                    {
                        MessageBox.Show("Ошибка ввода данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    }

                    tReconFWSTemp.bSignAudio = Convert.ToByte(chbExistAudioFWS.Checked);
                    

                }


                if (tReconFWS == null)
                    tReconFWS = new TReconFWS[1];
                else
                    Array.Resize(ref tReconFWS, tReconFWS.Length + 1);

                tReconFWS[tReconFWS.Length - 1] = tReconFWSTemp;

                ShowReconFWS(rtbMemoReconFWS, tReconFWS);
            }
            catch (Exception)
            { }
        }


        private void ShowReconFWS(RichTextBox rtbMemo, TReconFWS[] tReconFWS)
        {
            rtbMemo.Clear();

            if (tReconFWS != null)
            {


                for (int i = 0; i < tReconFWS.Length; i++)
                {
                    string strDetail = "";

                    strDetail += (i + 1).ToString() + "\n";
                    strDetail += "  ID " + tReconFWS[i].iID.ToString() + "\n";
                    strDetail += "  Частота " + tReconFWS[i].iFreq.ToString("#.#") + "\n";
                    strDetail += "  Время " + tReconFWS[i].tTime.bHour.ToString() + ":" + tReconFWS[i].tTime.bMinute.ToString() + ":" + tReconFWS[i].tTime.bSecond.ToString() + "\n";
                    strDetail += "  Пеленг ведущая " + tReconFWS[i].sBearingOwn.ToString() + "\n";
                    strDetail += "  Пеленг ведомая " + tReconFWS[i].sBearingLinked.ToString() + "\n";

                    if (tReconFWS[i].tCoord.bSignLatitude == 0)
                        strDetail += "  Широта (З) ";
                    else
                        strDetail += "  Широта (В) ";
                    strDetail += tReconFWS[i].tCoord.bDegreeLatitude.ToString() + "  " + tReconFWS[i].tCoord.bMinuteLatitude.ToString() + "  " + tReconFWS[i].tCoord.bSecondLatitude.ToString() + "\n";

                    if (tReconFWS[i].tCoord.bSignLongitude == 0)
                        strDetail += "  Долгота (С) ";
                    else
                        strDetail += "  Долгота (Ю) ";
                    strDetail += tReconFWS[i].tCoord.bDegreeLongitude.ToString() + "  " + tReconFWS[i].tCoord.bMinuteLongitude.ToString() + "  " + tReconFWS[i].tCoord.bSecondLongitude.ToString() + "\n";

                    strDetail += "  Ур. сигнала ведущая " + tReconFWS[i].bLevelOwn.ToString() + "\n";
                    strDetail += "  Ур. сигнала ведомая " + tReconFWS[i].bLevelLinked.ToString() + "\n";
                    strDetail += "  Ширина полосы " + tReconFWS[i].bBandWidth.ToString() + "\n";

                    strDetail += "  Вид модуляции " + tReconFWS[i].bModulation.ToString() + "\n";

                    strDetail += "  Наличие аудиозаписи " + tReconFWS[i].bSignAudio.ToString() + "\n";

                    rtbMemo.AppendText(strDetail);

                    rtbMemo.SelectionStart = rtbMemo.TextLength;
                    rtbMemo.ScrollToCaret();

                }

            }

        }

        private void bRangeReconClear_Click(object sender, EventArgs e)
        {
            tReconFWS = null;
            ShowReconFWS(rtbMemoReconFWS, tReconFWS);

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            rtbLogJammer.Clear();
        }

      
        private void nudAddressJammer_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                bOwnAddress = Convert.ToByte(nudAddressJammer.Value);
            }
            catch(Exception)
            {}
        }

       

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                TReconFHSS tReconFHSSTemp = new TReconFHSS();

                if (chbAutoReconFHSS.Checked)
                {
                    Random rnd = new Random();

                    tReconFHSSTemp.iID = (int)(rnd.Next(1, 100));
                    tReconFHSSTemp.iFreqMin = rnd.Next(1500, 30000);
                    tReconFHSSTemp.iFreqMax = rnd.Next((int)tReconFHSSTemp.iFreqMin, 30000);

                    tReconFHSSTemp.sBearingOwn = Convert.ToInt16(rnd.Next(0, 359));
                    tReconFHSSTemp.sBearingLinked = Convert.ToInt16(rnd.Next(0, 359));

                    tReconFHSSTemp.bModulation = Convert.ToByte(rnd.Next(0, 7));

                    tReconFHSSTemp.bBandWidth = Convert.ToByte(rnd.Next(0, 7));

                    tReconFHSSTemp.bLevelOwn = Convert.ToByte(rnd.Next(0, 250));
                    tReconFHSSTemp.bLevelLinked = Convert.ToByte(rnd.Next(0, 250));
                   
                    tReconFHSSTemp.bMinute = Convert.ToByte(rnd.Next(0, 59));
                    tReconFHSSTemp.bSecond = Convert.ToByte(rnd.Next(0, 59));

                    tReconFHSSTemp.tCoord.bSignLongitude = Convert.ToByte(rnd.Next(0, 1));
                    tReconFHSSTemp.tCoord.bDegreeLongitude = Convert.ToByte(rnd.Next(0, 180));
                    tReconFHSSTemp.tCoord.bMinuteLongitude = Convert.ToByte(rnd.Next(0, 60));
                    tReconFHSSTemp.tCoord.bSecondLongitude = Convert.ToByte(rnd.Next(0, 60));

                    tReconFHSSTemp.tCoord.bSignLatitude = Convert.ToByte(rnd.Next(0, 1));
                    tReconFHSSTemp.tCoord.bDegreeLatitude = Convert.ToByte(rnd.Next(0, 90));
                    tReconFHSSTemp.tCoord.bMinuteLatitude = Convert.ToByte(rnd.Next(0, 60));
                    tReconFHSSTemp.tCoord.bSecondLatitude = Convert.ToByte(rnd.Next(0, 60));


                }
                else
                {
                    tReconFHSSTemp.iID = (int)(nudIDSReconFWS.Value);
                    tReconFHSSTemp.iFreqMin = (float)nudFreqMinReconFHSS.Value;
                    tReconFHSSTemp.iFreqMax = (float)nudFreqMaxReconFHSS.Value;

                    tReconFHSSTemp.sBearingOwn = Convert.ToInt16(nudBearOwnReconFHSS.Value);
                    tReconFHSSTemp.sBearingLinked = Convert.ToInt16(nudBearLinkedReconFHSS.Value);

                    tReconFHSSTemp.bModulation = Convert.ToByte(cmbModeReconFHSS.SelectedIndex);


                    tReconFHSSTemp.bBandWidth = 0;

                    int iWidth = (int)(nudWidthReconFHSS.Value);

                    if (iWidth <= 50)
                        tReconFHSSTemp.bBandWidth = 0;

                    if (iWidth > 50 && iWidth <= 125)
                        tReconFHSSTemp.bBandWidth = 1;

                    if (iWidth > 125 && iWidth <= 250)
                        tReconFHSSTemp.bBandWidth = 2;

                    if (iWidth > 250 && iWidth <= 1250)
                        tReconFHSSTemp.bBandWidth = 3;

                    if (iWidth > 1250 && iWidth <= 30000)
                        tReconFHSSTemp.bBandWidth = 4;

                    if (iWidth > 30000 && iWidth <= 35000)
                        tReconFHSSTemp.bBandWidth = 5;

                    if (iWidth > 35000 && iWidth <= 83300)
                        tReconFHSSTemp.bBandWidth = 6;

                    if (iWidth > 83300)
                        tReconFHSSTemp.bBandWidth = 7;


                    tReconFHSSTemp.bLevelOwn = Convert.ToByte(nudLevelOwnReconFHSS.Value);
                    tReconFHSSTemp.bLevelLinked = Convert.ToByte(nudLevelLinkedReconFHSS.Value);


                    try
                    {                       
                        tReconFHSSTemp.bMinute = Convert.ToByte(nudMinuteReconFHSS.Value);
                        tReconFHSSTemp.bSecond = Convert.ToByte(nudSecondReconFHSS.Value);

                    }
                    catch
                    {

                    }


                    tReconFHSSTemp.tCoord.bSignLongitude = Convert.ToByte(chSignLongCoord.Checked);

                    try
                    {
                        tReconFHSSTemp.tCoord.bDegreeLongitude = Convert.ToByte(tbFWSDegB.Text);
                        tReconFHSSTemp.tCoord.bMinuteLongitude = Convert.ToByte(tbFWSMinB.Text);
                        tReconFHSSTemp.tCoord.bSecondLongitude = Convert.ToByte(tbFWSSecB.Text);

                        tReconFHSSTemp.tCoord.bSignLatitude = Convert.ToByte(chSignLatCoord.Checked);

                        tReconFHSSTemp.tCoord.bDegreeLatitude = Convert.ToByte(tbFWSDegL.Text);
                        tReconFHSSTemp.tCoord.bMinuteLatitude = Convert.ToByte(tbFWSMinL.Text);
                        tReconFHSSTemp.tCoord.bSecondLatitude = Convert.ToByte(tbFWSSecL.Text);
                    }

                    catch
                    {
                        MessageBox.Show("Ошибка ввода данных", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.ServiceNotification);
                    }


                    tReconFHSSTemp.bSignAudio = Convert.ToByte(chbExistAudioFHSS.Checked);
                }


                if (tReconFHSS == null)
                    tReconFHSS = new TReconFHSS[1];
                else
                    Array.Resize(ref tReconFHSS, tReconFHSS.Length + 1);

                tReconFHSS[tReconFHSS.Length - 1] = tReconFHSSTemp;

                ShowReconFHSS(rtbMemoReconFHSS, tReconFHSS);
            }
            catch (Exception)
            { }
        }


        private void ShowReconFHSS(RichTextBox rtbMemo, TReconFHSS[] tReconFHSS)
        {
            rtbMemo.Clear();

            if (tReconFHSS != null)
            {
                for (int i = 0; i < tReconFHSS.Length; i++)
                {
                    string strDetail = "";

                    strDetail += (i + 1).ToString() + "\n";
                    strDetail += "  ID " + tReconFHSS[i].iID.ToString() + "\n";
                    strDetail += "  Частота мин " + tReconFHSS[i].iFreqMin.ToString("#.#") + "\n";
                    strDetail += "  Частота макс " + tReconFHSS[i].iFreqMax.ToString("#.#") + "\n";

                    strDetail += "  Шаг сетки " + tReconFHSS[i].sStep.ToString() + "\n";
                    strDetail += "  Длительность импульса " + tReconFHSS[i].iDuration.ToString() + "\n";

                    strDetail += "  Время " + tReconFHSS[i].bMinute.ToString() + ":" + tReconFHSS[i].bSecond.ToString() + "\n";
                    strDetail += "  Пеленг ведущая " + tReconFHSS[i].sBearingOwn.ToString() + "\n";
                    strDetail += "  Пеленг ведомая " + tReconFHSS[i].sBearingLinked.ToString() + "\n";

                    if (tReconFHSS[i].tCoord.bSignLatitude == 0)
                        strDetail += "  Широта (З) ";
                    else
                        strDetail += "  Широта (В) ";
                    strDetail += tReconFHSS[i].tCoord.bDegreeLatitude.ToString() + "  " + tReconFHSS[i].tCoord.bMinuteLatitude.ToString() + "  " + tReconFHSS[i].tCoord.bSecondLatitude.ToString() + "\n";

                    if (tReconFHSS[i].tCoord.bSignLongitude == 0)
                        strDetail += "  Долгота (С) ";
                    else
                        strDetail += "  Долгота (Ю) ";
                    strDetail += tReconFHSS[i].tCoord.bDegreeLongitude.ToString() + "  " + tReconFHSS[i].tCoord.bMinuteLongitude.ToString() + "  " + tReconFHSS[i].tCoord.bSecondLongitude.ToString() + "\n";

                    strDetail += "  Ур. сигнала ведущая " + tReconFHSS[i].bLevelOwn.ToString() + "\n";
                    strDetail += "  Ур. сигнала ведомая " + tReconFHSS[i].bLevelLinked.ToString() + "\n";
                    strDetail += "  Ширина полосы " + tReconFHSS[i].bBandWidth.ToString() + "\n";

                    strDetail += "  Вид модуляции " + tReconFHSS[i].bModulation.ToString() + "\n";

                    strDetail += "  Наличие аудиозаписи " + tReconFHSS[i].bSignAudio.ToString() + "\n";

                        
                    rtbMemo.AppendText(strDetail);

                    rtbMemo.SelectionStart = rtbMemo.TextLength;
                    rtbMemo.ScrollToCaret();

                }

            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            tReconFHSS = null;
            ShowReconFHSS(rtbMemoReconFHSS, tReconFHSS);
        }

        private void bSendDataFHSS_Click(object sender, EventArgs e)
        {
            try
            {
                byte bCodeError = (byte)(nudCodeErrorSourceFHSS.Value);

                if (iExGRZ_BRZ != null)
                    iExGRZ_BRZ.SendReconFHSS(bCodeError, tReconFHSS);
            }
            catch
            {

            }
        }

        private void bSendStateSupressFreq_Click(object sender, EventArgs e)
        {
            try
            {
                byte bCodeError = (byte)(nudSupprCtrlCodeError.Value);

                byte bCountSource = (byte)(nudSupprCtrlCountSource.Value);

                Random rnd = new Random();

                if (iExGRZ_BRZ != null)
                {
                    switch (cmbTypeFreqState.SelectedIndex)
                    {
                        case 0:
                            TResSupprFWS[] tResSupprFWS = null;

                            if (bCountSource > 0)
                            {
                                tResSupprFWS = new TResSupprFWS[bCountSource];

                                for (int i=0; i<bCountSource; i++)
                                {
                                    tResSupprFWS[i].iID = Convert.ToInt32(rnd.Next(1, 1000));
                                    tResSupprFWS[i].iFreq = Convert.ToInt32(rnd.Next(1500, 30000));
                                    tResSupprFWS[i].bResCtrl = Convert.ToByte(rnd.Next(0,4));
                                    tResSupprFWS[i].bResSuppr = Convert.ToByte(rnd.Next(0, 4));
                                }
                            }

                            iExGRZ_BRZ.SendStateSupprFWS(bCodeError,tResSupprFWS);
                            break;

                        case 1:
                            TResSupprFHSS[] tResSupprFHSS = null;

                            if (bCountSource > 0)
                            {
                                tResSupprFHSS = new TResSupprFHSS[bCountSource];

                                for (int i = 0; i < bCountSource; i++)
                                {
                                    tResSupprFHSS[i].iID = Convert.ToInt32(rnd.Next(1, 1000));
                                    tResSupprFHSS[i].iFreqMin = (int)(rnd.Next(1500, 30000));
                                    tResSupprFHSS[i].iFreqMax = (int)(rnd.Next((int)tResSupprFHSS[i].iFreqMin, 30000));
                                    tResSupprFHSS[i].bResCtrl = Convert.ToByte(rnd.Next(0, 4));
                                    tResSupprFHSS[i].bResSuppr = Convert.ToByte(rnd.Next(0, 4));
                                }
                            }

                            iExGRZ_BRZ.SendStateSupprFHSS(bCodeError, tResSupprFHSS);
                            break;


                        default:
                            break;

                    }
                    
                    
                }
            }
            catch
            {

            }
        }

        private void chbAutoAnswer_CheckedChanged(object sender, EventArgs e)
        {
            blAutoAnswer = chbAutoAnswer.Checked;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TestClass.bRegime = 5;
        }

        private void btnConnectDB_Click(object sender, EventArgs e)
        {
            if (btnConnectDB.BackColor == clConnect)
            {
                InitDisconnectionDB();
            }
            else
            {
                InitConnectionDB();
            }
        }

        private void btnConnectServer_Click(object sender, EventArgs e)
        {
            if (btnConnectServer.BackColor == clConnect)
            {
                InitDisconnectionServer();
            }
            else
            {
                InitConnectionServer();
            }
        }

          #region ClientDB

        ServiceClient clientDB;
        string appName = "ODKvetka";

        void InitClientDB()
        {
            if (clientDB == null) return;

            clientDB.OnConnect += ClientDB_OnConnect;
            clientDB.OnDisconnect += ClientDB_OnDisconnect;
            (clientDB.Tables[NameTable.TableJammer] as ITableUpdate<TableJammer>).OnUpTable += HandlerUpdate_TableJammer;
            (clientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += HandlerUpdate_TableFreqForbidden;
            (clientDB.Tables[NameTable.TableFreqImportant] as ITableUpdate<TableFreqImportant>).OnUpTable += HandlerUpdate_TableFreqImportant;
            (clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += HandlerUpdate_TableFreqKnown;

            (clientDB.Tables[NameTable.TableFreqRangesElint] as ITableUpdate<TableFreqRangesElint>).OnUpTable += HandlerUpdate_TableFreqRangesElint;
            (clientDB.Tables[NameTable.TableFreqRangesJamming] as ITableUpdate<TableFreqRangesJamming>).OnUpTable += HandlerUpdate_TableFreqRangesJamming;
            (clientDB.Tables[NameTable.TableSectorsElint] as ITableUpdate<TableSectorsElint>).OnUpTable += HandlerUpdate_TableSectorsElint;


            (clientDB.Tables[NameTable.TableResFFJam] as ITableUpdate<TableResFFJam>).OnUpTable += HandlerUpdate_TableSuppressFWS;
            (clientDB.Tables[NameTable.TableResFHSSJam] as ITableUpdate<TableResFHSSJam>).OnUpTable += HandlerUpdate_TableSuppressFHSS;

            (clientDB.Tables[NameTable.TableResFF] as ITableUpdate<TableResFF>).OnUpTable += HandlerUpdate_TableResFF;
            (clientDB.Tables[NameTable.TableResFFDistribution] as ITableUpdate<TableResFFDistribution>).OnUpTable += TestInformExchangeGRZ_OnUpTable; ;
            //(clientDB.Tables[NameTable.TableDigitalResFF] as ITableUpdate<TableDigitalResFF>).OnUpTable += HandlerUpdate_TableDigitalResFF;
            //(clientDB.Tables[NameTable.TableAnalogResFF] as ITableUpdate<TableAnalogResFF>).OnUpTable += HandlerUpdate_TableAnalogResFF;
            (clientDB.Tables[NameTable.TableResFHSS] as ITableUpdate<TableResFHSS>).OnUpTable += HandlerUpdate_TableResFHSS;
            //(clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += TestInformExchangeGRZ_OnUpTable1; ;
        }

        

        private void ClientDB_OnDisconnect(object sender, ClientEventArgs e)
        {
            stateIndicateConnectionDB.SetConnectOff();
        }

        private void ClientDB_OnConnect(object sender, global::InheritorsEventArgs.ClientEventArgs e)
        {
            stateIndicateConnectionDB.SetConnectOn();
            LoadTables();
        }


        private void InitConnectionDB()
        {
            clientDB = new ServiceClient(appName, mtbDbIp.Text, Convert.ToInt32(mtbDbPort.Text));
            InitClientDB();

            try
            {
                clientDB.Connect();
            }
            catch (SystemException ex)
            {
            }
        }

        private void InitDisconnectionDB()
        {
            if (clientDB == null) return;

            clientDB.Disconnect();
            clientDB = null;
        }

        // Станции помех
        public List<TableJammer> lJammerStation = new List<TableJammer>();
        // Известные частоты (ИЧ)
        public List<FreqRanges> lFreqKnown = new List<FreqRanges>();
        // Важные частоты (ИЧ)
        public List<FreqRanges> lFreqImportant = new List<FreqRanges>();
        // Запрещенные частоты (ЗЧ)
        public List<FreqRanges> lFreqForbidden = new List<FreqRanges>();
        // Сектора радиоразведки (СРР)
        public List<TableSectorsElint> lSectorsElint = new List<TableSectorsElint>();
        // Сектора радиоразведки (ДРР)
        public List<FreqRanges> lFreqRangesElint = new List<FreqRanges>();
        // Диапазоны радиоразведки (ДРП)
        public List<FreqRanges> lFreqRangesJamming = new List<FreqRanges>();
        // ФРЧ на РП 
        public List<TableResFFJam> lResFFJam = new List<TableResFFJam>();
        // ППРЧ на РП
        public List<TableResFHSSJam> lResFHSSJam = new List<TableResFHSSJam>();
        // ФРЧ
        public List<TableResFF> lResFF = new List<TableResFF>();
        // ФРЧ Analog
        public List<TableAnalogResFF> lResFFAnalog = new List<TableAnalogResFF>();
        // ФРЧ Digital
        public List<TableDigitalResFF> lResFFDigital = new List<TableDigitalResFF>();
        // ФРЧ ЦР
        public List<TableResFFDistribution> lResFFDistribution = new List<TableResFFDistribution>();
        // ППРЧ
        public List<TableResFHSS> lResFHSS = new List<TableResFHSS>();
        // Global
        public GlobalProperties lGlobal = new GlobalProperties();

        int ownStation = 0;

        #region Update Tables handlers
        private void HandlerUpdate_TableJammer(object sender, TableEventArgs<TableJammer> e)
        {
            lJammerStation = e.Table;
            var hasOwn = lJammerStation.Find(x => x.IsOwn == true);
            ownStation = hasOwn == null ? 0 : hasOwn.Id;
            byte role = hasOwn == null ? (byte)0 : (byte)hasOwn.Role;
            
            var coords = hasOwn == null ? new Coord() : hasOwn.Coordinates;
            UpdatetStationInfo(ownStation, role, coords);
        }

        private void HandlerUpdate_TableFreqImportant(object sender, TableEventArgs<TableFreqImportant> e)
        {
            lFreqImportant = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == ownStation).ToList();
        }

        private void HandlerUpdate_TableFreqKnown(object sender, TableEventArgs<TableFreqKnown> e)
        {
            lFreqKnown = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == ownStation).ToList();
        }

        private void HandlerUpdate_TableFreqForbidden(object sender, TableEventArgs<TableFreqForbidden> e)
        {
            lFreqForbidden = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == ownStation).ToList();
        }

        private void HandlerUpdate_TableFreqRangesElint(object sender, TableEventArgs<TableFreqRangesElint> e)
        {
            lFreqRangesElint = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == ownStation).ToList();
        }

        private void HandlerUpdate_TableFreqRangesJamming(object sender, TableEventArgs<TableFreqRangesJamming> e)
        {
            lFreqRangesJamming = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == ownStation).ToList();
        }

        private void HandlerUpdate_TableSectorsElint(object sender, TableEventArgs<TableSectorsElint> e)
        {
            lSectorsElint = e.Table.Where(x => x.NumberASP == ownStation).ToList();
        }

        private void HandlerUpdate_TableSuppressFHSS(object sender, TableEventArgs<TableResFHSSJam> e)
        {
            lResFHSSJam = e.Table.Where(x => x.NumberASP == ownStation).ToList();
        }

        private void HandlerUpdate_TableSuppressFWS(object sender, TableEventArgs<TableResFFJam> e)
        {
            lResFFJam = e.Table.Where(x => x.NumberASP == ownStation).ToList();
        }

        private void HandlerUpdate_TableResFHSS(object sender, TableEventArgs<TableResFHSS> e)
        {
            lResFHSS = new List<TableResFHSS>(e.Table);
        }

        private void HandlerUpdate_TableResFF(object sender, TableEventArgs<TableResFF> e)
        {
            lResFF = new List<TableResFF>(e.Table);
        }

        private void HandlerUpdate_TableAnalogResFF(object sender, TableEventArgs<TableAnalogResFF> e)
        {
            lResFFAnalog = new List<TableAnalogResFF>(e.Table);
        }

        private void HandlerUpdate_TableDigitalResFF(object sender, TableEventArgs<TableDigitalResFF> e)
        {
            lResFFDigital = new List<TableDigitalResFF>(e.Table);
        }

        private void TestInformExchangeGRZ_OnUpTable(object sender, TableEventArgs<TableResFFDistribution> e)
        {
            lResFFDistribution = new List<TableResFFDistribution>(e.Table);
        }

        private void TestInformExchangeGRZ_OnUpTable1(object sender, TableEventArgs<GlobalProperties> e)
        {
            lGlobal = e.Table.First();
        }

        private void UpdatetStationInfo(int stationId, byte type, Coord coord)
        {
            nudStateNumber.Value = stationId;
            if (cmbStateRole.InvokeRequired)
            {
                cmbStateRole.Invoke((MethodInvoker)(delegate ()
                {
                    cmbStateRole.SelectedIndex = type;
                }));

            }
            else
            {
                cmbStateRole.SelectedIndex = type;
            }

            var location = ConvertCoordsToLocal(coord.Latitude, coord.Longitude);

            
            chSignLatCoord.Checked = location.bSignLatitude == 0 ? false : true;
            tbJamDegL.Text = location.bDegreeLatitude.ToString();
            tbJamMinL.Text = location.bMinuteLatitude.ToString();
            tbJamSecL.Text = location.bSecondLatitude.ToString();

            chSignLongCoord.Checked = location.bSignLongitude == 0 ? false : true;
            tbJamDegB.Text = location.bDegreeLongitude.ToString();
            tbJamMinB.Text = location.bMinuteLongitude.ToString();
            tbJamSecB.Text = location.bSecondLongitude.ToString();
            
        }
        #endregion

        private async void LoadTables()
        {
            try
            {
                lJammerStation = await clientDB.Tables[NameTable.TableJammer].LoadAsync<TableJammer>();
                var hasOwn = lJammerStation.Find(x => x.IsOwn == true);
                ownStation = hasOwn == null ? 0 : hasOwn.Id;

                if (ownStation == 0) return;

                try
                {
                    byte role = (byte)hasOwn.Role;
                    var coords = hasOwn == null ? new Coord() : hasOwn.Coordinates;
                    UpdatetStationInfo(ownStation, role, coords);
                }
                catch(Exception ex) { }

                lFreqKnown = await (clientDB.Tables[NameTable.TableFreqKnown] as IDependentAsp).LoadByFilterAsync<FreqRanges>(ownStation);
                lFreqImportant = await (clientDB.Tables[NameTable.TableFreqImportant] as IDependentAsp).LoadByFilterAsync<FreqRanges>(ownStation);
                lFreqForbidden = await (clientDB.Tables[NameTable.TableFreqForbidden] as IDependentAsp).LoadByFilterAsync<FreqRanges>(ownStation);
                lFreqRangesElint = await (clientDB.Tables[NameTable.TableFreqRangesElint] as IDependentAsp).LoadByFilterAsync<FreqRanges>(ownStation); 
                lFreqRangesJamming = await (clientDB.Tables[NameTable.TableFreqRangesJamming] as IDependentAsp).LoadByFilterAsync<FreqRanges>(ownStation);
                lSectorsElint = await (clientDB.Tables[NameTable.TableSectorsElint] as IDependentAsp).LoadByFilterAsync<TableSectorsElint>(ownStation);
                lResFFJam = await (clientDB.Tables[NameTable.TableResFFJam] as IDependentAsp).LoadByFilterAsync<TableResFFJam>(ownStation);
                lResFHSSJam = await (clientDB.Tables[NameTable.TableResFHSSJam] as IDependentAsp).LoadByFilterAsync<TableResFHSSJam>(ownStation);
                lResFF = await clientDB.Tables[NameTable.TableResFF].LoadAsync<TableResFF>();
                lResFHSS = await clientDB.Tables[NameTable.TableResFHSS].LoadAsync<TableResFHSS>();
                lResFFDistribution = await clientDB.Tables[NameTable.TableResFFDistribution].LoadAsync<TableResFFDistribution>();
                //var lGlobalTable = await clientDB.Tables[NameTable.GlobalProperties].LoadAsync<GlobalProperties>();
                //lGlobal = lGlobalTable.First();
            }
            catch (ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void SendFreqRangesToDB(byte bType, TRangeSpec[] tRangeSpec)
        {
            if (tRangeSpec == null || clientDB == null) return;

            var listRecords = new List<FreqRanges>();
            for (int i = 0; i < tRangeSpec.Length; i++) 
            {
                listRecords.Add(new FreqRanges(ownStation, tRangeSpec[i].iFregMin / 10, tRangeSpec[i].iFregMax / 10, "", false));
            }

            switch (bType)
            {
                case 0:
                    clientDB?.Tables[NameTable.TableFreqForbidden].AddRange(listRecords.Select(t => t.ToFreqForbidden()).ToList());
                    break;

                case 1:
                    clientDB?.Tables[NameTable.TableFreqKnown].AddRange(listRecords.Select(t => t.ToFreqKnown()).ToList());
                    break;

                case 2:
                    clientDB?.Tables[NameTable.TableFreqImportant].AddRange(listRecords.Select(t => t.ToFreqImportant()).ToList());
                    break;

                default:
                    break;
            }
        }


        private void SendSectorsRangesToDB(byte bType, TRangeSector[] tRangeSector)
        {
            if (tRangeSector == null || clientDB == null) return;

            var listRecordsRanges = new List<FreqRanges>();
            var listRecordsSectors = new List<TableSectorsElint>();
            for (int i = 0; i < tRangeSector.Length; i++)
            {
                listRecordsRanges.Add(new FreqRanges(ownStation, tRangeSector[i].iFregMin / 10, tRangeSector[i].iFregMax / 10, "", false));
                listRecordsSectors.Add(new TableSectorsElint(ownStation, tRangeSector[i].sAngleMin, tRangeSector[i].sAngleMax, "", false));
            }

            switch (bType)
            {
                case 0:
                    clientDB?.Tables[NameTable.TableFreqRangesElint].AddRange(listRecordsRanges.Select(t => t.ToFreqRangesElint()).ToList());
                    clientDB?.Tables[NameTable.TableSectorsElint].AddRange(listRecordsSectors);
                    break;

                case 1:
                    clientDB?.Tables[NameTable.TableFreqRangesJamming].AddRange(listRecordsRanges.Select(t => t.ToFreqRangesJamming()).ToList());
                    break;
                default:
                    break;
            }
        }


        private void SendFWSSuppress(int iDuration, TSupprFWS[] tSupprFWS)
        {
            if (tSupprFWS == null || clientDB == null) return;

            var listRecords = new List<TableResFFJam>();
            for (int i = 0; i < tSupprFWS.Length; i++)
            {
                var threshold = (short)(-1 * (int)tSupprFWS[i].bThreshold);
                listRecords.Add(new TableResFFJam(ownStation, false, 0, tSupprFWS[i].iFreq, tSupprFWS[i].sBearing, new InterferenceParameters(tSupprFWS[i].bModulation, tSupprFWS[i].bDeviation, tSupprFWS[i].bManipulation, tSupprFWS[i].bDuration), threshold, 0, tSupprFWS[i].bPrioritet, new Coord()));
            }

            clientDB?.Tables[NameTable.TableResFFJam].AddRange(listRecords);
        }

        private void SendFHSSSuppress(int iDuration, TSupprFHSS[] tSupprFHSS)
        {
            if (tSupprFHSS == null || clientDB == null) return;
            var listRecords = new List<TableResFHSSJam>();
            for (int i = 0; i < tSupprFHSS.Length; i++)
            {
                listRecords.Add(new TableResFHSSJam(ownStation, false, 0, tSupprFHSS[i].iFreqMin, tSupprFHSS[i].iFreqMax, new InterferenceParameters(tSupprFHSS[i].bModulation, tSupprFHSS[i].bDeviation, tSupprFHSS[i].bManipulation, 1), -90,  Array.Empty<byte>(), 0));
            }

            clientDB?.Tables[NameTable.TableResFHSSJam].AddRange(listRecords);
        }
        #endregion

        #region Server

        private Transmission.TransmissionClient client;
        private Channel channel;
        private DateTime Deadline => DateTime.UtcNow.AddSeconds(10);

        private delegate TResponse CommandDelegate<TRequest, TResponse>(
            TRequest request,
            Metadata headers = null,
            DateTime? deadline = null,
            CancellationToken cancellationToken = default);

        private async void InitConnectionServer()
        {
            channel = new Channel(mtbServerIp.Text, Convert.ToInt32(mtbServerPort.Text), ChannelCredentials.Insecure);
            client = new Transmission.TransmissionClient(this.channel);

            try
            {
                await client.PingAsync(new PingRequest() { Peer = new DefaultRequest() { PeerName = "OD server" } }, deadline: this.Deadline);
                stateIndicateConnectionServer.SetConnectOn();
                Task.Run(async () => await SubscribeServerState());
            }
            catch(Exception ex)
            {
                stateIndicateConnectionDB.SetConnectOff();
            }
        }


        private void InitDisconnectionServer()
        {
            channel.ShutdownAsync();
            stateIndicateConnectionServer.SetConnectOff();
        }

        private TResponse SendGprcCommandWithDeadline<TRequest, TResponse>(CommandDelegate<TRequest, TResponse> command, TRequest request)
            where TResponse : new()
        {
            try
            {
                var commandResponse = command(request: request, deadline: this.Deadline);
                stateIndicateConnectionServer.SetConnectOn();
                return commandResponse;
            }
            catch (RpcException)
            {
                stateIndicateConnectionServer.SetConnectOff();
                return new TResponse();
            }
        }


        
        //private bool Ping()
        //{
        //    var pingResponse = SendGprcCommandWithDeadline(client.Ping, new PingRequest());
        //    return true;
        //}

        private bool SetMode(byte mode)
        {
            if (client == null) return false;

            var request = new SetModeRequest() { Mode = (DspServerMode)mode };
            var pingResponse = SendGprcCommandWithDeadline(client.SetMode, request);
            return true;
        }

        private byte GetMode()
        {
            var pingResponse = SendGprcCommandWithDeadline(client.GetMode, new DefaultRequest());
            return (byte)pingResponse.Mode;
        }

        private async Task SubscribeServerState()
        {
            using (var stream = client.SubscribeServerEvents(new DefaultRequest()))
            {
                try
                {
                    var tokenSource = new CancellationTokenSource();
                    await DisplayAsync(stream.ResponseStream, tokenSource.Token);

                    tokenSource.Cancel();
                }
                catch
                {
                }
            }
        }

        private async Task DisplayAsync(IAsyncStreamReader<ServerState> stream, CancellationToken token)
        {
            try
            {
                while (await stream.MoveNext(cancellationToken: token))
                {
                    try
                    {
                        var responce = stream.Current;
                        if (cmbStateRegime.InvokeRequired)
                        {
                            cmbStateRegime.Invoke(
                                (MethodInvoker)(delegate()
                                                       {
                                                           cmbStateRegime.SelectedIndex = (int)responce.CurrentMode;
                                                       }));

                        }
                        else
                        {
                            cmbStateRegime.SelectedIndex = (int)responce.CurrentMode;

                        }
                    }
                    catch
                    {
                    }
                    //var nameTable = responce.CurrentMode = );
                }
            }
            catch (Exception excp)
            {
                return;
            }
        }

        private TCoord ConvertCoordsToLocal(double latitude, double longitude)
        {
            var coord = new TCoord();

            var bDegreeLatitude = (int)latitude;
            var bMinuteLatitude = ((latitude - bDegreeLatitude) * 60);
            var bSecondLatitude = ((bMinuteLatitude - (int)bMinuteLatitude) * 60);

            coord.bDegreeLatitude = (byte)bDegreeLatitude;
            coord.bMinuteLatitude = (byte)bMinuteLatitude;
            coord.bSecondLatitude = (byte)bSecondLatitude;
            coord.bSignLatitude = latitude >= 0 ? (byte)0 : (byte)1;

            var bDegreeLongitude = (int)longitude;
            var bMinuteLongitude = ((longitude - bDegreeLongitude) * 60);
            var bSecondLongitude = ((bMinuteLongitude - (int)bMinuteLongitude) * 60);

            coord.bDegreeLongitude = (byte)bDegreeLongitude;
            coord.bMinuteLongitude = (byte)bMinuteLongitude;
            coord.bSecondLongitude = (byte)bSecondLongitude;
            coord.bSignLongitude = longitude >= 0 ? (byte)0 : (byte)1;

            return coord;
        }

        private TReconFWS[] ConvertResFwsToLocal(List<TableResFF> records)
        {
            Random rnd = new Random();
            var tReconFWSAuto = new TReconFWS[records.Count];

            for (int i = 0; i < records.Count; i++)
            {
                tReconFWSAuto[i].iID = records[i].Id;
                tReconFWSAuto[i].iFreq = (float)records[i].FrequencyKHz;

                tReconFWSAuto[i].sBearingOwn = (short)records[i].BearingOwn;
                tReconFWSAuto[i].sBearingLinked = (short)records[i].BearingLinked;

                tReconFWSAuto[i].bModulation = Convert.ToByte(rnd.Next(0, 7));

                tReconFWSAuto[i].bBandWidth = Convert.ToByte(rnd.Next(0, 7));

                tReconFWSAuto[i].bLevelOwn = (byte)records[i].Level;
                tReconFWSAuto[i].bLevelLinked = 0;


                tReconFWSAuto[i].tTime.bHour = (byte)records[i].Time.Hour;
                tReconFWSAuto[i].tTime.bMinute = (byte)records[i].Time.Minute;
                tReconFWSAuto[i].tTime.bSecond = (byte)records[i].Time.Second;
                tReconFWSAuto[i].tCoord = ConvertCoordsToLocal(records[i].Coordinates.Latitude, records[i].Coordinates.Longitude);
                
            }

            return tReconFWSAuto;
        }

        private TReconFWS[] ConvertResFwsToLocal(List<TableResFFDistribution> records)
        {
            Random rnd = new Random();
            var tReconFWSAuto = new TReconFWS[records.Count];

            for (int i = 0; i < records.Count; i++)
            {
                tReconFWSAuto[i].iID = records[i].Id;
                tReconFWSAuto[i].iFreq = (float)records[i].FrequencyKHz;

                tReconFWSAuto[i].sBearingOwn = (short)records[i].BearingOwn;
                tReconFWSAuto[i].sBearingLinked = (short)records[i].BearingLinked;
                tReconFWSAuto[i].bModulation = Convert.ToByte(rnd.Next(0, 7));

                tReconFWSAuto[i].bBandWidth = Convert.ToByte(rnd.Next(0, 7));
                tReconFWSAuto[i].bLevelOwn = (byte)Math.Abs(records[i].Level);
                tReconFWSAuto[i].bLevelLinked = 0;


                tReconFWSAuto[i].tTime.bHour = (byte)records[i].Time.Hour;
                tReconFWSAuto[i].tTime.bMinute = (byte)records[i].Time.Minute;
                tReconFWSAuto[i].tTime.bSecond = (byte)records[i].Time.Second;
                tReconFWSAuto[i].tCoord = ConvertCoordsToLocal(records[i].Coordinates.Latitude, records[i].Coordinates.Longitude);

            }

            return tReconFWSAuto;
        }


        private TReconFHSS[] ConvertResFhssToLocal(List<TableResFHSS> records)
        {
            Random rnd = new Random();
            var tReconFHSSAuto = new TReconFHSS[records.Count];

            for (int i = 0; i < records.Count; i++)
            {
                tReconFHSSAuto[i].iID = records[i].Id;
                tReconFHSSAuto[i].iFreqMin = (float)records[i].FrequencyMinKHz;
                tReconFHSSAuto[i].iFreqMax = (float)records[i].FrequencyMaxKHz;

                tReconFHSSAuto[i].sBearingOwn = Convert.ToInt16(rnd.Next(0, 359));
                tReconFHSSAuto[i].sBearingLinked = Convert.ToInt16(rnd.Next(0, 359));

                tReconFHSSAuto[i].bModulation = records[i].Modulation;

                tReconFHSSAuto[i].bBandWidth = (byte)records[i].BandKHz;


                tReconFHSSAuto[i].bLevelOwn = Convert.ToByte(rnd.Next(0, 250));
                tReconFHSSAuto[i].bLevelLinked = 0;


                tReconFHSSAuto[i].bMinute = Convert.ToByte(rnd.Next(0, 59));
                tReconFHSSAuto[i].bSecond = Convert.ToByte(rnd.Next(0, 59));
                tReconFHSSAuto[i].tCoord = ConvertCoordsToLocal(rnd.Next(50,56), rnd.Next(25, 29));
                
            }

            return tReconFHSSAuto;
        }
        #endregion

        private void button4_Click(object sender, EventArgs e)
        {
            tbcCmdJammer.Visible = !tbcCmdJammer.Visible;
        }

        private void pTypeConnection_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
