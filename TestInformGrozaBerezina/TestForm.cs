﻿using FuncVLC;
using GrozaBerezinaDLL;
using Grpc.Core;
using GrpcClientLib;
using InheritorsEventArgs;
using KvetkaModelsDBLib;
using StructTypeGR_BRZ;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using TransmissionPackageKvetka;

namespace TestODServer
{


    public partial class TestInformExchangeBRZ : Form
    {

        const string CMD_Text = "  Текстовое сообщение";
        const string CMD_ConfirmText = "  Текстовое сообщение (подтверждение)";
        const string CMD_SynchTime = "  Назначение времени (синхронизация)";
        const string CMD_Regime = "  Назначение режима работы";
        const string CMD_RangeSector = "  Назначение секторов и диапазонов";
        const string CMD_RangeSpec = "  Назначение специальных частот";
        const string CMD_SupprFWS = "  Назначение ФРЧ на РП";
        const string CMD_SupprFHSS = "  Назначение ППРЧ на РП";
        const string CMD_State = "  Запрос состояния";
        const string CMD_Coord = "  Запрос координат";
        const string CMD_ReconFWS = "  Запрос ИРИ ФРЧ";
        const string CMD_ReconFHSS = "  Запрос ИРИ ППРЧ";
        const string CMD_ExecBear = "  Запрос на ИП";
        const string CMD_SimulBear = "  Запрос на КвП";
        const string CMD_StateSupprFWS = "  Запрос состояния РП ИРИ ФРЧ";
        const string CMD_StateSupprFHSS = "  Запрос состояния РП ИРИ ППРЧ";

        short TIME_INDICATE = 300;
        byte COUNT_CNT = 5;
        byte COUNT_RS232 = 3;
        byte COUNT_ETH = 2;
        byte COUNT_CLIENT = 6;

        const byte CntRRC = 1;
        const byte Cnt3G = 7;
        const byte CntModem = 8;
        const byte CntHF = 9;
        const byte CntVHF = 10;


        byte[] bTypeConnection = null;

        IEPC objIEPC;

        Image imgReadByte = Properties.Resources.green;
        Image imgWriteByte = Properties.Resources.red;
        Image imgWaitByte = Properties.Resources.gray;

        Color clConnect = Color.Green;
        Color clDisconnect = Color.Red;

        StateIndicate[] stateIndicateRead = null;
        StateIndicate[] stateIndicateWrite = null;
        StateConnection[] stateIndicateConnection = null;
        StateConnection stateIndicateConnectionDB = null;
        StateConnection stateIndicateConnectionServer = null;
        /* StateIndicate[] stateIndicateReadClient = null;
         StateIndicate[] stateIndicateWriteClient = null;
         StateConnection[] stateIndicateConnectionClient = null;*/

        Button[] bConnect = null;
        PictureBox[] pbReadByte = null;
        PictureBox[] pbWriteByte = null;
        ComboBox[] cmbCom = null;
        RichTextBox[] rtbLog = null;
        CheckBox[] chbRRD = null;

        Button[] bConnectClient = null;
        PictureBox[] pbReadByteClient = null;
        PictureBox[] pbWriteByteClient = null;

        struct ClientParamRRC
        {
            public string strIPAddress;
            public byte bAddressPC;
            public StateIndicate stateIndicateReadClient;
            public StateIndicate stateIndicateWriteClient;
            public StateConnection stateIndicateConnectionClient;

        }

        ClientParamRRC[] clientParamRRC = null;

        byte COUNT_RRD = 6;
        
        
        byte bAdrReceiver = 1;

        IExBRZ_GRZ[] iExBRZ_GRZ = null;

        MaskedTextBox[] mtbAddressIP = null;
        
        ShowLog showLog = null;

        ShowLog showLogClient = null;

        InitComEth initComEth = null;

        



        TRangeSector[] tRangeSectorR = null;
        TRangeSector[] tRangeSectorS = null;

        TSupprFWS[] tSupprFWS = null;
        TSupprFHSS[] tSupprFHSS = null;



        //************** DELEGATE **************//
        


        public TestInformExchangeBRZ()
        {
            InitializeComponent();

            InitParamVLC();

            InitIndicate();

            InitParamQuery();

            InitParamRegime();

            InitParamBearing();

            InitParamRangeSpec();

            InitParamSuppressFWS(cmbTypeModulationFWS);

            InitParamSuppressFWS(cmbTypeModulation);

            InitParamCodeFFT(cmbStepSupFHSS);


            InitConnection();

        }

        private void InitIndicate()
        {
            stateIndicateRead = new StateIndicate[COUNT_CNT];
            stateIndicateWrite = new StateIndicate[COUNT_CNT];
            stateIndicateConnection = new StateConnection[COUNT_CNT];

            if (stateIndicateRead != null && stateIndicateWrite != null && stateIndicateConnection != null)
            {
                for (int i = 0; i < COUNT_CNT; i++)
                {
                    stateIndicateRead[i] = new StateIndicate(pbReadByte[i], imgReadByte, imgWaitByte, TIME_INDICATE);
                    stateIndicateWrite[i] = new StateIndicate(pbWriteByte[i], imgWriteByte, imgWaitByte, TIME_INDICATE);
                    stateIndicateConnection[i] = new StateConnection(bConnect[i], clConnect, clDisconnect);

                    stateIndicateRead[i].SetIndicateOn();
                    stateIndicateWrite[i].SetIndicateOn();
                }                    
            }

            clientParamRRC = new ClientParamRRC[COUNT_CLIENT];

           

            if (clientParamRRC != null)
            {
                for (int i = 0; i < COUNT_CLIENT; i++)
                {
                    try
                    {
                        clientParamRRC[i].strIPAddress = string.Empty;
                        clientParamRRC[i].bAddressPC = 0;

                        clientParamRRC[i].stateIndicateReadClient = new StateIndicate(pbReadByteClient[i], imgReadByte, imgWaitByte, TIME_INDICATE);
                        clientParamRRC[i].stateIndicateWriteClient = new StateIndicate(pbWriteByteClient[i], imgWriteByte, imgWaitByte, TIME_INDICATE);
                        clientParamRRC[i].stateIndicateConnectionClient = new StateConnection(bConnectClient[i], clConnect, clDisconnect);

                        clientParamRRC[i].stateIndicateReadClient.SetIndicateOn();
                        clientParamRRC[i].stateIndicateWriteClient.SetIndicateOn();
                    }
                    catch (SystemException)
                    {}
                }
            }

            stateIndicateConnectionDB = new StateConnection(btnConnectDB, clConnect, clDisconnect);
            stateIndicateConnectionServer = new StateConnection(btnConnectServer, clConnect, clDisconnect);
        }

        private void ShowPanelClient(byte bTypeClient)
        {
            switch (bTypeClient)
            {
                case 0:
                    for (int i = 0; i < COUNT_RRD; i++)
                    {
                        initComEth.InitPort(cmbCom[i]);
                        cmbCom[i].Left = 123;
                        cmbCom[i].Visible = true;
                        mtbAddressIP[i].Visible = false;
                    }
                        break;

                case 1:
                        for (int i = 0; i < COUNT_RRD; i++)
                        {                           
                            cmbCom[i].Visible = false;
                            mtbAddressIP[i].Left = 123;
                            mtbAddressIP[i].Visible = true;
                        }
                        break;


            }
        }


        private void InitParamVLC()
        {

            bTypeConnection = new byte[COUNT_CNT];

            bTypeConnection[0] = CntRRC;
            bTypeConnection[1] = Cnt3G;
            bTypeConnection[2] = CntModem;
            bTypeConnection[3] = CntHF;
            bTypeConnection[4] = CntVHF;


            bConnect = new Button[COUNT_CNT];
            pbReadByte = new PictureBox[COUNT_CNT];
            pbWriteByte = new PictureBox[COUNT_CNT];
            chbRRD = new CheckBox[COUNT_CNT];
            rtbLog = new RichTextBox[COUNT_CNT];
            
            cmbCom = new ComboBox[COUNT_RS232];


            bConnectClient = new Button[COUNT_RRD];
            pbReadByteClient = new PictureBox[COUNT_RRD];
            pbWriteByteClient = new PictureBox[COUNT_RRD];

            mtbAddressIP = new MaskedTextBox[COUNT_ETH];

            bConnect[0] = bConnectRRC;
            bConnect[1] = bConnect3G;
            bConnect[2] = bConnectModem;
            bConnect[3] = bConnectHF;
            bConnect[4] = bConnectVHF;


            pbReadByte[0] = pbReadByteRRC;
            pbReadByte[1] = pbReadByte3G;
            pbReadByte[2] = pbReadByteModem;
            pbReadByte[3] = pbReadByteHF;
            pbReadByte[4] = pbReadByteVHF;

            pbWriteByte[0] = pbWriteByteRRC;
            pbWriteByte[1] = pbWriteByte3G;
            pbWriteByte[2] = pbWriteByteModem;
            pbWriteByte[3] = pbWriteByteHF;
            pbWriteByte[4] = pbWriteByteVHF;
            
            


            cmbCom[0] = cmbComModem;
            cmbCom[1] = cmbComHF;
            cmbCom[2] = cmbComVHF;


            bConnectClient[0] = bConnectClientRRC1;
            bConnectClient[1] = bConnectClientRRC2;
            bConnectClient[2] = bConnectClientRRC3;
            bConnectClient[3] = bConnectClientRRC4;
            bConnectClient[4] = bConnectClientRRC5;
            bConnectClient[5] = bConnectClientRRC6;

            pbReadByteClient[0] = pbReadByteClientRRC1;
            pbReadByteClient[1] = pbReadByteClientRRC2;
            pbReadByteClient[2] = pbReadByteClientRRC3;
            pbReadByteClient[3] = pbReadByteClientRRC4;
            pbReadByteClient[4] = pbReadByteClientRRC5;
            pbReadByteClient[5] = pbReadByteClientRRC6;

            pbWriteByteClient[0] = pbWriteByteClientRRC1;
            pbWriteByteClient[1] = pbWriteByteClientRRC2;
            pbWriteByteClient[2] = pbWriteByteClientRRC3;
            pbWriteByteClient[3] = pbWriteByteClientRRC4;
            pbWriteByteClient[4] = pbWriteByteClientRRC5;
            pbWriteByteClient[5] = pbWriteByteClientRRC6; 
            

          /*  mtbAddressIP[0] = mtbAddressIPRRD1;
            mtbAddressIP[1] = mtbAddressIPRRD2;
            mtbAddressIP[2] = mtbAddressIPRRD3;
            mtbAddressIP[3] = mtbAddressIPRRD4;
            mtbAddressIP[4] = mtbAddressIPRRD5;
            mtbAddressIP[5] = mtbAddressIPRRD6;*/


            rtbLog[0] = rtbLogRRC;
            rtbLog[1] = rtbLog3G;
            rtbLog[2] = rtbLogModem;
            rtbLog[3] = rtbLogHF;
            rtbLog[4] = rtbLogVHF;
           

            chbRRD[0] = chbRRC;
            chbRRD[1] = chb3G;
            chbRRD[2] = chbModem;
            chbRRD[3] = chbHF;
            chbRRD[4] = chbVHF;
          

            iExBRZ_GRZ = null;
            iExBRZ_GRZ = new IExBRZ_GRZ[COUNT_RRD];

            for (int i = 0; i < COUNT_RRD; i++)
                iExBRZ_GRZ[i] = new IExBRZ_GRZ(bAdrReceiver);

            showLog = new ShowLog();
            showLogClient = new ShowLog();

            initComEth = new InitComEth();

            for (int i = 0; i < COUNT_RS232; i++)
                initComEth.InitPort(cmbCom[i]);



            TestClass.OnChangeRegime += new TestClass.ChangeRegimeEventHandler(ChangeRegimeEventHandlerTest);
        }


        public void ChangeRegimeEventHandlerTest()
        {
            int f = 7;
        }
        

        private void InitParamQuery()
        {
            cmbTypeRequest.Items.Clear();
            cmbTypeRequest.Items.Add("Запрос состояния");
            cmbTypeRequest.Items.Add("Запрос координат");
            cmbTypeRequest.Items.Add("Запрос ИРИ ФРЧ");
            cmbTypeRequest.Items.Add("Запрос ИРИ ППРЧ");
            cmbTypeRequest.Items.Add("Запрос ИРИ ФРЧ РП");
            cmbTypeRequest.Items.Add("Запрос ИРИ ППРЧ РП");
            cmbTypeRequest.SelectedIndex = 0;

        }

        private void InitParamRegime()
        {
            cmbRegime.Items.Clear();
            cmbRegime.Items.Add("Подготовка");
            cmbRegime.Items.Add("Радиоразведка");
            cmbRegime.Items.Add("Радиоразведка с пеленгованием");
            cmbRegime.Items.Add("Радиоразведка ППРЧ");
            cmbRegime.Items.Add("Радиоподавление ФРЧ");
            cmbRegime.Items.Add("Радиоподавление ППРЧ");
            cmbRegime.SelectedIndex = 0;

        }

        private void InitParamBearing()
        {
            cmbTypeReqBearing.Items.Clear();
            cmbTypeReqBearing.Items.Add("Исполнительное");
            cmbTypeReqBearing.Items.Add("Квазиодновременное");
            cmbTypeReqBearing.SelectedIndex = 0;
        }

        private void InitParamRangeSpec()
        {
            cmbActForbid.Items.Clear();
            cmbActForbid.Items.Add("Запрещенные");
            cmbActForbid.Items.Add("Известные");
            cmbActForbid.Items.Add("Важные");
            cmbActForbid.SelectedIndex = 0;

        }

        private void InitParamSuppressFWS(ComboBox comboBox)
        {
            // вид модуляции 
            comboBox.Items.Clear();
            //comboBox.Items.Add("АМ");
            //comboBox.Items.Add("ЧМ");
            //comboBox.Items.Add("ЧМ-2");
            //comboBox.Items.Add("НБП");
            //comboBox.Items.Add("ВБП");
            //comboBox.Items.Add("ШПС");
            //comboBox.Items.Add("Несущая");
            //comboBox.Items.Add("Неопр.");
            //comboBox.Items.Add("АМ-2");
            //comboBox.Items.Add("ФМ-2");
            //comboBox.Items.Add("ФМ-4");
            //comboBox.Items.Add("ФМ-8");
            //comboBox.Items.Add("КАМ-16");
            //comboBox.Items.Add("КАМ-32");
            //comboBox.Items.Add("КАМ-64");
            //comboBox.Items.Add("КАМ-128");
            //comboBox.Items.Add("ППРЧ");
            comboBox.Items.Add("ЧМШ");
            comboBox.Items.Add("ЧМШ скан f.");
            comboBox.Items.Add("ЧМн-2");
            comboBox.Items.Add("ЧМн-4");
            comboBox.Items.Add("ЧМн-8");            
            comboBox.Items.Add("ФМн-2");
            comboBox.Items.Add("ФМн-4");
            comboBox.Items.Add("ФМн-8");
            comboBox.Items.Add("ЗШ");
            comboBox.Items.Add("АФМн");
            comboBox.Items.Add("ЧМ");
            comboBox.SelectedIndex = 0;
        }

        private void InitParamCodeFFT(ComboBox comboBox)
        {
            // шаг 
            comboBox.Items.Clear();
            comboBox.Items.Add("32768");
            comboBox.Items.Add("16384");
            comboBox.Items.Add("8192");
            comboBox.Items.Add("4096");
            cmbStepSupFHSS.SelectedIndex = 0;
        }

        

        private void SetDeviation(ComboBox comboBox, int iTypeModulation, int iInd, System.Windows.Forms.ComboBox cmbDeviation, Label deviationLabel)
        {
            comboBox.Items.Clear();

            switch (iTypeModulation)
            {
                case 0:
                case 1:
                case 10:
                    deviationLabel.Text = "Ширина спектра,кГц............................";
                    comboBox.Enabled = true;
                    comboBox.Items.Add("0,5");
                    comboBox.Items.Add("1");
                    comboBox.Items.Add("1,5");
                    comboBox.Items.Add("2");
                    comboBox.Items.Add("3,4");
                    comboBox.Items.Add("4");
                    comboBox.Items.Add("8");
                    comboBox.Items.Add("16");
                    comboBox.SelectedIndex = iInd;
                    break;

                case 2:
                case 3:
                case 4:
                    deviationLabel.Text = "Разнос по частоте,Гц............................";
                    comboBox.Enabled = true;
                    for (int i = 1; i <= 100; i++)
                    {
                        comboBox.Items.Add(i*10);
                    }
                    comboBox.SelectedIndex = iInd;
                    break;

                default:
                    deviationLabel.Text = "Девиация(+/-),кГц............................";
                    comboBox.Enabled = false;
                    break;
            }
        }

        private void SetManipulation(ComboBox comboBox, int iTypeModulation, int iInd, System.Windows.Forms.ComboBox cmbManipulation, Label manipulationLabel)
        {
            comboBox.Items.Clear();
            switch (iTypeModulation)
            {
                case 2:
                case 3:
                case 4:
                    manipulationLabel.Text = "Частота манипуляции,Гц......................................";
                    comboBox.Enabled = true;
                    comboBox.Items.Add("2000");
                    comboBox.Items.Add("1000");
                    comboBox.Items.Add("500");
                    comboBox.Items.Add("200");
                    comboBox.Items.Add("100");
                    comboBox.Items.Add("50");
                    comboBox.Items.Add("20");
                    comboBox.Items.Add("10");
                    comboBox.Items.Add("5");
                    comboBox.SelectedIndex = iInd;
                    break;

                case 5:
                case 6:
                case 7:
                    manipulationLabel.Text = "Частота манипуляции,Гц......................................";
                    comboBox.Enabled = true;
                    comboBox.Items.Add("100000");
                    comboBox.Items.Add("50000");
                    comboBox.Items.Add("20000");
                    comboBox.Items.Add("10000");
                    comboBox.Items.Add("5000");
                    comboBox.Items.Add("2400");
                    comboBox.Items.Add("2000");
                    comboBox.Items.Add("1200");
                    comboBox.Items.Add("1000");
                    comboBox.Items.Add("500");
                    comboBox.Items.Add("250");
                    comboBox.Items.Add("125");
                    comboBox.SelectedIndex = iInd;
                    break;
                case 8:
                    manipulationLabel.Text = "Ширина спектра,кГц......................................";
                    comboBox.Enabled = true;
                    comboBox.Items.Add("2500");
                    comboBox.Items.Add("1000");
                    comboBox.Items.Add("500");
                    comboBox.Items.Add("200");

                    comboBox.SelectedIndex = iInd;
                    break;
                case 9:
                    manipulationLabel.Text = "Частота манипуляции,Гц......................................";
                    comboBox.Enabled = true;
                    comboBox.Items.Add("100000");
                    comboBox.Items.Add("50000");
                    comboBox.Items.Add("20000");
                    comboBox.Items.Add("10000");
                    comboBox.Items.Add("5000");
                    comboBox.Items.Add("2000");
                    comboBox.Items.Add("1000");
                    comboBox.Items.Add("500");
                    comboBox.Items.Add("200");
                    comboBox.SelectedIndex = iInd;
                    break;

                default:
                    manipulationLabel.Text = "Частота манипуляции,Гц......................................";
                    comboBox.Enabled = false;
                    break;
            }

        }

       

        private void InitConnection()
        {
            try 
            {
                string strLocalHost = Dns.GetHostName();
                var allAdr = System.Net.Dns.GetHostByName(strLocalHost).AddressList;
                //System.Net.IPAddress ip = System.Net.Dns.GetHostByName(strLocalHost).AddressList[0];
                //string strIP = ip.ToString();
                cmbxServerIP.Enabled = true;
                foreach (var adr in allAdr)
                {
                    cmbxServerIP.Items.Add(adr.ToString());
                }
                cmbxServerIP.SelectedIndex = 0;
            }
            catch { }

            objIEPC = new IEPC();

            if (objIEPC != null)
            {
                try
                {

                    objIEPC.OnCreateServer += new IEPC.CreateServerEventHandler(CreateServer);
                    objIEPC.OnDestroyServer += new IEPC.CreateServerEventHandler(DestroyServer);

                    objIEPC.OnConnectClient += new IEPC.ConnectClientEventHandler(ConnectClient);                    
                    objIEPC.OnDisconnectClient += new IEPC.ConnectClientEventHandler(DisconnectClient);


                    objIEPC.OnOpenPort += new IEPC.ConnectEventHandler(OpenPort);
                    objIEPC.OnClosePort += new IEPC.ConnectEventHandler(ClosePort);

                    objIEPC.OnReadByte += new IEPC.ByteEventHandler(ReadByteCom);
                    objIEPC.OnWriteByte += new IEPC.ByteEventHandler(WriteByteCom);

                    objIEPC.OnReadByteEth += new IEPC.ByteEthEventHandler(ReadByteEth);
                    objIEPC.OnWriteByteEth += new IEPC.ByteEthEventHandler(WriteByteEth);

                    objIEPC.OnTextCmd += new IEPC.CmdTextEventHandler(Receive_Text);

                    objIEPC.OnConfirmTextCmd += new IEPC.ConfirmTextEventHandler(Receive_ConfirmText);
                    objIEPC.OnConfirmSynchTime += new IEPC.ConfirmSynchTimeEventHandler(Receive_ConfirmSynchTime);
                    objIEPC.OnConfirmRegime += new IEPC.ConfirmRegimeEventHandler(Receive_ConfirmRegime);
                    objIEPC.OnConfirmRangeSector += new IEPC.ConfirmRangeSectorEventHandler(Receive_ConfirmRangeSector);
                    objIEPC.OnConfirmRangeSpec += new IEPC.ConfirmRangeSpecEventHandler(Receive_ConfirmRangeSpec);
                    objIEPC.OnConfirmSupprFWS += new IEPC.ConfirmSupprFWSEventHandler(Receive_ConfirmSupprFWS);
                    objIEPC.OnConfirmSupprFHSS += new IEPC.ConfirmSupprFHSSEventHandler(Receive_ConfirmSupprFHSS);
                    objIEPC.OnConfirmState += new IEPC.ConfirmStateEventHandler(Receive_ConfirmState);
                    objIEPC.OnConfirmCoord += new IEPC.ConfirmCoordEventHandler(Receive_ConfirmCoord);
                    objIEPC.OnConfirmReconFWS += new IEPC.ConfirmReconFWSEventHandler(Receive_ConfirmReconFWS);
                    objIEPC.OnConfirmReconFHSS += new IEPC.ConfirmReconFHSSEventHandler(Receive_ConfirmReconFHSS);
                    objIEPC.OnConfirmExecBear += new IEPC.ConfirmExecBearEventHandler(Receive_ConfirmExecBear);
                    objIEPC.OnConfirmSimulBear += new IEPC.ConfirmSimulBearEventHandler(Receive_ConfirmSimulBear);
                    objIEPC.OnConfirmStateSupprFWS += new IEPC.ConfirmStateSupprFWSEventHandler(Receive_ConfirmStateSupprFWS);
                    objIEPC.OnConfirmStateSupprFHSS += new IEPC.ConfirmStateSupprFHSSEventHandler(Receive_ConfirmStateSupprFHSS);
                }
                catch (SystemException)
                { }
            }

        }

        #region Receive Event

        private void Connect(object sender)
        {
            for (int i = 0; i < COUNT_RRD; i++)
            {
                if (sender == iExBRZ_GRZ[i])
                    stateIndicateConnection[i].SetConnectOn();
            }                
        }

        private void Disconnect(object sender)
        {
            for (int i = 0; i < COUNT_RRD; i++)
            {
                if (sender == iExBRZ_GRZ[i])
                    stateIndicateConnection[i].SetConnectOff();
            }
            
        }

        private void OpenPort(byte bTypeCnt)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    stateIndicateConnection[i].SetConnectOn();

                    i = COUNT_CNT;
                }
                i++;
            }                                 
        }

        private void CreateServer(byte bTypeCnt)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    stateIndicateConnection[i].SetConnectOn();

                    i = COUNT_CNT;
                }
                i++;
            }                                 
        }


        private void DestroyServer(byte bTypeCnt)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    stateIndicateConnection[i].SetConnectOff();

                    i = COUNT_CNT;
                }
                i++;
            }                                 
        }


        private void ConnectClient(byte bTypeCnt, string strIPAddress, byte bAddressPC)
        {
            if (bTypeCnt == CntRRC)
            {
                int i = 0;
                while (i < COUNT_RRD)
                {
                    if (clientParamRRC[i].strIPAddress == string.Empty)
                    {
                        clientParamRRC[i].strIPAddress = strIPAddress;
                        clientParamRRC[i].bAddressPC = bAddressPC;
                        clientParamRRC[i].stateIndicateConnectionClient.SetConnectOn();

                        showLogClient.ShowMessage(rtbClientConnect, 0, strIPAddress.ToString() + " Клиент " + bAddressPC.ToString(), "", "");

                        i = COUNT_RRD;
                    }
                    i++;
                }                              
            }

            /*int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    stateIndicateConnectionClient[i].SetConnectOn();

                    i = COUNT_CNT;
                }
                i++;
            } */                             
        }

        private void DisconnectClient(byte bTypeCnt, string strIPAddress, byte bAddressPC)
        {
            if (bTypeCnt == CntRRC)
            {
                int i = 0;
                while (i < COUNT_RRD)
                {
                    if (clientParamRRC[i].strIPAddress == strIPAddress)
                    {

                        showLogClient.ShowMessage(rtbClientConnect, 1, strIPAddress.ToString() + " Клиент " + bAddressPC.ToString(), "", "");

                        clientParamRRC[i].strIPAddress = string.Empty;
                        clientParamRRC[i].bAddressPC = 0;
                        clientParamRRC[i].stateIndicateConnectionClient.SetConnectOff();

                        


                        i = COUNT_RRD;
                    }
                    i++;
                }
            }

        }
        
        


        
        private void ClosePort(byte bTypeCnt)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    stateIndicateConnection[i].SetConnectOff();

                    i = COUNT_CNT;
                }
                i++;
            }           
        }

        private void ReadByteCom(byte bTypeCnt, byte[] bData)
        {
            int i = 0;
            while ( i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    stateIndicateWrite[i].SetIndicateOn();

                    if (chbByteHex.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, showLog.DecodeHexByte(bData), "", "");

                    i = COUNT_CNT;
                }
                i++;
            }                                  
        }

        private void WriteByteCom(byte bTypeCnt,  byte[] bData)
        {

            int i = 0;
            while ( i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    stateIndicateWrite[i].SetIndicateOn();

                    if (chbByteHex.Checked)
                        showLog.ShowMessage(rtbLog[i], 1, showLog.DecodeHexByte(bData), "", "");

                    i = COUNT_CNT;
                }
                i++;
            }         
                            
        }

        private void ReadByteEth(byte bTypeCnt, string strIPAddress, byte[] bData)
        {
            if (bTypeCnt == CntRRC)
            {
                int i = 0;
                while (i < COUNT_RRD)
                {
                    if (clientParamRRC[i].strIPAddress == strIPAddress)
                    {
                        clientParamRRC[i].stateIndicateReadClient.SetIndicateOn();

                        if (chbByteHex.Checked)
                            showLog.ShowMessage(rtbLog[0], 0, showLog.DecodeHexByte(bData), "", "");

                        i = COUNT_RRD;
                    }
                    i++;
                }
            }
        }

        private void WriteByteEth(byte bTypeCnt, string strIPAddress, byte[] bData)
        {
            if (bTypeCnt == CntRRC)
            {
                int i = 0;
                while (i < COUNT_RRD)
                {
                    if (clientParamRRC[i].strIPAddress == strIPAddress)
                    {
                        clientParamRRC[i].stateIndicateWriteClient.SetIndicateOn();

                        if (chbByteHex.Checked)
                            showLog.ShowMessage(rtbLog[0], 1, showLog.DecodeHexByte(bData), "", "");

                        i = COUNT_RRD;
                    }
                    i++;
                }
            }

        }




        private void Receive_Text(byte bTypeCnt, byte bAddressSend, string strText)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_Text + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked)
                        strDetail = strText;

                    if (chbName.Checked || chbDetail.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                    
                    i = COUNT_CNT;
                }
                i++;
            }             
        }

        private void Receive_ConfirmText(byte bTypeCnt, byte bAddressSend, byte bCodeError)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {

                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_ConfirmText + " ( Адр " + bAddressSend.ToString() + ")\n";


                    string strDetail = "";
                    if (chbDetail.Checked == true)
                        strDetail = "Код ошибки " + bCodeError.ToString();

                    if (chbName.Checked || chbDetail.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }          
        }

        private void Receive_ConfirmSynchTime(byte bTypeCnt, byte bAddressSend, byte bCodeError, TTimeMy tTime)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_SynchTime + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "\nКод ошибки " + bCodeError.ToString() + "\n";
                        strDetail += "Время " + tTime.bHour.ToString() + ":" + tTime.bMinute.ToString() + ":" + tTime.bSecond.ToString() + "\n";
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }         
        }

        private void Receive_ConfirmRegime(byte bTypeCnt, byte bAddressSend, byte bCodeError)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_Regime + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "\nКод ошибки " + bCodeError.ToString() + "\n";                       
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }         
        }

        private void Receive_ConfirmRangeSector(byte bTypeCnt, byte bAddressSend, byte bCodeError, byte bTypeRange)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_RangeSector + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "\nКод ошибки " + bCodeError.ToString() + "\n";
                        strDetail += "\nТип " + bTypeRange.ToString() + "\n";
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }        
        }

        private void Receive_ConfirmRangeSpec(byte bTypeCnt, byte bAddressSend, byte bCodeError, byte bTypeSpec)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_RangeSpec + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "\nКод ошибки " + bCodeError.ToString() + "\n";
                        strDetail += "\nТип " + bTypeSpec.ToString() + "\n";
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }        
        }

        private void Receive_ConfirmSupprFWS(byte bTypeCnt, byte bAddressSend, byte bCodeError)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_SupprFWS + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "\nКод ошибки " + bCodeError.ToString() + "\n";
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }        
        }

        private void Receive_ConfirmSupprFHSS(byte bTypeCnt, byte bAddressSend, byte bCodeError)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_SupprFHSS + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "\nКод ошибки " + bCodeError.ToString() + "\n";
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }        
        }

        private void Receive_ConfirmState(byte bTypeCnt, byte bAddressSend, byte bCodeError, short sNum, byte bType, byte bRole, byte bRegime, byte[] bLetter)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_State + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                        switch (bRegime)
                        {
                            case 0:
                                strDetail += "Режим Подготовка  \n";
                                break;

                            case 1:
                                strDetail += "Режим Радиоразведка  \n";
                                break;

                            case 2:
                                strDetail += "Режим Радиоразведка с пеленгованием \n";
                                break;

                            case 3:
                                strDetail += "Режим Радиоразведка ППРЧ \n";
                                break;
                            case 4:
                                strDetail += "Режим Радиоподавление ФРЧ \n";
                                break;

                            case 5:
                                strDetail += "Режим Радиоподавление ППРЧ  \n";
                                break;

                            default:
                                break;
                        }

                        strDetail += "Номер  " + sNum.ToString() + "\n";

                        switch (bType)
                        {
                            case 0:
                                strDetail += "Тип Р-327  \n";
                                
                                break;

                            case 1:
                                strDetail += "Тип Гроза  \n";
                                
                                break;

                            case 2:
                                strDetail += "Тип Гроза-6 Апп1  \n";
                                
                                break;

                            case 3:
                                strDetail += "Тип Гроза-6 Апп2  \n";
                                
                                break;

                            case 4:
                                strDetail += "Тип Пурга  \n";
                                
                                break;

                            case 5:
                                strDetail += "Тип Р-325Б   \n";
                                
                                break;

                            case 6:
                                strDetail += "Тип Гриф   \n";
                                break;
                            case 7:
                                strDetail += "Тип Журавль   \n";
                                break;

                            default:
                                break;
                        }

                        switch (bRole)
                        {
                            case 0:
                                strDetail += "Роль Автономная  \n";
                                break;

                            case 1:
                                strDetail += "Роль Ведущая  \n";
                                break;

                            case 2:
                                strDetail += "Роль Ведомая  \n";
                                break;

                            default:
                                break;
                        }

                        if (bLetter != null)
                        {
                            for (int j = 0; j < bLetter.Length; j++)
                            {
                                strDetail += "Литера " + (j + 1).ToString() + " " + bLetter[j].ToString() + "\n";

                            }
                        }
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }      
        }

        private void Receive_ConfirmCoord(byte bTypeCnt, byte bAddressSend, byte bCodeError, TCoord tCoord)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_Coord + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "\nКод ошибки " + bCodeError.ToString() + "\n";
                        if (tCoord.bSignLatitude == 0)
                            strDetail += "Широта (З) ";
                        else
                            strDetail += "Широта (В) ";
                        strDetail += tCoord.bDegreeLatitude.ToString() + "  " + tCoord.bMinuteLatitude.ToString() + "  " + tCoord.bSecondLatitude.ToString() + "\n";

                        if (tCoord.bSignLongitude == 0)
                            strDetail += "Долгота (С) ";
                        else
                            strDetail += "Долгота (Ю) ";
                        strDetail += tCoord.bDegreeLongitude.ToString() + "  " + tCoord.bMinuteLongitude.ToString() + "  " + tCoord.bSecondLongitude.ToString() + "\n";
                        
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }      
        }

        private void Receive_ConfirmReconFWS(byte bTypeCnt, byte bAddressSend, byte bCodeError, TReconFWS[] tReconFWS)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    try
                    {
                        string strName = "";
                        if (chbName.Checked)
                            strName = CMD_ReconFWS + " ( Адр " + bAddressSend.ToString() + ")\n";

                        string strDetail = "";

                        if (chbDetail.Checked == true)
                        {
                            strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                            if (tReconFWS != null)
                            {
                                for (int j = 0; j < tReconFWS.Length; j++)
                                {
                                    strDetail += (j + 1).ToString() + "\n";
                                    strDetail += "  ID " + tReconFWS[j].iID.ToString() + "\n";
                                    strDetail += "  Частота " + tReconFWS[j].iFreq.ToString("#.#") + "\n";
                                    strDetail += "  Время " + tReconFWS[j].tTime.bHour.ToString() + ":" + tReconFWS[j].tTime.bMinute.ToString() + ":" + tReconFWS[j].tTime.bSecond.ToString() + "\n";
                                    strDetail += "  Пеленг ведущая " + tReconFWS[j].sBearingOwn.ToString() + "\n";
                                    strDetail += "  Пеленг ведомая " + tReconFWS[j].sBearingLinked.ToString() + "\n";

                                    if (tReconFWS[j].tCoord.bSignLatitude == 0)
                                        strDetail += "  Широта (З) ";
                                    else
                                        strDetail += "  Широта (В) ";
                                    strDetail += tReconFWS[j].tCoord.bDegreeLatitude.ToString() + "  " + tReconFWS[j].tCoord.bMinuteLatitude.ToString() + "  " + tReconFWS[j].tCoord.bSecondLatitude.ToString() + "\n";

                                    if (tReconFWS[j].tCoord.bSignLongitude == 0)
                                        strDetail += "  Долгота (С) ";
                                    else
                                        strDetail += "  Долгота (Ю) ";
                                    strDetail += tReconFWS[j].tCoord.bDegreeLongitude.ToString() + "  " + tReconFWS[j].tCoord.bMinuteLongitude.ToString() + "  " + tReconFWS[j].tCoord.bSecondLongitude.ToString() + "\n";

                                    strDetail += "  Ур. сигнала ведущая -" + tReconFWS[j].bLevelOwn.ToString() + "\n";
                                    strDetail += "  Ур. сигнала ведомая -" + tReconFWS[j].bLevelLinked.ToString() + "\n";
                                    strDetail += "  Ширина полосы " + tReconFWS[j].bBandWidth.ToString() + "\n";

                                    strDetail += "  Вид модуляции " + tReconFWS[j].bModulation.ToString() + "\n";

                                    //strDetail += "  Наличие аудиозаписи " + tReconFWS[j].bSignAudio.ToString() + "\n";

                                }
                            }
                        }

                        if (chbName.Checked)
                            showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                    }
                    catch (SystemException)
                    { }

                    i = COUNT_CNT;
                }
                i++;
            }      
            
            
        }

        private void Receive_ConfirmReconFHSS(byte bTypeCnt, byte bAddressSend, byte bCodeError, TReconFHSS[] tReconFHSS)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    try
                    {
                        string strName = "";
                        if (chbName.Checked)
                            strName = CMD_ReconFHSS + " ( Адр " + bAddressSend.ToString() + ")\n";

                        string strDetail = "";

                        if (chbDetail.Checked == true)
                        {
                            strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                            if (tReconFHSS != null)
                            {
                                for (int j = 0; j < tReconFHSS.Length; j++)
                                {
                                    strDetail += (j + 1).ToString() + "\n";
                                    strDetail += "  ID " + tReconFHSS[j].iID.ToString() + "\n";
                                    strDetail += "  Частота мин " + tReconFHSS[j].iFreqMin.ToString("#.#") + "\n";
                                    strDetail += "  Частота макс " + tReconFHSS[j].iFreqMax.ToString("#.#") + "\n";

                                    strDetail += "  Шаг сетки " + tReconFHSS[j].sStep.ToString() + "\n";
                                    strDetail += "  Длительность импульса " + tReconFHSS[j].iDuration.ToString() + "\n";

                                    strDetail += "  Время " + tReconFHSS[j].bMinute.ToString() + ":" + tReconFHSS[j].bSecond.ToString() + "\n";
                                    strDetail += "  Пеленг ведущая " + tReconFHSS[j].sBearingOwn.ToString() + "\n";
                                    strDetail += "  Пеленг ведомая " + tReconFHSS[j].sBearingLinked.ToString() + "\n";

                                    if (tReconFHSS[j].tCoord.bSignLatitude == 0)
                                        strDetail += "  Широта (З) ";
                                    else
                                        strDetail += "  Широта (В) ";
                                    strDetail += tReconFHSS[j].tCoord.bDegreeLatitude.ToString() + "  " + tReconFHSS[j].tCoord.bMinuteLatitude.ToString() + "  " + tReconFHSS[j].tCoord.bSecondLatitude.ToString() + "\n";

                                    if (tReconFHSS[j].tCoord.bSignLongitude == 0)
                                        strDetail += "  Долгота (С) ";
                                    else
                                        strDetail += "  Долгота (Ю) ";
                                    strDetail += tReconFHSS[j].tCoord.bDegreeLongitude.ToString() + "  " + tReconFHSS[j].tCoord.bMinuteLongitude.ToString() + "  " + tReconFHSS[j].tCoord.bSecondLongitude.ToString() + "\n";

                                    strDetail += "  Ур. сигнала ведущая " + tReconFHSS[j].bLevelOwn.ToString() + "\n";
                                    strDetail += "  Ур. сигнала ведомая " + tReconFHSS[j].bLevelLinked.ToString() + "\n";
                                    strDetail += "  Ширина полосы " + tReconFHSS[j].bBandWidth.ToString() + "\n";

                                    strDetail += "  Вид модуляции " + tReconFHSS[j].bModulation.ToString() + "\n";

                                    strDetail += "  Наличие аудиозаписи " + tReconFHSS[j].bSignAudio.ToString() + "\n";

                                }
                            }
                        }

                        if (chbName.Checked)
                            showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);
                    }
                    catch (SystemException)
                    { }

                    i = COUNT_CNT;
                }
                i++;
            }      
            
        }

        private void Receive_ConfirmExecBear(byte bTypeCnt, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_ExecBear + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                        strDetail += "ID " + iID.ToString() + "\n";
                        strDetail += "Частота " + Convert.ToDouble((double)iFreq).ToString() + "\n";
                        strDetail += "Пеленг " + sOwnBearing.ToString() + "\n";
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    if (cbxIsMaster.Checked) 
                        SaveToDbFFExecutingBearing(sOwnBearing);
                    i = COUNT_CNT;
                }
                i++;
            }      
        }

        private void Receive_ConfirmSimulBear(byte bTypeCnt, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing, short sLinkedBearing)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_SimulBear + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "Код ошибки  " + bCodeError.ToString() + "\n";

                        strDetail += "ID " + iID.ToString() + "\n";
                        strDetail += "Частота " + Convert.ToDouble((double)iFreq / 10).ToString() + "\n";
                        strDetail += "Пеленг ведущая " + sOwnBearing.ToString() + "\n";
                        strDetail += "Пеленг ведомая " + sLinkedBearing.ToString() + "\n";
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }      
        }

        private void Receive_ConfirmStateSupprFWS(byte bTypeCnt, byte bAddressSend, byte bCodeError, TResSupprFWS[] tResSupprFWS)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_StateSupprFWS + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "\nКод ошибки " + bCodeError.ToString() + "\n";


                        if (tResSupprFWS != null)
                        {
                            for (int j = 0; j < tResSupprFWS.Length; j++)
                            {
                                strDetail += (j + 1).ToString() + "\n";
                                strDetail += "  ID " + tResSupprFWS[j].iID.ToString() + "\n";
                                strDetail += "  Частота " +(tResSupprFWS[j].iFreq / 10).ToString("#.#") + "\n";
                                strDetail += "  Контроль " + tResSupprFWS[j].bResCtrl.ToString() + "\n";
                                strDetail += "  Подавление " + tResSupprFWS[j].bResSuppr.ToString() + "\n";
                            }   
                        }
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }      
        }

        private void Receive_ConfirmStateSupprFHSS(byte bTypeCnt, byte bAddressSend, byte bCodeError,  TResSupprFHSS[] tResSupprFHSS)
        {
            int i = 0;
            while (i < COUNT_CNT)
            {
                if (bTypeCnt == bTypeConnection[i])
                {
                    string strName = "";
                    if (chbName.Checked)
                        strName = CMD_StateSupprFHSS + " ( Адр " + bAddressSend.ToString() + ")\n";

                    string strDetail = "";
                    if (chbDetail.Checked == true)
                    {
                        strDetail = "\nКод ошибки " + bCodeError.ToString() + "\n";


                        if (tResSupprFHSS != null)
                        {
                            for (int j = 0; j < tResSupprFHSS.Length; j++)
                            {
                                strDetail += (i + 1).ToString() + "\n";
                                strDetail += "  ID " + tResSupprFHSS[j].iID.ToString() + "\n";
                                strDetail += "  Частота мин "  + (tResSupprFHSS[j].iFreqMin / 10).ToString("#.#") + "\n";
                                strDetail += "  Частота макс " + (tResSupprFHSS[j].iFreqMax / 10).ToString("#.#") + "\n";
                                strDetail += "  Контроль " + tResSupprFHSS[j].bResCtrl.ToString() + "\n";
                                strDetail += "  Подавление " + tResSupprFHSS[j].bResSuppr.ToString() + "\n";
                            }
                        }
                    }

                    if (chbName.Checked)
                        showLog.ShowMessage(rtbLog[i], 0, System.DateTime.Now.ToString("hh:mm:ss"), strName, strDetail);

                    i = COUNT_CNT;
                }
                i++;
            }      
        }

        # endregion



        #region Control Event

        private void bSendRequest_Click(object sender, EventArgs e)
        {
            if (objIEPC != null)
            {
                switch (cmbTypeRequest.SelectedIndex)
                {
                    case 0:
                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)

                                objIEPC.SendState(bTypeConnection[i],bAdrReceiver);
                        }
                            
                        break;

                    case 1:
                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)
                                objIEPC.SendCoord(bTypeConnection[i], bAdrReceiver);
                        }
                        
                        break;

                    case 2:
                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)
                                objIEPC.SendReconFWS(bTypeConnection[i], bAdrReceiver);
                        }
                        
                        break;

                    case 3:
                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)
                                objIEPC.SendReconFHSS(bTypeConnection[i], bAdrReceiver);
                        }
                        
                        break;

                    case 4:
                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)
                                objIEPC.SendStateSupprFWS(bTypeConnection[i], bAdrReceiver);
                        }
                        
                        break;

                    case 5:
                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)
                                objIEPC.SendStateSupprFHSS(bTypeConnection[i], bAdrReceiver);
                        }
                        
                        break;

                    default:
                        break;

                }
            }

            

        }

        private void bSendRegimeWork_Click(object sender, EventArgs e)
        {
            if (objIEPC != null)
            {
                try
                {
                    byte bRegime = 0;
                    bRegime = Convert.ToByte(cmbRegime.SelectedIndex);
               


                    for (int i = 0; i < COUNT_CNT; i++)
                    {
                        if (chbRRD[i].Checked)
                            objIEPC.SendRegime(bTypeConnection[i], bAdrReceiver, bRegime);
                    }
                }
                catch (SystemException)
                { }
            }
               
        }

        private void bSendRequestBearing_Click(object sender, EventArgs e)
        {
            if (iExBRZ_GRZ != null)
            {
                try
                {
                    int iFreq = Convert.ToInt32(Convert.ToDouble(tbFreqRecBearing.Text));
                    int iID = Convert.ToInt32(nudFreqID.Value);
                
                    switch (cmbTypeReqBearing.SelectedIndex)
                    {
                        case 0:

                            for (int i = 0; i < COUNT_CNT; i++)
                            {
                                if (chbRRD[i].Checked)

                                    objIEPC.SendExecBear(bTypeConnection[i], bAdrReceiver, iID, iFreq);
                            }                                                        
                            
                            break;

                        case 1:

                            for (int i = 0; i < COUNT_CNT; i++)
                            {
                                if (chbRRD[i].Checked)

                                    objIEPC.SendSimulBear(bTypeConnection[i], bAdrReceiver, iID, iFreq);
                            }    
                          
                            break;

                        default:
                            break;
                    }
                }
                catch (SystemException)
                {}
                
            }
        }

        private void bSendTextMessagePC_Click(object sender, EventArgs e)
        {
            if (iExBRZ_GRZ != null)
            {
                try
                {
                    string strText = rtbTextPC.Text;


                    for (int i = 0; i < COUNT_CNT; i++)
                    {
                        if (chbRRD[i].Checked)

                            objIEPC.SendText(bTypeConnection[i], bAdrReceiver, strText);
                    }

                }
                catch(SystemException)
                {}
            }
        }

        private void bSendSynchronizePC_Click(object sender, EventArgs e)
        {
            if (iExBRZ_GRZ != null)
            {
                try
                {
                    TTimeMy tTime = new TTimeMy();

                    DateTime dtTimeSynch = dtpTimeSinchroPC.Value;

                    tTime.bHour = Convert.ToByte(dtTimeSynch.Hour);
                    tTime.bMinute = Convert.ToByte(dtTimeSynch.Minute);
                    tTime.bSecond = Convert.ToByte(dtTimeSynch.Second);


                    for (int i = 0; i < COUNT_CNT; i++)
                    {
                        if (chbRRD[i].Checked)

                            objIEPC.SendSynchTime(bTypeConnection[i], bAdrReceiver, tTime);
                    }

                }
                catch (SystemException)
                { }
            }
        }

        private void bSendForbidRangeFreq_Click(object sender, EventArgs e)
        {
            if (iExBRZ_GRZ != null)
            {
                try
                {
                    TRangeSpec[] tRangeSpec = new TRangeSpec[1];

                    tRangeSpec[0].iFregMin = (int)(Convert.ToDouble(tbFreqMinForbid.Text) * 10);

                    tRangeSpec[0].iFregMax = (int)(Convert.ToDouble(tbFreqMaxForbid.Text) * 10);



                    for (int i = 0; i < COUNT_CNT; i++)
                    {
                        if (chbRRD[i].Checked)

                            objIEPC.SendRangeSpec(bTypeConnection[i], bAdrReceiver, (byte)cmbActForbid.SelectedIndex, tRangeSpec);
                    }

                }
                catch (SystemException)
                { }
            }
        }

        private void bSendReconSectorRange_Click(object sender, EventArgs e)
        {
            if (iExBRZ_GRZ != null)
            {
                try
                {
                    if (tRangeSectorR != null)
                    {

                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)

                                objIEPC.SendRangeSector(bTypeConnection[i], bAdrReceiver, 0, tRangeSectorR);
                        }
                                                                           
                    }

                }
                catch (SystemException)
                { }
            }
        }

        private void bSendSupressSectorRange_Click(object sender, EventArgs e)
        {
            if (iExBRZ_GRZ != null)
            {
                try
                {
                    if (tRangeSectorS != null)
                    {
                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)

                                objIEPC.SendRangeSector(bTypeConnection[i], bAdrReceiver, 1, tRangeSectorS);
                        }
                                
                        
                    }

                }
                catch (SystemException)
                { }
            }
            
            
            
            
        }

        private void bSendSupressFWS_Click(object sender, EventArgs e)
        {
            if (iExBRZ_GRZ != null)
            {
                try
                {
                    if (tSupprFWS != null)
                    {
                        int iDurationGlobal = (int)(nudDurationGlobalFWS.Value);


                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)

                                objIEPC.SendSupprFWS(bTypeConnection[i], bAdrReceiver, iDurationGlobal, tSupprFWS);
                        }
                        
                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void cmbTypeModulationFWS_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDeviation(cmbDeviationFWS, cmbTypeModulationFWS.SelectedIndex, 0, cmbDeviationFWS, lDeviationFWS);
            SetManipulation(cmbManipulationFWS, cmbTypeModulationFWS.SelectedIndex, 0, cmbManipulationFWS, label2);
        }

        private void bSendSupressFHSS_Click(object sender, EventArgs e)
        {
            if (iExBRZ_GRZ != null)
            {
                try
                {
                    if (tSupprFHSS != null)
                    {
                        int iDurationGlobal = (int)(nudDuratSupFHSS.Value);

                        for (int i = 0; i < COUNT_CNT; i++)
                        {
                            if (chbRRD[i].Checked)

                                objIEPC.SendSupprFHSS(bTypeConnection[i], bAdrReceiver, iDurationGlobal, tSupprFHSS);
                        }
                                                
                    }
                }
                catch (SystemException)
                { }
            
            }
        }

        private void cmbTypeModulation_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetDeviation(cmbDeviation,cmbTypeModulation.SelectedIndex, 0, cmbDeviation, lDeviation);
            SetManipulation(cmbManipulation,cmbTypeModulation.SelectedIndex, 0, cmbManipulation, lManipulation);
        }

       

        private void bRangeReconAdd_Click(object sender, EventArgs e)
        {            
            try
            {
                TRangeSector tRangeSectorTemp = new TRangeSector();

                if (chbAutoRangeR.Checked)
                {
                    Random rnd = new Random();
                  
                    tRangeSectorTemp.iFregMin = (int)(rnd.Next(300000, 30000000));
                    tRangeSectorTemp.iFregMax = (int)(rnd.Next(tRangeSectorTemp.iFregMin, 30000000));
                    tRangeSectorTemp.sAngleMin = (short)(rnd.Next(0, 359));  
                    tRangeSectorTemp.sAngleMax = (short)(rnd.Next(0, 359));
                }
                else
                {
                    tRangeSectorTemp.iFregMin = (int)(Convert.ToDouble(tbFreqMinRecon.Text) * 10);
                    tRangeSectorTemp.iFregMax = (int)(Convert.ToDouble(tbFreqMaxRecon.Text) * 10);
                    tRangeSectorTemp.sAngleMin = (short)(nudBearingMinRecon.Value);
                    tRangeSectorTemp.sAngleMax = (short)(nudBearingMaxRecon.Value);
                }


                if (tRangeSectorR == null)
                    tRangeSectorR = new TRangeSector[1];
                else
                    Array.Resize(ref tRangeSectorR, tRangeSectorR.Length + 1);

                tRangeSectorR[tRangeSectorR.Length - 1] = tRangeSectorTemp;

                ShowRangeSector(rtbMemoRangeRecon, tRangeSectorR);
            }
            catch (Exception)
            {}
            
        }

        private void ShowRangeSector(RichTextBox rtbMemo, TRangeSector[] tRangeSector)
        {
            rtbMemo.Clear();

            if (tRangeSector != null)
            {
                for (int i = 0; i < tRangeSector.Length; i++)
                {
                    rtbMemo.SelectionColor = Color.Black;
                    rtbMemo.AppendText(Convert.ToString(i + 1) + ". " + " Диап. ");

                    rtbMemo.AppendText(tRangeSector[i].iFregMin.ToString() + " - " + tRangeSector[i].iFregMax.ToString());

                    rtbMemo.AppendText(" Сек.  " + tRangeSector[i].sAngleMin.ToString() + " - " + tRangeSector[i].sAngleMax.ToString()+"\n");

                    rtbMemo.SelectionStart = rtbMemo.TextLength;
                    rtbMemo.ScrollToCaret();
                }
            }
 
        }

        private void bRangeReconClear_Click(object sender, EventArgs e)
        {
            try
            {
                if (tRangeSectorR != null)
                    tRangeSectorR = null;

                ShowRangeSector(rtbMemoRangeRecon, tRangeSectorR);
            }
            catch (Exception)
            { }
        }

       

        private void bRangeSupprClear_Click(object sender, EventArgs e)
        {
            try
            {
                if (tRangeSectorS != null)
                    tRangeSectorS = null;

                ShowRangeSector(rtbMemoRangeSuppr, tRangeSectorS);
            }
            catch (Exception)
            { }
        }

        private void bRangeSupprAdd_Click(object sender, EventArgs e)
        {

           
            try
            {
                TRangeSector tRangeSectorTemp = new TRangeSector();

                if (chbAutoRangeS.Checked)
                {
                    Random rnd = new Random();

                    tRangeSectorTemp.iFregMin = (int)(rnd.Next(300000, 30000000));
                    tRangeSectorTemp.iFregMax = (int)(rnd.Next(tRangeSectorTemp.iFregMin, 30000000));
                    tRangeSectorTemp.sAngleMin = (short)(rnd.Next(0, 359));
                    tRangeSectorTemp.sAngleMax = (short)(rnd.Next(0, 359));
                }
                else
                {


                    tRangeSectorTemp.iFregMin = (int)(Convert.ToDouble(tbFreqMinSupress.Text) * 10);
                    tRangeSectorTemp.iFregMax = (int)(Convert.ToDouble(tbFreqMaxSupress.Text) * 10);
                    tRangeSectorTemp.sAngleMin = (short)(nudBearingMinSupress.Value);
                    tRangeSectorTemp.sAngleMax = (short)(nudBearingMaxSupress.Value);
                }


                if (tRangeSectorS == null)
                    tRangeSectorS = new TRangeSector[1];
                else
                    Array.Resize(ref tRangeSectorS, tRangeSectorS.Length + 1);

                tRangeSectorS[tRangeSectorS.Length - 1] = tRangeSectorTemp;

                ShowRangeSector(rtbMemoRangeSuppr, tRangeSectorS);
            }
            catch (Exception)
            { }
        }

        private void nudSupprAddFWS_Click(object sender, EventArgs e)
        {
            try
            {
                TSupprFWS tSupprFWSTemp = new TSupprFWS();

                if (chbAutoSupprFWS.Checked)
                {
                    Random rnd = new Random();

                    tSupprFWSTemp.iFreq = (float)(rnd.Next(1500, 30000));
                    tSupprFWSTemp.bPrioritet = Convert.ToByte(rnd.Next(1, 9));
                    tSupprFWSTemp.sBearing = Convert.ToInt16(rnd.Next(0, 359));
                    tSupprFWSTemp.bThreshold = Convert.ToByte(rnd.Next(0, 145));


                    byte bModul = Convert.ToByte(rnd.Next(1, 7));

                    // считать параметры помехи
                    switch (bModul)
                    {
                        // ЧМ
                        case 0:
                            tSupprFWSTemp.bModulation = 0x01;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFWSTemp.bManipulation = 0;
                            tSupprFWSTemp.bDuration = 0;
                            break;

                        // ЧМ-2
                        case 1:
                            tSupprFWSTemp.bModulation = 0x02;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFWSTemp.bManipulation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFWSTemp.bDuration = 0;
                            break;

                        // ЧМ-4
                        case 2:
                            tSupprFWSTemp.bModulation = 0x03;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFWSTemp.bManipulation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFWSTemp.bDuration = 0;
                            break;

                        // ЧМШ
                        case 3:
                            tSupprFWSTemp.bModulation = 0x06;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFWSTemp.bManipulation = 0;
                            tSupprFWSTemp.bDuration = 0;
                            break;

                        // ФМн
                        case 4:
                            tSupprFWSTemp.bModulation = 0x07;
                            // в байт девиации значение манипуляции
                            tSupprFWSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFWSTemp.bManipulation = 0;
                            tSupprFWSTemp.bDuration = 0;
                            break;

                        // ЗШ
                        case 5:
                            tSupprFWSTemp.bModulation = 0x10;
                            tSupprFWSTemp.bDeviation = 0;
                            tSupprFWSTemp.bManipulation = 0;
                            tSupprFWSTemp.bDuration = 0;
                            break;

                        default:
                            tSupprFWSTemp.bModulation = 0x06;
                            tSupprFWSTemp.bDeviation = 0;
                            tSupprFWSTemp.bManipulation = 0;
                            tSupprFWSTemp.bDuration = 0;
                            break;
                    }
                   
                }
                else
                {
                    tSupprFWSTemp.iFreq = Convert.ToSingle(tbFreqSupFWS.Text);
                    tSupprFWSTemp.bPrioritet = Convert.ToByte(nudPriorSupFWS.Value);
                    tSupprFWSTemp.sBearing = Convert.ToInt16(nudBearingFWS.Value);
                    tSupprFWSTemp.bThreshold = Convert.ToByte(nudThresholdFWS.Value);
                    tSupprFWSTemp.bDuration = 1;
                    // считать параметры помехи
                    switch (cmbTypeModulationFWS.SelectedIndex)
                    {
                        // ЧМШ
                        case 0:
                            tSupprFWSTemp.bModulation = 0x01;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(cmbDeviationFWS.SelectedIndex + 1);
                            tSupprFWSTemp.bManipulation = 0;
                            break;

                        // ЧМШ скан
                        case 1:
                            tSupprFWSTemp.bModulation = 0x02;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(cmbDeviationFWS.SelectedIndex + 1);
                            tSupprFWSTemp.bManipulation = 0;
                            break;

                        // ЧМн-2
                        case 2:
                            tSupprFWSTemp.bModulation = 0x03;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(cmbDeviationFWS.SelectedIndex + 1);
                            tSupprFWSTemp.bManipulation = Convert.ToByte(cmbManipulationFWS.SelectedIndex + 1);
                            break;

                        // ЧМн-4
                        case 3:
                            tSupprFWSTemp.bModulation = 0x04;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(cmbDeviationFWS.SelectedIndex + 1);
                            tSupprFWSTemp.bManipulation = Convert.ToByte(cmbManipulationFWS.SelectedIndex + 1);
                            break;
                        // ЧМн-8
                        case 4:
                            tSupprFWSTemp.bModulation = 0x05;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(cmbDeviationFWS.SelectedIndex + 1);
                            tSupprFWSTemp.bManipulation = Convert.ToByte(cmbManipulationFWS.SelectedIndex + 1);
                            break;

                        // ФМн-2
                        case 5:
                            tSupprFWSTemp.bModulation = 0x06;
                            tSupprFWSTemp.bDeviation = 0;
                            tSupprFWSTemp.bManipulation = Convert.ToByte(cmbManipulationFWS.SelectedIndex + 1);
                            break;
                        // ФМн-4
                        case 6:
                            tSupprFWSTemp.bModulation = 0x07;
                            tSupprFWSTemp.bDeviation = 0;
                            tSupprFWSTemp.bManipulation = Convert.ToByte(cmbManipulationFWS.SelectedIndex + 1);
                            break;
                        // ФМн-8
                        case 7:
                            tSupprFWSTemp.bModulation = 0x08;
                            tSupprFWSTemp.bDeviation = 0;
                            tSupprFWSTemp.bManipulation = Convert.ToByte(cmbManipulationFWS.SelectedIndex + 1);
                            break;

                        // ЗШ
                        case 8:
                            tSupprFWSTemp.bModulation = 0x09;
                            tSupprFWSTemp.bDeviation = 0;
                            tSupprFWSTemp.bManipulation = Convert.ToByte(cmbManipulationFWS.SelectedIndex + 1);
                            break;
                        // АФМн
                        case 9:
                            tSupprFWSTemp.bModulation = 0x0c;
                            tSupprFWSTemp.bDeviation = 0;
                            tSupprFWSTemp.bManipulation = Convert.ToByte(cmbManipulationFWS.SelectedIndex + 1);
                            break;
                        // ЧМ
                        case 10:
                            tSupprFWSTemp.bModulation = 0x0D;
                            tSupprFWSTemp.bDeviation = Convert.ToByte(cmbDeviationFWS.SelectedIndex + 1);
                            tSupprFWSTemp.bManipulation = 0;
                            break;

                        default:
                            tSupprFWSTemp.bModulation = 0x06;
                            tSupprFWSTemp.bDeviation = 0;
                            tSupprFWSTemp.bManipulation = 0;
                            break;
                    }
                }


                if (tSupprFWS == null)
                    tSupprFWS = new TSupprFWS[1];
                else
                    Array.Resize(ref tSupprFWS, tSupprFWS.Length + 1);

                tSupprFWS[tSupprFWS.Length - 1] = tSupprFWSTemp;

                ShowSupprFWS(rtbMemoSupprFWS, tSupprFWS);
            }
            catch (Exception)
            { }

        }



        private void ShowSupprFWS(RichTextBox rtbMemo, TSupprFWS[] tSupprFWS)
        {
            rtbMemo.Clear();

            if (tSupprFWS != null)
            {
                for (int i = 0; i < tSupprFWS.Length; i++)
                {
                    rtbMemo.SelectionColor = Color.Black;
                    rtbMemo.AppendText(Convert.ToString(i + 1) + ". " + " Част. ");

                    rtbMemo.AppendText(tSupprFWS[i].iFreq.ToString());

                    rtbMemo.AppendText(" Приор.  " + tSupprFWS[i].bPrioritet.ToString() + " Порог " + tSupprFWS[i].bThreshold.ToString() + "\n");
                    rtbMemo.AppendText(" Пеленг  " + tSupprFWS[i].sBearing.ToString()+"\n");

                    rtbMemo.SelectionStart = rtbMemo.TextLength;
                    rtbMemo.ScrollToCaret();
                }
            }

        }

        private void bSupprAddFHSS_Click(object sender, EventArgs e)
        {
            try
            {
                TSupprFHSS tSupprFHSSTemp = new TSupprFHSS();

                if (chbAutoSupprFHSS.Checked)
                {
                    Random rnd = new Random();

                    tSupprFHSSTemp.bCodeFFT = Convert.ToByte(rnd.Next(0, 3));

                    tSupprFHSSTemp.iFreqMin = (float)(rnd.Next(1500, 30000));
                    tSupprFHSSTemp.iFreqMax = (float)(rnd.Next((int)tSupprFHSSTemp.iFreqMin, 30000));

                    byte bModul = Convert.ToByte(rnd.Next(1, 7));

                    // считать параметры помехи
                    switch (bModul)
                    {
                        // ЧМШ
                        case 0:
                            tSupprFHSSTemp.bModulation = 0x01;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFHSSTemp.bManipulation = 0;

                            break;

                        // ЧМ-2
                        case 1:
                            tSupprFHSSTemp.bModulation = 0x02;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(rnd.Next(1, 3));

                            break;

                        // ЧМ-4
                        case 2:
                            tSupprFHSSTemp.bModulation = 0x03;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(rnd.Next(1, 3));

                            break;

                        // ЧМШ
                        case 3:
                            tSupprFHSSTemp.bModulation = 0x06;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFHSSTemp.bManipulation = 0;

                            break;

                        // ФМн
                        case 4:
                            tSupprFHSSTemp.bModulation = 0x07;
                            // в байт девиации значение манипуляции
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(rnd.Next(1, 3));
                            tSupprFHSSTemp.bManipulation = 0;

                            break;

                        // ЗШ
                        case 5:
                            tSupprFHSSTemp.bModulation = 0x10;
                            tSupprFHSSTemp.bDeviation = 0;
                            tSupprFHSSTemp.bManipulation = 0;

                            break;

                        default:
                            tSupprFHSSTemp.bModulation = 0x06;
                            tSupprFHSSTemp.bDeviation = 0;
                            tSupprFHSSTemp.bManipulation = 0;

                            break;
                    }

                }
                else
                {
                    tSupprFHSSTemp.bCodeFFT = Convert.ToByte(cmbStepSupFHSS.SelectedIndex + 2);

                    tSupprFHSSTemp.iFreqMin = Convert.ToSingle(tbFreqMinSupFHSS.Text);
                    tSupprFHSSTemp.iFreqMax = Convert.ToSingle(tbFreqMaxSupFHSS.Text);

                    // считать параметры помехи
                    switch (cmbTypeModulation.SelectedIndex)
                    {
                        // ЧМШ
                        case 0:
                            tSupprFHSSTemp.bModulation = 0x01;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                            tSupprFHSSTemp.bManipulation = 0;
                            break;

                        // ЧМШ скан
                        case 1:
                            tSupprFHSSTemp.bModulation = 0x02;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                            tSupprFHSSTemp.bManipulation = 0;
                            break;

                        // ЧМн-2
                        case 2:
                            tSupprFHSSTemp.bModulation = 0x03;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                            break;

                        // ЧМн-4
                        case 3:
                            tSupprFHSSTemp.bModulation = 0x04;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                            break;
                        // ЧМн-8
                        case 4:
                            tSupprFHSSTemp.bModulation = 0x05;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                            break;

                        // ФМн-2
                        case 5:
                            tSupprFHSSTemp.bModulation = 0x06;
                            tSupprFHSSTemp.bDeviation = 0;
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                            break;
                        // ФМн-4
                        case 6:
                            tSupprFHSSTemp.bModulation = 0x07;
                            tSupprFHSSTemp.bDeviation = 0;
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                            break;
                        // ФМн-8
                        case 7:
                            tSupprFHSSTemp.bModulation = 0x08;
                            tSupprFHSSTemp.bDeviation = 0;
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                            break;

                        // ЗШ
                        case 8:
                            tSupprFHSSTemp.bModulation = 0x09;
                            tSupprFHSSTemp.bDeviation = 0;
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                            break;
                        // АФМн
                        case 9:
                            tSupprFHSSTemp.bModulation = 0x0c;
                            tSupprFHSSTemp.bDeviation = 0;
                            tSupprFHSSTemp.bManipulation = Convert.ToByte(cmbManipulation.SelectedIndex + 1);
                            break;
                        // ЧМ
                        case 10:
                            tSupprFHSSTemp.bModulation = 0x0D;
                            tSupprFHSSTemp.bDeviation = Convert.ToByte(cmbDeviation.SelectedIndex + 1);
                            tSupprFHSSTemp.bManipulation = 0;
                            break;

                        default:
                            tSupprFHSSTemp.bModulation = 0x06;
                            tSupprFHSSTemp.bDeviation = 0;
                            tSupprFHSSTemp.bManipulation = 0;
                            break;
                    }
                }


                if (tSupprFHSS == null)
                    tSupprFHSS = new TSupprFHSS[1];
                else
                    Array.Resize(ref tSupprFHSS, tSupprFHSS.Length + 1);

                tSupprFHSS[tSupprFHSS.Length - 1] = tSupprFHSSTemp;

                ShowSupprFHSS(rtbMemoSupprFHSS, tSupprFHSS);
            }
            catch (Exception)
            { }

        }

        private void ShowSupprFHSS(RichTextBox rtbMemo, TSupprFHSS[] tSupprFHSS)
        {
            rtbMemo.Clear();

            if (tSupprFHSS != null)
            {
                for (int i = 0; i < tSupprFHSS.Length; i++)
                {
                    rtbMemo.SelectionColor = Color.Black;
                    rtbMemo.AppendText(Convert.ToString(i + 1) + ". " + " Част.мин ");

                    rtbMemo.AppendText(tSupprFHSS[i].iFreqMin.ToString());
                    rtbMemo.AppendText(Convert.ToString(i + 1) + ". " + " Част.макс ");

                    rtbMemo.AppendText(tSupprFHSS[i].iFreqMax.ToString());

                    rtbMemo.AppendText(" БПФ  " + tSupprFHSS[i].bCodeFFT.ToString());
                    rtbMemo.AppendText(" Мод " + tSupprFHSS[i].bModulation.ToString() );
                    rtbMemo.AppendText(" Дев.  " + tSupprFHSS[i].bDeviation.ToString() ); 
                    rtbMemo.AppendText(" Ман.  " + tSupprFHSS[i].bManipulation.ToString() + "\n");

                    rtbMemo.SelectionStart = rtbMemo.TextLength;
                    rtbMemo.ScrollToCaret();
                }
            }

        }

       

        private void chbRRD1_CheckedChanged(object sender, EventArgs e)
        {
            int i = 0;

            while (i < COUNT_RRD)
            {
                if (sender == chbRRD[i])
                {
                    if (chbRRD[i].Checked)
                        chbRRD[i].BackColor = SystemColors.ActiveCaption;
                    else
                        chbRRD[i].BackColor = SystemColors.Control;

                    i = COUNT_RRD;

                }
                
                i++;
            }
        }

        private void bConnectModem_Click(object sender, EventArgs e)
        {

            if (bConnectModem.BackColor == clConnect)
            {
                if (objIEPC != null)
                    objIEPC.DisconnectModem();
            }
            else
            {
                if (objIEPC != null)
                    objIEPC.ConnectModem(cmbComModem.Items[cmbComModem.SelectedIndex].ToString(), 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            }
            
            
        }

        private void bConnectHF_Click(object sender, EventArgs e)
        {
            if (bConnectHF.BackColor == clConnect)
            {
                if (objIEPC != null)
                    objIEPC.DisconnectHF();
            }
            else
            {
                if (objIEPC != null)
                    objIEPC.ConnectHF(cmbComHF.Items[cmbComHF.SelectedIndex].ToString(), 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            }
        }

        private void bConnectVHF_Click(object sender, EventArgs e)
        {
            if (bConnectVHF.BackColor == clConnect)
            {
                if (objIEPC != null)
                    objIEPC.DisconnectVHF();
            }
            else
            {
                if (objIEPC != null)
                    objIEPC.ConnectVHF(cmbComVHF.Items[cmbComVHF.SelectedIndex].ToString(), 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            }
        }

        private void nudAddressReceiver_ValueChanged(object sender, EventArgs e)
        {
            bAdrReceiver = Convert.ToByte(nudAddressReceiver.Value);
        }

        # endregion

        private void bConnectRRC_Click(object sender, EventArgs e)
        {
            if (bConnectRRC.BackColor == clConnect)
            {
                if (objIEPC != null)
                  objIEPC.DestroyServerRRC();
            }
            else
            {
                if (objIEPC != null)
                {
                    try
                    {
                        //string strLocalHost = Dns.GetHostName();
                        //System.Net.IPAddress ip = System.Net.Dns.GetHostByName(strLocalHost).AddressList[0];
                        //string strIP = ip.ToString();
                        var port = Convert.ToInt32(tbPort.Text);
                        objIEPC.CreateServerRRC(cmbxServerIP.Text, port);
                    }
                    catch(SystemException)
                    {}
                }
            }
        }

        private void bConnect3G_Click(object sender, EventArgs e)
        {
            if (bConnect3G.BackColor == clConnect)
            {
                if (objIEPC != null)
                    objIEPC.DestroyServer3G();
            }
            else
            {
                if (objIEPC != null)
                {
                    try
                    {
                        string strLocalHost = Dns.GetHostName();
                        System.Net.IPAddress ip = System.Net.Dns.GetHostByName(strLocalHost).AddressList[1];
                        string strIP = ip.ToString();
                        objIEPC.CreateServer3G(strIP, 9101);
                    }
                    catch (SystemException)
                    { }
                }
            }
        }

        private void bSendReceiptPC_Click(object sender, EventArgs e)
        {
            if (iExBRZ_GRZ != null)
            {
                try
                {
                    byte bCodeError = (byte)(nudCodeErrorPC.Value);

                    for (int i = 0; i < COUNT_CNT; i++)
                    {
                        if (chbRRD[i].Checked)

                            objIEPC.SendConfirmText(bTypeConnection[i], bAdrReceiver, bCodeError);
                    }

                }
                catch (SystemException)
                { }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TestClass.OnChangeRegime += new TestClass.ChangeRegimeEventHandler(ChangeRegimeEventHandlerTest);

            byte bTest = TestClass.bRegime;
        }

        private void btnConnectDB_Click(object sender, EventArgs e)
        {
            if (btnConnectDB.BackColor == clConnect)
            {
                InitDisconnectionDB();
            }
            else
            {
                InitConnectionDB();
            }
        }

        private void btnConnectServer_Click_1(object sender, EventArgs e)
        {
            if (btnConnectServer.BackColor == clConnect)
            {
                InitDisconnectionServer();
            }
            else
            {
                InitConnectionServer();
            }
        }

        #region ClientDB

        ServiceClient clientDB;
        string appName = "ODKvetka";

        void InitClientDB()
        {
            if (clientDB == null) return;

            clientDB.OnConnect += ClientDB_OnConnect;
            clientDB.OnDisconnect += ClientDB_OnDisconnect;
            (clientDB.Tables[NameTable.TableJammer] as ITableUpdate<TableJammer>).OnUpTable += HandlerUpdate_TableJammer;
            (clientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += HandlerUpdate_TableFreqForbidden;
            (clientDB.Tables[NameTable.TableFreqImportant] as ITableUpdate<TableFreqImportant>).OnUpTable += HandlerUpdate_TableFreqImportant;
            (clientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += HandlerUpdate_TableFreqKnown;

            (clientDB.Tables[NameTable.TableFreqRangesElint] as ITableUpdate<TableFreqRangesElint>).OnUpTable += HandlerUpdate_TableFreqRangesElint;
            (clientDB.Tables[NameTable.TableFreqRangesJamming] as ITableUpdate<TableFreqRangesJamming>).OnUpTable += HandlerUpdate_TableFreqRangesJamming;
            (clientDB.Tables[NameTable.TableSectorsElint] as ITableUpdate<TableSectorsElint>).OnUpTable += HandlerUpdate_TableSectorsElint;


            (clientDB.Tables[NameTable.TableResFFJam] as ITableUpdate<TableResFFJam>).OnUpTable += HandlerUpdate_TableSuppressFWS;
            (clientDB.Tables[NameTable.TableResFHSSJam] as ITableUpdate<TableResFHSSJam>).OnUpTable += HandlerUpdate_TableSuppressFHSS;

            (clientDB.Tables[NameTable.TableResFF] as ITableUpdate<TableResFF>).OnUpTable += HandlerUpdate_TableResFF; 
            (clientDB.Tables[NameTable.TableResFHSS] as ITableUpdate<TableResFHSS>).OnUpTable += HandlerUpdate_TableResFHSS;
            (clientDB.Tables[NameTable.TableResFFDistribution] as ITableUpdate<TableResFFDistribution>).OnUpTable += HandlerUpdate_TableResFFDistribution;
            (clientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += HandlerUpdate_TableGlobalProperties;
        }

        

        private void ClientDB_OnDisconnect(object sender, ClientEventArgs e)
        {
            stateIndicateConnectionDB.SetConnectOff();
        }

        private void ClientDB_OnConnect(object sender, global::InheritorsEventArgs.ClientEventArgs e)
        {
            stateIndicateConnectionDB.SetConnectOn();
            LoadTables();
        }


        private void InitConnectionDB()
        {
            clientDB = new ServiceClient(appName, mtbDbIp.Text, Convert.ToInt32(mtbDbPort.Text));
            InitClientDB();

            try
            {
                clientDB.Connect();
            }
            catch (SystemException ex)
            {
            }
        }

        private void InitDisconnectionDB()
        {
            if (clientDB == null) return;

            clientDB.Disconnect();
            clientDB = null;
        }

        // Станции помех
        public List<TableJammer> lJammerStation = new List<TableJammer>();
        // Известные частоты (ИЧ)
        public List<FreqRanges> lFreqKnown = new List<FreqRanges>();
        // Важные частоты (ИЧ)
        public List<FreqRanges> lFreqImportant = new List<FreqRanges>();
        // Запрещенные частоты (ЗЧ)
        public List<FreqRanges> lFreqForbidden = new List<FreqRanges>();
        // Сектора радиоразведки (СРР)
        public List<TableSectorsElint> lSectorsElint = new List<TableSectorsElint>();
        // Сектора радиоразведки (ДРР)
        public List<FreqRanges> lFreqRangesElint = new List<FreqRanges>();
        // Диапазоны радиоразведки (ДРП)
        public List<FreqRanges> lFreqRangesJamming = new List<FreqRanges>();
        // ФРЧ на РП 
        public List<TableResFFJam> lResFFJam = new List<TableResFFJam>();
        // ППРЧ на РП
        public List<TableResFHSSJam> lResFHSSJam = new List<TableResFHSSJam>();
        // ФРЧ
        public List<TableResFF> lResFF = new List<TableResFF>();
        // ФРЧ ЦР
        public List<TableResFFDistribution> lResFFDistribution = new List<TableResFFDistribution>();
        // ППРЧ
        public List<TableResFHSS> lResFHSS = new List<TableResFHSS>();

        public GlobalProperties globalProp;

        int linkedStation = 0;

        #region Update Tables handlers
        private void HandlerUpdate_TableJammer(object sender, TableEventArgs<TableJammer> e)
        {
            lJammerStation = e.Table;
            var hasOwn = lJammerStation.Find(x => x.Role == StationRole.Linked);
            linkedStation = hasOwn == null ? 0 : hasOwn.Id;
            //var hasOwn = lJammerStation.Find(x => x.IsOwn == true);
            //ownStation = hasOwn == null ? 0 : hasOwn.Id;
            //byte role = hasOwn == null ? (byte)0 : (byte)hasOwn.Role;

            //var coords = hasOwn == null ? new Coord() : hasOwn.Coordinates;
            //UpdatetStationInfo(ownStation, role, coords);
        }

        private void HandlerUpdate_TableFreqImportant(object sender, TableEventArgs<TableFreqImportant> e)
        {
            lFreqImportant = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == linkedStation).ToList();
            SendFreqRangesToSlave(2, lFreqImportant);
        }

        private void HandlerUpdate_TableFreqKnown(object sender, TableEventArgs<TableFreqKnown> e)
        {
            lFreqKnown = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == linkedStation).ToList();
            SendFreqRangesToSlave(1, lFreqKnown);
        }

        private void HandlerUpdate_TableFreqForbidden(object sender, TableEventArgs<TableFreqForbidden> e)
        {
            lFreqForbidden = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == linkedStation).ToList();
            SendFreqRangesToSlave(0, lFreqForbidden);
        }

        private void HandlerUpdate_TableFreqRangesElint(object sender, TableEventArgs<TableFreqRangesElint> e)
        {
            lFreqRangesElint = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == linkedStation).ToList();
            SendSectorsRangesToSlave(0, lFreqRangesElint);
        }

        private void HandlerUpdate_TableFreqRangesJamming(object sender, TableEventArgs<TableFreqRangesJamming> e)
        {
            lFreqRangesJamming = (from t in e.Table let a = t as FreqRanges select a).Where(x => x.NumberASP == linkedStation).ToList();
            SendSectorsRangesToSlave(1, lFreqRangesJamming);
        }

        private void HandlerUpdate_TableSectorsElint(object sender, TableEventArgs<TableSectorsElint> e)
        {
            lSectorsElint = e.Table.Where(x => x.NumberASP == linkedStation).ToList();
            SendSectorsRangesToSlave(0, lSectorsElint);
        }

        private void HandlerUpdate_TableSuppressFHSS(object sender, TableEventArgs<TableResFHSSJam> e)
        {
            lResFHSSJam = e.Table.Where(x => x.NumberASP == linkedStation).ToList();
            SendFHSSSuppressToSlave(lResFHSSJam);
        }

        private void HandlerUpdate_TableSuppressFWS(object sender, TableEventArgs<TableResFFJam> e)
        {
            lResFFJam = e.Table.Where(x => x.NumberASP == linkedStation).ToList();
            SendFWSSuppressToSlave(lResFFJam);
        }

        private void HandlerUpdate_TableResFHSS(object sender, TableEventArgs<TableResFHSS> e)
        {
            lResFHSS = new List<TableResFHSS>(e.Table);
        }

        private void HandlerUpdate_TableResFF(object sender, TableEventArgs<TableResFF> e)
        {
            lResFF = new List<TableResFF>(e.Table);
        }

        TableResFFDistribution active;
        private void HandlerUpdate_TableResFFDistribution(object sender, TableEventArgs<TableResFFDistribution> e)
        {
            lResFFDistribution = new List<TableResFFDistribution>(e.Table);
            active = lResFFDistribution.FirstOrDefault(t => t.IsActive == true);
            if (active != null) 
                SendFFExecutingBearing((int)active.FrequencyKHz);
        }

        private void HandlerUpdate_TableGlobalProperties(object sender, TableEventArgs<GlobalProperties> e)
        {
            globalProp = e.Table.FirstOrDefault(x => x.Id == 1);
        }
        #endregion

        private async void LoadTables()
        {
            try
            {
                lJammerStation = await clientDB.Tables[NameTable.TableJammer].LoadAsync<TableJammer>();
                var hasOwn = lJammerStation.Find(x => x.Role == StationRole.Linked);
                linkedStation = hasOwn == null ? 0 : hasOwn.Id;

                if (linkedStation == 0) return;

                byte role = (byte)hasOwn.Role;
                var coords = hasOwn == null ? new Coord() : hasOwn.Coordinates;

                lFreqKnown = await (clientDB.Tables[NameTable.TableFreqKnown] as IDependentAsp).LoadByFilterAsync<FreqRanges>(linkedStation);
                lFreqImportant = await (clientDB.Tables[NameTable.TableFreqImportant] as IDependentAsp).LoadByFilterAsync<FreqRanges>(linkedStation);
                lFreqForbidden = await (clientDB.Tables[NameTable.TableFreqForbidden] as IDependentAsp).LoadByFilterAsync<FreqRanges>(linkedStation);
                lFreqRangesElint = await (clientDB.Tables[NameTable.TableFreqRangesElint] as IDependentAsp).LoadByFilterAsync<FreqRanges>(linkedStation);
                lFreqRangesJamming = await (clientDB.Tables[NameTable.TableFreqRangesJamming] as IDependentAsp).LoadByFilterAsync<FreqRanges>(linkedStation);
                lSectorsElint = await (clientDB.Tables[NameTable.TableSectorsElint] as IDependentAsp).LoadByFilterAsync<TableSectorsElint>(linkedStation);
                lResFFJam = await (clientDB.Tables[NameTable.TableResFFJam] as IDependentAsp).LoadByFilterAsync<TableResFFJam>(linkedStation);
                lResFHSSJam = await (clientDB.Tables[NameTable.TableResFHSSJam] as IDependentAsp).LoadByFilterAsync<TableResFHSSJam>(linkedStation);
                var lGlobalTable = await clientDB.Tables[NameTable.GlobalProperties].LoadAsync<GlobalProperties>();
                globalProp = lGlobalTable.FirstOrDefault(x=>x.Id ==1);
                //lResFF = await clientDB.Tables[NameTable.TableResFF].LoadAsync<TableResFF>();
                //lResFHSS = await clientDB.Tables[NameTable.TableResFHSS].LoadAsync<TableResFHSS>();
            }
            catch (ExceptionClient exeptClient)
            {
                MessageBox.Show(exeptClient.Message);
            }
            catch (ExceptionDatabase excpetService)
            {
                MessageBox.Show(excpetService.Message);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SendFreqRangesToSlave(byte bType, List<FreqRanges> records)
        {
            if (records == null || !cbxIsMaster.Checked) return;
            if (records.Count == 0) return;
            
            try
            {
                TRangeSpec[] tRangeSpec = new TRangeSpec[records.Count];
                for (int i = 0; i < tRangeSpec.Length; i++)
                {
                    tRangeSpec[i].iFregMin = (int)(records[i].FreqMinKHz*10);
                    tRangeSpec[i].iFregMax = (int)(records[i].FreqMaxKHz*10);
                }

                for (int i = 0; i < COUNT_CNT; i++)
                {
                    if (chbRRD[i].Checked)
                        objIEPC.SendRangeSpec(bTypeConnection[i], bAdrReceiver, bType, tRangeSpec);
                }
            }
            catch (SystemException)
            { }
        }

        private void SendSectorsRangesToSlave(byte bType, List<FreqRanges> records)
        {
            if (records == null || !cbxIsMaster.Checked) return;
            if (records.Count == 0) return;

            try
            {
                TRangeSector[] tRangeSector = new TRangeSector[records.Count];
                for (int i = 0; i < tRangeSector.Length; i++)
                {
                    tRangeSector[i].iFregMin = (int)(records[i].FreqMinKHz * 10);
                    tRangeSector[i].iFregMax = (int)(records[i].FreqMaxKHz * 10);
                }

                for (int i = 0; i < COUNT_CNT; i++)
                {
                    if (chbRRD[i].Checked)
                        objIEPC.SendRangeSector(bTypeConnection[i], bAdrReceiver, bType, tRangeSector);
                }
            }
            catch (SystemException)
            { }
        }

        private void SendSectorsRangesToSlave(byte bType, List<TableSectorsElint> records)
        {
            if (records == null || !cbxIsMaster.Checked) return;
            if (records.Count == 0) return;

            try
            {
                TRangeSector[] tRangeSector = new TRangeSector[records.Count];
                for (int i = 0; i < tRangeSector.Length; i++)
                {
                    tRangeSector[i].sAngleMin = (short)(records[i].AngleMin);
                    tRangeSector[i].sAngleMax = (short)(records[i].AngleMax);
                }

                for (int i = 0; i < COUNT_CNT; i++)
                {
                    if (chbRRD[i].Checked)
                        objIEPC.SendRangeSector(bTypeConnection[i], bAdrReceiver, bType, tRangeSector);
                }
            }
            catch (SystemException)
            { }
        }


        private void SendFWSSuppressToSlave(List<TableResFFJam> records)
        {
            if (records == null || !cbxIsMaster.Checked) return;
            if (records.Count == 0) return;

            try
            {
                TSupprFWS[] tSupprFWSTemp = new TSupprFWS[records.Count];
                for (int i = 0; i < records.Count; i++)
                {
                    tSupprFWSTemp[i].iFreq = (float)records[i].FrequencyKHz;
                    tSupprFWSTemp[i].bPrioritet = records[i].Priority;
                    tSupprFWSTemp[i].sBearing = (short)records[i].Bearing;
                    tSupprFWSTemp[i].bThreshold = (byte)Math.Abs(records[i].Threshold);

                    tSupprFWSTemp[i].bModulation = records[i].InterferenceParameters.Modulation;
                    tSupprFWSTemp[i].bDeviation = records[i].InterferenceParameters.Deviation;
                    tSupprFWSTemp[i].bManipulation = records[i].InterferenceParameters.Manipulation;
                    tSupprFWSTemp[i].bDuration = records[i].InterferenceParameters.Duration;
                }

                int iDurationGlobal = globalProp == null ? 500 : globalProp.TimeRadiationFF;

                for (int i = 0; i < COUNT_CNT; i++)
                {
                    if (chbRRD[i].Checked)
                        objIEPC.SendSupprFWS(bTypeConnection[i], bAdrReceiver, iDurationGlobal, tSupprFWSTemp);
                }
            }
            catch (SystemException)
            { }
        }

        private void SendFHSSSuppressToSlave(List<TableResFHSSJam> records)
        {
            if (records == null || !cbxIsMaster.Checked) return;
            if (records.Count == 0) return;

            try
            {
                TSupprFHSS[] tSupprFHSSTemp = new TSupprFHSS[records.Count];
                for (int i = 0; i < records.Count; i++)
                {
                    tSupprFHSSTemp[i].bCodeFFT = globalProp == null ? Convert.ToByte(cmbStepSupFHSS.SelectedIndex + 2) : (byte)globalProp.FhssFftResolution;

                    tSupprFHSSTemp[i].iFreqMin = (float)records[i].FrequencyMinKHz;
                    tSupprFHSSTemp[i].iFreqMax = (float)records[i].FrequencyMaxKHz;
                    
                    tSupprFHSSTemp[i].bModulation = records[i].InterferenceParameters.Modulation;
                    tSupprFHSSTemp[i].bDeviation = records[i].InterferenceParameters.Deviation;
                    tSupprFHSSTemp[i].bManipulation = records[i].InterferenceParameters.Manipulation;
                }

                int iDurationGlobal = globalProp == null ? 300 : globalProp.TimeRadiationFHSS;

                for (int i = 0; i < COUNT_CNT; i++)
                {
                    if (chbRRD[i].Checked)
                        objIEPC.SendSupprFHSS(bTypeConnection[i], bAdrReceiver, iDurationGlobal, tSupprFHSSTemp);
                }
            }
            catch (SystemException)
            { }
        }

        private void SendFFExecutingBearing(int freq)
        {
            if (iExBRZ_GRZ == null || !cbxIsMaster.Checked) return;

            try
            {
                int iID = Convert.ToInt32(nudFreqID.Value);

                for (int i = 0; i < COUNT_CNT; i++)
                {
                    if (chbRRD[i].Checked)
                        objIEPC.SendExecBear(bTypeConnection[i], bAdrReceiver, iID, freq);
                }
            }
            catch (SystemException)
            { }
        }


        private void SaveToDbFFExecutingBearing(float bearingLinked)
        {
            var hasOwn = lJammerStation.Find(x => x.IsOwn == true);
            var hasLinked = lJammerStation.Find(x => x.Role == StationRole.Linked);
            if (clientDB == null || active == null || hasOwn == null || hasLinked == null) return;

            try
            {
                active.BearingLinked = bearingLinked;
                active.Coordinates = CoordCalculator.CalculateCoord(active.BearingOwn, active.BearingLinked, hasOwn.Coordinates, hasLinked.Coordinates);
                active.IsActive = false;
                clientDB?.Tables[NameTable.TableResFFDistribution].Change(active);
            }
            catch (SystemException)
            { }
        }
        #endregion

        #region Server

        private Transmission.TransmissionClient client;
        private Channel channel;
        private DateTime Deadline => DateTime.UtcNow.AddSeconds(100);

        private delegate TResponse CommandDelegate<TRequest, TResponse>(
            TRequest request,
            Metadata headers = null,
            DateTime? deadline = null,
            CancellationToken cancellationToken = default);

        private async void InitConnectionServer()
        {
            channel = new Channel(mtbServerIp.Text, Convert.ToInt32(mtbServerPort.Text), ChannelCredentials.Insecure);
            client = new Transmission.TransmissionClient(this.channel);

            try
            {
                await client.PingAsync(new PingRequest() { Peer = new DefaultRequest() { PeerName = "OD server" } }, deadline: this.Deadline);
                stateIndicateConnectionServer.SetConnectOn();
                Task.Run(async () => await SubscribeServerState());
            }
            catch (Exception ex)
            {
                stateIndicateConnectionDB.SetConnectOff();
            }
        }


        private void InitDisconnectionServer()
        {
            channel.ShutdownAsync();
            stateIndicateConnectionServer.SetConnectOff();
        }

        private TResponse SendGprcCommandWithDeadline<TRequest, TResponse>(CommandDelegate<TRequest, TResponse> command, TRequest request)
            where TResponse : new()
        {
            try
            {
                var commandResponse = command(request: request, deadline: this.Deadline);
                stateIndicateConnectionServer.SetConnectOn();
                return commandResponse;
            }
            catch (RpcException)
            {
                stateIndicateConnectionServer.SetConnectOff();
                return new TResponse();
            }
        }



        //private bool Ping()
        //{
        //    var pingResponse = SendGprcCommandWithDeadline(client.Ping, new PingRequest());
        //    return true;
        //}

        //private bool SetMode(byte mode)
        //{
        //    var request = new SetModeRequest() { Mode = (DspServerMode)mode };
        //    var pingResponse = SendGprcCommandWithDeadline(client.SetMode, request);
        //    return true;
        //}

        //private byte GetMode()
        //{
        //    var pingResponse = SendGprcCommandWithDeadline(client.GetMode, new DefaultRequest());
        //    return (byte)pingResponse.Mode;
        //}

        private async Task SubscribeServerState()
        {
            using (var stream = client.SubscribeServerEvents(new DefaultRequest()))
            {
                var tokenSource = new CancellationTokenSource();
                await DisplayAsync(stream.ResponseStream, tokenSource.Token);

                tokenSource.Cancel();
            }
        }

        private async Task DisplayAsync(IAsyncStreamReader<ServerState> stream, CancellationToken token)
        {
            try
            {
                while (await stream.MoveNext(cancellationToken: token))
                {
                    try
                    {
                        var responce = stream.Current;
                        if (objIEPC != null)
                        {
                            byte bRegime = (byte)responce.CurrentMode;
                            
                            for (int i = 0; i < COUNT_CNT; i++)
                            {
                                if (chbRRD[i].Checked)
                                    objIEPC.SendRegime(bTypeConnection[i], bAdrReceiver, bRegime);
                            }

                        }
                    }
                    catch (Exception excp)
                    {
                        return;
                    }

                }
            }
            catch (Exception excp)
            {
                return;
            }
        }



        #endregion

        private void nudSupprClearFWS_Click(object sender, EventArgs e)
        {
            tSupprFWS = null;
            ShowSupprFWS(rtbMemoSupprFWS, tSupprFWS);
        }

        private void nudSupprClearFHSS_Click(object sender, EventArgs e)
        {
            tSupprFHSS = null;
            ShowSupprFHSS(rtbMemoSupprFHSS, tSupprFHSS);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            tbcCmdPC.Visible = !tbcCmdPC.Visible;
            pChooseRRD.Visible = !pChooseRRD.Visible;
        }
    }
}
