﻿namespace TestODServer
{
    partial class TestInformExchangeBRZ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pState = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.cbxIsMaster = new System.Windows.Forms.CheckBox();
            this.mtbServerPort = new System.Windows.Forms.MaskedTextBox();
            this.mtbServerIp = new System.Windows.Forms.MaskedTextBox();
            this.btnConnectServer = new System.Windows.Forms.Button();
            this.label54 = new System.Windows.Forms.Label();
            this.mtbDbPort = new System.Windows.Forms.MaskedTextBox();
            this.mtbDbIp = new System.Windows.Forms.MaskedTextBox();
            this.btnConnectDB = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.pParamCmd = new System.Windows.Forms.Panel();
            this.pChooseRRD = new System.Windows.Forms.Panel();
            this.nudAddressReceiver = new System.Windows.Forms.NumericUpDown();
            this.lAddressReceiver = new System.Windows.Forms.Label();
            this.chbVHF = new System.Windows.Forms.CheckBox();
            this.chbHF = new System.Windows.Forms.CheckBox();
            this.chbModem = new System.Windows.Forms.CheckBox();
            this.chb3G = new System.Windows.Forms.CheckBox();
            this.chbRRC = new System.Windows.Forms.CheckBox();
            this.tbcCmdPC = new System.Windows.Forms.TabControl();
            this.tbcRequest = new System.Windows.Forms.TabPage();
            this.bSendRequest = new System.Windows.Forms.Button();
            this.pRequest = new System.Windows.Forms.Panel();
            this.cmbTypeRequest = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.tbpRegime = new System.Windows.Forms.TabPage();
            this.bSendRegimeWork = new System.Windows.Forms.Button();
            this.pRegimeWork = new System.Windows.Forms.Panel();
            this.cmbRegime = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.tbpRangeRecon = new System.Windows.Forms.TabPage();
            this.chbAutoRangeR = new System.Windows.Forms.CheckBox();
            this.rtbMemoRangeRecon = new System.Windows.Forms.RichTextBox();
            this.bRangeReconClear = new System.Windows.Forms.Button();
            this.bRangeReconAdd = new System.Windows.Forms.Button();
            this.bSendReconSectorRange = new System.Windows.Forms.Button();
            this.pReconSectorRange = new System.Windows.Forms.Panel();
            this.nudBearingMaxRecon = new System.Windows.Forms.NumericUpDown();
            this.label46 = new System.Windows.Forms.Label();
            this.nudBearingMinRecon = new System.Windows.Forms.NumericUpDown();
            this.label47 = new System.Windows.Forms.Label();
            this.tbFreqMaxRecon = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.tbFreqMinRecon = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.tbcRangeSuppress = new System.Windows.Forms.TabPage();
            this.chbAutoRangeS = new System.Windows.Forms.CheckBox();
            this.rtbMemoRangeSuppr = new System.Windows.Forms.RichTextBox();
            this.bRangeSupprClear = new System.Windows.Forms.Button();
            this.bRangeSupprAdd = new System.Windows.Forms.Button();
            this.bSendSupressSectorRange = new System.Windows.Forms.Button();
            this.pSupressSectorRange = new System.Windows.Forms.Panel();
            this.nudBearingMaxSupress = new System.Windows.Forms.NumericUpDown();
            this.label48 = new System.Windows.Forms.Label();
            this.nudBearingMinSupress = new System.Windows.Forms.NumericUpDown();
            this.label49 = new System.Windows.Forms.Label();
            this.tbFreqMaxSupress = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.tbFreqMinSupress = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.tbcRangeForbid = new System.Windows.Forms.TabPage();
            this.bSendForbidRangeFreq = new System.Windows.Forms.Button();
            this.pForbidRangeFreq = new System.Windows.Forms.Panel();
            this.tbFreqMaxForbid = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.tbFreqMinForbid = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.cmbActForbid = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.tbpSynchronizePC = new System.Windows.Forms.TabPage();
            this.bSendSynchronizePC = new System.Windows.Forms.Button();
            this.pSynchronizePC = new System.Windows.Forms.Panel();
            this.dtpTimeSinchroPC = new System.Windows.Forms.DateTimePicker();
            this.label55 = new System.Windows.Forms.Label();
            this.tbpTextPC = new System.Windows.Forms.TabPage();
            this.bSendTextMessagePC = new System.Windows.Forms.Button();
            this.pTextMessagePC = new System.Windows.Forms.Panel();
            this.rtbTextPC = new System.Windows.Forms.RichTextBox();
            this.tbcSuppressFWS = new System.Windows.Forms.TabPage();
            this.chbAutoSupprFWS = new System.Windows.Forms.CheckBox();
            this.rtbMemoSupprFWS = new System.Windows.Forms.RichTextBox();
            this.nudSupprClearFWS = new System.Windows.Forms.Button();
            this.nudSupprAddFWS = new System.Windows.Forms.Button();
            this.bSendSupressFWS = new System.Windows.Forms.Button();
            this.pSupressFWS = new System.Windows.Forms.Panel();
            this.nudBearingFWS = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.nudThresholdFWS = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbManipulationFWS = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nudDurationGlobalFWS = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbDeviationFWS = new System.Windows.Forms.ComboBox();
            this.lDeviationFWS = new System.Windows.Forms.Label();
            this.cmbTypeModulationFWS = new System.Windows.Forms.ComboBox();
            this.lTypeModulationFWS = new System.Windows.Forms.Label();
            this.nudPriorSupFWS = new System.Windows.Forms.NumericUpDown();
            this.label59 = new System.Windows.Forms.Label();
            this.tbFreqSupFWS = new System.Windows.Forms.TextBox();
            this.label57 = new System.Windows.Forms.Label();
            this.tbcSuppressFHSS = new System.Windows.Forms.TabPage();
            this.chbAutoSupprFHSS = new System.Windows.Forms.CheckBox();
            this.rtbMemoSupprFHSS = new System.Windows.Forms.RichTextBox();
            this.nudSupprClearFHSS = new System.Windows.Forms.Button();
            this.bSupprAddFHSS = new System.Windows.Forms.Button();
            this.bSendSupressFHSS = new System.Windows.Forms.Button();
            this.pSupressFHSS = new System.Windows.Forms.Panel();
            this.tbFreqMaxSupFHSS = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.nudDuratSupFHSS = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbManipulation = new System.Windows.Forms.ComboBox();
            this.lManipulation = new System.Windows.Forms.Label();
            this.cmbDeviation = new System.Windows.Forms.ComboBox();
            this.lDeviation = new System.Windows.Forms.Label();
            this.cmbTypeModulation = new System.Windows.Forms.ComboBox();
            this.lTypeModulation = new System.Windows.Forms.Label();
            this.cmbStepSupFHSS = new System.Windows.Forms.ComboBox();
            this.label68 = new System.Windows.Forms.Label();
            this.tbFreqMinSupFHSS = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.tbcRequestBearing = new System.Windows.Forms.TabPage();
            this.bSendRequestBearing = new System.Windows.Forms.Button();
            this.pRequestBearing = new System.Windows.Forms.Panel();
            this.nudFreqID = new System.Windows.Forms.NumericUpDown();
            this.label23 = new System.Windows.Forms.Label();
            this.tbFreqRecBearing = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.cmbTypeReqBearing = new System.Windows.Forms.ComboBox();
            this.label71 = new System.Windows.Forms.Label();
            this.tbpReceiptPC = new System.Windows.Forms.TabPage();
            this.bSendReceiptPC = new System.Windows.Forms.Button();
            this.pReceiptPC = new System.Windows.Forms.Panel();
            this.nudCodeErrorPC = new System.Windows.Forms.NumericUpDown();
            this.label69 = new System.Windows.Forms.Label();
            this.pLog = new System.Windows.Forms.Panel();
            this.tbcLog = new System.Windows.Forms.TabControl();
            this.tbpRRD1 = new System.Windows.Forms.TabPage();
            this.button1 = new System.Windows.Forms.Button();
            this.rtbLogRRC = new System.Windows.Forms.RichTextBox();
            this.tbpRRD2 = new System.Windows.Forms.TabPage();
            this.rtbLog3G = new System.Windows.Forms.RichTextBox();
            this.tbpRRD3 = new System.Windows.Forms.TabPage();
            this.rtbLogModem = new System.Windows.Forms.RichTextBox();
            this.tbpRRD4 = new System.Windows.Forms.TabPage();
            this.rtbLogHF = new System.Windows.Forms.RichTextBox();
            this.tbpRRD5 = new System.Windows.Forms.TabPage();
            this.rtbLogVHF = new System.Windows.Forms.RichTextBox();
            this.pShowLog = new System.Windows.Forms.Panel();
            this.chbByteHex = new System.Windows.Forms.CheckBox();
            this.chbDetail = new System.Windows.Forms.CheckBox();
            this.chbName = new System.Windows.Forms.CheckBox();
            this.pStateConnection = new System.Windows.Forms.Panel();
            this.pCntUpd = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.lVHF = new System.Windows.Forms.Label();
            this.cmbComVHF = new System.Windows.Forms.ComboBox();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.bConnectVHF = new System.Windows.Forms.Button();
            this.panel29 = new System.Windows.Forms.Panel();
            this.pbWriteByteVHF = new System.Windows.Forms.PictureBox();
            this.pbReadByteVHF = new System.Windows.Forms.PictureBox();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.lHF = new System.Windows.Forms.Label();
            this.cmbComHF = new System.Windows.Forms.ComboBox();
            this.panel24 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.bConnectHF = new System.Windows.Forms.Button();
            this.panel25 = new System.Windows.Forms.Panel();
            this.pbWriteByteHF = new System.Windows.Forms.PictureBox();
            this.pbReadByteHF = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.lModem = new System.Windows.Forms.Label();
            this.cmbComModem = new System.Windows.Forms.ComboBox();
            this.panel19 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.bConnectModem = new System.Windows.Forms.Button();
            this.panel20 = new System.Windows.Forms.Panel();
            this.pbWriteByteModem = new System.Windows.Forms.PictureBox();
            this.pbReadByteModem = new System.Windows.Forms.PictureBox();
            this.panel34 = new System.Windows.Forms.Panel();
            this.pConnectClient3G = new System.Windows.Forms.Panel();
            this.pCreateServer3G = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.lServer3G = new System.Windows.Forms.Label();
            this.panel36 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.bConnect3G = new System.Windows.Forms.Button();
            this.panel37 = new System.Windows.Forms.Panel();
            this.pbWriteByte3G = new System.Windows.Forms.PictureBox();
            this.pbReadByte3G = new System.Windows.Forms.PictureBox();
            this.panel30 = new System.Windows.Forms.Panel();
            this.pConnectClientRRC = new System.Windows.Forms.Panel();
            this.rtbClientConnect = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.bConnectClientRRC6 = new System.Windows.Forms.Button();
            this.panel17 = new System.Windows.Forms.Panel();
            this.pbWriteByteClientRRC6 = new System.Windows.Forms.PictureBox();
            this.pbReadByteClientRRC6 = new System.Windows.Forms.PictureBox();
            this.pClientRRC5 = new System.Windows.Forms.Panel();
            this.panel18 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.bConnectClientRRC5 = new System.Windows.Forms.Button();
            this.panel39 = new System.Windows.Forms.Panel();
            this.pbWriteByteClientRRC5 = new System.Windows.Forms.PictureBox();
            this.pbReadByteClientRRC5 = new System.Windows.Forms.PictureBox();
            this.pClientRRC4 = new System.Windows.Forms.Panel();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.bConnectClientRRC4 = new System.Windows.Forms.Button();
            this.panel16 = new System.Windows.Forms.Panel();
            this.pbWriteByteClientRRC4 = new System.Windows.Forms.PictureBox();
            this.pbReadByteClientRRC4 = new System.Windows.Forms.PictureBox();
            this.pClientRRC3 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.bConnectClientRRC3 = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.pbWriteByteClientRRC3 = new System.Windows.Forms.PictureBox();
            this.pbReadByteClientRRC3 = new System.Windows.Forms.PictureBox();
            this.pClientRRC2 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.bConnectClientRRC2 = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.pbWriteByteClientRRC2 = new System.Windows.Forms.PictureBox();
            this.pbReadByteClientRRC2 = new System.Windows.Forms.PictureBox();
            this.pClientRRC1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lClient1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.bConnectClientRRC1 = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.pbWriteByteClientRRC1 = new System.Windows.Forms.PictureBox();
            this.pbReadByteClientRRC1 = new System.Windows.Forms.PictureBox();
            this.pCreateServerRRC = new System.Windows.Forms.Panel();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.cmbxServerIP = new System.Windows.Forms.ComboBox();
            this.panel31 = new System.Windows.Forms.Panel();
            this.lServerRRC = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.bConnectRRC = new System.Windows.Forms.Button();
            this.panel33 = new System.Windows.Forms.Panel();
            this.pbWriteByteRRC = new System.Windows.Forms.PictureBox();
            this.pbReadByteRRC = new System.Windows.Forms.PictureBox();
            this.pState.SuspendLayout();
            this.pParamCmd.SuspendLayout();
            this.pChooseRRD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAddressReceiver)).BeginInit();
            this.tbcCmdPC.SuspendLayout();
            this.tbcRequest.SuspendLayout();
            this.pRequest.SuspendLayout();
            this.tbpRegime.SuspendLayout();
            this.pRegimeWork.SuspendLayout();
            this.tbpRangeRecon.SuspendLayout();
            this.pReconSectorRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMaxRecon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMinRecon)).BeginInit();
            this.tbcRangeSuppress.SuspendLayout();
            this.pSupressSectorRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMaxSupress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMinSupress)).BeginInit();
            this.tbcRangeForbid.SuspendLayout();
            this.pForbidRangeFreq.SuspendLayout();
            this.tbpSynchronizePC.SuspendLayout();
            this.pSynchronizePC.SuspendLayout();
            this.tbpTextPC.SuspendLayout();
            this.pTextMessagePC.SuspendLayout();
            this.tbcSuppressFWS.SuspendLayout();
            this.pSupressFWS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresholdFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDurationGlobalFWS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPriorSupFWS)).BeginInit();
            this.tbcSuppressFHSS.SuspendLayout();
            this.pSupressFHSS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDuratSupFHSS)).BeginInit();
            this.tbcRequestBearing.SuspendLayout();
            this.pRequestBearing.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqID)).BeginInit();
            this.tbpReceiptPC.SuspendLayout();
            this.pReceiptPC.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorPC)).BeginInit();
            this.pLog.SuspendLayout();
            this.tbcLog.SuspendLayout();
            this.tbpRRD1.SuspendLayout();
            this.tbpRRD2.SuspendLayout();
            this.tbpRRD3.SuspendLayout();
            this.tbpRRD4.SuspendLayout();
            this.tbpRRD5.SuspendLayout();
            this.pShowLog.SuspendLayout();
            this.pStateConnection.SuspendLayout();
            this.pCntUpd.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel28.SuspendLayout();
            this.panel29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteVHF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteVHF)).BeginInit();
            this.panel22.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel24.SuspendLayout();
            this.panel25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteHF)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteHF)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteModem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteModem)).BeginInit();
            this.panel34.SuspendLayout();
            this.pCreateServer3G.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByte3G)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByte3G)).BeginInit();
            this.panel30.SuspendLayout();
            this.pConnectClientRRC.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC6)).BeginInit();
            this.pClientRRC5.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC5)).BeginInit();
            this.pClientRRC4.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC4)).BeginInit();
            this.pClientRRC3.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC3)).BeginInit();
            this.pClientRRC2.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC2)).BeginInit();
            this.pClientRRC1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC1)).BeginInit();
            this.pCreateServerRRC.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel32.SuspendLayout();
            this.panel33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteRRC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteRRC)).BeginInit();
            this.SuspendLayout();
            // 
            // pState
            // 
            this.pState.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pState.Controls.Add(this.button4);
            this.pState.Controls.Add(this.cbxIsMaster);
            this.pState.Controls.Add(this.mtbServerPort);
            this.pState.Controls.Add(this.mtbServerIp);
            this.pState.Controls.Add(this.btnConnectServer);
            this.pState.Controls.Add(this.label54);
            this.pState.Controls.Add(this.mtbDbPort);
            this.pState.Controls.Add(this.mtbDbIp);
            this.pState.Controls.Add(this.btnConnectDB);
            this.pState.Controls.Add(this.label53);
            this.pState.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pState.Location = new System.Drawing.Point(0, 461);
            this.pState.Name = "pState";
            this.pState.Size = new System.Drawing.Size(886, 30);
            this.pState.TabIndex = 1;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(3, 4);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(20, 20);
            this.button4.TabIndex = 119;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // cbxIsMaster
            // 
            this.cbxIsMaster.AutoSize = true;
            this.cbxIsMaster.Location = new System.Drawing.Point(26, 7);
            this.cbxIsMaster.Name = "cbxIsMaster";
            this.cbxIsMaster.Size = new System.Drawing.Size(71, 17);
            this.cbxIsMaster.TabIndex = 118;
            this.cbxIsMaster.Text = "Ведущая";
            this.cbxIsMaster.UseVisualStyleBackColor = true;
            // 
            // mtbServerPort
            // 
            this.mtbServerPort.Location = new System.Drawing.Point(483, 5);
            this.mtbServerPort.Name = "mtbServerPort";
            this.mtbServerPort.Size = new System.Drawing.Size(48, 20);
            this.mtbServerPort.TabIndex = 117;
            this.mtbServerPort.Text = "5900";
            // 
            // mtbServerIp
            // 
            this.mtbServerIp.Location = new System.Drawing.Point(385, 5);
            this.mtbServerIp.Name = "mtbServerIp";
            this.mtbServerIp.Size = new System.Drawing.Size(92, 20);
            this.mtbServerIp.TabIndex = 116;
            this.mtbServerIp.Text = "127.0.0.1";
            // 
            // btnConnectServer
            // 
            this.btnConnectServer.BackColor = System.Drawing.Color.Red;
            this.btnConnectServer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnConnectServer.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnConnectServer.FlatAppearance.BorderSize = 0;
            this.btnConnectServer.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnConnectServer.Location = new System.Drawing.Point(357, 3);
            this.btnConnectServer.Name = "btnConnectServer";
            this.btnConnectServer.Size = new System.Drawing.Size(22, 22);
            this.btnConnectServer.TabIndex = 115;
            this.btnConnectServer.UseVisualStyleBackColor = false;
            this.btnConnectServer.Click += new System.EventHandler(this.btnConnectServer_Click_1);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(311, 10);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(44, 13);
            this.label54.TabIndex = 114;
            this.label54.Text = "Сервер";
            // 
            // mtbDbPort
            // 
            this.mtbDbPort.Location = new System.Drawing.Point(257, 5);
            this.mtbDbPort.Name = "mtbDbPort";
            this.mtbDbPort.Size = new System.Drawing.Size(48, 20);
            this.mtbDbPort.TabIndex = 113;
            this.mtbDbPort.Text = "30051";
            // 
            // mtbDbIp
            // 
            this.mtbDbIp.Location = new System.Drawing.Point(159, 5);
            this.mtbDbIp.Name = "mtbDbIp";
            this.mtbDbIp.Size = new System.Drawing.Size(92, 20);
            this.mtbDbIp.TabIndex = 112;
            this.mtbDbIp.Text = "127.0.0.1";
            // 
            // btnConnectDB
            // 
            this.btnConnectDB.BackColor = System.Drawing.Color.Red;
            this.btnConnectDB.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnConnectDB.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnConnectDB.FlatAppearance.BorderSize = 0;
            this.btnConnectDB.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnConnectDB.Location = new System.Drawing.Point(131, 3);
            this.btnConnectDB.Name = "btnConnectDB";
            this.btnConnectDB.Size = new System.Drawing.Size(22, 22);
            this.btnConnectDB.TabIndex = 111;
            this.btnConnectDB.UseVisualStyleBackColor = false;
            this.btnConnectDB.Click += new System.EventHandler(this.btnConnectDB_Click);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(102, 10);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(23, 13);
            this.label53.TabIndex = 110;
            this.label53.Text = "БД";
            // 
            // pParamCmd
            // 
            this.pParamCmd.Controls.Add(this.pChooseRRD);
            this.pParamCmd.Controls.Add(this.tbcCmdPC);
            this.pParamCmd.Dock = System.Windows.Forms.DockStyle.Left;
            this.pParamCmd.Location = new System.Drawing.Point(0, 0);
            this.pParamCmd.Name = "pParamCmd";
            this.pParamCmd.Size = new System.Drawing.Size(263, 461);
            this.pParamCmd.TabIndex = 9;
            // 
            // pChooseRRD
            // 
            this.pChooseRRD.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pChooseRRD.Controls.Add(this.nudAddressReceiver);
            this.pChooseRRD.Controls.Add(this.lAddressReceiver);
            this.pChooseRRD.Controls.Add(this.chbVHF);
            this.pChooseRRD.Controls.Add(this.chbHF);
            this.pChooseRRD.Controls.Add(this.chbModem);
            this.pChooseRRD.Controls.Add(this.chb3G);
            this.pChooseRRD.Controls.Add(this.chbRRC);
            this.pChooseRRD.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pChooseRRD.Location = new System.Drawing.Point(0, 431);
            this.pChooseRRD.Name = "pChooseRRD";
            this.pChooseRRD.Size = new System.Drawing.Size(263, 30);
            this.pChooseRRD.TabIndex = 95;
            this.pChooseRRD.Visible = false;
            // 
            // nudAddressReceiver
            // 
            this.nudAddressReceiver.Location = new System.Drawing.Point(205, 4);
            this.nudAddressReceiver.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.nudAddressReceiver.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudAddressReceiver.Name = "nudAddressReceiver";
            this.nudAddressReceiver.Size = new System.Drawing.Size(50, 20);
            this.nudAddressReceiver.TabIndex = 7;
            this.nudAddressReceiver.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudAddressReceiver.ValueChanged += new System.EventHandler(this.nudAddressReceiver_ValueChanged);
            // 
            // lAddressReceiver
            // 
            this.lAddressReceiver.AutoSize = true;
            this.lAddressReceiver.Location = new System.Drawing.Point(149, 11);
            this.lAddressReceiver.Name = "lAddressReceiver";
            this.lAddressReceiver.Size = new System.Drawing.Size(59, 13);
            this.lAddressReceiver.TabIndex = 6;
            this.lAddressReceiver.Text = "Адрес.......";
            // 
            // chbVHF
            // 
            this.chbVHF.Appearance = System.Windows.Forms.Appearance.Button;
            this.chbVHF.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chbVHF.Location = new System.Drawing.Point(115, 3);
            this.chbVHF.Name = "chbVHF";
            this.chbVHF.Size = new System.Drawing.Size(21, 21);
            this.chbVHF.TabIndex = 4;
            this.chbVHF.Text = "5";
            this.chbVHF.UseVisualStyleBackColor = false;
            this.chbVHF.CheckedChanged += new System.EventHandler(this.chbRRD1_CheckedChanged);
            // 
            // chbHF
            // 
            this.chbHF.Appearance = System.Windows.Forms.Appearance.Button;
            this.chbHF.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chbHF.Location = new System.Drawing.Point(88, 3);
            this.chbHF.Name = "chbHF";
            this.chbHF.Size = new System.Drawing.Size(21, 21);
            this.chbHF.TabIndex = 3;
            this.chbHF.Text = "4";
            this.chbHF.UseVisualStyleBackColor = false;
            this.chbHF.CheckedChanged += new System.EventHandler(this.chbRRD1_CheckedChanged);
            // 
            // chbModem
            // 
            this.chbModem.Appearance = System.Windows.Forms.Appearance.Button;
            this.chbModem.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chbModem.Location = new System.Drawing.Point(61, 3);
            this.chbModem.Name = "chbModem";
            this.chbModem.Size = new System.Drawing.Size(21, 21);
            this.chbModem.TabIndex = 2;
            this.chbModem.Text = "3";
            this.chbModem.UseVisualStyleBackColor = false;
            this.chbModem.CheckedChanged += new System.EventHandler(this.chbRRD1_CheckedChanged);
            // 
            // chb3G
            // 
            this.chb3G.Appearance = System.Windows.Forms.Appearance.Button;
            this.chb3G.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chb3G.Location = new System.Drawing.Point(34, 3);
            this.chb3G.Name = "chb3G";
            this.chb3G.Size = new System.Drawing.Size(21, 21);
            this.chb3G.TabIndex = 1;
            this.chb3G.Text = "2";
            this.chb3G.UseVisualStyleBackColor = false;
            this.chb3G.CheckedChanged += new System.EventHandler(this.chbRRD1_CheckedChanged);
            // 
            // chbRRC
            // 
            this.chbRRC.Appearance = System.Windows.Forms.Appearance.Button;
            this.chbRRC.Checked = true;
            this.chbRRC.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbRRC.FlatAppearance.CheckedBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.chbRRC.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.chbRRC.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.chbRRC.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.chbRRC.Location = new System.Drawing.Point(7, 3);
            this.chbRRC.Name = "chbRRC";
            this.chbRRC.Size = new System.Drawing.Size(21, 21);
            this.chbRRC.TabIndex = 0;
            this.chbRRC.Text = "1";
            this.chbRRC.UseVisualStyleBackColor = false;
            this.chbRRC.CheckedChanged += new System.EventHandler(this.chbRRD1_CheckedChanged);
            // 
            // tbcCmdPC
            // 
            this.tbcCmdPC.Controls.Add(this.tbcRequest);
            this.tbcCmdPC.Controls.Add(this.tbpRegime);
            this.tbcCmdPC.Controls.Add(this.tbpRangeRecon);
            this.tbcCmdPC.Controls.Add(this.tbcRangeSuppress);
            this.tbcCmdPC.Controls.Add(this.tbcRangeForbid);
            this.tbcCmdPC.Controls.Add(this.tbpSynchronizePC);
            this.tbcCmdPC.Controls.Add(this.tbpTextPC);
            this.tbcCmdPC.Controls.Add(this.tbcSuppressFWS);
            this.tbcCmdPC.Controls.Add(this.tbcSuppressFHSS);
            this.tbcCmdPC.Controls.Add(this.tbcRequestBearing);
            this.tbcCmdPC.Controls.Add(this.tbpReceiptPC);
            this.tbcCmdPC.Dock = System.Windows.Forms.DockStyle.Top;
            this.tbcCmdPC.Location = new System.Drawing.Point(0, 0);
            this.tbcCmdPC.Multiline = true;
            this.tbcCmdPC.Name = "tbcCmdPC";
            this.tbcCmdPC.SelectedIndex = 0;
            this.tbcCmdPC.Size = new System.Drawing.Size(263, 434);
            this.tbcCmdPC.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tbcCmdPC.TabIndex = 94;
            // 
            // tbcRequest
            // 
            this.tbcRequest.BackColor = System.Drawing.SystemColors.Control;
            this.tbcRequest.Controls.Add(this.bSendRequest);
            this.tbcRequest.Controls.Add(this.pRequest);
            this.tbcRequest.Location = new System.Drawing.Point(4, 76);
            this.tbcRequest.Name = "tbcRequest";
            this.tbcRequest.Padding = new System.Windows.Forms.Padding(3);
            this.tbcRequest.Size = new System.Drawing.Size(255, 354);
            this.tbcRequest.TabIndex = 0;
            this.tbcRequest.Text = "Запрос";
            // 
            // bSendRequest
            // 
            this.bSendRequest.Location = new System.Drawing.Point(175, 327);
            this.bSendRequest.Name = "bSendRequest";
            this.bSendRequest.Size = new System.Drawing.Size(75, 23);
            this.bSendRequest.TabIndex = 35;
            this.bSendRequest.Text = "Отправить";
            this.bSendRequest.UseVisualStyleBackColor = true;
            this.bSendRequest.Click += new System.EventHandler(this.bSendRequest_Click);
            // 
            // pRequest
            // 
            this.pRequest.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pRequest.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pRequest.Controls.Add(this.cmbTypeRequest);
            this.pRequest.Controls.Add(this.label35);
            this.pRequest.Location = new System.Drawing.Point(3, 3);
            this.pRequest.Name = "pRequest";
            this.pRequest.Size = new System.Drawing.Size(245, 31);
            this.pRequest.TabIndex = 18;
            // 
            // cmbTypeRequest
            // 
            this.cmbTypeRequest.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeRequest.FormattingEnabled = true;
            this.cmbTypeRequest.Location = new System.Drawing.Point(97, 4);
            this.cmbTypeRequest.Name = "cmbTypeRequest";
            this.cmbTypeRequest.Size = new System.Drawing.Size(142, 21);
            this.cmbTypeRequest.TabIndex = 19;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(2, 12);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(176, 13);
            this.label35.TabIndex = 18;
            this.label35.Text = "Тип запроса...................................";
            // 
            // tbpRegime
            // 
            this.tbpRegime.BackColor = System.Drawing.SystemColors.Control;
            this.tbpRegime.Controls.Add(this.bSendRegimeWork);
            this.tbpRegime.Controls.Add(this.pRegimeWork);
            this.tbpRegime.Location = new System.Drawing.Point(4, 76);
            this.tbpRegime.Name = "tbpRegime";
            this.tbpRegime.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRegime.Size = new System.Drawing.Size(255, 354);
            this.tbpRegime.TabIndex = 1;
            this.tbpRegime.Text = "Режим";
            // 
            // bSendRegimeWork
            // 
            this.bSendRegimeWork.Location = new System.Drawing.Point(175, 327);
            this.bSendRegimeWork.Name = "bSendRegimeWork";
            this.bSendRegimeWork.Size = new System.Drawing.Size(75, 23);
            this.bSendRegimeWork.TabIndex = 36;
            this.bSendRegimeWork.Text = "Отправить";
            this.bSendRegimeWork.UseVisualStyleBackColor = true;
            this.bSendRegimeWork.Click += new System.EventHandler(this.bSendRegimeWork_Click);
            // 
            // pRegimeWork
            // 
            this.pRegimeWork.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pRegimeWork.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pRegimeWork.Controls.Add(this.cmbRegime);
            this.pRegimeWork.Controls.Add(this.label37);
            this.pRegimeWork.Location = new System.Drawing.Point(6, 6);
            this.pRegimeWork.Name = "pRegimeWork";
            this.pRegimeWork.Size = new System.Drawing.Size(245, 31);
            this.pRegimeWork.TabIndex = 20;
            // 
            // cmbRegime
            // 
            this.cmbRegime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRegime.FormattingEnabled = true;
            this.cmbRegime.Location = new System.Drawing.Point(117, 4);
            this.cmbRegime.Name = "cmbRegime";
            this.cmbRegime.Size = new System.Drawing.Size(122, 21);
            this.cmbRegime.TabIndex = 19;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(2, 12);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(187, 13);
            this.label37.TabIndex = 18;
            this.label37.Text = "Режим работы...................................";
            // 
            // tbpRangeRecon
            // 
            this.tbpRangeRecon.BackColor = System.Drawing.SystemColors.Control;
            this.tbpRangeRecon.Controls.Add(this.chbAutoRangeR);
            this.tbpRangeRecon.Controls.Add(this.rtbMemoRangeRecon);
            this.tbpRangeRecon.Controls.Add(this.bRangeReconClear);
            this.tbpRangeRecon.Controls.Add(this.bRangeReconAdd);
            this.tbpRangeRecon.Controls.Add(this.bSendReconSectorRange);
            this.tbpRangeRecon.Controls.Add(this.pReconSectorRange);
            this.tbpRangeRecon.Location = new System.Drawing.Point(4, 76);
            this.tbpRangeRecon.Name = "tbpRangeRecon";
            this.tbpRangeRecon.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRangeRecon.Size = new System.Drawing.Size(255, 354);
            this.tbpRangeRecon.TabIndex = 2;
            this.tbpRangeRecon.Text = "Диапазон РР";
            // 
            // chbAutoRangeR
            // 
            this.chbAutoRangeR.AutoSize = true;
            this.chbAutoRangeR.Location = new System.Drawing.Point(15, 115);
            this.chbAutoRangeR.Name = "chbAutoRangeR";
            this.chbAutoRangeR.Size = new System.Drawing.Size(50, 17);
            this.chbAutoRangeR.TabIndex = 41;
            this.chbAutoRangeR.Text = "Авто";
            this.chbAutoRangeR.UseVisualStyleBackColor = true;
            // 
            // rtbMemoRangeRecon
            // 
            this.rtbMemoRangeRecon.BackColor = System.Drawing.SystemColors.Control;
            this.rtbMemoRangeRecon.Location = new System.Drawing.Point(6, 138);
            this.rtbMemoRangeRecon.Name = "rtbMemoRangeRecon";
            this.rtbMemoRangeRecon.ReadOnly = true;
            this.rtbMemoRangeRecon.Size = new System.Drawing.Size(241, 171);
            this.rtbMemoRangeRecon.TabIndex = 40;
            this.rtbMemoRangeRecon.Text = "";
            // 
            // bRangeReconClear
            // 
            this.bRangeReconClear.Location = new System.Drawing.Point(90, 109);
            this.bRangeReconClear.Name = "bRangeReconClear";
            this.bRangeReconClear.Size = new System.Drawing.Size(75, 23);
            this.bRangeReconClear.TabIndex = 39;
            this.bRangeReconClear.Text = "Очистить";
            this.bRangeReconClear.UseVisualStyleBackColor = true;
            this.bRangeReconClear.Click += new System.EventHandler(this.bRangeReconClear_Click);
            // 
            // bRangeReconAdd
            // 
            this.bRangeReconAdd.Location = new System.Drawing.Point(171, 109);
            this.bRangeReconAdd.Name = "bRangeReconAdd";
            this.bRangeReconAdd.Size = new System.Drawing.Size(75, 23);
            this.bRangeReconAdd.TabIndex = 38;
            this.bRangeReconAdd.Text = "Добавить";
            this.bRangeReconAdd.UseVisualStyleBackColor = true;
            this.bRangeReconAdd.Click += new System.EventHandler(this.bRangeReconAdd_Click);
            // 
            // bSendReconSectorRange
            // 
            this.bSendReconSectorRange.Location = new System.Drawing.Point(175, 327);
            this.bSendReconSectorRange.Name = "bSendReconSectorRange";
            this.bSendReconSectorRange.Size = new System.Drawing.Size(75, 23);
            this.bSendReconSectorRange.TabIndex = 37;
            this.bSendReconSectorRange.Text = "Отправить";
            this.bSendReconSectorRange.UseVisualStyleBackColor = true;
            this.bSendReconSectorRange.Click += new System.EventHandler(this.bSendReconSectorRange_Click);
            // 
            // pReconSectorRange
            // 
            this.pReconSectorRange.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pReconSectorRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pReconSectorRange.Controls.Add(this.nudBearingMaxRecon);
            this.pReconSectorRange.Controls.Add(this.label46);
            this.pReconSectorRange.Controls.Add(this.nudBearingMinRecon);
            this.pReconSectorRange.Controls.Add(this.label47);
            this.pReconSectorRange.Controls.Add(this.tbFreqMaxRecon);
            this.pReconSectorRange.Controls.Add(this.label42);
            this.pReconSectorRange.Controls.Add(this.tbFreqMinRecon);
            this.pReconSectorRange.Controls.Add(this.label43);
            this.pReconSectorRange.Location = new System.Drawing.Point(6, 6);
            this.pReconSectorRange.Name = "pReconSectorRange";
            this.pReconSectorRange.Size = new System.Drawing.Size(245, 97);
            this.pReconSectorRange.TabIndex = 21;
            // 
            // nudBearingMaxRecon
            // 
            this.nudBearingMaxRecon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearingMaxRecon.Location = new System.Drawing.Point(166, 25);
            this.nudBearingMaxRecon.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudBearingMaxRecon.Name = "nudBearingMaxRecon";
            this.nudBearingMaxRecon.Size = new System.Drawing.Size(74, 20);
            this.nudBearingMaxRecon.TabIndex = 29;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(5, 32);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(171, 13);
            this.label46.TabIndex = 28;
            this.label46.Text = "Угол макс, гр...............................";
            // 
            // nudBearingMinRecon
            // 
            this.nudBearingMinRecon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearingMinRecon.Location = new System.Drawing.Point(167, 3);
            this.nudBearingMinRecon.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudBearingMinRecon.Name = "nudBearingMinRecon";
            this.nudBearingMinRecon.Size = new System.Drawing.Size(73, 20);
            this.nudBearingMinRecon.TabIndex = 27;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(5, 10);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(168, 13);
            this.label47.TabIndex = 26;
            this.label47.Text = "Угол мин, гр................................";
            // 
            // tbFreqMaxRecon
            // 
            this.tbFreqMaxRecon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqMaxRecon.Location = new System.Drawing.Point(166, 69);
            this.tbFreqMaxRecon.Name = "tbFreqMaxRecon";
            this.tbFreqMaxRecon.Size = new System.Drawing.Size(73, 20);
            this.tbFreqMaxRecon.TabIndex = 25;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(3, 76);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(192, 13);
            this.label42.TabIndex = 24;
            this.label42.Text = "Частота макс,кГц...............................";
            // 
            // tbFreqMinRecon
            // 
            this.tbFreqMinRecon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqMinRecon.Location = new System.Drawing.Point(166, 47);
            this.tbFreqMinRecon.Name = "tbFreqMinRecon";
            this.tbFreqMinRecon.Size = new System.Drawing.Size(73, 20);
            this.tbFreqMinRecon.TabIndex = 23;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(3, 54);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(186, 13);
            this.label43.TabIndex = 22;
            this.label43.Text = "Частота мин,кГц...............................";
            // 
            // tbcRangeSuppress
            // 
            this.tbcRangeSuppress.BackColor = System.Drawing.SystemColors.Control;
            this.tbcRangeSuppress.Controls.Add(this.chbAutoRangeS);
            this.tbcRangeSuppress.Controls.Add(this.rtbMemoRangeSuppr);
            this.tbcRangeSuppress.Controls.Add(this.bRangeSupprClear);
            this.tbcRangeSuppress.Controls.Add(this.bRangeSupprAdd);
            this.tbcRangeSuppress.Controls.Add(this.bSendSupressSectorRange);
            this.tbcRangeSuppress.Controls.Add(this.pSupressSectorRange);
            this.tbcRangeSuppress.Location = new System.Drawing.Point(4, 76);
            this.tbcRangeSuppress.Name = "tbcRangeSuppress";
            this.tbcRangeSuppress.Padding = new System.Windows.Forms.Padding(3);
            this.tbcRangeSuppress.Size = new System.Drawing.Size(255, 354);
            this.tbcRangeSuppress.TabIndex = 3;
            this.tbcRangeSuppress.Text = "Диапазон РП";
            // 
            // chbAutoRangeS
            // 
            this.chbAutoRangeS.AutoSize = true;
            this.chbAutoRangeS.Location = new System.Drawing.Point(8, 121);
            this.chbAutoRangeS.Name = "chbAutoRangeS";
            this.chbAutoRangeS.Size = new System.Drawing.Size(50, 17);
            this.chbAutoRangeS.TabIndex = 44;
            this.chbAutoRangeS.Text = "Авто";
            this.chbAutoRangeS.UseVisualStyleBackColor = true;
            // 
            // rtbMemoRangeSuppr
            // 
            this.rtbMemoRangeSuppr.BackColor = System.Drawing.SystemColors.Control;
            this.rtbMemoRangeSuppr.Location = new System.Drawing.Point(8, 146);
            this.rtbMemoRangeSuppr.Name = "rtbMemoRangeSuppr";
            this.rtbMemoRangeSuppr.ReadOnly = true;
            this.rtbMemoRangeSuppr.Size = new System.Drawing.Size(241, 171);
            this.rtbMemoRangeSuppr.TabIndex = 43;
            this.rtbMemoRangeSuppr.Text = "";
            // 
            // bRangeSupprClear
            // 
            this.bRangeSupprClear.Location = new System.Drawing.Point(92, 117);
            this.bRangeSupprClear.Name = "bRangeSupprClear";
            this.bRangeSupprClear.Size = new System.Drawing.Size(75, 23);
            this.bRangeSupprClear.TabIndex = 42;
            this.bRangeSupprClear.Text = "Очистить";
            this.bRangeSupprClear.UseVisualStyleBackColor = true;
            this.bRangeSupprClear.Click += new System.EventHandler(this.bRangeSupprClear_Click);
            // 
            // bRangeSupprAdd
            // 
            this.bRangeSupprAdd.Location = new System.Drawing.Point(173, 117);
            this.bRangeSupprAdd.Name = "bRangeSupprAdd";
            this.bRangeSupprAdd.Size = new System.Drawing.Size(75, 23);
            this.bRangeSupprAdd.TabIndex = 41;
            this.bRangeSupprAdd.Text = "Добавить";
            this.bRangeSupprAdd.UseVisualStyleBackColor = true;
            this.bRangeSupprAdd.Click += new System.EventHandler(this.bRangeSupprAdd_Click);
            // 
            // bSendSupressSectorRange
            // 
            this.bSendSupressSectorRange.Location = new System.Drawing.Point(175, 327);
            this.bSendSupressSectorRange.Name = "bSendSupressSectorRange";
            this.bSendSupressSectorRange.Size = new System.Drawing.Size(75, 23);
            this.bSendSupressSectorRange.TabIndex = 38;
            this.bSendSupressSectorRange.Text = "Отправить";
            this.bSendSupressSectorRange.UseVisualStyleBackColor = true;
            this.bSendSupressSectorRange.Click += new System.EventHandler(this.bSendSupressSectorRange_Click);
            // 
            // pSupressSectorRange
            // 
            this.pSupressSectorRange.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pSupressSectorRange.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pSupressSectorRange.Controls.Add(this.nudBearingMaxSupress);
            this.pSupressSectorRange.Controls.Add(this.label48);
            this.pSupressSectorRange.Controls.Add(this.nudBearingMinSupress);
            this.pSupressSectorRange.Controls.Add(this.label49);
            this.pSupressSectorRange.Controls.Add(this.tbFreqMaxSupress);
            this.pSupressSectorRange.Controls.Add(this.label50);
            this.pSupressSectorRange.Controls.Add(this.tbFreqMinSupress);
            this.pSupressSectorRange.Controls.Add(this.label51);
            this.pSupressSectorRange.Location = new System.Drawing.Point(6, 6);
            this.pSupressSectorRange.Name = "pSupressSectorRange";
            this.pSupressSectorRange.Size = new System.Drawing.Size(245, 105);
            this.pSupressSectorRange.TabIndex = 24;
            // 
            // nudBearingMaxSupress
            // 
            this.nudBearingMaxSupress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearingMaxSupress.Location = new System.Drawing.Point(166, 25);
            this.nudBearingMaxSupress.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudBearingMaxSupress.Name = "nudBearingMaxSupress";
            this.nudBearingMaxSupress.Size = new System.Drawing.Size(74, 20);
            this.nudBearingMaxSupress.TabIndex = 29;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(5, 34);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(159, 13);
            this.label48.TabIndex = 28;
            this.label48.Text = "Пеленг макс, гр.......................";
            // 
            // nudBearingMinSupress
            // 
            this.nudBearingMinSupress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearingMinSupress.Location = new System.Drawing.Point(167, 3);
            this.nudBearingMinSupress.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
            this.nudBearingMinSupress.Name = "nudBearingMinSupress";
            this.nudBearingMinSupress.Size = new System.Drawing.Size(73, 20);
            this.nudBearingMinSupress.TabIndex = 27;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(5, 12);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(159, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "Пеленг мин, гр.........................";
            // 
            // tbFreqMaxSupress
            // 
            this.tbFreqMaxSupress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqMaxSupress.Location = new System.Drawing.Point(166, 72);
            this.tbFreqMaxSupress.Name = "tbFreqMaxSupress";
            this.tbFreqMaxSupress.Size = new System.Drawing.Size(73, 20);
            this.tbFreqMaxSupress.TabIndex = 25;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(3, 81);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(192, 13);
            this.label50.TabIndex = 24;
            this.label50.Text = "Частота макс,кГц...............................";
            // 
            // tbFreqMinSupress
            // 
            this.tbFreqMinSupress.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqMinSupress.Location = new System.Drawing.Point(166, 50);
            this.tbFreqMinSupress.Name = "tbFreqMinSupress";
            this.tbFreqMinSupress.Size = new System.Drawing.Size(73, 20);
            this.tbFreqMinSupress.TabIndex = 23;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(3, 59);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(186, 13);
            this.label51.TabIndex = 22;
            this.label51.Text = "Частота мин,кГц...............................";
            // 
            // tbcRangeForbid
            // 
            this.tbcRangeForbid.BackColor = System.Drawing.SystemColors.Control;
            this.tbcRangeForbid.Controls.Add(this.bSendForbidRangeFreq);
            this.tbcRangeForbid.Controls.Add(this.pForbidRangeFreq);
            this.tbcRangeForbid.Location = new System.Drawing.Point(4, 76);
            this.tbcRangeForbid.Name = "tbcRangeForbid";
            this.tbcRangeForbid.Padding = new System.Windows.Forms.Padding(3);
            this.tbcRangeForbid.Size = new System.Drawing.Size(255, 354);
            this.tbcRangeForbid.TabIndex = 4;
            this.tbcRangeForbid.Text = "Специальные";
            // 
            // bSendForbidRangeFreq
            // 
            this.bSendForbidRangeFreq.Location = new System.Drawing.Point(175, 327);
            this.bSendForbidRangeFreq.Name = "bSendForbidRangeFreq";
            this.bSendForbidRangeFreq.Size = new System.Drawing.Size(75, 23);
            this.bSendForbidRangeFreq.TabIndex = 34;
            this.bSendForbidRangeFreq.Text = "Отправить";
            this.bSendForbidRangeFreq.UseVisualStyleBackColor = true;
            this.bSendForbidRangeFreq.Click += new System.EventHandler(this.bSendForbidRangeFreq_Click);
            // 
            // pForbidRangeFreq
            // 
            this.pForbidRangeFreq.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pForbidRangeFreq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pForbidRangeFreq.Controls.Add(this.tbFreqMaxForbid);
            this.pForbidRangeFreq.Controls.Add(this.label40);
            this.pForbidRangeFreq.Controls.Add(this.tbFreqMinForbid);
            this.pForbidRangeFreq.Controls.Add(this.label41);
            this.pForbidRangeFreq.Controls.Add(this.cmbActForbid);
            this.pForbidRangeFreq.Controls.Add(this.label38);
            this.pForbidRangeFreq.Location = new System.Drawing.Point(3, 3);
            this.pForbidRangeFreq.Name = "pForbidRangeFreq";
            this.pForbidRangeFreq.Size = new System.Drawing.Size(245, 78);
            this.pForbidRangeFreq.TabIndex = 23;
            // 
            // tbFreqMaxForbid
            // 
            this.tbFreqMaxForbid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqMaxForbid.Location = new System.Drawing.Point(165, 50);
            this.tbFreqMaxForbid.Name = "tbFreqMaxForbid";
            this.tbFreqMaxForbid.Size = new System.Drawing.Size(73, 20);
            this.tbFreqMaxForbid.TabIndex = 25;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(1, 57);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(192, 13);
            this.label40.TabIndex = 24;
            this.label40.Text = "Частота макс,кГц...............................";
            // 
            // tbFreqMinForbid
            // 
            this.tbFreqMinForbid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqMinForbid.Location = new System.Drawing.Point(165, 28);
            this.tbFreqMinForbid.Name = "tbFreqMinForbid";
            this.tbFreqMinForbid.Size = new System.Drawing.Size(73, 20);
            this.tbFreqMinForbid.TabIndex = 23;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(1, 35);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(186, 13);
            this.label41.TabIndex = 22;
            this.label41.Text = "Частота мин,кГц...............................";
            // 
            // cmbActForbid
            // 
            this.cmbActForbid.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbActForbid.FormattingEnabled = true;
            this.cmbActForbid.Location = new System.Drawing.Point(123, 4);
            this.cmbActForbid.Name = "cmbActForbid";
            this.cmbActForbid.Size = new System.Drawing.Size(115, 21);
            this.cmbActForbid.TabIndex = 19;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(2, 12);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(131, 13);
            this.label38.TabIndex = 18;
            this.label38.Text = "Тип...................................";
            // 
            // tbpSynchronizePC
            // 
            this.tbpSynchronizePC.BackColor = System.Drawing.SystemColors.Control;
            this.tbpSynchronizePC.Controls.Add(this.bSendSynchronizePC);
            this.tbpSynchronizePC.Controls.Add(this.pSynchronizePC);
            this.tbpSynchronizePC.Location = new System.Drawing.Point(4, 76);
            this.tbpSynchronizePC.Name = "tbpSynchronizePC";
            this.tbpSynchronizePC.Padding = new System.Windows.Forms.Padding(3);
            this.tbpSynchronizePC.Size = new System.Drawing.Size(255, 354);
            this.tbpSynchronizePC.TabIndex = 5;
            this.tbpSynchronizePC.Text = "Синхронизация";
            // 
            // bSendSynchronizePC
            // 
            this.bSendSynchronizePC.Location = new System.Drawing.Point(175, 327);
            this.bSendSynchronizePC.Name = "bSendSynchronizePC";
            this.bSendSynchronizePC.Size = new System.Drawing.Size(75, 23);
            this.bSendSynchronizePC.TabIndex = 33;
            this.bSendSynchronizePC.Text = "Отправить";
            this.bSendSynchronizePC.UseVisualStyleBackColor = true;
            this.bSendSynchronizePC.Click += new System.EventHandler(this.bSendSynchronizePC_Click);
            // 
            // pSynchronizePC
            // 
            this.pSynchronizePC.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pSynchronizePC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pSynchronizePC.Controls.Add(this.dtpTimeSinchroPC);
            this.pSynchronizePC.Controls.Add(this.label55);
            this.pSynchronizePC.Location = new System.Drawing.Point(6, 6);
            this.pSynchronizePC.Name = "pSynchronizePC";
            this.pSynchronizePC.Size = new System.Drawing.Size(245, 31);
            this.pSynchronizePC.TabIndex = 19;
            // 
            // dtpTimeSinchroPC
            // 
            this.dtpTimeSinchroPC.CalendarMonthBackground = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dtpTimeSinchroPC.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpTimeSinchroPC.Location = new System.Drawing.Point(164, 3);
            this.dtpTimeSinchroPC.Name = "dtpTimeSinchroPC";
            this.dtpTimeSinchroPC.ShowUpDown = true;
            this.dtpTimeSinchroPC.Size = new System.Drawing.Size(74, 20);
            this.dtpTimeSinchroPC.TabIndex = 19;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(2, 12);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(166, 13);
            this.label55.TabIndex = 18;
            this.label55.Text = "Время..........................................";
            // 
            // tbpTextPC
            // 
            this.tbpTextPC.BackColor = System.Drawing.SystemColors.Control;
            this.tbpTextPC.Controls.Add(this.bSendTextMessagePC);
            this.tbpTextPC.Controls.Add(this.pTextMessagePC);
            this.tbpTextPC.Location = new System.Drawing.Point(4, 76);
            this.tbpTextPC.Name = "tbpTextPC";
            this.tbpTextPC.Padding = new System.Windows.Forms.Padding(3);
            this.tbpTextPC.Size = new System.Drawing.Size(255, 354);
            this.tbpTextPC.TabIndex = 6;
            this.tbpTextPC.Text = "Текст";
            // 
            // bSendTextMessagePC
            // 
            this.bSendTextMessagePC.Location = new System.Drawing.Point(175, 327);
            this.bSendTextMessagePC.Name = "bSendTextMessagePC";
            this.bSendTextMessagePC.Size = new System.Drawing.Size(75, 23);
            this.bSendTextMessagePC.TabIndex = 32;
            this.bSendTextMessagePC.Text = "Отправить";
            this.bSendTextMessagePC.UseVisualStyleBackColor = true;
            this.bSendTextMessagePC.Click += new System.EventHandler(this.bSendTextMessagePC_Click);
            // 
            // pTextMessagePC
            // 
            this.pTextMessagePC.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pTextMessagePC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pTextMessagePC.Controls.Add(this.rtbTextPC);
            this.pTextMessagePC.Location = new System.Drawing.Point(3, 3);
            this.pTextMessagePC.Name = "pTextMessagePC";
            this.pTextMessagePC.Size = new System.Drawing.Size(245, 110);
            this.pTextMessagePC.TabIndex = 13;
            // 
            // rtbTextPC
            // 
            this.rtbTextPC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbTextPC.Location = new System.Drawing.Point(0, 0);
            this.rtbTextPC.MaxLength = 125;
            this.rtbTextPC.Name = "rtbTextPC";
            this.rtbTextPC.Size = new System.Drawing.Size(243, 108);
            this.rtbTextPC.TabIndex = 0;
            this.rtbTextPC.Text = "";
            // 
            // tbcSuppressFWS
            // 
            this.tbcSuppressFWS.BackColor = System.Drawing.SystemColors.Control;
            this.tbcSuppressFWS.Controls.Add(this.chbAutoSupprFWS);
            this.tbcSuppressFWS.Controls.Add(this.rtbMemoSupprFWS);
            this.tbcSuppressFWS.Controls.Add(this.nudSupprClearFWS);
            this.tbcSuppressFWS.Controls.Add(this.nudSupprAddFWS);
            this.tbcSuppressFWS.Controls.Add(this.bSendSupressFWS);
            this.tbcSuppressFWS.Controls.Add(this.pSupressFWS);
            this.tbcSuppressFWS.Location = new System.Drawing.Point(4, 76);
            this.tbcSuppressFWS.Name = "tbcSuppressFWS";
            this.tbcSuppressFWS.Padding = new System.Windows.Forms.Padding(3);
            this.tbcSuppressFWS.Size = new System.Drawing.Size(255, 354);
            this.tbcSuppressFWS.TabIndex = 7;
            this.tbcSuppressFWS.Text = "ИРИ ФРЧ на РП";
            // 
            // chbAutoSupprFWS
            // 
            this.chbAutoSupprFWS.AutoSize = true;
            this.chbAutoSupprFWS.Location = new System.Drawing.Point(13, 222);
            this.chbAutoSupprFWS.Name = "chbAutoSupprFWS";
            this.chbAutoSupprFWS.Size = new System.Drawing.Size(50, 17);
            this.chbAutoSupprFWS.TabIndex = 45;
            this.chbAutoSupprFWS.Text = "Авто";
            this.chbAutoSupprFWS.UseVisualStyleBackColor = true;
            // 
            // rtbMemoSupprFWS
            // 
            this.rtbMemoSupprFWS.BackColor = System.Drawing.SystemColors.Control;
            this.rtbMemoSupprFWS.Location = new System.Drawing.Point(0, 245);
            this.rtbMemoSupprFWS.Name = "rtbMemoSupprFWS";
            this.rtbMemoSupprFWS.ReadOnly = true;
            this.rtbMemoSupprFWS.Size = new System.Drawing.Size(247, 76);
            this.rtbMemoSupprFWS.TabIndex = 44;
            this.rtbMemoSupprFWS.Text = "";
            // 
            // nudSupprClearFWS
            // 
            this.nudSupprClearFWS.Location = new System.Drawing.Point(92, 216);
            this.nudSupprClearFWS.Name = "nudSupprClearFWS";
            this.nudSupprClearFWS.Size = new System.Drawing.Size(75, 23);
            this.nudSupprClearFWS.TabIndex = 43;
            this.nudSupprClearFWS.Text = "Очистить";
            this.nudSupprClearFWS.UseVisualStyleBackColor = true;
            this.nudSupprClearFWS.Click += new System.EventHandler(this.nudSupprClearFWS_Click);
            // 
            // nudSupprAddFWS
            // 
            this.nudSupprAddFWS.Location = new System.Drawing.Point(172, 216);
            this.nudSupprAddFWS.Name = "nudSupprAddFWS";
            this.nudSupprAddFWS.Size = new System.Drawing.Size(75, 23);
            this.nudSupprAddFWS.TabIndex = 42;
            this.nudSupprAddFWS.Text = "Добавить";
            this.nudSupprAddFWS.UseVisualStyleBackColor = true;
            this.nudSupprAddFWS.Click += new System.EventHandler(this.nudSupprAddFWS_Click);
            // 
            // bSendSupressFWS
            // 
            this.bSendSupressFWS.Location = new System.Drawing.Point(175, 327);
            this.bSendSupressFWS.Name = "bSendSupressFWS";
            this.bSendSupressFWS.Size = new System.Drawing.Size(75, 23);
            this.bSendSupressFWS.TabIndex = 27;
            this.bSendSupressFWS.Text = "Отправить";
            this.bSendSupressFWS.UseVisualStyleBackColor = true;
            this.bSendSupressFWS.Click += new System.EventHandler(this.bSendSupressFWS_Click);
            // 
            // pSupressFWS
            // 
            this.pSupressFWS.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pSupressFWS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pSupressFWS.Controls.Add(this.nudBearingFWS);
            this.pSupressFWS.Controls.Add(this.label4);
            this.pSupressFWS.Controls.Add(this.nudThresholdFWS);
            this.pSupressFWS.Controls.Add(this.label3);
            this.pSupressFWS.Controls.Add(this.cmbManipulationFWS);
            this.pSupressFWS.Controls.Add(this.label2);
            this.pSupressFWS.Controls.Add(this.nudDurationGlobalFWS);
            this.pSupressFWS.Controls.Add(this.label1);
            this.pSupressFWS.Controls.Add(this.cmbDeviationFWS);
            this.pSupressFWS.Controls.Add(this.lDeviationFWS);
            this.pSupressFWS.Controls.Add(this.cmbTypeModulationFWS);
            this.pSupressFWS.Controls.Add(this.lTypeModulationFWS);
            this.pSupressFWS.Controls.Add(this.nudPriorSupFWS);
            this.pSupressFWS.Controls.Add(this.label59);
            this.pSupressFWS.Controls.Add(this.tbFreqSupFWS);
            this.pSupressFWS.Controls.Add(this.label57);
            this.pSupressFWS.Location = new System.Drawing.Point(3, 6);
            this.pSupressFWS.Name = "pSupressFWS";
            this.pSupressFWS.Size = new System.Drawing.Size(245, 204);
            this.pSupressFWS.TabIndex = 26;
            // 
            // nudBearingFWS
            // 
            this.nudBearingFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudBearingFWS.Location = new System.Drawing.Point(168, 176);
            this.nudBearingFWS.Maximum = new decimal(new int[] {
            361,
            0,
            0,
            0});
            this.nudBearingFWS.Name = "nudBearingFWS";
            this.nudBearingFWS.Size = new System.Drawing.Size(74, 20);
            this.nudBearingFWS.TabIndex = 59;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(200, 13);
            this.label4.TabIndex = 58;
            this.label4.Text = "Пеленг....................................................";
            // 
            // nudThresholdFWS
            // 
            this.nudThresholdFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudThresholdFWS.Location = new System.Drawing.Point(167, 150);
            this.nudThresholdFWS.Name = "nudThresholdFWS";
            this.nudThresholdFWS.Size = new System.Drawing.Size(74, 20);
            this.nudThresholdFWS.TabIndex = 57;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "Порог(-)......................................................";
            // 
            // cmbManipulationFWS
            // 
            this.cmbManipulationFWS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbManipulationFWS.FormattingEnabled = true;
            this.cmbManipulationFWS.Location = new System.Drawing.Point(167, 97);
            this.cmbManipulationFWS.Name = "cmbManipulationFWS";
            this.cmbManipulationFWS.Size = new System.Drawing.Size(74, 21);
            this.cmbManipulationFWS.TabIndex = 55;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(248, 13);
            this.label2.TabIndex = 54;
            this.label2.Text = "Частота манипуляции,Гц......................................";
            // 
            // nudDurationGlobalFWS
            // 
            this.nudDurationGlobalFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudDurationGlobalFWS.Location = new System.Drawing.Point(167, 3);
            this.nudDurationGlobalFWS.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudDurationGlobalFWS.Name = "nudDurationGlobalFWS";
            this.nudDurationGlobalFWS.Size = new System.Drawing.Size(73, 20);
            this.nudDurationGlobalFWS.TabIndex = 53;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 13);
            this.label1.TabIndex = 52;
            this.label1.Text = "Длительность цикла, мс................................";
            // 
            // cmbDeviationFWS
            // 
            this.cmbDeviationFWS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDeviationFWS.FormattingEnabled = true;
            this.cmbDeviationFWS.Location = new System.Drawing.Point(167, 73);
            this.cmbDeviationFWS.Name = "cmbDeviationFWS";
            this.cmbDeviationFWS.Size = new System.Drawing.Size(75, 21);
            this.cmbDeviationFWS.TabIndex = 49;
            // 
            // lDeviationFWS
            // 
            this.lDeviationFWS.AutoSize = true;
            this.lDeviationFWS.Location = new System.Drawing.Point(6, 81);
            this.lDeviationFWS.Name = "lDeviationFWS";
            this.lDeviationFWS.Size = new System.Drawing.Size(183, 13);
            this.lDeviationFWS.TabIndex = 48;
            this.lDeviationFWS.Text = "Девиация(+/-),кГц............................";
            // 
            // cmbTypeModulationFWS
            // 
            this.cmbTypeModulationFWS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeModulationFWS.FormattingEnabled = true;
            this.cmbTypeModulationFWS.Location = new System.Drawing.Point(168, 49);
            this.cmbTypeModulationFWS.Name = "cmbTypeModulationFWS";
            this.cmbTypeModulationFWS.Size = new System.Drawing.Size(74, 21);
            this.cmbTypeModulationFWS.TabIndex = 47;
            this.cmbTypeModulationFWS.SelectedIndexChanged += new System.EventHandler(this.cmbTypeModulationFWS_SelectedIndexChanged);
            // 
            // lTypeModulationFWS
            // 
            this.lTypeModulationFWS.AutoSize = true;
            this.lTypeModulationFWS.Location = new System.Drawing.Point(6, 57);
            this.lTypeModulationFWS.Name = "lTypeModulationFWS";
            this.lTypeModulationFWS.Size = new System.Drawing.Size(168, 13);
            this.lTypeModulationFWS.TabIndex = 46;
            this.lTypeModulationFWS.Text = "Вид модуляции............................";
            // 
            // nudPriorSupFWS
            // 
            this.nudPriorSupFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudPriorSupFWS.Location = new System.Drawing.Point(167, 124);
            this.nudPriorSupFWS.Name = "nudPriorSupFWS";
            this.nudPriorSupFWS.Size = new System.Drawing.Size(74, 20);
            this.nudPriorSupFWS.TabIndex = 33;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(6, 134);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(175, 13);
            this.label59.TabIndex = 32;
            this.label59.Text = "Приоритет......................................";
            // 
            // tbFreqSupFWS
            // 
            this.tbFreqSupFWS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqSupFWS.Location = new System.Drawing.Point(167, 27);
            this.tbFreqSupFWS.Name = "tbFreqSupFWS";
            this.tbFreqSupFWS.Size = new System.Drawing.Size(73, 20);
            this.tbFreqSupFWS.TabIndex = 23;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(4, 33);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(186, 13);
            this.label57.TabIndex = 22;
            this.label57.Text = "Частота мин,кГц...............................";
            // 
            // tbcSuppressFHSS
            // 
            this.tbcSuppressFHSS.BackColor = System.Drawing.SystemColors.Control;
            this.tbcSuppressFHSS.Controls.Add(this.chbAutoSupprFHSS);
            this.tbcSuppressFHSS.Controls.Add(this.rtbMemoSupprFHSS);
            this.tbcSuppressFHSS.Controls.Add(this.nudSupprClearFHSS);
            this.tbcSuppressFHSS.Controls.Add(this.bSupprAddFHSS);
            this.tbcSuppressFHSS.Controls.Add(this.bSendSupressFHSS);
            this.tbcSuppressFHSS.Controls.Add(this.pSupressFHSS);
            this.tbcSuppressFHSS.Location = new System.Drawing.Point(4, 76);
            this.tbcSuppressFHSS.Name = "tbcSuppressFHSS";
            this.tbcSuppressFHSS.Padding = new System.Windows.Forms.Padding(3);
            this.tbcSuppressFHSS.Size = new System.Drawing.Size(255, 354);
            this.tbcSuppressFHSS.TabIndex = 8;
            this.tbcSuppressFHSS.Text = "ИРИ ППРЧ на РП";
            // 
            // chbAutoSupprFHSS
            // 
            this.chbAutoSupprFHSS.AutoSize = true;
            this.chbAutoSupprFHSS.Location = new System.Drawing.Point(13, 200);
            this.chbAutoSupprFHSS.Name = "chbAutoSupprFHSS";
            this.chbAutoSupprFHSS.Size = new System.Drawing.Size(50, 17);
            this.chbAutoSupprFHSS.TabIndex = 49;
            this.chbAutoSupprFHSS.Text = "Авто";
            this.chbAutoSupprFHSS.UseVisualStyleBackColor = true;
            // 
            // rtbMemoSupprFHSS
            // 
            this.rtbMemoSupprFHSS.BackColor = System.Drawing.SystemColors.Control;
            this.rtbMemoSupprFHSS.Location = new System.Drawing.Point(3, 222);
            this.rtbMemoSupprFHSS.Name = "rtbMemoSupprFHSS";
            this.rtbMemoSupprFHSS.ReadOnly = true;
            this.rtbMemoSupprFHSS.Size = new System.Drawing.Size(247, 98);
            this.rtbMemoSupprFHSS.TabIndex = 48;
            this.rtbMemoSupprFHSS.Text = "";
            // 
            // nudSupprClearFHSS
            // 
            this.nudSupprClearFHSS.Location = new System.Drawing.Point(92, 194);
            this.nudSupprClearFHSS.Name = "nudSupprClearFHSS";
            this.nudSupprClearFHSS.Size = new System.Drawing.Size(75, 23);
            this.nudSupprClearFHSS.TabIndex = 47;
            this.nudSupprClearFHSS.Text = "Очистить";
            this.nudSupprClearFHSS.UseVisualStyleBackColor = true;
            this.nudSupprClearFHSS.Click += new System.EventHandler(this.nudSupprClearFHSS_Click);
            // 
            // bSupprAddFHSS
            // 
            this.bSupprAddFHSS.Location = new System.Drawing.Point(172, 194);
            this.bSupprAddFHSS.Name = "bSupprAddFHSS";
            this.bSupprAddFHSS.Size = new System.Drawing.Size(75, 23);
            this.bSupprAddFHSS.TabIndex = 46;
            this.bSupprAddFHSS.Text = "Добавить";
            this.bSupprAddFHSS.UseVisualStyleBackColor = true;
            this.bSupprAddFHSS.Click += new System.EventHandler(this.bSupprAddFHSS_Click);
            // 
            // bSendSupressFHSS
            // 
            this.bSendSupressFHSS.Location = new System.Drawing.Point(175, 327);
            this.bSendSupressFHSS.Name = "bSendSupressFHSS";
            this.bSendSupressFHSS.Size = new System.Drawing.Size(75, 23);
            this.bSendSupressFHSS.TabIndex = 28;
            this.bSendSupressFHSS.Text = "Отправить";
            this.bSendSupressFHSS.UseVisualStyleBackColor = true;
            this.bSendSupressFHSS.Click += new System.EventHandler(this.bSendSupressFHSS_Click);
            // 
            // pSupressFHSS
            // 
            this.pSupressFHSS.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pSupressFHSS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pSupressFHSS.Controls.Add(this.tbFreqMaxSupFHSS);
            this.pSupressFHSS.Controls.Add(this.label6);
            this.pSupressFHSS.Controls.Add(this.nudDuratSupFHSS);
            this.pSupressFHSS.Controls.Add(this.label5);
            this.pSupressFHSS.Controls.Add(this.cmbManipulation);
            this.pSupressFHSS.Controls.Add(this.lManipulation);
            this.pSupressFHSS.Controls.Add(this.cmbDeviation);
            this.pSupressFHSS.Controls.Add(this.lDeviation);
            this.pSupressFHSS.Controls.Add(this.cmbTypeModulation);
            this.pSupressFHSS.Controls.Add(this.lTypeModulation);
            this.pSupressFHSS.Controls.Add(this.cmbStepSupFHSS);
            this.pSupressFHSS.Controls.Add(this.label68);
            this.pSupressFHSS.Controls.Add(this.tbFreqMinSupFHSS);
            this.pSupressFHSS.Controls.Add(this.label62);
            this.pSupressFHSS.Location = new System.Drawing.Point(3, 3);
            this.pSupressFHSS.Name = "pSupressFHSS";
            this.pSupressFHSS.Size = new System.Drawing.Size(245, 185);
            this.pSupressFHSS.TabIndex = 27;
            // 
            // tbFreqMaxSupFHSS
            // 
            this.tbFreqMaxSupFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqMaxSupFHSS.Location = new System.Drawing.Point(168, 51);
            this.tbFreqMaxSupFHSS.Name = "tbFreqMaxSupFHSS";
            this.tbFreqMaxSupFHSS.Size = new System.Drawing.Size(73, 20);
            this.tbFreqMaxSupFHSS.TabIndex = 57;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(192, 13);
            this.label6.TabIndex = 56;
            this.label6.Text = "Частота макс,кГц...............................";
            // 
            // nudDuratSupFHSS
            // 
            this.nudDuratSupFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudDuratSupFHSS.Location = new System.Drawing.Point(168, 4);
            this.nudDuratSupFHSS.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudDuratSupFHSS.Name = "nudDuratSupFHSS";
            this.nudDuratSupFHSS.Size = new System.Drawing.Size(73, 20);
            this.nudDuratSupFHSS.TabIndex = 55;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 12);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(229, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "Длительность цикла, мс................................";
            // 
            // cmbManipulation
            // 
            this.cmbManipulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbManipulation.FormattingEnabled = true;
            this.cmbManipulation.Location = new System.Drawing.Point(166, 158);
            this.cmbManipulation.Name = "cmbManipulation";
            this.cmbManipulation.Size = new System.Drawing.Size(74, 21);
            this.cmbManipulation.TabIndex = 45;
            // 
            // lManipulation
            // 
            this.lManipulation.AutoSize = true;
            this.lManipulation.Location = new System.Drawing.Point(6, 166);
            this.lManipulation.Name = "lManipulation";
            this.lManipulation.Size = new System.Drawing.Size(218, 13);
            this.lManipulation.TabIndex = 44;
            this.lManipulation.Text = "Частота манипуляции,Гц............................";
            // 
            // cmbDeviation
            // 
            this.cmbDeviation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDeviation.FormattingEnabled = true;
            this.cmbDeviation.Location = new System.Drawing.Point(166, 131);
            this.cmbDeviation.Name = "cmbDeviation";
            this.cmbDeviation.Size = new System.Drawing.Size(75, 21);
            this.cmbDeviation.TabIndex = 43;
            // 
            // lDeviation
            // 
            this.lDeviation.AutoSize = true;
            this.lDeviation.Location = new System.Drawing.Point(6, 139);
            this.lDeviation.Name = "lDeviation";
            this.lDeviation.Size = new System.Drawing.Size(183, 13);
            this.lDeviation.TabIndex = 42;
            this.lDeviation.Text = "Девиация(+/-),кГц............................";
            // 
            // cmbTypeModulation
            // 
            this.cmbTypeModulation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeModulation.FormattingEnabled = true;
            this.cmbTypeModulation.Location = new System.Drawing.Point(165, 104);
            this.cmbTypeModulation.Name = "cmbTypeModulation";
            this.cmbTypeModulation.Size = new System.Drawing.Size(74, 21);
            this.cmbTypeModulation.TabIndex = 41;
            this.cmbTypeModulation.SelectedIndexChanged += new System.EventHandler(this.cmbTypeModulation_SelectedIndexChanged);
            // 
            // lTypeModulation
            // 
            this.lTypeModulation.AutoSize = true;
            this.lTypeModulation.Location = new System.Drawing.Point(5, 112);
            this.lTypeModulation.Name = "lTypeModulation";
            this.lTypeModulation.Size = new System.Drawing.Size(168, 13);
            this.lTypeModulation.TabIndex = 40;
            this.lTypeModulation.Text = "Вид модуляции............................";
            // 
            // cmbStepSupFHSS
            // 
            this.cmbStepSupFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.cmbStepSupFHSS.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStepSupFHSS.FormattingEnabled = true;
            this.cmbStepSupFHSS.Location = new System.Drawing.Point(165, 77);
            this.cmbStepSupFHSS.Name = "cmbStepSupFHSS";
            this.cmbStepSupFHSS.Size = new System.Drawing.Size(74, 21);
            this.cmbStepSupFHSS.TabIndex = 39;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(6, 85);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(172, 13);
            this.label68.TabIndex = 38;
            this.label68.Text = "Код БПФ.......................................";
            // 
            // tbFreqMinSupFHSS
            // 
            this.tbFreqMinSupFHSS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqMinSupFHSS.Location = new System.Drawing.Point(168, 28);
            this.tbFreqMinSupFHSS.Name = "tbFreqMinSupFHSS";
            this.tbFreqMinSupFHSS.Size = new System.Drawing.Size(73, 20);
            this.tbFreqMinSupFHSS.TabIndex = 23;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(5, 35);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(186, 13);
            this.label62.TabIndex = 22;
            this.label62.Text = "Частота мин,кГц...............................";
            // 
            // tbcRequestBearing
            // 
            this.tbcRequestBearing.BackColor = System.Drawing.SystemColors.Control;
            this.tbcRequestBearing.Controls.Add(this.bSendRequestBearing);
            this.tbcRequestBearing.Controls.Add(this.pRequestBearing);
            this.tbcRequestBearing.Location = new System.Drawing.Point(4, 76);
            this.tbcRequestBearing.Name = "tbcRequestBearing";
            this.tbcRequestBearing.Padding = new System.Windows.Forms.Padding(3);
            this.tbcRequestBearing.Size = new System.Drawing.Size(255, 354);
            this.tbcRequestBearing.TabIndex = 9;
            this.tbcRequestBearing.Text = "Запрос Пел.";
            // 
            // bSendRequestBearing
            // 
            this.bSendRequestBearing.Location = new System.Drawing.Point(175, 327);
            this.bSendRequestBearing.Name = "bSendRequestBearing";
            this.bSendRequestBearing.Size = new System.Drawing.Size(75, 23);
            this.bSendRequestBearing.TabIndex = 29;
            this.bSendRequestBearing.Text = "Отправить";
            this.bSendRequestBearing.UseVisualStyleBackColor = true;
            this.bSendRequestBearing.Click += new System.EventHandler(this.bSendRequestBearing_Click);
            // 
            // pRequestBearing
            // 
            this.pRequestBearing.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pRequestBearing.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pRequestBearing.Controls.Add(this.nudFreqID);
            this.pRequestBearing.Controls.Add(this.label23);
            this.pRequestBearing.Controls.Add(this.tbFreqRecBearing);
            this.pRequestBearing.Controls.Add(this.label72);
            this.pRequestBearing.Controls.Add(this.cmbTypeReqBearing);
            this.pRequestBearing.Controls.Add(this.label71);
            this.pRequestBearing.Location = new System.Drawing.Point(3, 6);
            this.pRequestBearing.Name = "pRequestBearing";
            this.pRequestBearing.Size = new System.Drawing.Size(245, 89);
            this.pRequestBearing.TabIndex = 22;
            // 
            // nudFreqID
            // 
            this.nudFreqID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudFreqID.Location = new System.Drawing.Point(166, 27);
            this.nudFreqID.Name = "nudFreqID";
            this.nudFreqID.Size = new System.Drawing.Size(73, 20);
            this.nudFreqID.TabIndex = 57;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(1, 35);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(189, 13);
            this.label23.TabIndex = 56;
            this.label23.Text = "ID.........................................................";
            // 
            // tbFreqRecBearing
            // 
            this.tbFreqRecBearing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbFreqRecBearing.Location = new System.Drawing.Point(166, 51);
            this.tbFreqRecBearing.Name = "tbFreqRecBearing";
            this.tbFreqRecBearing.Size = new System.Drawing.Size(74, 20);
            this.tbFreqRecBearing.TabIndex = 23;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(4, 58);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(163, 13);
            this.label72.TabIndex = 22;
            this.label72.Text = "Частота,кГц...............................";
            // 
            // cmbTypeReqBearing
            // 
            this.cmbTypeReqBearing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTypeReqBearing.FormattingEnabled = true;
            this.cmbTypeReqBearing.Location = new System.Drawing.Point(96, 3);
            this.cmbTypeReqBearing.Name = "cmbTypeReqBearing";
            this.cmbTypeReqBearing.Size = new System.Drawing.Size(144, 21);
            this.cmbTypeReqBearing.TabIndex = 21;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(3, 12);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(176, 13);
            this.label71.TabIndex = 20;
            this.label71.Text = "Тип запроса...................................";
            // 
            // tbpReceiptPC
            // 
            this.tbpReceiptPC.BackColor = System.Drawing.SystemColors.Control;
            this.tbpReceiptPC.Controls.Add(this.bSendReceiptPC);
            this.tbpReceiptPC.Controls.Add(this.pReceiptPC);
            this.tbpReceiptPC.Location = new System.Drawing.Point(4, 76);
            this.tbpReceiptPC.Name = "tbpReceiptPC";
            this.tbpReceiptPC.Padding = new System.Windows.Forms.Padding(3);
            this.tbpReceiptPC.Size = new System.Drawing.Size(255, 354);
            this.tbpReceiptPC.TabIndex = 10;
            this.tbpReceiptPC.Text = "Квитанция";
            // 
            // bSendReceiptPC
            // 
            this.bSendReceiptPC.Location = new System.Drawing.Point(174, 325);
            this.bSendReceiptPC.Name = "bSendReceiptPC";
            this.bSendReceiptPC.Size = new System.Drawing.Size(75, 23);
            this.bSendReceiptPC.TabIndex = 30;
            this.bSendReceiptPC.Text = "Отправить";
            this.bSendReceiptPC.UseVisualStyleBackColor = true;
            this.bSendReceiptPC.Click += new System.EventHandler(this.bSendReceiptPC_Click);
            // 
            // pReceiptPC
            // 
            this.pReceiptPC.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.pReceiptPC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pReceiptPC.Controls.Add(this.nudCodeErrorPC);
            this.pReceiptPC.Controls.Add(this.label69);
            this.pReceiptPC.Location = new System.Drawing.Point(3, 6);
            this.pReceiptPC.Name = "pReceiptPC";
            this.pReceiptPC.Size = new System.Drawing.Size(245, 39);
            this.pReceiptPC.TabIndex = 11;
            // 
            // nudCodeErrorPC
            // 
            this.nudCodeErrorPC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.nudCodeErrorPC.Location = new System.Drawing.Point(170, 6);
            this.nudCodeErrorPC.Maximum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.nudCodeErrorPC.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudCodeErrorPC.Name = "nudCodeErrorPC";
            this.nudCodeErrorPC.Size = new System.Drawing.Size(70, 20);
            this.nudCodeErrorPC.TabIndex = 17;
            this.nudCodeErrorPC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(2, 13);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(208, 13);
            this.label69.TabIndex = 16;
            this.label69.Text = "Код ошибки...............................................";
            // 
            // pLog
            // 
            this.pLog.Controls.Add(this.tbcLog);
            this.pLog.Controls.Add(this.pShowLog);
            this.pLog.Location = new System.Drawing.Point(263, 0);
            this.pLog.Name = "pLog";
            this.pLog.Size = new System.Drawing.Size(373, 462);
            this.pLog.TabIndex = 10;
            // 
            // tbcLog
            // 
            this.tbcLog.Controls.Add(this.tbpRRD1);
            this.tbcLog.Controls.Add(this.tbpRRD2);
            this.tbcLog.Controls.Add(this.tbpRRD3);
            this.tbcLog.Controls.Add(this.tbpRRD4);
            this.tbcLog.Controls.Add(this.tbpRRD5);
            this.tbcLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbcLog.Location = new System.Drawing.Point(0, 28);
            this.tbcLog.Multiline = true;
            this.tbcLog.Name = "tbcLog";
            this.tbcLog.SelectedIndex = 0;
            this.tbcLog.Size = new System.Drawing.Size(373, 434);
            this.tbcLog.TabIndex = 8;
            // 
            // tbpRRD1
            // 
            this.tbpRRD1.Controls.Add(this.button1);
            this.tbpRRD1.Controls.Add(this.rtbLogRRC);
            this.tbpRRD1.Location = new System.Drawing.Point(4, 22);
            this.tbpRRD1.Name = "tbpRRD1";
            this.tbpRRD1.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRRD1.Size = new System.Drawing.Size(365, 408);
            this.tbpRRD1.TabIndex = 0;
            this.tbpRRD1.Text = "ОД";
            this.tbpRRD1.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(284, 378);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rtbLogRRC
            // 
            this.rtbLogRRC.BackColor = System.Drawing.Color.Tan;
            this.rtbLogRRC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLogRRC.Location = new System.Drawing.Point(3, 3);
            this.rtbLogRRC.Name = "rtbLogRRC";
            this.rtbLogRRC.Size = new System.Drawing.Size(359, 402);
            this.rtbLogRRC.TabIndex = 8;
            this.rtbLogRRC.Text = "";
            // 
            // tbpRRD2
            // 
            this.tbpRRD2.Controls.Add(this.rtbLog3G);
            this.tbpRRD2.Location = new System.Drawing.Point(4, 22);
            this.tbpRRD2.Name = "tbpRRD2";
            this.tbpRRD2.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRRD2.Size = new System.Drawing.Size(365, 408);
            this.tbpRRD2.TabIndex = 1;
            this.tbpRRD2.Text = "3G";
            this.tbpRRD2.UseVisualStyleBackColor = true;
            // 
            // rtbLog3G
            // 
            this.rtbLog3G.BackColor = System.Drawing.Color.Tan;
            this.rtbLog3G.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLog3G.Location = new System.Drawing.Point(3, 3);
            this.rtbLog3G.Name = "rtbLog3G";
            this.rtbLog3G.Size = new System.Drawing.Size(359, 402);
            this.rtbLog3G.TabIndex = 8;
            this.rtbLog3G.Text = "";
            // 
            // tbpRRD3
            // 
            this.tbpRRD3.Controls.Add(this.rtbLogModem);
            this.tbpRRD3.Location = new System.Drawing.Point(4, 22);
            this.tbpRRD3.Name = "tbpRRD3";
            this.tbpRRD3.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRRD3.Size = new System.Drawing.Size(365, 408);
            this.tbpRRD3.TabIndex = 2;
            this.tbpRRD3.Text = "Modem";
            this.tbpRRD3.UseVisualStyleBackColor = true;
            // 
            // rtbLogModem
            // 
            this.rtbLogModem.BackColor = System.Drawing.Color.Tan;
            this.rtbLogModem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLogModem.Location = new System.Drawing.Point(3, 3);
            this.rtbLogModem.Name = "rtbLogModem";
            this.rtbLogModem.Size = new System.Drawing.Size(359, 402);
            this.rtbLogModem.TabIndex = 8;
            this.rtbLogModem.Text = "";
            // 
            // tbpRRD4
            // 
            this.tbpRRD4.Controls.Add(this.rtbLogHF);
            this.tbpRRD4.Location = new System.Drawing.Point(4, 22);
            this.tbpRRD4.Name = "tbpRRD4";
            this.tbpRRD4.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRRD4.Size = new System.Drawing.Size(365, 408);
            this.tbpRRD4.TabIndex = 3;
            this.tbpRRD4.Text = "HF";
            this.tbpRRD4.UseVisualStyleBackColor = true;
            // 
            // rtbLogHF
            // 
            this.rtbLogHF.BackColor = System.Drawing.Color.Tan;
            this.rtbLogHF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLogHF.Location = new System.Drawing.Point(3, 3);
            this.rtbLogHF.Name = "rtbLogHF";
            this.rtbLogHF.Size = new System.Drawing.Size(359, 402);
            this.rtbLogHF.TabIndex = 8;
            this.rtbLogHF.Text = "";
            // 
            // tbpRRD5
            // 
            this.tbpRRD5.Controls.Add(this.rtbLogVHF);
            this.tbpRRD5.Location = new System.Drawing.Point(4, 22);
            this.tbpRRD5.Name = "tbpRRD5";
            this.tbpRRD5.Padding = new System.Windows.Forms.Padding(3);
            this.tbpRRD5.Size = new System.Drawing.Size(365, 408);
            this.tbpRRD5.TabIndex = 4;
            this.tbpRRD5.Text = "VHF";
            this.tbpRRD5.UseVisualStyleBackColor = true;
            // 
            // rtbLogVHF
            // 
            this.rtbLogVHF.BackColor = System.Drawing.Color.Tan;
            this.rtbLogVHF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbLogVHF.Location = new System.Drawing.Point(3, 3);
            this.rtbLogVHF.Name = "rtbLogVHF";
            this.rtbLogVHF.Size = new System.Drawing.Size(359, 402);
            this.rtbLogVHF.TabIndex = 8;
            this.rtbLogVHF.Text = "";
            // 
            // pShowLog
            // 
            this.pShowLog.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pShowLog.Controls.Add(this.chbByteHex);
            this.pShowLog.Controls.Add(this.chbDetail);
            this.pShowLog.Controls.Add(this.chbName);
            this.pShowLog.Dock = System.Windows.Forms.DockStyle.Top;
            this.pShowLog.Location = new System.Drawing.Point(0, 0);
            this.pShowLog.Name = "pShowLog";
            this.pShowLog.Size = new System.Drawing.Size(373, 28);
            this.pShowLog.TabIndex = 6;
            // 
            // chbByteHex
            // 
            this.chbByteHex.AutoSize = true;
            this.chbByteHex.Checked = true;
            this.chbByteHex.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbByteHex.Location = new System.Drawing.Point(9, 4);
            this.chbByteHex.Name = "chbByteHex";
            this.chbByteHex.Size = new System.Drawing.Size(48, 17);
            this.chbByteHex.TabIndex = 69;
            this.chbByteHex.Text = "HEX";
            this.chbByteHex.UseVisualStyleBackColor = true;
            // 
            // chbDetail
            // 
            this.chbDetail.AutoSize = true;
            this.chbDetail.Checked = true;
            this.chbDetail.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbDetail.Location = new System.Drawing.Point(156, 3);
            this.chbDetail.Name = "chbDetail";
            this.chbDetail.Size = new System.Drawing.Size(45, 17);
            this.chbDetail.TabIndex = 67;
            this.chbDetail.Text = "Код";
            this.chbDetail.UseVisualStyleBackColor = true;
            // 
            // chbName
            // 
            this.chbName.AutoSize = true;
            this.chbName.Checked = true;
            this.chbName.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chbName.Location = new System.Drawing.Point(79, 3);
            this.chbName.Name = "chbName";
            this.chbName.Size = new System.Drawing.Size(76, 17);
            this.chbName.TabIndex = 68;
            this.chbName.Text = "Название";
            this.chbName.UseVisualStyleBackColor = true;
            // 
            // pStateConnection
            // 
            this.pStateConnection.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pStateConnection.Controls.Add(this.pCntUpd);
            this.pStateConnection.Dock = System.Windows.Forms.DockStyle.Right;
            this.pStateConnection.Location = new System.Drawing.Point(640, 0);
            this.pStateConnection.Name = "pStateConnection";
            this.pStateConnection.Size = new System.Drawing.Size(246, 461);
            this.pStateConnection.TabIndex = 11;
            // 
            // pCntUpd
            // 
            this.pCntUpd.Controls.Add(this.panel26);
            this.pCntUpd.Controls.Add(this.panel22);
            this.pCntUpd.Controls.Add(this.panel1);
            this.pCntUpd.Controls.Add(this.panel34);
            this.pCntUpd.Controls.Add(this.panel30);
            this.pCntUpd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pCntUpd.Location = new System.Drawing.Point(0, 0);
            this.pCntUpd.Name = "pCntUpd";
            this.pCntUpd.Size = new System.Drawing.Size(242, 457);
            this.pCntUpd.TabIndex = 99;
            // 
            // panel26
            // 
            this.panel26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Controls.Add(this.cmbComVHF);
            this.panel26.Controls.Add(this.panel28);
            this.panel26.Controls.Add(this.panel29);
            this.panel26.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel26.Location = new System.Drawing.Point(0, 428);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(242, 30);
            this.panel26.TabIndex = 101;
            // 
            // panel27
            // 
            this.panel27.Controls.Add(this.lVHF);
            this.panel27.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel27.Location = new System.Drawing.Point(0, 0);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(55, 26);
            this.panel27.TabIndex = 95;
            // 
            // lVHF
            // 
            this.lVHF.AutoSize = true;
            this.lVHF.Location = new System.Drawing.Point(6, 7);
            this.lVHF.Name = "lVHF";
            this.lVHF.Size = new System.Drawing.Size(28, 13);
            this.lVHF.TabIndex = 1;
            this.lVHF.Text = "VHF";
            // 
            // cmbComVHF
            // 
            this.cmbComVHF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbComVHF.FormattingEnabled = true;
            this.cmbComVHF.Location = new System.Drawing.Point(145, 2);
            this.cmbComVHF.Name = "cmbComVHF";
            this.cmbComVHF.Size = new System.Drawing.Size(61, 21);
            this.cmbComVHF.TabIndex = 94;
            // 
            // panel28
            // 
            this.panel28.Controls.Add(this.label12);
            this.panel28.Controls.Add(this.bConnectVHF);
            this.panel28.Location = new System.Drawing.Point(61, 2);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(30, 21);
            this.panel28.TabIndex = 87;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(28, 2);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(0, 15);
            this.label12.TabIndex = 84;
            // 
            // bConnectVHF
            // 
            this.bConnectVHF.BackColor = System.Drawing.Color.Red;
            this.bConnectVHF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectVHF.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectVHF.FlatAppearance.BorderSize = 0;
            this.bConnectVHF.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectVHF.Location = new System.Drawing.Point(0, -1);
            this.bConnectVHF.Name = "bConnectVHF";
            this.bConnectVHF.Size = new System.Drawing.Size(22, 22);
            this.bConnectVHF.TabIndex = 83;
            this.bConnectVHF.UseVisualStyleBackColor = false;
            this.bConnectVHF.Click += new System.EventHandler(this.bConnectVHF_Click);
            // 
            // panel29
            // 
            this.panel29.Controls.Add(this.pbWriteByteVHF);
            this.panel29.Controls.Add(this.pbReadByteVHF);
            this.panel29.Location = new System.Drawing.Point(97, 2);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(42, 21);
            this.panel29.TabIndex = 88;
            // 
            // pbWriteByteVHF
            // 
            this.pbWriteByteVHF.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteVHF.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteVHF.Name = "pbWriteByteVHF";
            this.pbWriteByteVHF.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteVHF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteVHF.TabIndex = 24;
            this.pbWriteByteVHF.TabStop = false;
            // 
            // pbReadByteVHF
            // 
            this.pbReadByteVHF.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteVHF.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteVHF.Name = "pbReadByteVHF";
            this.pbReadByteVHF.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteVHF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteVHF.TabIndex = 23;
            this.pbReadByteVHF.TabStop = false;
            // 
            // panel22
            // 
            this.panel22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel22.Controls.Add(this.panel23);
            this.panel22.Controls.Add(this.cmbComHF);
            this.panel22.Controls.Add(this.panel24);
            this.panel22.Controls.Add(this.panel25);
            this.panel22.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel22.Location = new System.Drawing.Point(0, 398);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(242, 30);
            this.panel22.TabIndex = 100;
            // 
            // panel23
            // 
            this.panel23.Controls.Add(this.lHF);
            this.panel23.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel23.Location = new System.Drawing.Point(0, 0);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(55, 26);
            this.panel23.TabIndex = 95;
            // 
            // lHF
            // 
            this.lHF.AutoSize = true;
            this.lHF.Location = new System.Drawing.Point(6, 7);
            this.lHF.Name = "lHF";
            this.lHF.Size = new System.Drawing.Size(21, 13);
            this.lHF.TabIndex = 1;
            this.lHF.Text = "HF";
            // 
            // cmbComHF
            // 
            this.cmbComHF.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbComHF.FormattingEnabled = true;
            this.cmbComHF.Location = new System.Drawing.Point(145, 2);
            this.cmbComHF.Name = "cmbComHF";
            this.cmbComHF.Size = new System.Drawing.Size(61, 21);
            this.cmbComHF.TabIndex = 94;
            // 
            // panel24
            // 
            this.panel24.Controls.Add(this.label8);
            this.panel24.Controls.Add(this.bConnectHF);
            this.panel24.Location = new System.Drawing.Point(61, 2);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(30, 21);
            this.panel24.TabIndex = 87;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(28, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 15);
            this.label8.TabIndex = 84;
            // 
            // bConnectHF
            // 
            this.bConnectHF.BackColor = System.Drawing.Color.Red;
            this.bConnectHF.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectHF.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectHF.FlatAppearance.BorderSize = 0;
            this.bConnectHF.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectHF.Location = new System.Drawing.Point(0, -1);
            this.bConnectHF.Name = "bConnectHF";
            this.bConnectHF.Size = new System.Drawing.Size(22, 22);
            this.bConnectHF.TabIndex = 83;
            this.bConnectHF.UseVisualStyleBackColor = false;
            this.bConnectHF.Click += new System.EventHandler(this.bConnectHF_Click);
            // 
            // panel25
            // 
            this.panel25.Controls.Add(this.pbWriteByteHF);
            this.panel25.Controls.Add(this.pbReadByteHF);
            this.panel25.Location = new System.Drawing.Point(97, 2);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(42, 21);
            this.panel25.TabIndex = 88;
            // 
            // pbWriteByteHF
            // 
            this.pbWriteByteHF.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteHF.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteHF.Name = "pbWriteByteHF";
            this.pbWriteByteHF.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteHF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteHF.TabIndex = 24;
            this.pbWriteByteHF.TabStop = false;
            // 
            // pbReadByteHF
            // 
            this.pbReadByteHF.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteHF.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteHF.Name = "pbReadByteHF";
            this.pbReadByteHF.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteHF.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteHF.TabIndex = 23;
            this.pbReadByteHF.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.panel21);
            this.panel1.Controls.Add(this.cmbComModem);
            this.panel1.Controls.Add(this.panel19);
            this.panel1.Controls.Add(this.panel20);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 368);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(242, 30);
            this.panel1.TabIndex = 99;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.lModem);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(55, 26);
            this.panel21.TabIndex = 95;
            // 
            // lModem
            // 
            this.lModem.AutoSize = true;
            this.lModem.Location = new System.Drawing.Point(5, 7);
            this.lModem.Name = "lModem";
            this.lModem.Size = new System.Drawing.Size(42, 13);
            this.lModem.TabIndex = 0;
            this.lModem.Text = "Modem";
            // 
            // cmbComModem
            // 
            this.cmbComModem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbComModem.FormattingEnabled = true;
            this.cmbComModem.Location = new System.Drawing.Point(145, 2);
            this.cmbComModem.Name = "cmbComModem";
            this.cmbComModem.Size = new System.Drawing.Size(61, 21);
            this.cmbComModem.TabIndex = 94;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.label10);
            this.panel19.Controls.Add(this.bConnectModem);
            this.panel19.Location = new System.Drawing.Point(61, 2);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(30, 21);
            this.panel19.TabIndex = 87;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(28, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 15);
            this.label10.TabIndex = 84;
            // 
            // bConnectModem
            // 
            this.bConnectModem.BackColor = System.Drawing.Color.Red;
            this.bConnectModem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectModem.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectModem.FlatAppearance.BorderSize = 0;
            this.bConnectModem.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectModem.Location = new System.Drawing.Point(0, -1);
            this.bConnectModem.Name = "bConnectModem";
            this.bConnectModem.Size = new System.Drawing.Size(22, 22);
            this.bConnectModem.TabIndex = 83;
            this.bConnectModem.UseVisualStyleBackColor = false;
            this.bConnectModem.Click += new System.EventHandler(this.bConnectModem_Click);
            // 
            // panel20
            // 
            this.panel20.Controls.Add(this.pbWriteByteModem);
            this.panel20.Controls.Add(this.pbReadByteModem);
            this.panel20.Location = new System.Drawing.Point(97, 2);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(42, 21);
            this.panel20.TabIndex = 88;
            // 
            // pbWriteByteModem
            // 
            this.pbWriteByteModem.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteModem.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteModem.Name = "pbWriteByteModem";
            this.pbWriteByteModem.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteModem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteModem.TabIndex = 24;
            this.pbWriteByteModem.TabStop = false;
            // 
            // pbReadByteModem
            // 
            this.pbReadByteModem.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteModem.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteModem.Name = "pbReadByteModem";
            this.pbReadByteModem.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteModem.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteModem.TabIndex = 23;
            this.pbReadByteModem.TabStop = false;
            // 
            // panel34
            // 
            this.panel34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel34.Controls.Add(this.pConnectClient3G);
            this.panel34.Controls.Add(this.pCreateServer3G);
            this.panel34.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel34.Location = new System.Drawing.Point(0, 310);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(242, 58);
            this.panel34.TabIndex = 98;
            // 
            // pConnectClient3G
            // 
            this.pConnectClient3G.Location = new System.Drawing.Point(3, 30);
            this.pConnectClient3G.Name = "pConnectClient3G";
            this.pConnectClient3G.Size = new System.Drawing.Size(232, 20);
            this.pConnectClient3G.TabIndex = 98;
            // 
            // pCreateServer3G
            // 
            this.pCreateServer3G.Controls.Add(this.panel35);
            this.pCreateServer3G.Controls.Add(this.panel36);
            this.pCreateServer3G.Controls.Add(this.panel37);
            this.pCreateServer3G.Dock = System.Windows.Forms.DockStyle.Top;
            this.pCreateServer3G.Location = new System.Drawing.Point(0, 0);
            this.pCreateServer3G.Name = "pCreateServer3G";
            this.pCreateServer3G.Size = new System.Drawing.Size(238, 27);
            this.pCreateServer3G.TabIndex = 96;
            // 
            // panel35
            // 
            this.panel35.Controls.Add(this.lServer3G);
            this.panel35.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel35.Location = new System.Drawing.Point(0, 0);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(47, 27);
            this.panel35.TabIndex = 98;
            // 
            // lServer3G
            // 
            this.lServer3G.AutoSize = true;
            this.lServer3G.Location = new System.Drawing.Point(6, 7);
            this.lServer3G.Name = "lServer3G";
            this.lServer3G.Size = new System.Drawing.Size(21, 13);
            this.lServer3G.TabIndex = 1;
            this.lServer3G.Text = "3G";
            // 
            // panel36
            // 
            this.panel36.Controls.Add(this.label19);
            this.panel36.Controls.Add(this.bConnect3G);
            this.panel36.Location = new System.Drawing.Point(61, 4);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(30, 21);
            this.panel36.TabIndex = 96;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(28, 2);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(0, 15);
            this.label19.TabIndex = 84;
            // 
            // bConnect3G
            // 
            this.bConnect3G.BackColor = System.Drawing.Color.Red;
            this.bConnect3G.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnect3G.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnect3G.FlatAppearance.BorderSize = 0;
            this.bConnect3G.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnect3G.Location = new System.Drawing.Point(0, -1);
            this.bConnect3G.Name = "bConnect3G";
            this.bConnect3G.Size = new System.Drawing.Size(22, 22);
            this.bConnect3G.TabIndex = 83;
            this.bConnect3G.UseVisualStyleBackColor = false;
            this.bConnect3G.Click += new System.EventHandler(this.bConnect3G_Click);
            // 
            // panel37
            // 
            this.panel37.Controls.Add(this.pbWriteByte3G);
            this.panel37.Controls.Add(this.pbReadByte3G);
            this.panel37.Location = new System.Drawing.Point(100, 3);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(42, 21);
            this.panel37.TabIndex = 97;
            // 
            // pbWriteByte3G
            // 
            this.pbWriteByte3G.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByte3G.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByte3G.Name = "pbWriteByte3G";
            this.pbWriteByte3G.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByte3G.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByte3G.TabIndex = 24;
            this.pbWriteByte3G.TabStop = false;
            // 
            // pbReadByte3G
            // 
            this.pbReadByte3G.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByte3G.Location = new System.Drawing.Point(3, 3);
            this.pbReadByte3G.Name = "pbReadByte3G";
            this.pbReadByte3G.Size = new System.Drawing.Size(15, 15);
            this.pbReadByte3G.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByte3G.TabIndex = 23;
            this.pbReadByte3G.TabStop = false;
            // 
            // panel30
            // 
            this.panel30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel30.Controls.Add(this.pConnectClientRRC);
            this.panel30.Controls.Add(this.pCreateServerRRC);
            this.panel30.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel30.Location = new System.Drawing.Point(0, 0);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(242, 310);
            this.panel30.TabIndex = 97;
            // 
            // pConnectClientRRC
            // 
            this.pConnectClientRRC.Controls.Add(this.rtbClientConnect);
            this.pConnectClientRRC.Controls.Add(this.panel2);
            this.pConnectClientRRC.Controls.Add(this.pClientRRC5);
            this.pConnectClientRRC.Controls.Add(this.pClientRRC4);
            this.pConnectClientRRC.Controls.Add(this.pClientRRC3);
            this.pConnectClientRRC.Controls.Add(this.pClientRRC2);
            this.pConnectClientRRC.Controls.Add(this.pClientRRC1);
            this.pConnectClientRRC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pConnectClientRRC.Location = new System.Drawing.Point(0, 26);
            this.pConnectClientRRC.Name = "pConnectClientRRC";
            this.pConnectClientRRC.Size = new System.Drawing.Size(238, 280);
            this.pConnectClientRRC.TabIndex = 97;
            // 
            // rtbClientConnect
            // 
            this.rtbClientConnect.BackColor = System.Drawing.Color.Tan;
            this.rtbClientConnect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rtbClientConnect.Location = new System.Drawing.Point(0, 156);
            this.rtbClientConnect.Name = "rtbClientConnect";
            this.rtbClientConnect.Size = new System.Drawing.Size(238, 124);
            this.rtbClientConnect.TabIndex = 103;
            this.rtbClientConnect.Text = "";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel9);
            this.panel2.Controls.Add(this.panel13);
            this.panel2.Controls.Add(this.panel17);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 130);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(238, 26);
            this.panel2.TabIndex = 102;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.label21);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(118, 26);
            this.panel9.TabIndex = 98;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(5, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "IP";
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.label22);
            this.panel13.Controls.Add(this.bConnectClientRRC6);
            this.panel13.Location = new System.Drawing.Point(159, 3);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(30, 21);
            this.panel13.TabIndex = 96;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(28, 2);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(0, 15);
            this.label22.TabIndex = 84;
            // 
            // bConnectClientRRC6
            // 
            this.bConnectClientRRC6.BackColor = System.Drawing.Color.Red;
            this.bConnectClientRRC6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectClientRRC6.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectClientRRC6.FlatAppearance.BorderSize = 0;
            this.bConnectClientRRC6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectClientRRC6.Location = new System.Drawing.Point(3, 1);
            this.bConnectClientRRC6.Name = "bConnectClientRRC6";
            this.bConnectClientRRC6.Size = new System.Drawing.Size(18, 18);
            this.bConnectClientRRC6.TabIndex = 83;
            this.bConnectClientRRC6.UseVisualStyleBackColor = false;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.pbWriteByteClientRRC6);
            this.panel17.Controls.Add(this.pbReadByteClientRRC6);
            this.panel17.Location = new System.Drawing.Point(192, 3);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(42, 21);
            this.panel17.TabIndex = 97;
            // 
            // pbWriteByteClientRRC6
            // 
            this.pbWriteByteClientRRC6.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteClientRRC6.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteClientRRC6.Name = "pbWriteByteClientRRC6";
            this.pbWriteByteClientRRC6.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteClientRRC6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteClientRRC6.TabIndex = 24;
            this.pbWriteByteClientRRC6.TabStop = false;
            // 
            // pbReadByteClientRRC6
            // 
            this.pbReadByteClientRRC6.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteClientRRC6.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteClientRRC6.Name = "pbReadByteClientRRC6";
            this.pbReadByteClientRRC6.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteClientRRC6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteClientRRC6.TabIndex = 23;
            this.pbReadByteClientRRC6.TabStop = false;
            // 
            // pClientRRC5
            // 
            this.pClientRRC5.Controls.Add(this.panel18);
            this.pClientRRC5.Controls.Add(this.panel38);
            this.pClientRRC5.Controls.Add(this.panel39);
            this.pClientRRC5.Dock = System.Windows.Forms.DockStyle.Top;
            this.pClientRRC5.Location = new System.Drawing.Point(0, 104);
            this.pClientRRC5.Name = "pClientRRC5";
            this.pClientRRC5.Size = new System.Drawing.Size(238, 26);
            this.pClientRRC5.TabIndex = 101;
            // 
            // panel18
            // 
            this.panel18.Controls.Add(this.label18);
            this.panel18.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel18.Location = new System.Drawing.Point(0, 0);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(118, 26);
            this.panel18.TabIndex = 98;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "IP";
            // 
            // panel38
            // 
            this.panel38.Controls.Add(this.label20);
            this.panel38.Controls.Add(this.bConnectClientRRC5);
            this.panel38.Location = new System.Drawing.Point(159, 3);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(30, 21);
            this.panel38.TabIndex = 96;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(28, 2);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(0, 15);
            this.label20.TabIndex = 84;
            // 
            // bConnectClientRRC5
            // 
            this.bConnectClientRRC5.BackColor = System.Drawing.Color.Red;
            this.bConnectClientRRC5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectClientRRC5.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectClientRRC5.FlatAppearance.BorderSize = 0;
            this.bConnectClientRRC5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectClientRRC5.Location = new System.Drawing.Point(3, 1);
            this.bConnectClientRRC5.Name = "bConnectClientRRC5";
            this.bConnectClientRRC5.Size = new System.Drawing.Size(18, 18);
            this.bConnectClientRRC5.TabIndex = 83;
            this.bConnectClientRRC5.UseVisualStyleBackColor = false;
            // 
            // panel39
            // 
            this.panel39.Controls.Add(this.pbWriteByteClientRRC5);
            this.panel39.Controls.Add(this.pbReadByteClientRRC5);
            this.panel39.Location = new System.Drawing.Point(192, 3);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(42, 21);
            this.panel39.TabIndex = 97;
            // 
            // pbWriteByteClientRRC5
            // 
            this.pbWriteByteClientRRC5.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteClientRRC5.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteClientRRC5.Name = "pbWriteByteClientRRC5";
            this.pbWriteByteClientRRC5.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteClientRRC5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteClientRRC5.TabIndex = 24;
            this.pbWriteByteClientRRC5.TabStop = false;
            // 
            // pbReadByteClientRRC5
            // 
            this.pbReadByteClientRRC5.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteClientRRC5.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteClientRRC5.Name = "pbReadByteClientRRC5";
            this.pbReadByteClientRRC5.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteClientRRC5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteClientRRC5.TabIndex = 23;
            this.pbReadByteClientRRC5.TabStop = false;
            // 
            // pClientRRC4
            // 
            this.pClientRRC4.Controls.Add(this.panel14);
            this.pClientRRC4.Controls.Add(this.panel15);
            this.pClientRRC4.Controls.Add(this.panel16);
            this.pClientRRC4.Dock = System.Windows.Forms.DockStyle.Top;
            this.pClientRRC4.Location = new System.Drawing.Point(0, 78);
            this.pClientRRC4.Name = "pClientRRC4";
            this.pClientRRC4.Size = new System.Drawing.Size(238, 26);
            this.pClientRRC4.TabIndex = 100;
            // 
            // panel14
            // 
            this.panel14.Controls.Add(this.label15);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel14.Location = new System.Drawing.Point(0, 0);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(118, 26);
            this.panel14.TabIndex = 98;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(5, 7);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "IP";
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.label17);
            this.panel15.Controls.Add(this.bConnectClientRRC4);
            this.panel15.Location = new System.Drawing.Point(159, 3);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(30, 21);
            this.panel15.TabIndex = 96;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(28, 2);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(0, 15);
            this.label17.TabIndex = 84;
            // 
            // bConnectClientRRC4
            // 
            this.bConnectClientRRC4.BackColor = System.Drawing.Color.Red;
            this.bConnectClientRRC4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectClientRRC4.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectClientRRC4.FlatAppearance.BorderSize = 0;
            this.bConnectClientRRC4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectClientRRC4.Location = new System.Drawing.Point(3, 1);
            this.bConnectClientRRC4.Name = "bConnectClientRRC4";
            this.bConnectClientRRC4.Size = new System.Drawing.Size(18, 18);
            this.bConnectClientRRC4.TabIndex = 83;
            this.bConnectClientRRC4.UseVisualStyleBackColor = false;
            // 
            // panel16
            // 
            this.panel16.Controls.Add(this.pbWriteByteClientRRC4);
            this.panel16.Controls.Add(this.pbReadByteClientRRC4);
            this.panel16.Location = new System.Drawing.Point(192, 3);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(42, 21);
            this.panel16.TabIndex = 97;
            // 
            // pbWriteByteClientRRC4
            // 
            this.pbWriteByteClientRRC4.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteClientRRC4.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteClientRRC4.Name = "pbWriteByteClientRRC4";
            this.pbWriteByteClientRRC4.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteClientRRC4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteClientRRC4.TabIndex = 24;
            this.pbWriteByteClientRRC4.TabStop = false;
            // 
            // pbReadByteClientRRC4
            // 
            this.pbReadByteClientRRC4.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteClientRRC4.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteClientRRC4.Name = "pbReadByteClientRRC4";
            this.pbReadByteClientRRC4.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteClientRRC4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteClientRRC4.TabIndex = 23;
            this.pbReadByteClientRRC4.TabStop = false;
            // 
            // pClientRRC3
            // 
            this.pClientRRC3.Controls.Add(this.panel10);
            this.pClientRRC3.Controls.Add(this.panel11);
            this.pClientRRC3.Controls.Add(this.panel12);
            this.pClientRRC3.Dock = System.Windows.Forms.DockStyle.Top;
            this.pClientRRC3.Location = new System.Drawing.Point(0, 52);
            this.pClientRRC3.Name = "pClientRRC3";
            this.pClientRRC3.Size = new System.Drawing.Size(238, 26);
            this.pClientRRC3.TabIndex = 99;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.label13);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(118, 26);
            this.panel10.TabIndex = 98;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 7);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "IP";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.label14);
            this.panel11.Controls.Add(this.bConnectClientRRC3);
            this.panel11.Location = new System.Drawing.Point(159, 3);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(30, 21);
            this.panel11.TabIndex = 96;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(28, 2);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(0, 15);
            this.label14.TabIndex = 84;
            // 
            // bConnectClientRRC3
            // 
            this.bConnectClientRRC3.BackColor = System.Drawing.Color.Red;
            this.bConnectClientRRC3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectClientRRC3.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectClientRRC3.FlatAppearance.BorderSize = 0;
            this.bConnectClientRRC3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectClientRRC3.Location = new System.Drawing.Point(3, 1);
            this.bConnectClientRRC3.Name = "bConnectClientRRC3";
            this.bConnectClientRRC3.Size = new System.Drawing.Size(18, 18);
            this.bConnectClientRRC3.TabIndex = 83;
            this.bConnectClientRRC3.UseVisualStyleBackColor = false;
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.pbWriteByteClientRRC3);
            this.panel12.Controls.Add(this.pbReadByteClientRRC3);
            this.panel12.Location = new System.Drawing.Point(192, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(42, 21);
            this.panel12.TabIndex = 97;
            // 
            // pbWriteByteClientRRC3
            // 
            this.pbWriteByteClientRRC3.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteClientRRC3.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteClientRRC3.Name = "pbWriteByteClientRRC3";
            this.pbWriteByteClientRRC3.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteClientRRC3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteClientRRC3.TabIndex = 24;
            this.pbWriteByteClientRRC3.TabStop = false;
            // 
            // pbReadByteClientRRC3
            // 
            this.pbReadByteClientRRC3.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteClientRRC3.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteClientRRC3.Name = "pbReadByteClientRRC3";
            this.pbReadByteClientRRC3.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteClientRRC3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteClientRRC3.TabIndex = 23;
            this.pbReadByteClientRRC3.TabStop = false;
            // 
            // pClientRRC2
            // 
            this.pClientRRC2.Controls.Add(this.panel6);
            this.pClientRRC2.Controls.Add(this.panel7);
            this.pClientRRC2.Controls.Add(this.panel8);
            this.pClientRRC2.Dock = System.Windows.Forms.DockStyle.Top;
            this.pClientRRC2.Location = new System.Drawing.Point(0, 26);
            this.pClientRRC2.Name = "pClientRRC2";
            this.pClientRRC2.Size = new System.Drawing.Size(238, 26);
            this.pClientRRC2.TabIndex = 98;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label7);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(118, 26);
            this.panel6.TabIndex = 98;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(5, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "IP";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.bConnectClientRRC2);
            this.panel7.Location = new System.Drawing.Point(159, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(30, 21);
            this.panel7.TabIndex = 96;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(28, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 15);
            this.label11.TabIndex = 84;
            // 
            // bConnectClientRRC2
            // 
            this.bConnectClientRRC2.BackColor = System.Drawing.Color.Red;
            this.bConnectClientRRC2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectClientRRC2.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectClientRRC2.FlatAppearance.BorderSize = 0;
            this.bConnectClientRRC2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectClientRRC2.Location = new System.Drawing.Point(3, 1);
            this.bConnectClientRRC2.Name = "bConnectClientRRC2";
            this.bConnectClientRRC2.Size = new System.Drawing.Size(18, 18);
            this.bConnectClientRRC2.TabIndex = 83;
            this.bConnectClientRRC2.UseVisualStyleBackColor = false;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.pbWriteByteClientRRC2);
            this.panel8.Controls.Add(this.pbReadByteClientRRC2);
            this.panel8.Location = new System.Drawing.Point(192, 3);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(42, 21);
            this.panel8.TabIndex = 97;
            // 
            // pbWriteByteClientRRC2
            // 
            this.pbWriteByteClientRRC2.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteClientRRC2.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteClientRRC2.Name = "pbWriteByteClientRRC2";
            this.pbWriteByteClientRRC2.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteClientRRC2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteClientRRC2.TabIndex = 24;
            this.pbWriteByteClientRRC2.TabStop = false;
            // 
            // pbReadByteClientRRC2
            // 
            this.pbReadByteClientRRC2.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteClientRRC2.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteClientRRC2.Name = "pbReadByteClientRRC2";
            this.pbReadByteClientRRC2.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteClientRRC2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteClientRRC2.TabIndex = 23;
            this.pbReadByteClientRRC2.TabStop = false;
            // 
            // pClientRRC1
            // 
            this.pClientRRC1.Controls.Add(this.panel3);
            this.pClientRRC1.Controls.Add(this.panel4);
            this.pClientRRC1.Controls.Add(this.panel5);
            this.pClientRRC1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pClientRRC1.Location = new System.Drawing.Point(0, 0);
            this.pClientRRC1.Name = "pClientRRC1";
            this.pClientRRC1.Size = new System.Drawing.Size(238, 26);
            this.pClientRRC1.TabIndex = 97;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lClient1);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(118, 26);
            this.panel3.TabIndex = 98;
            // 
            // lClient1
            // 
            this.lClient1.AutoSize = true;
            this.lClient1.Location = new System.Drawing.Point(5, 7);
            this.lClient1.Name = "lClient1";
            this.lClient1.Size = new System.Drawing.Size(17, 13);
            this.lClient1.TabIndex = 0;
            this.lClient1.Text = "IP";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.bConnectClientRRC1);
            this.panel4.Location = new System.Drawing.Point(159, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(30, 21);
            this.panel4.TabIndex = 96;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(28, 2);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 15);
            this.label9.TabIndex = 84;
            // 
            // bConnectClientRRC1
            // 
            this.bConnectClientRRC1.BackColor = System.Drawing.Color.Red;
            this.bConnectClientRRC1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectClientRRC1.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectClientRRC1.FlatAppearance.BorderSize = 0;
            this.bConnectClientRRC1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectClientRRC1.Location = new System.Drawing.Point(3, 1);
            this.bConnectClientRRC1.Name = "bConnectClientRRC1";
            this.bConnectClientRRC1.Size = new System.Drawing.Size(18, 18);
            this.bConnectClientRRC1.TabIndex = 83;
            this.bConnectClientRRC1.UseVisualStyleBackColor = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.pbWriteByteClientRRC1);
            this.panel5.Controls.Add(this.pbReadByteClientRRC1);
            this.panel5.Location = new System.Drawing.Point(192, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(42, 21);
            this.panel5.TabIndex = 97;
            // 
            // pbWriteByteClientRRC1
            // 
            this.pbWriteByteClientRRC1.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteClientRRC1.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteClientRRC1.Name = "pbWriteByteClientRRC1";
            this.pbWriteByteClientRRC1.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteClientRRC1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteClientRRC1.TabIndex = 24;
            this.pbWriteByteClientRRC1.TabStop = false;
            // 
            // pbReadByteClientRRC1
            // 
            this.pbReadByteClientRRC1.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteClientRRC1.Location = new System.Drawing.Point(3, 3);
            this.pbReadByteClientRRC1.Name = "pbReadByteClientRRC1";
            this.pbReadByteClientRRC1.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteClientRRC1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteClientRRC1.TabIndex = 23;
            this.pbReadByteClientRRC1.TabStop = false;
            // 
            // pCreateServerRRC
            // 
            this.pCreateServerRRC.Controls.Add(this.tbPort);
            this.pCreateServerRRC.Controls.Add(this.cmbxServerIP);
            this.pCreateServerRRC.Controls.Add(this.panel31);
            this.pCreateServerRRC.Controls.Add(this.panel32);
            this.pCreateServerRRC.Controls.Add(this.panel33);
            this.pCreateServerRRC.Dock = System.Windows.Forms.DockStyle.Top;
            this.pCreateServerRRC.Location = new System.Drawing.Point(0, 0);
            this.pCreateServerRRC.Name = "pCreateServerRRC";
            this.pCreateServerRRC.Size = new System.Drawing.Size(238, 26);
            this.pCreateServerRRC.TabIndex = 96;
            // 
            // tbPort
            // 
            this.tbPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.tbPort.Location = new System.Drawing.Point(190, 2);
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(47, 20);
            this.tbPort.TabIndex = 60;
            this.tbPort.Text = "9102";
            // 
            // cmbxServerIP
            // 
            this.cmbxServerIP.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbxServerIP.FormattingEnabled = true;
            this.cmbxServerIP.Location = new System.Drawing.Point(106, 1);
            this.cmbxServerIP.Name = "cmbxServerIP";
            this.cmbxServerIP.Size = new System.Drawing.Size(81, 21);
            this.cmbxServerIP.TabIndex = 60;
            // 
            // panel31
            // 
            this.panel31.Controls.Add(this.lServerRRC);
            this.panel31.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel31.Location = new System.Drawing.Point(0, 0);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(34, 26);
            this.panel31.TabIndex = 98;
            // 
            // lServerRRC
            // 
            this.lServerRRC.AutoSize = true;
            this.lServerRRC.Location = new System.Drawing.Point(5, 7);
            this.lServerRRC.Name = "lServerRRC";
            this.lServerRRC.Size = new System.Drawing.Size(24, 13);
            this.lServerRRC.TabIndex = 0;
            this.lServerRRC.Text = "ОД";
            // 
            // panel32
            // 
            this.panel32.Controls.Add(this.label16);
            this.panel32.Controls.Add(this.bConnectRRC);
            this.panel32.Location = new System.Drawing.Point(31, 2);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(30, 21);
            this.panel32.TabIndex = 96;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(28, 2);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(0, 15);
            this.label16.TabIndex = 84;
            // 
            // bConnectRRC
            // 
            this.bConnectRRC.BackColor = System.Drawing.Color.Red;
            this.bConnectRRC.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bConnectRRC.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.bConnectRRC.FlatAppearance.BorderSize = 0;
            this.bConnectRRC.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.bConnectRRC.Location = new System.Drawing.Point(7, -1);
            this.bConnectRRC.Name = "bConnectRRC";
            this.bConnectRRC.Size = new System.Drawing.Size(22, 22);
            this.bConnectRRC.TabIndex = 83;
            this.bConnectRRC.UseVisualStyleBackColor = false;
            this.bConnectRRC.Click += new System.EventHandler(this.bConnectRRC_Click);
            // 
            // panel33
            // 
            this.panel33.Controls.Add(this.pbWriteByteRRC);
            this.panel33.Controls.Add(this.pbReadByteRRC);
            this.panel33.Location = new System.Drawing.Point(61, 1);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(42, 21);
            this.panel33.TabIndex = 97;
            // 
            // pbWriteByteRRC
            // 
            this.pbWriteByteRRC.BackColor = System.Drawing.Color.Transparent;
            this.pbWriteByteRRC.Location = new System.Drawing.Point(24, 3);
            this.pbWriteByteRRC.Name = "pbWriteByteRRC";
            this.pbWriteByteRRC.Size = new System.Drawing.Size(15, 15);
            this.pbWriteByteRRC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbWriteByteRRC.TabIndex = 24;
            this.pbWriteByteRRC.TabStop = false;
            // 
            // pbReadByteRRC
            // 
            this.pbReadByteRRC.BackColor = System.Drawing.Color.Transparent;
            this.pbReadByteRRC.Location = new System.Drawing.Point(5, 3);
            this.pbReadByteRRC.Name = "pbReadByteRRC";
            this.pbReadByteRRC.Size = new System.Drawing.Size(15, 15);
            this.pbReadByteRRC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbReadByteRRC.TabIndex = 23;
            this.pbReadByteRRC.TabStop = false;
            // 
            // TestInformExchangeBRZ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 491);
            this.Controls.Add(this.pStateConnection);
            this.Controls.Add(this.pLog);
            this.Controls.Add(this.pParamCmd);
            this.Controls.Add(this.pState);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "TestInformExchangeBRZ";
            this.Text = "ОД сервер";
            this.pState.ResumeLayout(false);
            this.pState.PerformLayout();
            this.pParamCmd.ResumeLayout(false);
            this.pChooseRRD.ResumeLayout(false);
            this.pChooseRRD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudAddressReceiver)).EndInit();
            this.tbcCmdPC.ResumeLayout(false);
            this.tbcRequest.ResumeLayout(false);
            this.pRequest.ResumeLayout(false);
            this.pRequest.PerformLayout();
            this.tbpRegime.ResumeLayout(false);
            this.pRegimeWork.ResumeLayout(false);
            this.pRegimeWork.PerformLayout();
            this.tbpRangeRecon.ResumeLayout(false);
            this.tbpRangeRecon.PerformLayout();
            this.pReconSectorRange.ResumeLayout(false);
            this.pReconSectorRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMaxRecon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMinRecon)).EndInit();
            this.tbcRangeSuppress.ResumeLayout(false);
            this.tbcRangeSuppress.PerformLayout();
            this.pSupressSectorRange.ResumeLayout(false);
            this.pSupressSectorRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMaxSupress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingMinSupress)).EndInit();
            this.tbcRangeForbid.ResumeLayout(false);
            this.pForbidRangeFreq.ResumeLayout(false);
            this.pForbidRangeFreq.PerformLayout();
            this.tbpSynchronizePC.ResumeLayout(false);
            this.pSynchronizePC.ResumeLayout(false);
            this.pSynchronizePC.PerformLayout();
            this.tbpTextPC.ResumeLayout(false);
            this.pTextMessagePC.ResumeLayout(false);
            this.tbcSuppressFWS.ResumeLayout(false);
            this.tbcSuppressFWS.PerformLayout();
            this.pSupressFWS.ResumeLayout(false);
            this.pSupressFWS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudBearingFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudThresholdFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDurationGlobalFWS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPriorSupFWS)).EndInit();
            this.tbcSuppressFHSS.ResumeLayout(false);
            this.tbcSuppressFHSS.PerformLayout();
            this.pSupressFHSS.ResumeLayout(false);
            this.pSupressFHSS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDuratSupFHSS)).EndInit();
            this.tbcRequestBearing.ResumeLayout(false);
            this.pRequestBearing.ResumeLayout(false);
            this.pRequestBearing.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudFreqID)).EndInit();
            this.tbpReceiptPC.ResumeLayout(false);
            this.pReceiptPC.ResumeLayout(false);
            this.pReceiptPC.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCodeErrorPC)).EndInit();
            this.pLog.ResumeLayout(false);
            this.tbcLog.ResumeLayout(false);
            this.tbpRRD1.ResumeLayout(false);
            this.tbpRRD2.ResumeLayout(false);
            this.tbpRRD3.ResumeLayout(false);
            this.tbpRRD4.ResumeLayout(false);
            this.tbpRRD5.ResumeLayout(false);
            this.pShowLog.ResumeLayout(false);
            this.pShowLog.PerformLayout();
            this.pStateConnection.ResumeLayout(false);
            this.pCntUpd.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel27.PerformLayout();
            this.panel28.ResumeLayout(false);
            this.panel28.PerformLayout();
            this.panel29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteVHF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteVHF)).EndInit();
            this.panel22.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel23.PerformLayout();
            this.panel24.ResumeLayout(false);
            this.panel24.PerformLayout();
            this.panel25.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteHF)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteHF)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.panel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteModem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteModem)).EndInit();
            this.panel34.ResumeLayout(false);
            this.pCreateServer3G.ResumeLayout(false);
            this.panel35.ResumeLayout(false);
            this.panel35.PerformLayout();
            this.panel36.ResumeLayout(false);
            this.panel36.PerformLayout();
            this.panel37.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByte3G)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByte3G)).EndInit();
            this.panel30.ResumeLayout(false);
            this.pConnectClientRRC.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC6)).EndInit();
            this.pClientRRC5.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel18.PerformLayout();
            this.panel38.ResumeLayout(false);
            this.panel38.PerformLayout();
            this.panel39.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC5)).EndInit();
            this.pClientRRC4.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.panel16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC4)).EndInit();
            this.pClientRRC3.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC3)).EndInit();
            this.pClientRRC2.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC2)).EndInit();
            this.pClientRRC1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteClientRRC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteClientRRC1)).EndInit();
            this.pCreateServerRRC.ResumeLayout(false);
            this.pCreateServerRRC.PerformLayout();
            this.panel31.ResumeLayout(false);
            this.panel31.PerformLayout();
            this.panel32.ResumeLayout(false);
            this.panel32.PerformLayout();
            this.panel33.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbWriteByteRRC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbReadByteRRC)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pState;
        private System.Windows.Forms.Panel pParamCmd;
        private System.Windows.Forms.Panel pLog;
        private System.Windows.Forms.Panel pShowLog;
        private System.Windows.Forms.CheckBox chbByteHex;
        private System.Windows.Forms.CheckBox chbDetail;
        private System.Windows.Forms.CheckBox chbName;
        private System.Windows.Forms.TabControl tbcCmdPC;
        private System.Windows.Forms.TabPage tbcRequest;
        private System.Windows.Forms.Button bSendRequest;
        private System.Windows.Forms.Panel pRequest;
        private System.Windows.Forms.ComboBox cmbTypeRequest;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TabPage tbpRegime;
        private System.Windows.Forms.Button bSendRegimeWork;
        private System.Windows.Forms.Panel pRegimeWork;
        private System.Windows.Forms.ComboBox cmbRegime;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.TabPage tbpRangeRecon;
        private System.Windows.Forms.Button bSendReconSectorRange;
        private System.Windows.Forms.Panel pReconSectorRange;
        private System.Windows.Forms.NumericUpDown nudBearingMaxRecon;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.NumericUpDown nudBearingMinRecon;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox tbFreqMaxRecon;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox tbFreqMinRecon;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TabPage tbcRangeSuppress;
        private System.Windows.Forms.Button bSendSupressSectorRange;
        private System.Windows.Forms.Panel pSupressSectorRange;
        private System.Windows.Forms.NumericUpDown nudBearingMaxSupress;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.NumericUpDown nudBearingMinSupress;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TextBox tbFreqMaxSupress;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox tbFreqMinSupress;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TabPage tbcRangeForbid;
        private System.Windows.Forms.Button bSendForbidRangeFreq;
        private System.Windows.Forms.Panel pForbidRangeFreq;
        private System.Windows.Forms.TextBox tbFreqMaxForbid;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox tbFreqMinForbid;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.ComboBox cmbActForbid;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TabPage tbpSynchronizePC;
        private System.Windows.Forms.Button bSendSynchronizePC;
        private System.Windows.Forms.Panel pSynchronizePC;
        private System.Windows.Forms.DateTimePicker dtpTimeSinchroPC;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TabPage tbpTextPC;
        private System.Windows.Forms.Button bSendTextMessagePC;
        private System.Windows.Forms.Panel pTextMessagePC;
        private System.Windows.Forms.RichTextBox rtbTextPC;
        private System.Windows.Forms.TabPage tbcSuppressFWS;
        private System.Windows.Forms.Button bSendSupressFWS;
        private System.Windows.Forms.Panel pSupressFWS;
        private System.Windows.Forms.NumericUpDown nudBearingFWS;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown nudThresholdFWS;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbManipulationFWS;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudDurationGlobalFWS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbDeviationFWS;
        private System.Windows.Forms.Label lDeviationFWS;
        private System.Windows.Forms.ComboBox cmbTypeModulationFWS;
        private System.Windows.Forms.Label lTypeModulationFWS;
        private System.Windows.Forms.NumericUpDown nudPriorSupFWS;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox tbFreqSupFWS;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TabPage tbcSuppressFHSS;
        private System.Windows.Forms.Button bSendSupressFHSS;
        private System.Windows.Forms.Panel pSupressFHSS;
        private System.Windows.Forms.TextBox tbFreqMaxSupFHSS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown nudDuratSupFHSS;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbManipulation;
        private System.Windows.Forms.Label lManipulation;
        private System.Windows.Forms.ComboBox cmbDeviation;
        private System.Windows.Forms.Label lDeviation;
        private System.Windows.Forms.ComboBox cmbTypeModulation;
        private System.Windows.Forms.Label lTypeModulation;
        private System.Windows.Forms.ComboBox cmbStepSupFHSS;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox tbFreqMinSupFHSS;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.TabPage tbcRequestBearing;
        private System.Windows.Forms.Button bSendRequestBearing;
        private System.Windows.Forms.Panel pRequestBearing;
        private System.Windows.Forms.TextBox tbFreqRecBearing;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.ComboBox cmbTypeReqBearing;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TabPage tbpReceiptPC;
        private System.Windows.Forms.Button bSendReceiptPC;
        private System.Windows.Forms.Panel pReceiptPC;
        private System.Windows.Forms.NumericUpDown nudCodeErrorPC;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Button bRangeReconClear;
        private System.Windows.Forms.Button bRangeReconAdd;
        private System.Windows.Forms.RichTextBox rtbMemoRangeRecon;
        private System.Windows.Forms.RichTextBox rtbMemoRangeSuppr;
        private System.Windows.Forms.Button bRangeSupprClear;
        private System.Windows.Forms.Button bRangeSupprAdd;
        private System.Windows.Forms.CheckBox chbAutoRangeR;
        private System.Windows.Forms.CheckBox chbAutoRangeS;
        private System.Windows.Forms.CheckBox chbAutoSupprFWS;
        private System.Windows.Forms.RichTextBox rtbMemoSupprFWS;
        private System.Windows.Forms.Button nudSupprClearFWS;
        private System.Windows.Forms.Button nudSupprAddFWS;
        private System.Windows.Forms.CheckBox chbAutoSupprFHSS;
        private System.Windows.Forms.RichTextBox rtbMemoSupprFHSS;
        private System.Windows.Forms.Button nudSupprClearFHSS;
        private System.Windows.Forms.Button bSupprAddFHSS;
        private System.Windows.Forms.TabControl tbcLog;
        private System.Windows.Forms.TabPage tbpRRD1;
        private System.Windows.Forms.TabPage tbpRRD2;
        private System.Windows.Forms.TabPage tbpRRD3;
        private System.Windows.Forms.TabPage tbpRRD4;
        private System.Windows.Forms.TabPage tbpRRD5;
        private System.Windows.Forms.RichTextBox rtbLogRRC;
        private System.Windows.Forms.RichTextBox rtbLog3G;
        private System.Windows.Forms.RichTextBox rtbLogModem;
        private System.Windows.Forms.RichTextBox rtbLogHF;
        private System.Windows.Forms.RichTextBox rtbLogVHF;
        private System.Windows.Forms.Panel pStateConnection;
        private System.Windows.Forms.Panel pChooseRRD;
        private System.Windows.Forms.CheckBox chbVHF;
        private System.Windows.Forms.CheckBox chbHF;
        private System.Windows.Forms.CheckBox chbModem;
        private System.Windows.Forms.CheckBox chb3G;
        private System.Windows.Forms.CheckBox chbRRC;
        private System.Windows.Forms.Panel pCntUpd;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Label lVHF;
        private System.Windows.Forms.ComboBox cmbComVHF;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button bConnectVHF;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.PictureBox pbWriteByteVHF;
        private System.Windows.Forms.PictureBox pbReadByteVHF;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label lHF;
        private System.Windows.Forms.ComboBox cmbComHF;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button bConnectHF;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.PictureBox pbWriteByteHF;
        private System.Windows.Forms.PictureBox pbReadByteHF;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Label lModem;
        private System.Windows.Forms.ComboBox cmbComModem;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button bConnectModem;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.PictureBox pbWriteByteModem;
        private System.Windows.Forms.PictureBox pbReadByteModem;
        private System.Windows.Forms.NumericUpDown nudAddressReceiver;
        private System.Windows.Forms.Label lAddressReceiver;
        private System.Windows.Forms.Panel pCreateServerRRC;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label lServerRRC;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button bConnectRRC;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.PictureBox pbWriteByteRRC;
        private System.Windows.Forms.PictureBox pbReadByteRRC;
        private System.Windows.Forms.Panel pCreateServer3G;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label lServer3G;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Button bConnect3G;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.PictureBox pbWriteByte3G;
        private System.Windows.Forms.PictureBox pbReadByte3G;
        private System.Windows.Forms.Panel pConnectClient3G;
        private System.Windows.Forms.Panel pConnectClientRRC;
        private System.Windows.Forms.Panel pClientRRC5;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button bConnectClientRRC5;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.PictureBox pbWriteByteClientRRC5;
        private System.Windows.Forms.PictureBox pbReadByteClientRRC5;
        private System.Windows.Forms.Panel pClientRRC4;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button bConnectClientRRC4;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.PictureBox pbWriteByteClientRRC4;
        private System.Windows.Forms.PictureBox pbReadByteClientRRC4;
        private System.Windows.Forms.Panel pClientRRC3;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button bConnectClientRRC3;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.PictureBox pbWriteByteClientRRC3;
        private System.Windows.Forms.PictureBox pbReadByteClientRRC3;
        private System.Windows.Forms.Panel pClientRRC2;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button bConnectClientRRC2;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.PictureBox pbWriteByteClientRRC2;
        private System.Windows.Forms.PictureBox pbReadByteClientRRC2;
        private System.Windows.Forms.Panel pClientRRC1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lClient1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button bConnectClientRRC1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pbWriteByteClientRRC1;
        private System.Windows.Forms.PictureBox pbReadByteClientRRC1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button bConnectClientRRC6;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.PictureBox pbWriteByteClientRRC6;
        private System.Windows.Forms.PictureBox pbReadByteClientRRC6;
        private System.Windows.Forms.NumericUpDown nudFreqID;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.RichTextBox rtbClientConnect;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MaskedTextBox mtbServerPort;
        private System.Windows.Forms.MaskedTextBox mtbServerIp;
        private System.Windows.Forms.Button btnConnectServer;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.MaskedTextBox mtbDbPort;
        private System.Windows.Forms.MaskedTextBox mtbDbIp;
        private System.Windows.Forms.Button btnConnectDB;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.CheckBox cbxIsMaster;
        private System.Windows.Forms.ComboBox cmbxServerIP;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Button button4;
    }
}

