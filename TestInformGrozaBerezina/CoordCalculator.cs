﻿using Bearing;
using KvetkaModelsDBLib;

namespace TestODServer
{
    public static class CoordCalculator
    {
        public static Coord CalculateCoord(float bearingOwn, float bearingLinked, Coord stationOwn, Coord stationLinked)
        {
            double tempLat = 0;
            double tempLong = 0;

            if (bearingOwn >=0 && bearingLinked >=0)
            {
                try
                {
                    ClassBearing.f_Triangulation(1000, 1000, bearingOwn, bearingLinked, stationOwn.Latitude,
                                                 stationOwn.Longitude,stationLinked.Latitude,stationLinked.Longitude,
                                                 1, 300000, ref tempLat, ref tempLong);
                }
                catch
                {}
            }
            return new Coord() { Latitude = tempLat, Longitude = tempLong };
        }
    }
}
