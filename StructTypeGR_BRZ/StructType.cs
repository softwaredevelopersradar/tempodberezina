﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace StructTypeGR_BRZ
{


   



    public struct TServicePart
    {
        public byte bAdrSend;
        public byte bAdrReceive;
        public byte bCode;
        public byte bCounter;
        public int iLength; 
    }
    
    public struct TRangeSector
    {
        public int iFregMin;
        public int iFregMax;
        public short sAngleMin;
        public short sAngleMax;
    }

    public struct TRangeSpec
    {
        public int iFregMin;
        public int iFregMax;
    }


    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct TSupprFWS
    {
        public int iID;
        public float iFreq;
        public byte bModulation;
        public byte bDeviation;
        public byte bManipulation;
        public byte bDuration;
        public byte bPrioritet;
        public byte bThreshold;
        public short sBearing;
    }

    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
    public struct TSupprFHSS
    {
        public int iID;
        public float iFreqMin;
        public float iFreqMax;
        public byte bCodeFFT;
        public byte bModulation;
        public byte bDeviation;
        public byte bManipulation;        
    }

    

    public struct TCoord
    {
        public byte bSignLongitude;
        public byte bDegreeLongitude;
        public byte bMinuteLongitude;
        public byte bSecondLongitude;
        public byte bSignLatitude;
        public byte bDegreeLatitude;
        public byte bMinuteLatitude;
        public byte bSecondLatitude;     
    }


    public struct TTimeMy
    {
        public byte bHour;
        public byte bMinute;
        public byte bSecond;
 
    }

    public struct TReconFWSAnalog
    {
        public int iID;
        public double dFreq;
        public float fBand;
        public TTimeMy tTime;
        public float sBearingOwn;
        public TCoord tCoord;
        public short bLevelOwn;
        public byte bTypeSignal;
        public int bBandDemodulation;
    }

    public struct TReconFWSDigital
    {
        public int iID;
        public double dFreq;
        public TTimeMy tTimeBegin;
        public TTimeMy tTimeEnd;
        public short bTypeSignal;
        public float sBearingOwn;
        public TCoord tCoord;
        public byte bSignAudio;
    }

    public struct TReconFWS
    {
        public int iID;
        public float iFreq;
        public TTimeMy tTime;
        public short sBearingOwn;
        public short sBearingLinked;
        public TCoord tCoord;
        public byte bLevelOwn;
        public byte bLevelLinked;
        public byte bBandWidth;
        public byte bModulation;
        public byte bSignAudio;     
    }

    public struct TReconFHSS
    {
        public int iID;
        public float iFreqMin;
        public float iFreqMax;
        public short sStep;
        public int iDuration;
        public byte bMinute;
        public byte bSecond;
        public short sBearingOwn;
        public short sBearingLinked;
        public TCoord tCoord;
        public byte bLevelOwn;
        public byte bLevelLinked;
        public byte bBandWidth;
        public byte bModulation;
        public byte bSignAudio;     
    }

    public struct TResSupprFWS
    {
        public int iID;
        public float iFreq;
        public byte bResCtrl;
        public byte bResSuppr;
    }

    public struct TResSupprFHSS
    {
        public int iID;
        public float iFreqMin;
        public float iFreqMax;
        public byte bResCtrl;
        public byte bResSuppr;
    }

 

}
