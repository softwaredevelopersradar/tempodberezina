﻿using Func;
using StructTypeGR_BRZ;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace GrozaBerezinaDLL
{
   // Класс-обработчик клиента
    class Client: DeCodeCmd
        
    {

        public delegate void ConnectClientEventHandler(object sender, string strIPAddress);

        public delegate void ByteEventHandler(object sender, byte[] bByte);
        public delegate void CmdEventHandler(object sender, object obj);

        public event ConnectClientEventHandler OnDisconnect;

        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;


        private Thread thrRead;
        private NetworkStream streamClient;
        private TcpClient tcpClient;

        // Отправка страницы с ошибкой
        private void SendError(TcpClient Client, int Code)
        {
            // Получаем строку вида "200 OK"
            // HttpStatusCode хранит в себе все статус-коды HTTP/1.1
            string CodeStr = Code.ToString() + " " + ((HttpStatusCode)Code).ToString();
            // Код простой HTML-странички
            string Html = "<html><body><h1>" + CodeStr + "</h1></body></html>";
            // Необходимые заголовки: ответ сервера, тип и длина содержимого. После двух пустых строк - само содержимое
            string Str = "HTTP/1.1 " + CodeStr + "\nContent-type: text/html\nContent-Length:" + Html.Length.ToString() + "\n\n" + Html;
            // Приведем строку к виду массива байт
            byte[] Buffer = Encoding.ASCII.GetBytes(Str);
            // Отправим его клиенту

            Client.GetStream().Write(Buffer, 0, Buffer.Length);
            // Закроем соединение
            Client.Close();
        }


        // Write data (byte) 
        public bool WriteData(byte[] bData)
        {
            try
            {
                // if there is client
                if (tcpClient != null)
                {
                    tcpClient.GetStream().Write(bData, 0, bData.Length);
                    
                    // write data 
                    //streamClient.Write(bData, 0, bData.Length);

                    // generate event
                    if (OnWriteByte != null)
                        OnWriteByte(this, bData);
                }
            }
            catch (System.Exception ex)
            {
          
                return false;
            }

            return true;
        }


        public Client(TcpClient Client)
        {
            tcpClient = Client;

            streamClient = tcpClient.GetStream();

            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }

            try
            {

                thrRead = new Thread(new ThreadStart(ReadData));
                thrRead.IsBackground = true;
                thrRead.Start();

                //OnConnectClient();
            }
            catch (System.Exception ex)
            {
                //OnDestroyServer();
                

            }
        }

        // Read data (byte) (thread)
        private void ReadData()
        {
            byte[] bBufRead = new byte[LEN_ARRAY];
            byte[] bBufSave = new byte[LEN_ARRAY];

            int iReadByte = -1;
            int iTempLength = 0;

            bool blExistCmd = true;

            while (true)
            {
                try
                {
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = streamClient.Read(bBufRead, 0, bBufRead.Length);

                    if (iReadByte > 0)
                    {
                        Array.Resize(ref bBufRead, iReadByte);

                        if (OnReadByte != null)
                            OnReadByte(this, bBufRead);


                        Array.Copy(bBufRead, 0, bBufSave, iTempLength, iReadByte);

                        iTempLength += iReadByte;

                        blExistCmd = true;

                        while (iTempLength >= LEN_HEAD && blExistCmd)
                        {

                            byte[] bBufHead = new byte[LEN_HEAD];
                            Array.Copy(bBufSave, 0, bBufHead, 0, LEN_HEAD);

                            TServicePart tServicePart = new TServicePart();
                            object obj = tServicePart;
                            StrArr.ByteArrayToStructure(bBufHead, ref obj);
                            tServicePart = (TServicePart)obj;

                            int iLengthCmd = tServicePart.iLength;
                            if (iTempLength - LEN_HEAD >= iLengthCmd)
                            {
                                byte[] bBufDecode = new byte[iLengthCmd];
                                Array.Copy(bBufSave, LEN_HEAD, bBufDecode, 0, iLengthCmd);
                                DecodeCommand(tServicePart, bBufDecode);

                                Array.Reverse(bBufSave);
                                Array.Resize(ref bBufSave, bBufSave.Length - (LEN_HEAD + iLengthCmd));
                                Array.Reverse(bBufSave);

                                iTempLength -= (LEN_HEAD + iLengthCmd);

                                blExistCmd = true;

                            }

                            else

                                blExistCmd = false;

                        } // while (iTempLength > LEN_HEAD)
                    }
                    else
                    {
                        iTempLength = 0;
                        if (OnDisconnect != null)
                            OnDisconnect(this, Convert.ToString(((System.Net.IPEndPoint)tcpClient.Client.RemoteEndPoint).Address));

                        if (thrRead != null)
                        {

                            thrRead.Abort();
                            thrRead.Join(500);
                            thrRead = null;
                        }

                        if (tcpClient != null)
                        {
                            tcpClient.Close();
                            tcpClient = null;
                        }
                    }
                }

                catch (System.Exception)
                {
                    
                }


            }
        }


        public void DisconnectClient()
        {
           
            //streamClient = tcpClient.GetStream();

            if (OnDisconnect != null)
                OnDisconnect(this, Convert.ToString(((System.Net.IPEndPoint)tcpClient.Client.RemoteEndPoint).Address));

            if (thrRead != null)
            {
                thrRead.Abort();
                thrRead.Join(500);
                thrRead = null;
            }

            if (streamClient != null)
            {
                streamClient.Close();
                streamClient = null;
            }
           
            if (tcpClient != null)
            {
                tcpClient.Close();
                tcpClient = null;
            }

            

            
        }

        // Конструктор класса. Ему нужно передавать принятого клиента от TcpListener
      /*  public Client(TcpClient Client)
        {
            // Объявим строку, в которой будет хранится запрос клиента
            string Request = "";
            // Буфер для хранения принятых от клиента данных
            byte[] Buffer = new byte[1024];

            NetworkStream streamClient;
            // Переменная для хранения количества байт, принятых от клиента
            int Count;

            try
            {
                // Читаем из потока клиента до тех пор, пока от него поступают данные
                while ((Count = Client.GetStream().Read(Buffer, 0, Buffer.Length)) > 0)
                {
                    // Преобразуем эти данные в строку и добавим ее к переменной Request
                    Request += Encoding.ASCII.GetString(Buffer, 0, Count);
                    // Запрос должен обрываться последовательностью \r\n\r\n
                    // Либо обрываем прием данных сами, если длина строки Request превышает 4 килобайта
                    // Нам не нужно получать данные из POST-запроса (и т. п.), а обычный запрос
                    // по идее не должен быть больше 4 килобайт
                    if (Request.IndexOf("\r\n\r\n") >= 0 || Request.Length > 4096)
                    {
                        break;
                    }

                }

            }
            catch (SystemException)
            {
                int f = 45;
            }


            // Парсим строку запроса с использованием регулярных выражений
            // При этом отсекаем все переменные GET-запроса
            /*  Match ReqMatch = Regex.Match(Request, @"^\w+\s+([^\s\?]+)[^\s]*\s+HTTP/.*|");
     
              // Если запрос не удался
              if (ReqMatch == Match.Empty)
              {
                  // Передаем клиенту ошибку 400 - неверный запрос
                  SendError(Client, 400);
                  return;
              }
     
              // Получаем строку запроса
              string RequestUri = ReqMatch.Groups[1].Value;
     
              // Приводим ее к изначальному виду, преобразуя экранированные символы
              // Например, "%20" -> " "
              RequestUri = Uri.UnescapeDataString(RequestUri);
     
              // Если в строке содержится двоеточие, передадим ошибку 400
              // Это нужно для защиты от URL типа http://example.com/../../file.txt
              if (RequestUri.IndexOf("..") >= 0)
              {
                  SendError(Client, 400);
                  return;
              }
     
              // Если строка запроса оканчивается на "/", то добавим к ней index.html
              if (RequestUri.EndsWith("/"))
              {
                  RequestUri += "index.html";
              }
     
              string FilePath = "www/" + RequestUri;
     
              // Если в папке www не существует данного файла, посылаем ошибку 404
              if (!File.Exists(FilePath))
              {
                  SendError(Client, 404);
                  return;
              }
     
             // Получаем расширение файла из строки запроса
             string Extension = RequestUri.Substring(RequestUri.LastIndexOf('.'));
    
             // Тип содержимого
             string ContentType = "";
    
             // Пытаемся определить тип содержимого по расширению файла
             switch (Extension)
             {
                 case ".htm":
                 case ".html":
                     ContentType = "text/html";
                     break;
                 case ".css":
                    ContentType = "text/stylesheet";
                     break;
                 case ".js":
                    ContentType = "text/javascript";
                     break;
                 case ".jpg":
                     ContentType = "image/jpeg";
                     break;
                case ".jpeg":
                case ".png":
                case ".gif":
                     ContentType = "image/" + Extension.Substring(1);
                     break;
                     if (Extension.Length > 1)
                     {
                         ContentType = "application/" + Extension.Substring(1);
                    }
                    else
                   {
                         ContentType = "application/unknown";
                     }
                     break;
               
    
            // Открываем файл, страхуясь на случай ошибки
             FileStream FS;
            try
             {
                 FS = new FileStream(FilePath, FileMode.Open, FileAccess.Read, FileShare.Read);
             }
             catch (Exception)
            {
                 // Если случилась ошибка, посылаем клиенту ошибку 500
               SendError(Client, 500);
                 return;
            }
    
             // Посылаем заголовки
             string Headers = "HTTP/1.1 200 OK\nContent-Type: " + ContentType + "\nContent-Length: " + FS.Length + "\n\n";
             byte[] HeadersBuffer = Encoding.ASCII.GetBytes(Headers);
             Client.GetStream().Write(HeadersBuffer, 0, HeadersBuffer.Length);
    
             // Пока не достигнут конец файла
             while (FS.Position < FS.Length)
             {
                 // Читаем данные из файла
                Count = FS.Read(Buffer, 0, Buffer.Length);
                // И передаем их клиенту
                 Client.GetStream().Write(Buffer, 0, Count);
            }
    
             // Закроем файл и соединение
             FS.Close();
             Client.Close();
         }
        }*/


    }


    


    public class TcpServerPC : DeCodeCmd
       {
        
        
        struct TcpClientPC
        {
            public string strAddressIP;
            public byte bAddressPC;
            public Client tempClient;
 
        }


           TcpListener tcpListener; // Объект, принимающий TCP-клиентов
           private Thread thrClient;
          // Client[] tempClient = null;

           TcpClientPC[] objTcpClientPC = null;

           public delegate void ConnectServerEventHandler(object sender);
           public delegate void ConnectClientEventHandler(object sender, string strIPAddress, byte bAddressPC);
          
           public delegate void ByteEventHandler(object sender, string strIP, byte[] bByte);
           public delegate void CmdEventHandler(object sender, object obj);


           public event ConnectServerEventHandler OnCreate;
           public event ConnectServerEventHandler OnDestroy;
           public event ConnectClientEventHandler OnConnectUndef;
           public event ConnectClientEventHandler OnConnect;
           public event ConnectClientEventHandler OnDisconnect;
           
           public event ByteEventHandler OnReadByte;
           public event ByteEventHandler OnWriteByte;



           #region Delegates

           public delegate void CmdTextEventHandler(object sender, byte bAddressSend, string strText);
           public delegate void ConfirmAddressEventHandler(object sender, byte bAddressSend);
           public delegate void ConfirmTextEventHandler(object sender, byte bAddressSend, byte bCodeError);
           public delegate void ConfirmSynchTimeEventHandler(object sender, byte bAddressSend, byte bCodeError, TTimeMy tTime);
           public delegate void ConfirmRegimeEventHandler(object sender, byte bAddressSend, byte bCodeError);
           public delegate void ConfirmRangeSectorEventHandler(object sender, byte bAddressSend, byte bCodeError, byte bTypeRange);
           public delegate void ConfirmRangeSpecEventHandler(object sender, byte bAddressSend, byte bCodeError, byte bTypeSpec);
           public delegate void ConfirmSupprFWSEventHandler(object sender, byte bAddressSend, byte bCodeError);
           public delegate void ConfirmSupprFHSSEventHandler(object sender, byte bAddressSend, byte bCodeError);

           public delegate void ConfirmStateEventHandler(object sender, byte bAddressSend, byte bCodeError, short sNum, byte bType, byte bRole, byte bRegime, byte[] bLetter);
           public delegate void ConfirmCoordEventHandler(object sender, byte bAddressSend, byte bCodeError, TCoord tCoord);
           public delegate void ConfirmReconFWSEventHandler(object sender, byte bAddressSend, byte bCodeError, TReconFWS[] tReconFWS);
           public delegate void ConfirmReconFHSSEventHandler(object sender, byte bAddressSend, byte bCodeError, TReconFHSS[] tReconFHSS);
           public delegate void ConfirmExecBearEventHandler(object sender, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing);
           public delegate void ConfirmSimulBearEventHandler(object sender, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing, short sLinkedBearing);
           public delegate void ConfirmStateSupprFWSEventHandler(object sender, byte bAddressSend, byte bCodeError, TResSupprFWS[] tResSupprFWS);
           public delegate void ConfirmStateSupprFHSSEventHandler(object sender, byte bAddressSend, byte bCodeError, TResSupprFHSS[] tResSupprFHSS);



           #endregion

           #region Events


           public event CmdTextEventHandler OnTextCmd;
           public event ConfirmAddressEventHandler OnConfirmAddress;
           public event ConfirmTextEventHandler OnConfirmText;
           public event ConfirmSynchTimeEventHandler OnConfirmSynchTime;
           public event ConfirmRegimeEventHandler OnConfirmRegime;
           public event ConfirmRangeSectorEventHandler OnConfirmRangeSector;
           public event ConfirmRangeSpecEventHandler OnConfirmRangeSpec;
           public event ConfirmSupprFWSEventHandler OnConfirmSupprFWS;
           public event ConfirmSupprFHSSEventHandler OnConfirmSupprFHSS;

           public event ConfirmStateEventHandler OnConfirmState;
           public event ConfirmCoordEventHandler OnConfirmCoord;
           public event ConfirmReconFWSEventHandler OnConfirmReconFWS;
           public event ConfirmReconFHSSEventHandler OnConfirmReconFHSS;
           public event ConfirmExecBearEventHandler OnConfirmExecBear;
           public event ConfirmSimulBearEventHandler OnConfirmSimulBear;
           public event ConfirmStateSupprFWSEventHandler OnConfirmStateSupprFWS;
           public event ConfirmStateSupprFHSSEventHandler OnConfirmStateSupprFHSS;

           #endregion



           // Запуск сервера
           public TcpServerPC()
           {
               
              
           }


           public bool CreateServer(System.Net.IPAddress ip, int Port)
           {
               try
               {

                   //string strLocalHost = Dns.GetHostName();
                   //System.Net.IPAddress ip = System.Net.Dns.GetHostByName(strLocalHost).AddressList[0];

                   string strIP = ip.ToString();

                   //strIP = "0.0.0.0";
                   // Set the listener on the local IP address
                   // and specify the port.
                   //tcpListener = new TcpListener(IPAddress.Parse(strIP), PortOperChannel);

                   tcpListener = new TcpListener(IPAddress.Parse(strIP), Port); // Создаем "слушателя" для указанного порта
                   tcpListener.Start(); // Запускаем его


                   thrClient = new Thread(new ThreadStart(ClientThreadFunc));
                   thrClient.Start();

                   if (OnCreate != null)
                       OnCreate(this);
               }
               catch (SystemException)
               {
                   if (OnDestroy != null)
                       OnDestroy(this);

                   return false;
               }

               return true;
           }


           // thread for accept client (listen to)
           private void ClientThreadFunc()
           {
               try
               {
                    while (true)
                    {
                        // Принимаем нового клиента
                        TcpClient Client = tcpListener.AcceptTcpClient();
                        // Создаем поток
                        Thread Thread = new Thread(new ParameterizedThreadStart(ClientThread));
                        // И запускаем этот поток, передавая ему принятого клиента
                        Thread.Start(Client);

                        
                    }
               }
               catch (System.Exception)
               {

                   //DestroyServer();
               }
           }


           private void ClientThread(Object StateInfo)
           {
              // Просто создаем новый экземпляр класса Client и передаем ему приведенный к классу TcpClient объект StateInfo

               try
               {
                   TcpClient tcpTempClient = (TcpClient)StateInfo;

                   string strIPad = tcpTempClient.Client.RemoteEndPoint.ToString();

                   if (objTcpClientPC == null)
                       objTcpClientPC = new TcpClientPC[1];
                   else
                   {
                       Array.Resize(ref objTcpClientPC, objTcpClientPC.Length + 1);

                   }
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient = new Client(tcpTempClient);
                   objTcpClientPC[objTcpClientPC.Length - 1].strAddressIP = Convert.ToString(((System.Net.IPEndPoint)tcpTempClient.Client.RemoteEndPoint).Address);
                   objTcpClientPC[objTcpClientPC.Length - 1].bAddressPC = 0;

                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnDisconnect += new Client.ConnectClientEventHandler(DisconnectClientEvent);


                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnReadByte += new Client.ByteEventHandler(ReadByte);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnWriteByte += new Client.ByteEventHandler(WriteByte);

                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmAddress += new Client.ConfirmAddressEventHandler(ConfirmAddress);

                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnTextCmd += new Client.CmdTextEventHandler(TextCmd);
                   

                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmSynchTime += new Client.ConfirmSynchTimeEventHandler(ConfirmSynchTime);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmText += new Client.ConfirmTextEventHandler(ConfirmText);
                   
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmRegime += new Client.ConfirmRegimeEventHandler(ConfirmRegime);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmRangeSector += new Client.ConfirmRangeSectorEventHandler(ConfirmRangeSector);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmRangeSpec += new Client.ConfirmRangeSpecEventHandler(ConfirmRangeSpec);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmSupprFWS += new Client.ConfirmSupprFWSEventHandler(ConfirmSupprFWS);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmSupprFHSS += new Client.ConfirmSupprFHSSEventHandler(ConfirmSupprFHSS);

                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmState += new Client.ConfirmStateEventHandler(ConfirmState);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmCoord += new Client.ConfirmCoordEventHandler(ConfirmCoord);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmReconFWS += new Client.ConfirmReconFWSEventHandler(ConfirmReconFWS);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmReconFHSS += new Client.ConfirmReconFHSSEventHandler(ConfirmReconFHSS);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmExecBear += new Client.ConfirmExecBearEventHandler(ConfirmExecBear);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmSimulBear += new Client.ConfirmSimulBearEventHandler(ConfirmSimulBear);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmStateSupprFWS += new Client.ConfirmStateSupprFWSEventHandler(ConfirmStateSupprFWS);
                   objTcpClientPC[objTcpClientPC.Length - 1].tempClient.OnConfirmStateSupprFHSS += new Client.ConfirmStateSupprFHSSEventHandler(ConfirmStateSupprFHSS);



                   if (OnConnectUndef != null)
                       OnConnectUndef(this, objTcpClientPC[objTcpClientPC.Length - 1].strAddressIP, objTcpClientPC[objTcpClientPC.Length - 1].bAddressPC);

                  

               }
               catch (System.Exception)
               {}


               //Client tempClient = new Client(tcpTempClient);

              
          }


           private void DisconnectClientEvent(object sender, string strIPAddress )
           {
               Client tempClientByte = (Client)sender;

               
                int i = 0;
                try
                {
                   while (i < objTcpClientPC.Length)
                   {
                       if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                       {
                           if (OnDisconnect != null)
                               OnDisconnect(this, objTcpClientPC[i].strAddressIP, objTcpClientPC[i].bAddressPC);
                           i = objTcpClientPC.Length;

                           Array.Resize(ref objTcpClientPC, objTcpClientPC.Length - 1);

                       }
                       i++;
                   }
               }

               catch(Exception)
               {
                   if (OnDisconnect != null)
                       OnDisconnect(this, objTcpClientPC[i].strAddressIP, objTcpClientPC[i].bAddressPC);
               }
 
 
           }



           # region ReceiveEvent

           private void ReadByte(object sender, byte[] bByte)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       if (OnReadByte != null)
                           OnReadByte(this, objTcpClientPC[i].strAddressIP, bByte);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
 
           }

           private void WriteByte(object sender, byte[] bByte)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       if (OnWriteByte != null)
                           OnWriteByte(this, objTcpClientPC[i].strAddressIP, bByte);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmAddress(object sender, byte bAddressSend)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;
                       
                       if (OnConnect != null)
                           OnConnect(this, objTcpClientPC[i].strAddressIP, bAddressSend);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void TextCmd(object sender, byte bAddressSend, string strText)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnTextCmd != null)
                           OnTextCmd(this, objTcpClientPC[i].bAddressPC, strText);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmSynchTime(object sender, byte bAddressSend, byte bCodeError, TTimeMy tTime)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmSynchTime != null)
                           OnConfirmSynchTime(this, objTcpClientPC[i].bAddressPC, bCodeError, tTime);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmText(object sender, byte bAddressSend, byte bCodeError)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmText != null)
                           OnConfirmText(this, objTcpClientPC[i].bAddressPC, bCodeError);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmRegime(object sender, byte bAddressSend, byte bCodeError)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmRegime != null)
                           OnConfirmRegime(this, objTcpClientPC[i].bAddressPC, bCodeError);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmRangeSector(object sender, byte bAddressSend, byte bCodeError, byte bTypeRange)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmRangeSector != null)
                           OnConfirmRangeSector(this, objTcpClientPC[i].bAddressPC, bCodeError, bTypeRange);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmRangeSpec(object sender, byte bAddressSend, byte bCodeError, byte bTypeSpec)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmRangeSpec != null)
                           OnConfirmRangeSpec(this, objTcpClientPC[i].bAddressPC, bCodeError, bTypeSpec);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmSupprFWS(object sender, byte bAddressSend, byte bCodeError)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmSupprFWS != null)
                           OnConfirmSupprFWS(this, objTcpClientPC[i].bAddressPC, bCodeError);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmSupprFHSS(object sender, byte bAddressSend, byte bCodeError)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmSupprFHSS != null)
                           OnConfirmSupprFHSS(this, objTcpClientPC[i].bAddressPC, bCodeError);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmState(object sender, byte bAddressSend, byte bCodeError, short sNum, byte bType, byte bRole, byte bRegime, byte[] bLetter)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmState != null)
                           OnConfirmState(this, objTcpClientPC[i].bAddressPC, bCodeError, sNum, bType, bRole, bRegime, bLetter);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmCoord(object sender, byte bAddressSend, byte bCodeError, TCoord tCoord)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmCoord != null)
                           OnConfirmCoord(this, objTcpClientPC[i].bAddressPC, bCodeError, tCoord);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmReconFWS(object sender, byte bAddressSend, byte bCodeError, TReconFWS[] tReconFWS)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmReconFWS != null)
                           OnConfirmReconFWS(this, objTcpClientPC[i].bAddressPC, bCodeError, tReconFWS);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmReconFHSS(object sender, byte bAddressSend, byte bCodeError, TReconFHSS[] tReconFHSS)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmReconFHSS != null)
                           OnConfirmReconFHSS(this, objTcpClientPC[i].bAddressPC, bCodeError, tReconFHSS);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmExecBear(object sender, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmExecBear != null)
                           OnConfirmExecBear(this, objTcpClientPC[i].bAddressPC, bCodeError, iID, iFreq, sOwnBearing);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmSimulBear(object sender, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing, short sLinkedBearing)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmSimulBear != null)
                           OnConfirmSimulBear(this, objTcpClientPC[i].bAddressPC, bCodeError, iID, iFreq, sOwnBearing, sLinkedBearing);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmStateSupprFWS(object sender, byte bAddressSend, byte bCodeError, TResSupprFWS[] tResSupprFWS)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmStateSupprFWS != null)
                           OnConfirmStateSupprFWS(this, objTcpClientPC[i].bAddressPC, bCodeError, tResSupprFWS);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }

           private void ConfirmStateSupprFHSS(object sender, byte bAddressSend, byte bCodeError, TResSupprFHSS[] tResSupprFHSS)
           {
               Client tempClientByte = (Client)sender;

               int i = 0;
               while (i < objTcpClientPC.Length)
               {
                   if (objTcpClientPC[i].tempClient.Equals(tempClientByte))
                   {
                       objTcpClientPC[i].bAddressPC = bAddressSend;

                       if (OnConfirmStateSupprFHSS != null)
                           OnConfirmStateSupprFHSS(this, objTcpClientPC[i].bAddressPC, bCodeError, tResSupprFHSS);
                       i = objTcpClientPC.Length;
                   }
                   i++;
               }
           }
        
        

            # endregion

           // destroy server
           public bool DestroyServer()
           {
              
               try
               {
                   if (tcpListener != null)
                   {
                       tcpListener.Stop();
                   }

                   

                   if (objTcpClientPC != null)
                   {
                       for (int i = 0; i < objTcpClientPC.Length; i++)
                       {
                           objTcpClientPC[i].tempClient.DisconnectClient();
                          
                       }
                   }

                   if (objTcpClientPC != null)
                       objTcpClientPC = null;


                   if (thrClient != null)
                   {
                       thrClient.Abort();
                       thrClient.Join(500);
                       thrClient = null;
                   }



                   if (OnDestroy != null)
                       OnDestroy(this);
               }
               catch (SystemException)
               {
                   return false;
               }

               return true;
           }


           private int  DefineClient(byte bAddressReceive)
           {
               int iResClient = -1;
               if(objTcpClientPC != null)
               {
                   int i = 0;
                   while (i < objTcpClientPC.Length)
                   {
                       if (objTcpClientPC[i].bAddressPC == bAddressReceive)
                       {
                           iResClient = i;
                           i = objTcpClientPC.Length;
                       }
                       i++;
                   }
               }

               return iResClient;
 
           }


           private int DefineClientIP(string strIPAddress)
           {
               int iResClient = -1;
               if (objTcpClientPC != null)
               {
                   int i = 0;
                   while (i < objTcpClientPC.Length)
                   {
                       if (objTcpClientPC[i].strAddressIP == strIPAddress)
                       {
                           iResClient = i;
                           i = objTcpClientPC.Length;
                       }
                       i++;
                   }
               }

               return iResClient;

           }


           #region SendFunction

           public bool SendAddress(string strIPAddress)
           {
               byte[] bSend = FormSendAddress();


               int iResClient = DefineClientIP(strIPAddress);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend);
               }
               catch (SystemException)
               {
                   return false;
               }

               return true;

           }
        
           public bool SendText(byte bAddressReceive, string strText)
           {
               byte[] bSend = FormSendText(bAddressReceive, strText);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
              
           }

           public bool SendConfirmText(byte bAddressReceive, byte bCodeError)
           {
               byte[] bSend = FormSendConfirmText(bAddressReceive, bCodeError);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend);
               }
               catch (SystemException)
               {
                   return false;
               }

               return true;

           }

           public bool SendSynchTime(byte bAddressReceive, TTimeMy tTime)
           {
               byte[] bSend = FormSendSynchTime(bAddressReceive, tTime);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend);
               }
               catch (SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendRegime(byte bAddressReceive, byte bRegime)
           {
               byte[] bSend = FormSendRegime(bAddressReceive, bRegime);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;

           }

          public bool SendRangeSector(byte bAddressReceive, byte bSign, TRangeSector[] tRangeSector)
           {
               byte[] bSend = FormSendRangeSector(bAddressReceive, bSign, tRangeSector);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendRangeSpec(byte bAddressReceive, byte bSign, TRangeSpec[] tRangeSpec)
           {
               byte[] bSend = FormSendRangeSpec(bAddressReceive, bSign, tRangeSpec);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendSupprFWS(byte bAddressReceive, int iDuration, TSupprFWS[] tSupprFWS)
           {
               byte[] bSend = FormSendSupprFWS(bAddressReceive, iDuration, tSupprFWS);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendSupprFHSS(byte bAddressReceive, int iDuration, TSupprFHSS[] tSupprFHSS)
           {
               byte[] bSend = FormSendSupprFHSS(bAddressReceive, iDuration, tSupprFHSS);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendState(byte bAddressReceive)
           {
               byte[] bSend = FormSendState(bAddressReceive);

               int iResClient = DefineClient(bAddressReceive);

               

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendCoord(byte bAddressReceive)
           {
               byte[] bSend = FormSendCoord(bAddressReceive);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendReconFWS(byte bAddressReceive)
           {
               byte[] bSend = FormSendReconFWS(bAddressReceive);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendReconFHSS(byte bAddressReceive)
           {
               byte[] bSend = FormSendReconFHSS(bAddressReceive);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendExecBear(byte bAddressReceive, int iID, int iFreq)
           {
               byte[] bSend = FormSendExecBear(bAddressReceive,iID, iFreq);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendSimulBear(byte bAddressReceive, int iID, int iFreq)
           {
               byte[] bSend = FormSendSimulBear(bAddressReceive,iID, iFreq);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendStateSupprFWS(byte bAddressReceive)
           {
               byte[] bSend = FormSendStateSupprFWS(bAddressReceive);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendStateSupprFHSS(byte bAddressReceive)
           {
               byte[] bSend = FormSendStateSupprFHSS(bAddressReceive);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           public bool SendAudio(byte bAddressReceive, int iFreq)
           {
               byte[] bSend = FormSendAudio(bAddressReceive, iFreq);

               int iResClient = DefineClient(bAddressReceive);

               try
               {
                   objTcpClientPC[iResClient].tempClient.WriteData(bSend); 
               }
               catch(SystemException)
               {
                   return false;
               }

               return true;
           }

           #endregion

       }


}
