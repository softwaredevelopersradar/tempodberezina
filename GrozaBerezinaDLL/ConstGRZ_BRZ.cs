﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaBerezinaDLL
{
    public class ConstGRZ_BRZ
    {
        #region Code and Const

        public const byte ADDRESS = 0x12;

        public const byte TEXT = 0x01;

        public const byte CONFIRM_TEXT = 0x11;

        public const byte SYNCH_TIME = 0x02;
        public const byte REGIME = 0x03;
        public const byte RANGE_SECTOR = 0x04;
        public const byte RANGE_SPEC = 0x05;
        public const byte SUPPR_FWS = 0x06;
        public const byte SUPPR_FHSS = 0x07;

        public const byte STATE = 0x08;
        public const byte COORD = 0x09;
        public const byte RECON_FWS = 0x0A;
        public const byte RECON_FWS_ANALOG = 0x1A;
        public const byte RECON_FWS_DIGITAL = 0x2A;
        public const byte RECON_FHSS = 0x0B;
        public const byte EXEC_BEAR = 0x0C;
        public const byte SIMUL_BEAR = 0x0D;
        public const byte STATE_SUPPR_FWS = 0x0E;
        public const byte STATE_SUPPR_FHSS = 0x0F;

        

        public const byte AUDIO = 0x10;

        public const byte LEN_HEAD = 8;
        public const short LEN_ARRAY = 1000;
        

        #endregion

    }
}
