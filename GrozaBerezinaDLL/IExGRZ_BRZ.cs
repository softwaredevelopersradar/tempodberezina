﻿using Func;
using StructTypeGR_BRZ;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GrozaBerezinaDLL
{
    public class IExGRZ_BRZ : DeCodeCmd
    {
        private Thread thrRead;

        private SerialPort _port;

        public NetworkStream streamClient;
        private TcpClient tcpClient;

        private byte bAddressReceive = 0;

        byte bAddressSend;
       
        byte bCounter;


        public IExGRZ_BRZ(byte bAddress)
        {
            bAddressSend = bAddress;
        }

        #region Delegates

        public delegate void ConnectEventHandler(object sender);
        public delegate void ByteEventHandler(object sender, byte[] bByte);
        public delegate void CmdEventHandler(object sender, object obj);

        public delegate void CmdTextEventHandler(object sender, string strText);
        public delegate void ConfirmTextEventHandler(object sender,  byte bCodeError);

        public delegate void RequestAddressEventHandler(object sender);

        public delegate void RequestSynchTimeEventHandler(object sender, TTimeMy tTime);
        public delegate void RequestRegimeEventHandler(object sender,  byte bRegime);
        public delegate void RequestRangeSectorEventHandler(object sender, byte bType, TRangeSector[] tRangeSector);
        public delegate void RequestRangeSpecEventHandler(object sender, byte bType, TRangeSpec[] tRangeSpec);
        public delegate void RequestSupprFWSEventHandler(object sender, int iDuration, TSupprFWS[] tSupprFWS);
        public delegate void RequestSupprFHSSEventHandler(object sender, int iDuration, TSupprFHSS[] tSupprFHSS);

        public delegate void RequestStateEventHandler(object sender);
        public delegate void RequestCoordEventHandler(object sender);
        public delegate void RequestReconFWSEventHandler(object sender);
        public delegate void RequestReconFHSSEventHandler(object sender);
        public delegate void RequestExecBearEventHandler(object sender, int iID, int iFreq);
        public delegate void RequestSimulBearEventHandler(object sender, int iID, int iFreq);
        public delegate void RequestStateSupprFWSEventHandler(object sender);
        public delegate void RequestStateSupprFHSSEventHandler(object sender);



        public delegate void SendSynchTimeEventHandler(object sender, byte bCodeError, TTimeMy tTime);
        public delegate void SendRegimeEventHandler(object sender, byte bCodeError);
        public delegate void SendRangeSectorEventHandler(object sender, byte bCodeError, byte bTypeRange);
        public delegate void SendRangeSpecEventHandler(object sender, byte bCodeError, byte bTypeSpec);
        public delegate void SendSupprFWSEventHandler(object sender, byte bCodeError);
        public delegate void SendSupprFHSSEventHandler(object sender, byte bCodeError);

        public delegate void SendStateEventHandler(object sender, byte bCodeError, byte bRegime, short sNum, byte bType, byte bRole, byte[] bLetter);
        public delegate void SendCoordEventHandler(object sender, byte bCodeError, TCoord tCoord);
        public delegate void SendReconFWSEventHandler(object sender, byte bCodeError, TReconFWS[] tReconFWS);
        public delegate void SendReconFHSSEventHandler(object sender, byte bCodeError, TReconFHSS[] tReconFHSS);
        public delegate void SendExecBearEventHandler(object sender, byte bCodeError, int iID, int iFreq, short sOwnBearing);
        public delegate void SendSimulBearEventHandler(object sender, byte bCodeError, int iID, int iFreq, short sOwnBearing, short sLinkedBearing);
        public delegate void SendStateSupprFWSEventHandler(object sender, byte bCodeError, TResSupprFWS[] tResSupprFWS);
        public delegate void SendStateSupprFHSSEventHandler(object sender, byte bCodeError, TResSupprFHSS[] tResSupprFHSS);



        #endregion

        #region Events
        public event ConnectEventHandler OnConnect;
        public event ConnectEventHandler OnDisconnect;

        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;

        public event CmdTextEventHandler OnReceiveTextCmd;
        public event CmdTextEventHandler OnSendTextCmd;
        public event ConfirmTextEventHandler OnReceiveConfirmTextCmd;
        public event ConfirmTextEventHandler OnSendConfirmTextCmd;

        public event RequestAddressEventHandler OnRequestAddress;

        public event RequestSynchTimeEventHandler OnRequestSynchTime;
        public event RequestRegimeEventHandler OnRequestRegime;
        public event RequestRangeSectorEventHandler OnRequestRangeSector;
        public event RequestRangeSpecEventHandler OnRequestRangeSpec;
        public event RequestSupprFWSEventHandler OnRequestSupprFWS;
        public event RequestSupprFHSSEventHandler OnRequestSupprFHSS;

        public event RequestStateEventHandler OnRequestState;
        public event RequestCoordEventHandler OnRequestCoord;
        public event RequestReconFWSEventHandler OnRequestReconFWS;
        public event RequestReconFHSSEventHandler OnRequestReconFHSS;
        public event RequestExecBearEventHandler OnRequestExecBear;
        public event RequestSimulBearEventHandler OnRequestSimulBear;
        public event RequestStateSupprFWSEventHandler OnRequestStateSupprFWS;
        public event RequestStateSupprFHSSEventHandler OnRequestStateSupprFHSS;


        public event SendSynchTimeEventHandler OnSendSynchTime;
        public event SendRegimeEventHandler OnSendRegime;
        public event SendRangeSectorEventHandler OnSendRangeSector;
        public event SendRangeSpecEventHandler OnSendRangeSpec;
        public event SendSupprFWSEventHandler OnSendSupprFWS;
        public event SendSupprFHSSEventHandler OnSendSupprFHSS;

        public event SendStateEventHandler OnSendState;
        public event SendCoordEventHandler OnSendCoord;
        public event SendReconFWSEventHandler OnSendReconFWS;
        public event SendReconFHSSEventHandler OnSendReconFHSS;
        public event SendExecBearEventHandler OnSendExecBear;
        public event SendSimulBearEventHandler OnSendSimulBear;
        public event SendStateSupprFWSEventHandler OnSendStateSupprFWS;
        public event SendStateSupprFHSSEventHandler OnSendStateSupprFHSS;

        #endregion



        // connect to server
        public bool Connect(string strIPServer, int iPortServer)
        {
            if (_port != null)
                _port = null;

            // if there is client
            if (tcpClient != null)
                tcpClient = null;

            // create client
            tcpClient = new TcpClient();

            try
            {
                // begin connect 
                var result = tcpClient.BeginConnect(strIPServer, iPortServer, null, null);

                // try connect
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1));


                if (!success)
                {
                    throw new Exception("Failed to connect.");
                }

                // we have connected
                tcpClient.EndConnect(result);


            }
            catch (System.Exception )
            {                
                //generate event
                if (OnDisconnect != null)
                    OnDisconnect(this);
                return false;
            }


            if (tcpClient.Connected)
            {
                // create stream for client
                if (streamClient != null)
                    streamClient = null;
                streamClient = tcpClient.GetStream();

                // destroy thread for reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // create thread for reading
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }

                catch (System.Exception )
                {                    
                    // generate event
                    if (OnDisconnect != null)
                        OnDisconnect(this);
                    return false;

                }


                //tcpClient.OnCreate += new TcpServerPC.ConnectServerEventHandler(CreateServer);
                //tcpClient.OnDestroy += new TcpServerPC.ConnectServerEventHandler(DestroyServer);
            }

            if (OnConnect != null)
                OnConnect(this);
            return true;
        }



        public void Connect(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            // if there is client
            if (tcpClient != null)
                tcpClient = null;

            // Open COM port
            _port = null;

            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)

                // close it
                DisconnectCom();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;

                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;

                _port.RtsEnable = true;
                _port.DtrEnable = true;

                _port.ReceivedBytesThreshold = 1000;

                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                {

                }

                if (OnConnect != null)
                    OnConnect(this);

            }
            catch (System.Exception)
            {
                if (OnDisconnect != null)
                    OnDisconnect(this);

                //return false;
            }
        }


        public void Disconnect()
        {
            DisconnectCom();

            DisconnectEth();
        }

        private void DisconnectCom()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception)
            {


            }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception)
            {

            }

            try
            {
                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // raise event
                if (OnDisconnect != null)
                    OnDisconnect(this);
            }

            catch (System.Exception)
            {

            }

        }

        // disconnet from server
        private void DisconnectEth()
        {
            // if there is client
            if (streamClient != null)
            {
                // close client stream
                try
                {
                    streamClient.Close();
                    streamClient = null;
                }
                catch (System.Exception)
                {
                    
                }

            }

            // if there is client
            if (tcpClient != null)
            {
                // close client connection
                try
                {
                    tcpClient.Close();
                    tcpClient = null;
                }
                catch (System.Exception )
                {
                    
                }

            }

            // if there is client
            if (thrRead != null)
            {
                // destroy thread for reading
                try
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                catch (System.Exception )
                {
                    
                }

            }

            // generate event
            if (OnDisconnect != null)
                OnDisconnect(this);
        }

        private bool WriteData(byte[] bSend)
        {
            try
            {
                if (_port != null)
                    _port.Write(bSend, 0, bSend.Length);
                if (tcpClient != null)
                    streamClient.Write(bSend, 0, bSend.Length);

                if (OnWriteByte != null)
                    OnWriteByte(this, bSend);

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }



        #region SendFunction

        public void SendAddress()
        {
            byte[] bSend = FormAddress();


            WriteData(bSend);
        }

        public void SendText(string strText)
        {
            byte[] bSend = FormText(strText);

            if (OnSendTextCmd != null)
                OnSendTextCmd(this, strText);

            WriteData(bSend);
        }

        public void SendConfirmText(byte bCodeError)
        {
            byte[] bSend = FormConfirmText(bCodeError);

            if (OnSendConfirmTextCmd != null)
                OnSendConfirmTextCmd(this, bCodeError);

            WriteData(bSend);
        }

        public void SendSynchTime(byte bCodeError, TTimeMy tTime)
        {
            byte[] bSend = FormConfirmSynchTime(bCodeError, tTime);

            if (OnSendSynchTime != null)
                OnSendSynchTime(this, bCodeError, tTime);

            WriteData(bSend);
        }

        public void SendRegime(byte bCodeError)
        {
            byte[] bSend = FormConfirmRegime(bCodeError);

            if (OnSendRegime != null)
                OnSendRegime(this, bCodeError);

            WriteData(bSend);
        }

        public void SendRangeSector(byte bCodeError, byte bTypeRange)
        {
            byte[] bSend = FormConfirmRangeSector(bCodeError, bTypeRange);

            if (OnSendRangeSector != null)
                OnSendRangeSector(this, bCodeError, bTypeRange);

            WriteData(bSend);
        }

        public void SendRangeSpec(byte bCodeError, byte bTypeSpec)
        {
            byte[] bSend = FormConfirmRangeSpec(bCodeError, bTypeSpec);

            if (OnSendRangeSpec != null)
                OnSendRangeSpec(this, bCodeError, bTypeSpec);

            WriteData(bSend);
        }

        public void SendSupprFWS(byte bCodeError)
        {
            byte[] bSend = FormConfirmSupprFWS(bCodeError);

            if (OnSendSupprFWS != null)
                OnSendSupprFWS(this, bCodeError);

            WriteData(bSend);
        }

        public void SendSupprFHSS(byte bCodeError)
        {
            byte[] bSend = FormConfirmSupprFHSS(bCodeError);

            if (OnSendReconFHSS != null)
                OnSendSupprFHSS(this, bCodeError);

            WriteData(bSend);
        }

        public void SendState(byte bCodeError, byte bRegime, short sNum, byte bType, byte bRole, byte[] bLetter)
        {
            byte[] bSend = FormConfirmState(bCodeError, bRegime, sNum, bType, bRole, bLetter);

            if (OnSendState != null)
                OnSendState(this, bCodeError, bRegime, sNum, bType, bRole, bLetter);

            WriteData(bSend);
        }

        public void SendCoord(byte bCodeError, TCoord tCoord)
        {
            byte[] bSend = FormConfirmCoord(bCodeError, tCoord);

            if (OnSendCoord != null)
                OnSendCoord(this, bCodeError, tCoord);

            WriteData(bSend);
        }

        public void SendReconFWS(byte bCodeError, TReconFWS[] tReconFWS)
        {
            byte[] bSend = FormConfirmReconFWS(bCodeError, tReconFWS);

            if (OnSendReconFWS != null)
                OnSendReconFWS(this, bCodeError, tReconFWS);

            WriteData(bSend);
        }

        public void SendReconFHSS(byte bCodeError, TReconFHSS[] tReconFHSS)
        {
            byte[] bSend = FormConfirmReconFHSS(bCodeError, tReconFHSS);

            if (OnSendReconFHSS != null)
                OnSendReconFHSS(this, bCodeError, tReconFHSS);

            WriteData(bSend);
        }

        public void SendExecBear(byte bCodeError, int iID, int iFreq, short sOwnBearing)
        {
            byte[] bSend = FormConfirmExecBear(bCodeError, iID, iFreq, sOwnBearing);

            if (OnSendExecBear != null)
                OnSendExecBear(this, bCodeError, iID, iFreq, sOwnBearing);

            WriteData(bSend);
        }

        public void SendSimulBear(byte bCodeError, int iID, int iFreq, short sOwnBearing, short sLinkedBearing)
        {
            byte[] bSend = FormConfirmSimulBear(bCodeError,iID, iFreq, sOwnBearing, sLinkedBearing);

            if (OnSendSimulBear != null)
                OnSendSimulBear(this, bCodeError, iID, iFreq, sOwnBearing, sLinkedBearing);

            WriteData(bSend);
        }

        public void SendStateSupprFWS(byte bCodeError, TResSupprFWS[] tResSupprFWS)
        {
            byte[] bSend = FormConfirmStateSupprFWS(bCodeError, tResSupprFWS);

            if (OnSendStateSupprFWS != null)
                OnSendStateSupprFWS(this, bCodeError, tResSupprFWS);

            WriteData(bSend);
        }

        public void SendStateSupprFHSS(byte bCodeError,  TResSupprFHSS[] tResSupprFHSS)
        {
            byte[] bSend = FormConfirmStateSupprFHSS(bCodeError, tResSupprFHSS);

            if (OnSendStateSupprFHSS != null)
                OnSendStateSupprFHSS(this, bCodeError, tResSupprFHSS);

            WriteData(bSend);
        }

        public void SendAudio(byte bCodeError)
        {
            byte[] bSend = FormSendAudio(bCodeError);

            WriteData(bSend);
        }

        #endregion


        private byte[] FormAddress()
        {            
            int iLengthData = 0;
            byte[] bData = null;

            

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, ADDRESS, iLengthData);

           
            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

          
            return bSend;
        }
        

        private byte[] FormText(string strText)
        {

            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = Encoding.Default.GetBytes(strText);

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, TEXT, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

            return bSend;


            
        }

        private byte[] FormConfirmSynchTime(byte bCodeError, TTimeMy tTime)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[4];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SYNCH_TIME, iLengthData);

            bData[0] = bCodeError;

            Array.Copy(StrArr.StructToByteArray(tTime), 0, bData,1, 3);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

            return bSend;
        }

        private byte[] FormConfirmRegime(byte bCodeError)
        {            
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[1];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, REGIME, iLengthData);

            bData[0] = bCodeError;

            
            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

          
            return bSend;
        }

        private byte[] FormConfirmText(byte bCodeError)
        {            
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[1];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, CONFIRM_TEXT, iLengthData);

            bData[0] = bCodeError;

            
            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

          
            return bSend;
        }

        private byte[] FormConfirmRangeSector(byte bCodeError, byte bTypeRange)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[2];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RANGE_SECTOR, iLengthData);

            bData[0] = bCodeError;
            bData[1] = bTypeRange;


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }


            return bSend;
        }

        private byte[] FormConfirmRangeSpec(byte bCodeError, byte bTypeSpec)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[2];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RANGE_SPEC, iLengthData);

            bData[0] = bCodeError;
            bData[1] = bTypeSpec;

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }


            return bSend;
        }

        private byte[] FormConfirmSupprFWS(byte bCodeError)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[1];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SUPPR_FWS, iLengthData);

            bData[0] = bCodeError;


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }


            return bSend;
        }

        private byte[] FormConfirmSupprFHSS(byte bCodeError)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[1];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SUPPR_FHSS, iLengthData);

            bData[0] = bCodeError;


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }


            return bSend;
        }

        private byte[] FormConfirmState(byte bCodeError, byte bRegime, short sNum, byte bType, byte bRole, byte[] bLetter)
        {
            int iLengthData = 0;
            byte[] bData = null;

            if (bLetter != null)

                // create data array
                bData = new byte[7 + bLetter.Length];
            else
                bData = new byte[7];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE, iLengthData);

            bData[0] = bCodeError;
            bData[1] = bRegime;
            bData[2] = 0;

            Array.Copy(BitConverter.GetBytes(sNum), 0, bData, 3, 2);

            bData[5] = bType;
            bData[6] = bRole;

            if (bLetter != null)
            {
                for (int i = 0; i < bLetter.Length; i++)
                    bData[7 + i] = bLetter[i];
                        
            }
            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

            return bSend;
        }

        private byte[] FormConfirmCoord(byte bCodeError, TCoord tCoord)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[9];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, COORD, iLengthData);

            bData[0] = bCodeError;

            Array.Copy(StrArr.StructToByteArray(tCoord), 0, bData, 1, 8);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

            return bSend;
        }

        private byte[] FormConfirmReconFWS(byte bCodeError, TReconFWS[] tReconFWS)
        {            
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tReconFWS != null)
            {
                int iLengthPack = 28;

                bData = new byte[1 + tReconFWS.Length * iLengthPack];
                bData[0] = bCodeError;


                for (int i = 0; i < tReconFWS.Length; i++)
                {
                    Array.Copy(BitConverter.GetBytes(tReconFWS[i].iID), 0, bData, 1 + i * iLengthPack, 4);
                    Array.Copy(BitConverter.GetBytes(tReconFWS[i].iFreq), 0, bData, 5 + i * iLengthPack, 4);
                    bData[9 + i * iLengthPack] = tReconFWS[i].tTime.bHour;
                    bData[10 + i * iLengthPack] = tReconFWS[i].tTime.bMinute;
                    bData[11 + i * iLengthPack] = tReconFWS[i].tTime.bSecond;

                    Array.Copy(BitConverter.GetBytes(tReconFWS[i].sBearingOwn), 0, bData, 12 + i * iLengthPack, 2);
                    Array.Copy(BitConverter.GetBytes(tReconFWS[i].sBearingLinked), 0, bData, 14 + i * iLengthPack, 2);

                    Array.Copy(StrArr.StructToByteArray(tReconFWS[i].tCoord), 0, bData, 16 + i * iLengthPack, 8);

                    bData[24 + i * iLengthPack] = tReconFWS[i].bLevelOwn;
                    bData[25 + i * iLengthPack] = tReconFWS[i].bLevelLinked;
                    bData[26 + i * iLengthPack] = tReconFWS[i].bBandWidth;
                    bData[27 + i * iLengthPack] = tReconFWS[i].bModulation;
                    bData[28 + i * iLengthPack] = tReconFWS[i].bSignAudio;
                    
                }


            }
            else
            {
                bData = new byte[1];
                bData[0] = bCodeError;
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RECON_FWS, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;

        }

        private byte[] FormConfirmReconFHSS(byte bCodeError, TReconFHSS[] tReconFHSS)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tReconFHSS != null)
            {
                int iLengthPack = 37;

                bData = new byte[1 + tReconFHSS.Length * iLengthPack];
                bData[0] = bCodeError;


                for (int i = 0; i < tReconFHSS.Length; i++)
                {
                    Array.Copy(BitConverter.GetBytes(tReconFHSS[i].iID), 0, bData, 1 + i * iLengthPack, 4);
                    Array.Copy(BitConverter.GetBytes(tReconFHSS[i].iFreqMin), 0, bData, 5 + i * iLengthPack, 4);
                    Array.Copy(BitConverter.GetBytes(tReconFHSS[i].iFreqMax), 0, bData, 9 + i * iLengthPack, 4);

                    Array.Copy(BitConverter.GetBytes(tReconFHSS[i].sStep), 0, bData, 13 + i * iLengthPack, 2);
                    Array.Copy(BitConverter.GetBytes(tReconFHSS[i].iDuration), 0, bData, 15 + i * iLengthPack, 4);
                    
                    bData[19 + i * iLengthPack] = tReconFHSS[i].bMinute;
                    bData[20 + i * iLengthPack] = tReconFHSS[i].bSecond;

                    Array.Copy(BitConverter.GetBytes(tReconFHSS[i].sBearingOwn), 0, bData, 21 + i * iLengthPack, 2);
                    Array.Copy(BitConverter.GetBytes(tReconFHSS[i].sBearingLinked), 0, bData, 23 + i * iLengthPack, 2);

                    Array.Copy(StrArr.StructToByteArray(tReconFHSS[i].tCoord), 0, bData, 25 + i * iLengthPack, 8);

                    bData[33 + i * iLengthPack] = tReconFHSS[i].bLevelOwn;
                    bData[34 + i * iLengthPack] = tReconFHSS[i].bLevelLinked;
                    bData[35 + i * iLengthPack] = tReconFHSS[i].bBandWidth;
                    bData[36 + i * iLengthPack] = tReconFHSS[i].bModulation;
                    bData[37 + i * iLengthPack] = tReconFHSS[i].bSignAudio;

                }


            }
            else
            {
                bData = new byte[1];
                bData[0] = bCodeError;
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RECON_FHSS, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        private byte[] FormConfirmExecBear(byte bCodeError, int iID, int iFreq, short sOwnBearing)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[11];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, EXEC_BEAR, iLengthData);

            bData[0] = bCodeError;

            Array.Copy(BitConverter.GetBytes(iID), 0, bData, 1, 4);
            Array.Copy(BitConverter.GetBytes(iFreq), 0, bData, 5, 4);
            Array.Copy(BitConverter.GetBytes(sOwnBearing), 0, bData, 9, 2);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

            return bSend;
        }

        private byte[] FormConfirmSimulBear(byte bCodeError, int iID, int iFreq, short sOwnBearing, short sLinkedBearing)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[13];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SIMUL_BEAR, iLengthData);

            bData[0] = bCodeError;

            Array.Copy(BitConverter.GetBytes(iID), 0, bData, 1, 4);
            Array.Copy(BitConverter.GetBytes(iFreq), 0, bData, 5, 4);
            Array.Copy(BitConverter.GetBytes(sOwnBearing), 0, bData, 9, 2);
            Array.Copy(BitConverter.GetBytes(sLinkedBearing), 0, bData, 11, 2);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

            return bSend;
        }

        private byte[] FormConfirmStateSupprFWS(byte bCodeError, TResSupprFWS[] tResSupprFWS)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tResSupprFWS != null)
            {
                int iLengthPack = 10;

                bData = new byte[1 + tResSupprFWS.Length * iLengthPack];
                bData[0] = bCodeError;


                for (int i = 0; i < tResSupprFWS.Length; i++)
                {
                    Array.Copy(BitConverter.GetBytes(tResSupprFWS[i].iID), 0, bData, 1 + i * iLengthPack, 4);
                    Array.Copy(BitConverter.GetBytes(tResSupprFWS[i].iFreq), 0, bData, 5 + i * iLengthPack, 4);
                    bData[9 + i * iLengthPack] = tResSupprFWS[i].bResCtrl;
                    bData[10 + i * iLengthPack] = tResSupprFWS[i].bResSuppr;                  
                }
            }
            else
            {
                bData = new byte[1];
                bData[0] = bCodeError;
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE_SUPPR_FWS, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        private byte[] FormConfirmStateSupprFHSS(byte bCodeError, TResSupprFHSS[] tResSupprFHSS)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tResSupprFHSS != null)
            {
                int iLengthPack = 14;

                bData = new byte[1 + tResSupprFHSS.Length * iLengthPack];
                bData[0] = bCodeError;


                for (int i = 0; i < tResSupprFHSS.Length; i++)
                {
                    Array.Copy(BitConverter.GetBytes(tResSupprFHSS[i].iID), 0, bData, 1 + i * iLengthPack, 4);
                    Array.Copy(BitConverter.GetBytes(tResSupprFHSS[i].iFreqMin), 0, bData, 5 + i * iLengthPack, 4);
                    Array.Copy(BitConverter.GetBytes(tResSupprFHSS[i].iFreqMax), 0, bData, 9 + i * iLengthPack, 4);
                    bData[13 + i * iLengthPack] = tResSupprFHSS[i].bResCtrl;
                    bData[14 + i * iLengthPack] = tResSupprFHSS[i].bResSuppr;
                }
            }
            else
            {
                bData = new byte[1];
                bData[0] = bCodeError;
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE_SUPPR_FHSS, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        private byte[] FormSendAudio(byte bCodeError)
        {
            byte[] bSend = null;
            return bSend;
        }



        #region FormConfirmFunction

        public byte[] SetServicePart(byte bAddressReceive, byte bCode, int iLength)
        {
            byte[] bHead = null;

            TServicePart tServicePart = new TServicePart();
            tServicePart.bAdrSend = bAddressSend;
            tServicePart.bAdrReceive = bAddressReceive;
            tServicePart.bCode = bCode;
            if (bCounter == 255)
                bCounter = 0;
            tServicePart.bCounter = bCounter++;
            tServicePart.iLength = iLength;

            bHead = StrArr.StructToByteArray(tServicePart);


            return bHead;
        }

        public void DecodeCommand(TServicePart tServicePart, byte[] bData)
        {
            if (tServicePart.bCode == ADDRESS)
            {
                try
                {
                    SendAddress();

                    if (OnRequestAddress != null)
                        OnRequestAddress(this);


                }

                catch (Exception)
                { }

               
            }


            if (tServicePart.bAdrReceive == bAddressSend)
            {
                switch (tServicePart.bCode)
                {


                    case TEXT:

                        try
                        {
                        
                            string strText = String.Empty;
                          
                            strText = Encoding.GetEncoding(1251).GetString(bData);

                            if (OnReceiveTextCmd != null)
                                OnReceiveTextCmd(this, strText);
                        }

                        catch (Exception)
                        {}
                        
                        break;

                    case CONFIRM_TEXT:

                        try
                        {

                            byte bCodeError = bData[0];

                            if (OnReceiveConfirmTextCmd != null)
                                OnReceiveConfirmTextCmd(this, bCodeError);
                        }

                        catch (Exception)
                        { }

                        break;

                    case SYNCH_TIME:

                        try
                        {                          
                            TTimeMy tTime = new TTimeMy();
                            object obj = tTime;
                            StrArr.ByteArrayToStructure(bData, ref obj);
                            tTime = (TTimeMy)obj;

                            if (OnRequestSynchTime != null)
                                OnRequestSynchTime(this, tTime);

                        }
                        catch (Exception)
                        { }
                        break;

                    case REGIME:
                        try
                        {
                            byte bRegime = bData[0];

                            if (OnRequestRegime != null)
                                OnRequestRegime(this, bRegime);
                        }
                        catch (Exception)
                        { }
                        break;

                    case RANGE_SECTOR:
                        try
                        {                            
                            if (bData != null)
                            {
                                TRangeSector[] tRangeSector = null;
                                byte bType = bData[0];
                            
                                int iCountPack = 0;
                                int iSizePack =  12;
                                iCountPack = (bData.Length - 1)/iSizePack;

                                tRangeSector = new TRangeSector[iCountPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    byte[] bDecode = new byte[iSizePack];
                                    Array.Copy(bData, 1+iSizePack*i,bDecode,0,iSizePack);

                                    TRangeSector tRangeSectorTemp = new TRangeSector();
                                    object obj = tRangeSectorTemp;
                                    StrArr.ByteArrayToStructure(bDecode, ref obj);
                                    tRangeSectorTemp = (TRangeSector)obj;

                                    tRangeSector[i] = tRangeSectorTemp;
                                } 
     
                                if (OnRequestRangeSector != null)
                                    OnRequestRangeSector(this, bType, tRangeSector);
                                       
                            }
                        }
                        catch (Exception)
                        { }
                        break;

                    case RANGE_SPEC:
                        try
                        {
                            

                            if (bData != null)
                            {
                                TRangeSpec[] tRangeSpec = null;
                                byte bType = bData[0];

                                int iCountPack = 0;
                                int iSizePack = 8;
                                iCountPack = (bData.Length - 1) / iSizePack;

                                tRangeSpec = new TRangeSpec[iCountPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    byte[] bDecode = new byte[iSizePack];
                                    Array.Copy(bData, 1 + iSizePack * i, bDecode, 0, iSizePack);

                                    TRangeSpec tRangeSpecTemp = new TRangeSpec();
                                    object obj = tRangeSpecTemp;
                                    StrArr.ByteArrayToStructure(bDecode, ref obj);
                                    tRangeSpecTemp = (TRangeSpec)obj;

                                    tRangeSpec[i] = tRangeSpecTemp;                                                                  
                                }

                                if (OnRequestRangeSpec != null)
                                    OnRequestRangeSpec(this, bType, tRangeSpec);
                                        
                            }
                        }
                        catch (Exception)
                        { }
                        break;

                    case SUPPR_FWS:
                        try
                        {                                                        
                            if (bData != null)
                            {
                                TSupprFWS[] tSupprFWS = null;
                                
                                int iDuration = BitConverter.ToInt32(bData, 0);

                                int iCountPack = 0;
                                int iSizePack = 16;
                                iCountPack = (bData.Length - 4) / iSizePack;

                                tSupprFWS = new TSupprFWS[iCountPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    byte[] bDecode = new byte[iSizePack];
                                    Array.Copy(bData, 4 + iSizePack * i, bDecode, 0, iSizePack);

                                    TSupprFWS tSupprFWSTemp = new TSupprFWS();

                                    /*tSupprFWSTemp.iID = BitConverter.ToInt32(bDecode, 0 + i * iSizePack);
                                    tSupprFWSTemp.iFreq = BitConverter.ToInt32(bDecode, 4 + i * iSizePack);
                                    tSupprFWSTemp.bModulation = bDecode[8 + i * iSizePack];
                                    tSupprFWSTemp.bDeviation = bDecode[9 + i * iSizePack];
                                    tSupprFWSTemp.bManipulation = bDecode[10 + i * iSizePack];
                                    tSupprFWSTemp.bDuration = bDecode[11 + i * iSizePack];
                                    tSupprFWSTemp.bPrioritet = bDecode[12 + i * iSizePack];
                                    tSupprFWSTemp.bThreshold = bDecode[13 + i * iSizePack];
                                    tSupprFWSTemp.sBearing = BitConverter.ToInt16(bDecode, 14 + i * iSizePack);*/

                                    
                                    object obj = tSupprFWSTemp;
                                    StrArr.ByteArrayToStructure(bDecode, ref obj);
                                    tSupprFWSTemp = (TSupprFWS)obj;

                                    tSupprFWS[i] = tSupprFWSTemp;
                                }

                                if (OnRequestSupprFWS != null)
                                    OnRequestSupprFWS(this, iDuration, tSupprFWS);
                            }
                        }
                        catch (Exception)
                        { }
                        break;

                    case SUPPR_FHSS:
                        try
                        {
                            if (bData != null)
                            {
                                TSupprFHSS[] tSupprFHSS = null;

                                int iDuration = BitConverter.ToInt32(bData, 0);

                                int iCountPack = 0;
                                int iSizePack = 16;
                                iCountPack = (bData.Length - 4) / iSizePack;

                                tSupprFHSS = new TSupprFHSS[iCountPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    byte[] bDecode = new byte[iSizePack];
                                    Array.Copy(bData, 4 + iSizePack * i, bDecode, 0, iSizePack);

                                    TSupprFHSS tSupprFHSSTemp = new TSupprFHSS();
                                    object obj = tSupprFHSSTemp;
                                    StrArr.ByteArrayToStructure(bDecode, ref obj);
                                    tSupprFHSSTemp = (TSupprFHSS)obj;

                                    tSupprFHSS[i] = tSupprFHSSTemp;
                                }

                                if (OnRequestSupprFHSS != null)
                                    OnRequestSupprFHSS(this, iDuration, tSupprFHSS);
                            }
                        }
                        catch (Exception)
                        { }
                        break;

                    case STATE:
                        try
                        {
                            
                            if (OnRequestState != null)
                                OnRequestState(this);

                        }
                        catch (Exception)
                        { }
                        break;

                    case COORD:
                        try
                        {                            
                            if (OnRequestCoord != null)
                                OnRequestCoord(this);

                        }
                        catch (Exception)
                        { }
                        break;

                    case RECON_FWS:
                        try
                        {                            
                            if (OnRequestReconFWS != null)
                                OnRequestReconFWS(this);

                        }
                        catch (Exception)
                        { }
                        break;

                    case RECON_FHSS:
                        try
                        {                            
                            if (OnRequestReconFHSS != null)
                                OnRequestReconFHSS(this);

                        }
                        catch (Exception)
                        { }
                        break;

                    case EXEC_BEAR:
                        try
                        {
                            int iID = BitConverter.ToInt32(bData, 0);
                            int iFreq = BitConverter.ToInt32(bData, 4);

                            if (OnRequestExecBear != null)
                                OnRequestExecBear(this, iID, iFreq);

                        }
                        catch (Exception)
                        { }

                        break;

                    case SIMUL_BEAR:
                        try
                        {
                            int iID = BitConverter.ToInt32(bData, 0);
                            int iFreq = BitConverter.ToInt32(bData, 4);

                            if (OnRequestSimulBear != null)
                                OnRequestSimulBear(this, iID, iFreq);

                        }
                        catch (Exception)
                        { }

                        break;

                    case STATE_SUPPR_FWS:
                        try
                        {
                            
                            if (OnRequestStateSupprFWS != null)
                                OnRequestStateSupprFWS(this);

                        }
                        catch (Exception)
                        { }
                        break;

                    case STATE_SUPPR_FHSS:
                        try
                        {
                          
                            if (OnRequestStateSupprFHSS != null)
                                OnRequestStateSupprFHSS(this);

                        }
                        catch (Exception)
                        { }
                        break;

                    default:
                        break;


                }
            }
        }

        // Read data (byte) (thread)
        public void ReadData()
        {
            byte[] bBufRead = new byte[LEN_ARRAY];
            byte[] bBufSave = new byte[LEN_ARRAY];

            int iReadByte = -1;
            int iTempLength = 0;

            bool blExistCmd = true;

            while (true)
            {
                try
                {
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    if (_port == null)// if there is client            
                        iReadByte = streamClient.Read(bBufRead, 0, bBufRead.Length);
                    if (tcpClient == null)
                        iReadByte = _port.Read(bBufRead, 0, bBufRead.Length);


                    if (iReadByte > 0)
                    {
                        Array.Resize(ref bBufRead, iReadByte);

                        if (OnReadByte != null)
                            OnReadByte(this, bBufRead);


                        Array.Copy(bBufRead, 0, bBufSave, iTempLength, iReadByte);

                        iTempLength += iReadByte;

                        blExistCmd = true;

                        while (iTempLength >= LEN_HEAD && blExistCmd)
                        {

                            byte[] bBufHead = new byte[LEN_HEAD];
                            Array.Copy(bBufSave, 0, bBufHead, 0, LEN_HEAD);

                            TServicePart tServicePart = new TServicePart();
                            object obj = tServicePart;
                            StrArr.ByteArrayToStructure(bBufHead, ref obj);
                            tServicePart = (TServicePart)obj;

                            int iLengthCmd = tServicePart.iLength;
                            if (iTempLength - LEN_HEAD >= iLengthCmd)
                            {
                                byte[] bBufDecode = new byte[iLengthCmd];
                                Array.Copy(bBufSave, LEN_HEAD, bBufDecode, 0, iLengthCmd);
                                DecodeCommand(tServicePart, bBufDecode);

                                Array.Reverse(bBufSave);
                                Array.Resize(ref bBufSave, bBufSave.Length - (LEN_HEAD + iLengthCmd));
                                Array.Reverse(bBufSave);

                                iTempLength -= (LEN_HEAD + iLengthCmd);

                                blExistCmd = true;

                            }

                            else

                                blExistCmd = false;

                        } // while (iTempLength > LEN_HEAD)
                    }
                    else 
                    {
                       Disconnect();
                    }
                }

                catch (System.Exception )
                {
                    Disconnect();
                }

                Thread.Sleep(10);
            }
        }

        #endregion
    }
}
