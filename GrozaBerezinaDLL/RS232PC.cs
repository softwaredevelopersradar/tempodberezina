﻿using Func;
using StructTypeGR_BRZ;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GrozaBerezinaDLL
{
    class RS232PC : DeCodeCmd
    {
        private Thread thrRead;

        private SerialPort _port;

        
        public delegate void ConnectEventHandler(object sender);
        public delegate void ByteEventHandler(object sender, byte[] bByte);
        public delegate void CmdEventHandler(object sender, object obj);

        public event ConnectEventHandler OnOpenPort;
        public event ConnectEventHandler OnClosePort;

        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;





        public RS232PC()
        {
            
        }

        public bool OpenPort(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            // Open COM port

            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)

                // close it
                ClosePort();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;

                
                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;

                _port.RtsEnable = true;
                _port.DtrEnable = true;

                _port.ReceivedBytesThreshold = 1000;

                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                {

                }

                if (OnOpenPort != null)
                    OnOpenPort(this);

                return true;

            }
            catch (System.Exception)
            {
                if (OnClosePort != null)
                    OnClosePort(this);

                return false;
            }
        }

        public bool ClosePort()
        {
          
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception)
            {


            }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception)
            {

            }

            try
            {
                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // raise event
                if (OnClosePort != null)
                    OnClosePort(this);

                return true;
            }

            catch (System.Exception)
            {
                return false;
            }

        }

        private bool WriteData(byte[] bSend)
        {
            try
            {
                _port.Write(bSend, 0, bSend.Length);

                if (OnWriteByte != null)
                    OnWriteByte(this,  bSend);

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }



        #region SendFunction

        public bool  SendText(byte bAddressReceive, string strText)
        {
            byte[] bSend = FormSendText(bAddressReceive, strText);

            return WriteData(bSend);
        }

        public bool SendConfirmText(byte bAddressReceive, byte bCodeError)
        {
            byte[] bSend = FormSendConfirmText(bAddressReceive, bCodeError);

            return WriteData(bSend);
        }

        public bool SendSynchTime(byte bAddressReceive, TTimeMy tTime)
        {
            byte[] bSend = FormSendSynchTime(bAddressReceive, tTime);

            return WriteData(bSend);
        }

        public bool SendRegime(byte bAddressReceive, byte bRegime)
        {
            byte[] bSend = FormSendRegime(bAddressReceive, bRegime);

            return WriteData(bSend);
        }

        public bool SendRangeSector(byte bAddressReceive, byte bSign, TRangeSector[] tRangeSector)
        {
            byte[] bSend = FormSendRangeSector(bAddressReceive, bSign, tRangeSector);

            return WriteData(bSend);
        }

        public bool SendRangeSpec(byte bAddressReceive, byte bSign, TRangeSpec[] tRangeSpec)
        {
            byte[] bSend = FormSendRangeSpec(bAddressReceive, bSign, tRangeSpec);

            return WriteData(bSend);
        }

        public bool SendSupprFWS(byte bAddressReceive, int iDuration, TSupprFWS[] tSupprFWS)
        {
            byte[] bSend = FormSendSupprFWS(bAddressReceive, iDuration, tSupprFWS);

            return WriteData(bSend);
        }

        public bool SendSupprFHSS(byte bAddressReceive, int iDuration, TSupprFHSS[] tSupprFHSS)
        {
            byte[] bSend = FormSendSupprFHSS(bAddressReceive, iDuration, tSupprFHSS);

            return WriteData(bSend);
        }

        public bool SendState(byte bAddressReceive)
        {
            byte[] bSend = FormSendState(bAddressReceive);

            return WriteData(bSend);
        }

        public bool SendCoord(byte bAddressReceive)
        {
            byte[] bSend = FormSendCoord(bAddressReceive);

            return WriteData(bSend);
        }

        public bool SendReconFWS(byte bAddressReceive)
        {
            byte[] bSend = FormSendReconFWS(bAddressReceive);

            return WriteData(bSend);
        }

        public bool SendReconFHSS(byte bAddressReceive)
        {
            byte[] bSend = FormSendReconFHSS(bAddressReceive);

            return WriteData(bSend);
        }

        public bool SendExecBear(byte bAddressReceive, int iID, int iFreq)
        {
            byte[] bSend = FormSendExecBear(bAddressReceive,iID, iFreq);

            return WriteData(bSend);
        }

        public bool SendSimulBear(byte bAddressReceive, int iID, int iFreq)
        {
            byte[] bSend = FormSendSimulBear(bAddressReceive,iID, iFreq);

            return WriteData(bSend);
        }

        public bool SendStateSupprFWS(byte bAddressReceive)
        {
            byte[] bSend = FormSendStateSupprFWS(bAddressReceive);

            return WriteData(bSend);
        }

        public bool SendStateSupprFHSS(byte bAddressReceive )
        {
            byte[] bSend = FormSendStateSupprFHSS(bAddressReceive);

            return WriteData(bSend);
        }

        public bool SendAudio(byte bAddressReceive, int iFreq)
        {
            byte[] bSend = FormSendAudio(bAddressReceive, iFreq);

            return WriteData(bSend);
        }

        #endregion



        // Read data (byte) (thread)
        private void ReadData()
        {
            byte[] bBufRead = new byte[LEN_ARRAY];
            byte[] bBufSave = new byte[LEN_ARRAY];

            int iReadByte = -1;
            int iTempLength = 0;

            bool blExistCmd = true;

            while (true)
            {
                try
                {
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = _port.Read(bBufRead, 0, bBufRead.Length);

                    if (iReadByte > 0)
                    {
                        Array.Resize(ref bBufRead, iReadByte);

                        if (OnReadByte != null)
                            OnReadByte(this, bBufRead);


                        Array.Copy(bBufRead, 0, bBufSave, iTempLength, iReadByte);

                        iTempLength += iReadByte;

                        blExistCmd = true;

                        while (iTempLength >= LEN_HEAD && blExistCmd)
                        {

                            byte[] bBufHead = new byte[LEN_HEAD];
                            Array.Copy(bBufSave, 0, bBufHead, 0, LEN_HEAD);

                            TServicePart tServicePart = new TServicePart();
                            object obj = tServicePart;
                            StrArr.ByteArrayToStructure(bBufHead, ref obj);
                            tServicePart = (TServicePart)obj;

                            int iLengthCmd = tServicePart.iLength;
                            if (iTempLength - LEN_HEAD >= iLengthCmd)
                            {
                                byte[] bBufDecode = new byte[iLengthCmd];
                                Array.Copy(bBufSave, LEN_HEAD, bBufDecode, 0, iLengthCmd);
                                DecodeCommand(tServicePart, bBufDecode);

                                Array.Reverse(bBufSave);
                                Array.Resize(ref bBufSave, bBufSave.Length - (LEN_HEAD + iLengthCmd));
                                Array.Reverse(bBufSave);

                                iTempLength -= (LEN_HEAD + iLengthCmd);

                                blExistCmd = true;

                            }

                            else

                                blExistCmd = false;

                        } // while (iTempLength > LEN_HEAD)
                    }
                    else
                    {
                        if (OnClosePort != null)
                            OnClosePort(this);
                    }
                }

                catch (System.Exception)
                {

                }

                Thread.Sleep(10);


            }
        }


    }
}
