﻿using Func;
using StructTypeGR_BRZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrozaBerezinaDLL
{
    public class DeCodeCmd :ConstGRZ_BRZ
    {

        private byte bAddressReceive = 0;

        byte bAddressSend;

        byte bCounter;

        #region Delegates

        public delegate void CmdTextEventHandler(object sender, byte bAddressSend, string strText);
        public delegate void ConfirmAddressEventHandler(object sender, byte bAddressSend);
        public delegate void ConfirmTextEventHandler(object sender, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmSynchTimeEventHandler(object sender, byte bAddressSend, byte bCodeError, TTimeMy tTime);
        public delegate void ConfirmRegimeEventHandler(object sender, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmRangeSectorEventHandler(object sender, byte bAddressSend, byte bCodeError, byte bTypeRange);
        public delegate void ConfirmRangeSpecEventHandler(object sender, byte bAddressSend, byte bCodeError, byte bTypeSpec);
        public delegate void ConfirmSupprFWSEventHandler(object sender, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmSupprFHSSEventHandler(object sender, byte bAddressSend, byte bCodeError);

        public delegate void ConfirmStateEventHandler(object sender, byte bAddressSend, byte bCodeError, short sNum, byte bType, byte bRole, byte bRegime, byte[] bLetter);
        public delegate void ConfirmCoordEventHandler(object sender, byte bAddressSend, byte bCodeError, TCoord tCoord);
        public delegate void ConfirmReconFWSEventHandler(object sender, byte bAddressSend, byte bCodeError, TReconFWS[] tReconFWS);
        public delegate void ConfirmReconFWSAnalogEventHandler(object sender, byte bAddressSend, byte bCodeError, TReconFWSAnalog[] tReconFWS);
        public delegate void ConfirmReconFWSDigitalEventHandler(object sender, byte bAddressSend, byte bCodeError, TReconFWSDigital[] tReconFWS);
        public delegate void ConfirmReconFHSSEventHandler(object sender, byte bAddressSend, byte bCodeError, TReconFHSS[] tReconFHSS);
        public delegate void ConfirmExecBearEventHandler(object sender, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing);
        public delegate void ConfirmSimulBearEventHandler(object sender, byte bAddressSend, byte bCodeError,int iID, int iFreq, short sOwnBearing, short sLinkedBearing);
        public delegate void ConfirmStateSupprFWSEventHandler(object sender, byte bAddressSend, byte bCodeError, TResSupprFWS[] tResSupprFWS);
        public delegate void ConfirmStateSupprFHSSEventHandler(object sender, byte bAddressSend, byte bCodeError, TResSupprFHSS[] tResSupprFHSS);



        #endregion

        #region Events
        

        public event CmdTextEventHandler OnTextCmd;
        public event ConfirmAddressEventHandler OnConfirmAddress;
        public event ConfirmTextEventHandler OnConfirmText;
        public event ConfirmSynchTimeEventHandler OnConfirmSynchTime;
        public event ConfirmRegimeEventHandler OnConfirmRegime;
        public event ConfirmRangeSectorEventHandler OnConfirmRangeSector;
        public event ConfirmRangeSpecEventHandler OnConfirmRangeSpec;
        public event ConfirmSupprFWSEventHandler OnConfirmSupprFWS;
        public event ConfirmSupprFHSSEventHandler OnConfirmSupprFHSS;

        public event ConfirmStateEventHandler OnConfirmState;
        public event ConfirmCoordEventHandler OnConfirmCoord;
        public event ConfirmReconFWSEventHandler OnConfirmReconFWS;
        public event ConfirmReconFWSAnalogEventHandler OnConfirmReconFWSAnalog;
        public event ConfirmReconFWSDigitalEventHandler OnConfirmReconFWSDigital;
        public event ConfirmReconFHSSEventHandler OnConfirmReconFHSS;
        public event ConfirmExecBearEventHandler OnConfirmExecBear;
        public event ConfirmSimulBearEventHandler OnConfirmSimulBear;
        public event ConfirmStateSupprFWSEventHandler OnConfirmStateSupprFWS;
        public event ConfirmStateSupprFHSSEventHandler OnConfirmStateSupprFHSS;

        #endregion



        #region FormSendFunction

        public byte[] FormSendAddress()
        {
            int iLengthData = 0;
            byte[] bData = null;

          
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(0, ADDRESS, iLengthData);
          

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] SetServicePart(byte bAddressReceive, byte bCode, int iLength)
        {
            byte[] bHead = null;

            TServicePart tServicePart = new TServicePart();
            tServicePart.bAdrSend = bAddressSend;
            tServicePart.bAdrReceive = bAddressReceive;
            tServicePart.bCode = bCode;
            if (bCounter == 255)
                bCounter = 0;
            tServicePart.bCounter = bCounter++;
            tServicePart.iLength = iLength;

            bHead = StrArr.StructToByteArray(tServicePart);


            return bHead;
        }

        public byte[] FormSendText(byte bAddressReceive, string strText)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = Encoding.GetEncoding(1251).GetBytes(strText);

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, TEXT, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

            return bSend;
        }

        public byte[] FormSendConfirmText(byte bAddressReceive, byte bCodeError)
        {
            int iLengthData = 0;
            byte[] bData = null;

            bData = new byte[1];

            bData[0] = bCodeError;
            
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, CONFIRM_TEXT, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

            return bSend;
        }

        public byte[] FormSendSynchTime(byte bAddressReceive, TTimeMy tTime)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[3];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SYNCH_TIME, iLengthData);

            bData = StrArr.StructToByteArray(tTime);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            { }

            return bSend;
        }

        public byte[] FormSendRegime(byte bAddressReceive, byte bRegime)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[1];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, REGIME, iLengthData);

            bData[0] = bRegime;

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendRangeSector(byte bAddressReceive, byte bSign, TRangeSector[] tRangeSector)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tRangeSector != null)
            {
                int iLengthPack = 12;

                bData = new byte[1 + tRangeSector.Length * iLengthPack];
                bData[0] = bSign;

                for (int i = 0; i < tRangeSector.Length; i++)
                    Array.Copy(StrArr.StructToByteArray(tRangeSector[i]), 0, bData, 1 + i * iLengthPack, iLengthPack);
            }
            else
            {
                bData = new byte[1];
                bData[0] = bSign;
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RANGE_SECTOR, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendRangeSpec(byte bAddressReceive, byte bSign, TRangeSpec[] tRangeSpec)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tRangeSpec != null)
            {
                int iLengthPack = 8;

                bData = new byte[1 + tRangeSpec.Length * iLengthPack];
                bData[0] = bSign;

                for (int i = 0; i < tRangeSpec.Length; i++)
                    Array.Copy(StrArr.StructToByteArray(tRangeSpec[i]), 0, bData, 1 + i * iLengthPack, iLengthPack);
            }
            else
            {
                bData = new byte[1];
                bData[0] = bSign;
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RANGE_SPEC, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendSupprFWS(byte bAddressReceive, int iDuration, TSupprFWS[] tSupprFWS)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tSupprFWS != null)
            {
                int iLengthPack = 16;

                bData = new byte[4 + tSupprFWS.Length * iLengthPack];
                Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 4);

                for (int i = 0; i < tSupprFWS.Length; i++)
                {
                    Array.Copy(BitConverter.GetBytes(tSupprFWS[i].iID), 0, bData, 4 + i * iLengthPack, 4);
                    Array.Copy(BitConverter.GetBytes(tSupprFWS[i].iFreq), 0, bData, 8 + i * iLengthPack, 4);
                    bData[12 + i * iLengthPack] = tSupprFWS[i].bModulation;
                    bData[13 + i * iLengthPack] = tSupprFWS[i].bDeviation;
                    bData[14 + i * iLengthPack] = tSupprFWS[i].bManipulation;
                    bData[15 + i * iLengthPack] = tSupprFWS[i].bDuration;
                    bData[16 + i * iLengthPack] = tSupprFWS[i].bPrioritet;
                    bData[17 + i * iLengthPack] = tSupprFWS[i].bThreshold;
                    Array.Copy(BitConverter.GetBytes(tSupprFWS[i].sBearing), 0, bData, 18 + i * iLengthPack, 2);
 
                }

                    
            }
            else
            {
                bData = new byte[4];
                Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 4);
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SUPPR_FWS, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendSupprFHSS(byte bAddressReceive, int iDuration, TSupprFHSS[] tSupprFHSS)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tSupprFHSS != null)
            {
                int iLengthPack = 16;

                bData = new byte[4 + tSupprFHSS.Length * iLengthPack];
                Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 4);

                for (int i = 0; i < tSupprFHSS.Length; i++)
                    Array.Copy(StrArr.StructToByteArray(tSupprFHSS[i]), 0, bData, 4 + i * iLengthPack, iLengthPack);
            }
            else
            {
                bData = new byte[4];
                Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 4);
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SUPPR_FHSS, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendState(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array           
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE, iLengthData);


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendCoord(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array           
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, COORD, iLengthData);


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendReconFWS(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array           
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RECON_FWS, iLengthData);


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendReconFHSS(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array           
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RECON_FHSS, iLengthData);


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendExecBear(byte bAddressReceive, int iID, int iFreq)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[8];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, EXEC_BEAR, iLengthData);

            Array.Copy(BitConverter.GetBytes(iID), 0, bData, 0, 4);
            Array.Copy(BitConverter.GetBytes(iFreq), 0, bData, 4, 4);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendSimulBear(byte bAddressReceive, int iID, int iFreq)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[8];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SIMUL_BEAR, iLengthData);

            Array.Copy(BitConverter.GetBytes(iID), 0, bData, 0, 4);
            Array.Copy(BitConverter.GetBytes(iFreq), 0, bData, 4, 4);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendStateSupprFWS(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;


           
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE_SUPPR_FWS, iLengthData);

           
            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            {
            }

            return bSend;
        }

        public byte[] FormSendStateSupprFHSS(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;

          
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE_SUPPR_FHSS, iLengthData);

           
            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            {
            }

            return bSend;
        }

        public byte[] FormSendAudio(byte bAddressReceive, int iFreq)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[4];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, AUDIO, iLengthData);

            Array.Copy(BitConverter.GetBytes(iFreq), 0, bData, 0, 4);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        #endregion


        #region ReceiveFunction


        public void DecodeCommand(TServicePart tServicePart, byte[] bData)
        {
            if (tServicePart.bAdrReceive == bAddressSend)
            {
                switch (tServicePart.bCode)
                {

                    case ADDRESS:
                        try
                        {

                           
                            if (OnConfirmAddress != null)
                                OnConfirmAddress(this, tServicePart.bAdrSend);
                        }

                        catch (Exception)
                        { }


                        break;

                    case TEXT:
                        try
                        {

                            string strText = String.Empty;

                            strText = Encoding.GetEncoding(1251).GetString(bData);

                            if (OnTextCmd != null)
                                OnTextCmd(this, tServicePart.bAdrSend, strText);
                        }

                        catch (Exception)
                        { }


                        break;

                    case CONFIRM_TEXT:
                        try
                        {
                            byte bCode = bData[0];

                            if (OnConfirmText != null)
                                OnConfirmText(this, tServicePart.bAdrSend, bCode);
                        }
                        catch (Exception)
                        { }
                        break;

                    case SYNCH_TIME:

                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            TTimeMy tTime = new TTimeMy();
                            object obj = tTime;
                            StrArr.ByteArrayToStructure(bData, ref obj);
                            tTime = (TTimeMy)obj;

                            if (OnConfirmSynchTime != null)
                                OnConfirmSynchTime(this, tServicePart.bAdrSend, bCode, tTime);

                        }
                        catch (Exception)
                        { }
                        break;

                    case REGIME:
                        try
                        {
                            byte bCode = bData[0];

                            if (OnConfirmRegime != null)
                                OnConfirmRegime(this, tServicePart.bAdrSend, bCode);
                        }
                        catch (Exception)
                        { }
                        break;

                    case RANGE_SECTOR:
                        try
                        {
                            byte bCode = bData[0];
                            byte bTypeRange = bData[1];

                            if (OnConfirmRangeSector != null)
                                OnConfirmRangeSector(this, tServicePart.bAdrSend, bCode, bTypeRange);
                        }
                        catch (Exception)
                        { }
                        break;

                    case RANGE_SPEC:
                        try
                        {
                            byte bCode = bData[0];
                            byte bTypeSpec = bData[1];

                            if (OnConfirmRangeSpec != null)
                                OnConfirmRangeSpec(this, tServicePart.bAdrSend, bCode, bTypeSpec);
                        }
                        catch (Exception)
                        { }
                        break;

                    case SUPPR_FWS:
                        try
                        {
                            byte bCode = bData[0];

                            if (OnConfirmSupprFWS != null)
                                OnConfirmSupprFWS(this, tServicePart.bAdrSend, bCode);
                        }
                        catch (Exception)
                        { }
                        break;

                    case SUPPR_FHSS:
                        try
                        {
                            byte bCode = bData[0];

                            if (OnConfirmSupprFHSS != null)
                                OnConfirmSupprFHSS(this, tServicePart.bAdrSend, bCode);
                        }
                        catch (Exception)
                        { }
                        break;

                    case STATE:
                        try
                        {
                            byte bCode = bData[0];
                            byte bRegime = bData[1];
                            short sNum = BitConverter.ToInt16(bData, 3);

                            byte bType = bData[5];
                            byte bRole = bData[6];

                            

                            byte[] bLetter = new byte[bData.Length - 7];
                            Array.Copy(bData, 7, bLetter, 0, bLetter.Length);

                            if (OnConfirmState != null)
                                OnConfirmState(this, tServicePart.bAdrSend, bCode, sNum, bType, bRole, bRegime, bLetter);

                        }
                        catch (Exception)
                        { }
                        break;

                    case COORD:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            TCoord tCoord = new TCoord();
                            object obj = tCoord;
                            StrArr.ByteArrayToStructure(bData, ref obj);
                            tCoord = (TCoord)obj;

                            if (OnConfirmCoord != null)
                                OnConfirmCoord(this, tServicePart.bAdrSend, bCode, tCoord);

                        }
                        catch (Exception)
                        { }
                        break;

                    case RECON_FWS:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 28;
                            int iCountPack = bData.Length / iLengthPack;

                            TReconFWS[] tReconFWS = null;

                            if (iCountPack > 0)
                            {
                                tReconFWS = new TReconFWS[iCountPack];
                                byte[] bPack = new byte[iLengthPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    Array.Clear(bPack, 0, bPack.Length);
                                    Array.Copy(bData, i * iLengthPack, bPack, 0, iLengthPack);

                                    TReconFWS tReconTempFWS = new TReconFWS();

                                    tReconTempFWS.iID = BitConverter.ToInt32(bPack, 0);
                                    tReconTempFWS.iFreq = BitConverter.ToSingle(bPack, 4);
                                    
                                    byte[] bTime = new byte[3];
                                    Array.Copy(bPack,8,bTime,0,3);

                                    TTimeMy tTime = new TTimeMy();
                                    object obj = tTime;
                                    StrArr.ByteArrayToStructure(bTime, ref obj);
                                    tTime = (TTimeMy)obj;
                                    
                                    tReconTempFWS.tTime = tTime;

                                    tReconTempFWS.sBearingOwn = BitConverter.ToInt16(bPack, 11);
                                    tReconTempFWS.sBearingLinked = BitConverter.ToInt16(bPack, 13);



                                    byte[] bCoord = new byte[8];
                                    Array.Copy(bPack, 15, bCoord, 0, 8);

                                    TCoord tCoord = new TCoord();
                                    obj = tCoord;
                                    StrArr.ByteArrayToStructure(bCoord, ref obj);
                                    tCoord = (TCoord)obj;
                                    
                                    tReconTempFWS.tCoord = tCoord;

                                    tReconTempFWS.bLevelOwn = bPack[23];
                                    tReconTempFWS.bLevelLinked = bPack[24];
                                    tReconTempFWS.bBandWidth = bPack[25];
                                    tReconTempFWS.bModulation = bPack[26];
                                    tReconTempFWS.bSignAudio = bPack[27];

                                    /*object obj = tReconTempFWS;
                                    StrArr.ByteArrayToStructure(bPack, ref obj);
                                    tReconTempFWS = (TReconFWS)obj;*/

                                    tReconFWS[i] = tReconTempFWS;
                                }
                            }

                            if (OnConfirmReconFWS != null)
                                OnConfirmReconFWS(this, tServicePart.bAdrSend, bCode, tReconFWS);

                        }
                        catch (Exception)
                        { }
                        break;

                    case RECON_FWS_ANALOG:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 38;
                            int iCountPack = bData.Length / iLengthPack;

                            TReconFWSAnalog[] tReconFWSAnalog = null;

                            if (iCountPack > 0)
                            {
                                tReconFWSAnalog = new TReconFWSAnalog[iCountPack];
                                byte[] bPack = new byte[iLengthPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    Array.Clear(bPack, 0, bPack.Length);
                                    Array.Copy(bData, i * iLengthPack, bPack, 0, iLengthPack);

                                    TReconFWSAnalog tReconTempFWS = new TReconFWSAnalog();

                                    tReconTempFWS.iID = BitConverter.ToInt32(bPack, 0);
                                    tReconTempFWS.dFreq = BitConverter.ToDouble(bPack, 4);
                                    tReconTempFWS.fBand = BitConverter.ToSingle(bPack, 12);
                                    byte[] bTime = new byte[3];
                                    Array.Copy(bPack, 16, bTime, 0, 3);

                                    TTimeMy tTime = new TTimeMy();
                                    object obj = tTime;
                                    StrArr.ByteArrayToStructure(bTime, ref obj);
                                    tTime = (TTimeMy)obj;

                                    tReconTempFWS.tTime = tTime;

                                    tReconTempFWS.sBearingOwn = BitConverter.ToSingle(bPack, 19);



                                    byte[] bCoord = new byte[8];
                                    Array.Copy(bPack, 23, bCoord, 0, 8);

                                    TCoord tCoord = new TCoord();
                                    obj = tCoord;
                                    StrArr.ByteArrayToStructure(bCoord, ref obj);
                                    tCoord = (TCoord)obj;

                                    tReconTempFWS.tCoord = tCoord;

                                    tReconTempFWS.bLevelOwn = BitConverter.ToInt16(bPack, 31);
                                    tReconTempFWS.bTypeSignal = bPack[33];
                                    tReconTempFWS.bBandDemodulation = BitConverter.ToInt32(bPack, 34);

                                    /*object obj = tReconTempFWS;
                                    StrArr.ByteArrayToStructure(bPack, ref obj);
                                    tReconTempFWS = (TReconFWS)obj;*/

                                    tReconFWSAnalog[i] = tReconTempFWS;
                                }
                            }

                            if (OnConfirmReconFWSAnalog != null)
                                OnConfirmReconFWSAnalog(this, tServicePart.bAdrSend, bCode, tReconFWSAnalog);

                        }
                        catch (Exception)
                        { }
                        break;

                    case RECON_FWS_DIGITAL:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 33;
                            int iCountPack = bData.Length / iLengthPack;

                            TReconFWSDigital[] tReconFWSDigital = null;

                            if (iCountPack > 0)
                            {
                                tReconFWSDigital = new TReconFWSDigital[iCountPack];
                                byte[] bPack = new byte[iLengthPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    Array.Clear(bPack, 0, bPack.Length);
                                    Array.Copy(bData, i * iLengthPack, bPack, 0, iLengthPack);

                                    TReconFWSDigital tReconTempFWS = new TReconFWSDigital();

                                    tReconTempFWS.iID = BitConverter.ToInt32(bPack, 0);
                                    tReconTempFWS.dFreq = BitConverter.ToDouble(bPack, 4);

                                    byte[] bTime = new byte[3];
                                    Array.Copy(bPack, 12, bTime, 0, 3);

                                    TTimeMy tTime = new TTimeMy();
                                    object obj = tTime;
                                    StrArr.ByteArrayToStructure(bTime, ref obj);
                                    tTime = (TTimeMy)obj;

                                    tReconTempFWS.tTimeBegin = tTime;

                                    byte[] bTimeEnd = new byte[3];
                                    Array.Copy(bPack, 15, bTimeEnd, 0, 3);

                                    TTimeMy tTimeEnd = new TTimeMy();
                                    object objEnd = tTimeEnd;
                                    StrArr.ByteArrayToStructure(bTimeEnd, ref objEnd);
                                    tTimeEnd = (TTimeMy)objEnd;

                                    tReconTempFWS.tTimeEnd = tTimeEnd;

                                    tReconTempFWS.bTypeSignal = BitConverter.ToInt16(bPack, 18);
                                    tReconTempFWS.sBearingOwn = BitConverter.ToSingle(bPack, 20);



                                    byte[] bCoord = new byte[8];
                                    Array.Copy(bPack, 24, bCoord, 0, 8);

                                    TCoord tCoord = new TCoord();
                                    obj = tCoord;
                                    StrArr.ByteArrayToStructure(bCoord, ref obj);
                                    tCoord = (TCoord)obj;

                                    tReconTempFWS.tCoord = tCoord;

                                    tReconTempFWS.bSignAudio = bPack[32];

                                    /*object obj = tReconTempFWS;
                                    StrArr.ByteArrayToStructure(bPack, ref obj);
                                    tReconTempFWS = (TReconFWS)obj;*/

                                    tReconFWSDigital[i] = tReconTempFWS;
                                }
                            }

                            if (OnConfirmReconFWSDigital != null)
                                OnConfirmReconFWSDigital(this, tServicePart.bAdrSend, bCode, tReconFWSDigital);

                        }
                        catch (Exception)
                        { }
                        break;

                    case RECON_FHSS:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 37;
                            int iCountPack = bData.Length / iLengthPack;

                            TReconFHSS[] tReconFHSS = null;

                            if (iCountPack > 0)
                            {
                                tReconFHSS = new TReconFHSS[iCountPack];
                                byte[] bPack = new byte[iLengthPack];
                                for (int i = 0; i < iCountPack; i++)
                                {
                                    Array.Clear(bPack, 0, bPack.Length);
                                    Array.Copy(bData, i * iLengthPack, bPack, 0, iLengthPack);

                                    TReconFHSS tReconTempFHSS = new TReconFHSS();

                                    tReconTempFHSS.iID = BitConverter.ToInt32(bPack, 0);
                                    tReconTempFHSS.iFreqMin = BitConverter.ToSingle(bPack, 4);
                                    tReconTempFHSS.iFreqMax = BitConverter.ToSingle(bPack, 8);

                                    tReconTempFHSS.sStep = BitConverter.ToInt16(bPack, 12);

                                    tReconTempFHSS.iDuration = BitConverter.ToInt32(bPack, 14);
                                    
                                    tReconTempFHSS.bMinute = bPack[18];
                                    tReconTempFHSS.bSecond = bPack[19];

                                    tReconTempFHSS.sBearingOwn = BitConverter.ToInt16(bPack, 20);
                                    tReconTempFHSS.sBearingLinked = BitConverter.ToInt16(bPack, 22);



                                    byte[] bCoord = new byte[8];
                                    Array.Copy(bPack, 24, bCoord, 0, 8);

                                    TCoord tCoord = new TCoord();
                                    object obj = tCoord;
                                    StrArr.ByteArrayToStructure(bCoord, ref obj);
                                    tCoord = (TCoord)obj;

                                    tReconTempFHSS.tCoord = tCoord;

                                    tReconTempFHSS.bLevelOwn = bPack[32];
                                    tReconTempFHSS.bLevelLinked = bPack[33];
                                    tReconTempFHSS.bBandWidth = bPack[34];
                                    tReconTempFHSS.bModulation = bPack[35];
                                    tReconTempFHSS.bSignAudio = bPack[36];




                                    /*object obj = tReconTempFHSS;
                                    StrArr.ByteArrayToStructure(bPack, ref obj);
                                    tReconTempFHSS = (TReconFHSS)obj;*/

                                    tReconFHSS[i] = tReconTempFHSS;
                                }
                            }

                            if (OnConfirmReconFHSS != null)
                                OnConfirmReconFHSS(this, tServicePart.bAdrSend, bCode, tReconFHSS);

                        }
                        catch (Exception)
                        { }
                        break;

                    case EXEC_BEAR:
                        try
                        {
                            byte bCode = bData[0];
                            int iID = BitConverter.ToInt32(bData, 1);
                            int iFreq = BitConverter.ToInt32(bData, 5);
                            short sOwnBearing = BitConverter.ToInt16(bData, 9);

                            if (OnConfirmExecBear != null)
                                OnConfirmExecBear(this, tServicePart.bAdrSend, bCode, iID, iFreq, sOwnBearing);

                        }
                        catch (Exception)
                        { }

                        break;

                    case SIMUL_BEAR:
                        try
                        {
                            byte bCode = bData[0];
                            int iID = BitConverter.ToInt32(bData, 1);
                            int iFreq = BitConverter.ToInt32(bData, 5);  

                            short sOwnBearing = BitConverter.ToInt16(bData, 9);
                            short sLinkedBearing = BitConverter.ToInt16(bData, 11);

                            if (OnConfirmSimulBear != null)
                                OnConfirmSimulBear(this, tServicePart.bAdrSend, bCode, iID, iFreq, sOwnBearing, sLinkedBearing);

                        }
                        catch (Exception)
                        { }

                        break;

                    case STATE_SUPPR_FWS:
                        try
                        {
                            byte bCode = bData[0];
                           
                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 10;
                            int iCountPack = bData.Length / iLengthPack;

                            TResSupprFWS[] tResSupprFWS = null;

                            if (iCountPack > 0)
                            {
                                tResSupprFWS = new TResSupprFWS[iCountPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    tResSupprFWS[i].iID = BitConverter.ToInt32(bData, i * iLengthPack );
                                    tResSupprFWS[i].iFreq = BitConverter.ToSingle(bData, i * iLengthPack + 4);
                                    tResSupprFWS[i].bResCtrl = bData[i * iLengthPack + 8];
                                    tResSupprFWS[i].bResSuppr = bData[i * iLengthPack + 9];
                                }
                            }

                            if (OnConfirmStateSupprFWS != null)
                                OnConfirmStateSupprFWS(this, tServicePart.bAdrSend, bCode, tResSupprFWS);

                        }
                        catch (Exception)
                        { }
                        break;

                    case STATE_SUPPR_FHSS:
                        try
                        {
                            byte bCode = bData[0];                     

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 14;
                            int iCountPack = bData.Length / iLengthPack;

                            TResSupprFHSS[] tResSupprFHSS = null;

                            if (iCountPack > 0)
                            {
                                tResSupprFHSS = new TResSupprFHSS[iCountPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    tResSupprFHSS[i].iID = BitConverter.ToInt32(bData, i * iLengthPack );
                                    tResSupprFHSS[i].iFreqMin = BitConverter.ToSingle(bData, i * iLengthPack + 4);
                                    tResSupprFHSS[i].iFreqMax = BitConverter.ToSingle(bData, i * iLengthPack + 8);
                                    tResSupprFHSS[i].bResCtrl = bData[i * iLengthPack + 12];
                                    tResSupprFHSS[i].bResSuppr = bData[i * iLengthPack + 13];
                                }
                            }

                            if (OnConfirmStateSupprFHSS != null)
                                OnConfirmStateSupprFHSS(this, tServicePart.bAdrSend, bCode, tResSupprFHSS);

                        }
                        catch (Exception)
                        { }
                        break;

                    default:
                        break;


                }
            }
        }

        #endregion
    }
}
