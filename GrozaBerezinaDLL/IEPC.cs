﻿using StructTypeGR_BRZ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GrozaBerezinaDLL
{
    public class IEPC
    {
        const byte CntRRC = 1;
        const byte Cnt3G = 7;
        const byte CntModem = 8;
        const byte CntHF = 9;
        const byte CntVHF = 10;

        byte COUNT_RS232 = 3;
        byte COUNT_TCP = 2;

        int iPortListener = 7900;

        RS232PC[] objRS232;

        TcpServerPC[] objTcpServer;

        byte[] bTypeCntRS232;
        byte[] bTypeCntTcpServer;


        #region Delegates

        public delegate void ConnectEventHandler(byte bTypeCnt);
       
        public delegate void CreateServerEventHandler(byte bTypeCnt);
        public delegate void ConnectClientEventHandler(byte bTypeCnt, string strIPAddress, byte bAddressPC);
        public delegate void ByteEventHandler(byte bTypeCnt,  byte[] bByte);
        public delegate void ByteEthEventHandler(byte bTypeCnt, string strIPAddress, byte[] bByte);
      
        public delegate void CmdEventHandler(byte bTypeCnt, object obj);

        public delegate void CmdTextEventHandler(byte bTypeCnt, byte bAddressSend, string strText);
        public delegate void ConfirmAddressEventHandler(byte bTypeCnt, byte bAddressSend);
        public delegate void ConfirmTextEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmSynchTimeEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, TTimeMy tTime);
        public delegate void ConfirmRegimeEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmRangeSectorEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, byte bTypeRange);
        public delegate void ConfirmRangeSpecEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, byte bTypeSpec);
        public delegate void ConfirmSupprFWSEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmSupprFHSSEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError);

        public delegate void ConfirmStateEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, short sNum, byte bType, byte bRole, byte bRegime, byte[] bLetter);
        public delegate void ConfirmCoordEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, TCoord tCoord);
        public delegate void ConfirmReconFWSEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, TReconFWS[] tReconFWS);
        public delegate void ConfirmReconFHSSEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, TReconFHSS[] tReconFHSS);
        public delegate void ConfirmExecBearEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing);
        public delegate void ConfirmSimulBearEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing, short sLinkedBearing);
        public delegate void ConfirmStateSupprFWSEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, TResSupprFWS[] tResSupprFWS);
        public delegate void ConfirmStateSupprFHSSEventHandler(byte bTypeCnt, byte bAddressSend, byte bCodeError, TResSupprFHSS[] tResSupprFHSS);



        #endregion

        #region Events

        public event CreateServerEventHandler OnCreateServer;
        public event CreateServerEventHandler OnDestroyServer;

        public event ConnectClientEventHandler OnConnectClient;
        public event ConnectClientEventHandler OnDisconnectClient;

        public event ConnectEventHandler OnOpenPort;
        public event ConnectEventHandler OnClosePort;

        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;

        public event ByteEthEventHandler OnReadByteEth;
        public event ByteEthEventHandler OnWriteByteEth;

      
        public event CmdTextEventHandler OnTextCmd;
        public event ConfirmAddressEventHandler OnConfirmAddress;
        public event ConfirmTextEventHandler OnConfirmTextCmd;
        public event ConfirmSynchTimeEventHandler OnConfirmSynchTime;
        public event ConfirmRegimeEventHandler OnConfirmRegime;
        public event ConfirmRangeSectorEventHandler OnConfirmRangeSector;
        public event ConfirmRangeSpecEventHandler OnConfirmRangeSpec;
        public event ConfirmSupprFWSEventHandler OnConfirmSupprFWS;
        public event ConfirmSupprFHSSEventHandler OnConfirmSupprFHSS;

        public event ConfirmStateEventHandler OnConfirmState;
        public event ConfirmCoordEventHandler OnConfirmCoord;
        public event ConfirmReconFWSEventHandler OnConfirmReconFWS;
        public event ConfirmReconFHSSEventHandler OnConfirmReconFHSS;
        public event ConfirmExecBearEventHandler OnConfirmExecBear;
        public event ConfirmSimulBearEventHandler OnConfirmSimulBear;
        public event ConfirmStateSupprFWSEventHandler OnConfirmStateSupprFWS;
        public event ConfirmStateSupprFHSSEventHandler OnConfirmStateSupprFHSS;

        #endregion


        public IEPC()
        {
            bTypeCntRS232 = new byte[COUNT_RS232];

            bTypeCntRS232[0] = CntModem;
            bTypeCntRS232[1] = CntHF;
            bTypeCntRS232[2] = CntVHF;
            
            objRS232 = new RS232PC[COUNT_RS232];

            bTypeCntTcpServer = new byte[COUNT_TCP];

            bTypeCntTcpServer[0] = CntRRC;
            bTypeCntTcpServer[1] = Cnt3G;
           

            objTcpServer = new TcpServerPC[COUNT_TCP];

            for (int i = 0; i < COUNT_TCP; i++)
            {
                objTcpServer[i] = new TcpServerPC();

                objTcpServer[i].OnCreate += new TcpServerPC.ConnectServerEventHandler(CreateServer);
                objTcpServer[i].OnDestroy += new TcpServerPC.ConnectServerEventHandler(DestroyServer);


                objTcpServer[i].OnConnect += new TcpServerPC.ConnectClientEventHandler(ConnectClient);
                objTcpServer[i].OnConnectUndef += new TcpServerPC.ConnectClientEventHandler(ConnectClientUndef);
                objTcpServer[i].OnDisconnect += new TcpServerPC.ConnectClientEventHandler(DisconnectClient);

                objTcpServer[i].OnReadByte += new TcpServerPC.ByteEventHandler(ReadByteEth);
                objTcpServer[i].OnWriteByte += new TcpServerPC.ByteEventHandler(WriteByteEth);

                objTcpServer[i].OnTextCmd += new TcpServerPC.CmdTextEventHandler(Receive_Text);
                objTcpServer[i].OnConfirmSynchTime += new TcpServerPC.ConfirmSynchTimeEventHandler(Receive_ConfirmSynchTime);
                objTcpServer[i].OnConfirmText += new TcpServerPC.ConfirmTextEventHandler(Receive_ConfirmText);
                objTcpServer[i].OnConfirmAddress += new TcpServerPC.ConfirmAddressEventHandler(Receive_ConfirmAddress);
                objTcpServer[i].OnConfirmRegime += new TcpServerPC.ConfirmRegimeEventHandler(Receive_ConfirmRegime);
                objTcpServer[i].OnConfirmRangeSector += new TcpServerPC.ConfirmRangeSectorEventHandler(Receive_ConfirmRangeSector);
                objTcpServer[i].OnConfirmRangeSpec += new TcpServerPC.ConfirmRangeSpecEventHandler(Receive_ConfirmRangeSpec);
                objTcpServer[i].OnConfirmSupprFWS += new TcpServerPC.ConfirmSupprFWSEventHandler(Receive_ConfirmSupprFWS);
                objTcpServer[i].OnConfirmSupprFHSS += new TcpServerPC.ConfirmSupprFHSSEventHandler(Receive_ConfirmSupprFHSS);

                objTcpServer[i].OnConfirmState += new TcpServerPC.ConfirmStateEventHandler(Receive_ConfirmState);
                objTcpServer[i].OnConfirmCoord += new TcpServerPC.ConfirmCoordEventHandler(Receive_ConfirmCoord);
                objTcpServer[i].OnConfirmReconFWS += new TcpServerPC.ConfirmReconFWSEventHandler(Receive_ConfirmReconFWS);
                objTcpServer[i].OnConfirmReconFHSS += new TcpServerPC.ConfirmReconFHSSEventHandler(Receive_ConfirmReconFHSS);
                objTcpServer[i].OnConfirmExecBear += new TcpServerPC.ConfirmExecBearEventHandler(Receive_ConfirmExecBear);
                objTcpServer[i].OnConfirmSimulBear += new TcpServerPC.ConfirmSimulBearEventHandler(Receive_ConfirmSimulBear);
                objTcpServer[i].OnConfirmStateSupprFWS += new TcpServerPC.ConfirmStateSupprFWSEventHandler(Receive_ConfirmStateSupprFWS);
                objTcpServer[i].OnConfirmStateSupprFHSS += new TcpServerPC.ConfirmStateSupprFHSSEventHandler(Receive_ConfirmStateSupprFHSS);
            }

            for (int i = 0; i < COUNT_RS232; i++)
            {
                objRS232[i] = new RS232PC();

                objRS232[i].OnOpenPort += new RS232PC.ConnectEventHandler(OpenPort);
                objRS232[i].OnClosePort += new RS232PC.ConnectEventHandler(ClosePort);

                objRS232[i].OnReadByte += new RS232PC.ByteEventHandler(ReadByteCom);
                objRS232[i].OnWriteByte += new RS232PC.ByteEventHandler(WriteByteCom);

                objRS232[i].OnTextCmd += new RS232PC.CmdTextEventHandler(Receive_Text);
                objRS232[i].OnConfirmSynchTime += new RS232PC.ConfirmSynchTimeEventHandler(Receive_ConfirmSynchTime);

                objRS232[i].OnConfirmText += new RS232PC.ConfirmTextEventHandler(Receive_ConfirmText);

                objRS232[i].OnConfirmRegime += new RS232PC.ConfirmRegimeEventHandler(Receive_ConfirmRegime);
                objRS232[i].OnConfirmRangeSector += new RS232PC.ConfirmRangeSectorEventHandler(Receive_ConfirmRangeSector);
                objRS232[i].OnConfirmRangeSpec += new RS232PC.ConfirmRangeSpecEventHandler(Receive_ConfirmRangeSpec);
                objRS232[i].OnConfirmSupprFWS += new RS232PC.ConfirmSupprFWSEventHandler(Receive_ConfirmSupprFWS);
                objRS232[i].OnConfirmSupprFHSS += new RS232PC.ConfirmSupprFHSSEventHandler(Receive_ConfirmSupprFHSS);

                objRS232[i].OnConfirmState += new RS232PC.ConfirmStateEventHandler(Receive_ConfirmState);
                objRS232[i].OnConfirmCoord += new RS232PC.ConfirmCoordEventHandler(Receive_ConfirmCoord);
                objRS232[i].OnConfirmReconFWS += new RS232PC.ConfirmReconFWSEventHandler(Receive_ConfirmReconFWS);
                objRS232[i].OnConfirmReconFHSS += new RS232PC.ConfirmReconFHSSEventHandler(Receive_ConfirmReconFHSS);
                objRS232[i].OnConfirmExecBear += new RS232PC.ConfirmExecBearEventHandler(Receive_ConfirmExecBear);
                objRS232[i].OnConfirmSimulBear += new RS232PC.ConfirmSimulBearEventHandler(Receive_ConfirmSimulBear);
                objRS232[i].OnConfirmStateSupprFWS += new RS232PC.ConfirmStateSupprFWSEventHandler(Receive_ConfirmStateSupprFWS);
                objRS232[i].OnConfirmStateSupprFHSS += new RS232PC.ConfirmStateSupprFHSSEventHandler(Receive_ConfirmStateSupprFHSS);
            }
 
        }

        public bool CreateServerRRC(string strIP, int iPort)
        {
            bool blRes = false;
            if (objTcpServer != null)
            {
                try
                {
                    string strLocalHost = Dns.GetHostName();
                    //System.Net.IPAddress ip = System.Net.Dns.GetHostByName(strLocalHost).AddressList[0];
                    System.Net.IPAddress ip = IPAddress.Parse(strIP);
                    objTcpServer[0].CreateServer(ip, iPort);

                    blRes = true;
                }
                catch (SystemException)
                {
                    return false;
                }
            }

            return blRes;

        }

        public bool CreateServer3G(string strIP, int iPort)
        {
            bool blRes = false;
            if (objTcpServer != null)
            {
                try
                {
                    string strLocalHost = Dns.GetHostName();
                    //System.Net.IPAddress ip = System.Net.Dns.GetHostByName(strLocalHost).AddressList[1];
                    System.Net.IPAddress ip = IPAddress.Parse(strIP);
                    blRes = objTcpServer[1].CreateServer(ip, iPort);
                }
                catch (SystemException)
                {
                    return false;
                }
            }

            return blRes;

        }


        public bool DestroyServerRRC()
        {
            bool blRes = false;
            if (objTcpServer != null)
            {
                try
                {
                    blRes = objTcpServer[0].DestroyServer();
                }
                catch (SystemException)
                { }
            }

            return blRes;
        }

        public bool DestroyServer3G()
        {
            bool blRes = false;
            if (objTcpServer != null)
            {
                try
                {
                    blRes = objTcpServer[1].DestroyServer();
                }
                catch (SystemException)
                { }
            }
            return blRes;
        }


        public bool ConnectModem(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            bool blRes = false;
            if (objRS232 != null)
            {
                try
                {
                    blRes = objRS232[0].OpenPort(portName, baudRate, parity, dataBits, stopBits);
                    
                }
                catch (SystemException)
                {
                    return false;
                }
            }
            return blRes;
        }

        public bool ConnectHF(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            if (objRS232 != null)
            {
                try
                {
                    return objRS232[1].OpenPort(portName, baudRate, parity, dataBits, stopBits);

                }
                catch (SystemException)
                {
                    return false;
                }
            }

            return false;
        }

        public bool ConnectVHF(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            if (objRS232 != null)
            {
                try
                {
                    return objRS232[2].OpenPort(portName, baudRate, parity, dataBits, stopBits);

                }
                catch (SystemException)
                {
                    return false;
                }
            }
            return false;
        }




        public bool DisconnectModem()
        {
            if (objRS232 != null)
            {
                try
                {
                    return objRS232[0].ClosePort();
                }
                catch (SystemException)
                {
                    return false;
                }
            }

            return false;
        }

        public bool DisconnectHF()
        {
            try
            {
                return objRS232[1].ClosePort();
            }
            catch (SystemException)
            {
                return false;
            }

            return false;
            
        }

        public bool DisconnectVHF()
        {
            try
            {
                return objRS232[2].ClosePort();
            }
            catch (SystemException)
            {
                return false;
            }

            return false;
        }





        #region SendFunction


        public bool SendDefineAddress(byte bTypeCnt, string strIPAddress)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            
            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendAddress(strIPAddress);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendText(byte bTypeCnt, byte bAddressReceive, string strText)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendText(bAddressReceive, strText);                        
                    }
                    catch (SystemException)
                    {}

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                           blSendReturn = objTcpServer[i].SendText(bAddressReceive, strText);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }
 
            }

            return blSendReturn;
        }

        public bool SendConfirmText(byte bTypeCnt, byte bAddressReceive, byte bCodeError)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendConfirmText(bAddressReceive, bCodeError);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendConfirmText(bAddressReceive, bCodeError);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendState(byte bTypeCnt, byte bAddressReceive)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendState(bAddressReceive);

                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }

                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendState(bAddressReceive);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendSynchTime(byte bTypeCnt, byte bAddressReceive, TTimeMy tTime)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendSynchTime(bAddressReceive, tTime);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }

                i++;
            }


            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendSynchTime(bAddressReceive, tTime);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }
            return blSendReturn;
           
        }

        public bool SendRegime(byte bTypeCnt, byte bAddressReceive, byte bRegime)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendRegime(bAddressReceive, bRegime);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }

                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendRegime(bAddressReceive, bRegime);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
                       
        }

        public bool SendRangeSector(byte bTypeCnt, byte bAddressReceive, byte bSign, TRangeSector[] tRangeSector)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendRangeSector(bAddressReceive, bSign, tRangeSector);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }

                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendRangeSector(bAddressReceive, bSign, tRangeSector);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendRangeSpec(byte bTypeCnt, byte bAddressReceive, byte bSign, TRangeSpec[] tRangeSpec)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendRangeSpec(bAddressReceive, bSign, tRangeSpec);

                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }

                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendRangeSpec(bAddressReceive, bSign, tRangeSpec);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendSupprFWS(byte bTypeCnt, byte bAddressReceive, int iDuration, TSupprFWS[] tSupprFWS)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendSupprFWS(bAddressReceive, iDuration, tSupprFWS);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }

                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendSupprFWS(bAddressReceive, iDuration, tSupprFWS);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendSupprFHSS(byte bTypeCnt, byte bAddressReceive, int iDuration, TSupprFHSS[] tSupprFHSS)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendSupprFHSS(bAddressReceive, iDuration, tSupprFHSS);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendSupprFHSS(bAddressReceive, iDuration, tSupprFHSS);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendCoord(byte bTypeCnt, byte bAddressReceive)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendCoord(bAddressReceive);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendCoord(bAddressReceive);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendReconFWS(byte bTypeCnt, byte bAddressReceive)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendReconFWS(bAddressReceive);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendReconFWS(bAddressReceive);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendReconFHSS(byte bTypeCnt, byte bAddressReceive)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendReconFHSS(bAddressReceive);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendReconFHSS(bAddressReceive);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendExecBear(byte bTypeCnt, byte bAddressReceive, int iID, int iFreq)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendExecBear(bAddressReceive, iID, iFreq);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendExecBear(bAddressReceive,iID, iFreq);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendSimulBear(byte bTypeCnt, byte bAddressReceive, int iID, int iFreq)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendSimulBear(bAddressReceive, iID, iFreq);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendSimulBear(bAddressReceive, iID,iFreq);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendStateSupprFWS(byte bTypeCnt, byte bAddressReceive)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendStateSupprFWS(bAddressReceive);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendStateSupprFWS(bAddressReceive);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendStateSupprFHSS(byte bTypeCnt, byte bAddressReceive)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendStateSupprFHSS(bAddressReceive);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendStateSupprFHSS(bAddressReceive);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        public bool SendAudio(byte bTypeCnt, byte bAddressReceive, int iFreq)
        {
            bool blSendRS = false;
            bool blSendReturn = false;
            int i = 0;
            while (i < bTypeCntRS232.Length)
            {
                if (bTypeCnt == bTypeCntRS232[i])
                {
                    try
                    {
                        blSendReturn = objRS232[i].SendAudio(bAddressReceive, iFreq);
                    }
                    catch (SystemException)
                    { }

                    blSendRS = true;
                    i = bTypeCntRS232.Length;
                }
                i++;
            }

            if (blSendRS == false)
            {
                i = 0;

                while (i < bTypeCntTcpServer.Length)
                {
                    if (bTypeCnt == bTypeCntTcpServer[i])
                    {
                        try
                        {
                            blSendReturn = objTcpServer[i].SendAudio(bAddressReceive, iFreq);
                        }
                        catch (SystemException)
                        { }

                        blSendRS = true;
                        i = bTypeCntTcpServer.Length;
                    }
                    i++;
                }

            }

            return blSendReturn;
        }

        #endregion


        #region Function Event

        private void CreateServer(object sender)
        {
            TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

            int i = 0;
            while (i < objTcpServer.Length)
            {
                if (objTcpServer[i].Equals(tempTcpServerPC))
                {
                    if (OnCreateServer != null)
                        OnCreateServer(bTypeCntTcpServer[i]);
                    i = objTcpServer.Length;
                }
                i++;
            }
        }

        private void DestroyServer(object sender)
        {
            TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

            int i = 0;
            while (i < objTcpServer.Length)
            {
                if (objTcpServer[i].Equals(tempTcpServerPC))
                {
                    if (OnDestroyServer != null)
                        OnDestroyServer(bTypeCntTcpServer[i]);
                    i = objTcpServer.Length;
                }
                i++;
            }
        }


        private void ConnectClient(object sender, string strIPAddress, byte bAddressPC)
        {
            TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

            int i = 0;
            while (i < objTcpServer.Length)
            {
                if (objTcpServer[i].Equals(tempTcpServerPC))
                {
                    if (OnConnectClient != null)
                        OnConnectClient(bTypeCntTcpServer[i], strIPAddress, bAddressPC);
                    i = objTcpServer.Length;
                }
                i++;
            }
        }


        private void ConnectClientUndef(object sender, string strIPAddress, byte bAddressPC)
        {
            TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

            int i = 0;
            while (i < objTcpServer.Length)
            {
                if (objTcpServer[i].Equals(tempTcpServerPC))
                {
                    SendDefineAddress(bTypeCntTcpServer[i], strIPAddress);
                    
                    /*if (OnConnectClient != null)
                        OnConnectClient(bTypeCntTcpServer[i], strIPAddress, bAddressPC);*/

                    i = objTcpServer.Length;
                }
                i++;
            }
        }
        

        private void DisconnectClient(object sender, string strIPAddress, byte bAddressPC)
        {
            TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

            int i = 0;
            while (i < objTcpServer.Length)
            {
                if (objTcpServer[i].Equals(tempTcpServerPC))
                {
                    if (OnDisconnectClient != null)
                        OnDisconnectClient(bTypeCntTcpServer[i], strIPAddress, bAddressPC);
                    i = objTcpServer.Length;
                }
                i++;
            }
        }
        

        private void OpenPort(object sender)
        {
            RS232PC tempRS232PC = (RS232PC)sender;

            int i = 0;
            while (i < objRS232.Length)
            {
                if (objRS232[i].Equals(tempRS232PC))
                {
                    if (OnOpenPort != null)
                        OnOpenPort(bTypeCntRS232[i]);
                    i = objRS232.Length;
                }
                i++;
            }                       
        }

        private void ClosePort(object sender)
        {
            RS232PC tempRS232PC = (RS232PC)sender;

            int i = 0;
            while (i < objRS232.Length)
            {
                if (objRS232[i].Equals(tempRS232PC))
                {
                    if (OnClosePort != null)
                        OnClosePort(bTypeCntRS232[i]);
                    i = objRS232.Length;
                }
                i++;
            }
        }

        private void ReadByteCom(object sender, byte[] bByte)
        {
            RS232PC tempRS232PC = (RS232PC)sender;

            int i = 0;
            while (i < objRS232.Length)
            {
                if (objRS232[i].Equals(tempRS232PC))
                {
                    if (OnReadByte != null)
                        OnReadByte(bTypeCntRS232[i], bByte);
                    i = objRS232.Length;
                }
                i++;
            }
        }

        private void WriteByteCom(object sender,  byte[] bByte)
        {
            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;

                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnWriteByte != null)
                            OnWriteByte(bTypeCntRS232[i], bByte);
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}


          
        }



        private void ReadByteEth(object sender, string strIPAddress, byte[] bByte)
        {
            try
            {
                TcpServerPC tempTcpServerPC = (TcpServerPC)sender;
                int i = 0;
                while (i < objTcpServer.Length)
                {
                    if (objTcpServer[i].Equals(tempTcpServerPC))
                    {
                        if (OnReadByteEth != null)
                            OnReadByteEth(bTypeCntTcpServer[i], strIPAddress, bByte);
                        i = objTcpServer.Length;
                    }
                    i++;
                }
            }
            catch (SystemException)
            { }
        }

        private void WriteByteEth(object sender, string strIPAddress, byte[] bByte)
        {            
            try
            {
                TcpServerPC tempTcpServerPC = (TcpServerPC)sender;
                int i = 0;
                while (i < objTcpServer.Length)
                {
                    if (objTcpServer[i].Equals(tempTcpServerPC))
                    {
                        if (OnWriteByteEth != null)
                            OnWriteByteEth(bTypeCntTcpServer[i], strIPAddress, bByte);
                        i = objTcpServer.Length;
                    }
                    i++;
                }
            }
            catch (SystemException)
            { }
        }




        private void Receive_ConfirmAddress(object sender, byte bAddressSend)
        {
            bool blReceive = false;
            
            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;
                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmAddress != null)
                            OnConfirmAddress(bTypeCntRS232[i], bAddressSend);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmAddress != null)
                                OnConfirmAddress(bTypeCntTcpServer[i], bAddressSend);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;
 
                    }
                }
                catch (SystemException)
                {}
            }
        }        

        private void Receive_Text(object sender, byte bAddressSend, string strText)
        {
            bool blReceive = false;

            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;

                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnTextCmd != null)
                            OnTextCmd(bTypeCntRS232[i], bAddressSend, strText);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnTextCmd != null)
                                OnTextCmd(bTypeCntTcpServer[i], bAddressSend, strText);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmText(object sender, byte bAddressSend, byte bCodeError)
        {
            bool blReceive = false;

            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;
                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmTextCmd != null)
                            OnConfirmTextCmd(bTypeCntRS232[i], bAddressSend, bCodeError);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmTextCmd != null)
                                OnConfirmTextCmd(bTypeCntTcpServer[i], bAddressSend, bCodeError);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmSynchTime(object sender, byte bAddressSend, byte bCodeError, TTimeMy tTime)
        {
           bool blReceive = false;
            
            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;

                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmSynchTime != null)
                            OnConfirmSynchTime(bTypeCntRS232[i], bAddressSend, bCodeError, tTime);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }

            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
            
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmSynchTime != null)
                                OnConfirmSynchTime(bTypeCntTcpServer[i], bAddressSend, bCodeError, tTime);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
            
                catch(SystemException)
                {}
            }
        }

        private void Receive_ConfirmRegime(object sender, byte bAddressSend, byte bCodeError)
        {
            bool blReceive = false;

            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;

                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmRegime != null)
                            OnConfirmRegime(bTypeCntRS232[i], bAddressSend, bCodeError);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmRegime != null)
                                OnConfirmRegime(bTypeCntTcpServer[i],  bAddressSend, bCodeError);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmRangeSector(object sender, byte bAddressSend, byte bCodeError, byte bTypeRange)
        {
            bool blReceive = false;

            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;
                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmRangeSector != null)
                            OnConfirmRangeSector(bTypeCntRS232[i], bAddressSend, bCodeError, bTypeRange);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}
            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmRangeSector != null)
                                OnConfirmRangeSector(bTypeCntTcpServer[i], bAddressSend, bCodeError, bTypeRange);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmRangeSpec(object sender, byte bAddressSend, byte bCodeError, byte bTypeSpec)
        {
            bool blReceive = false;
            
            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;
                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmRangeSpec != null)
                            OnConfirmRangeSpec(bTypeCntRS232[i], bAddressSend, bCodeError, bTypeSpec);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmRangeSpec != null)
                                OnConfirmRangeSpec(bTypeCntTcpServer[i], bAddressSend, bCodeError, bTypeSpec);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmSupprFWS(object sender, byte bAddressSend, byte bCodeError)
        {
            bool blReceive = false;

            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;
                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmSupprFWS != null)
                            OnConfirmSupprFWS(bTypeCntRS232[i], bAddressSend, bCodeError);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmSupprFWS != null)
                                OnConfirmSupprFWS(bTypeCntTcpServer[i], bAddressSend, bCodeError);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmSupprFHSS(object sender, byte bAddressSend, byte bCodeError)
        {
            bool blReceive = false;

            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;

                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmSupprFHSS != null)
                            OnConfirmSupprFHSS(bTypeCntRS232[i], bAddressSend, bCodeError);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}


            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmSupprFHSS != null)
                                OnConfirmSupprFHSS(bTypeCntTcpServer[i], bAddressSend, bCodeError);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmState(object sender, byte bAddressSend, byte bCodeError, short sNum, byte bType, byte bRole, byte bRegime, byte[] bLetter)
        {
            bool blReceive = false;
            
            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;

                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmState != null)
                            OnConfirmState(bTypeCntRS232[i], bAddressSend, bCodeError, sNum, bType, bRole, bRegime, bLetter);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }

            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmState != null)
                                OnConfirmState(bTypeCntTcpServer[i], bAddressSend, bCodeError, sNum, bType, bRole, bRegime, bLetter);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmCoord(object sender, byte bAddressSend, byte bCodeError, TCoord tCoord)
        {
            bool blReceive = false;
            
            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;
                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmCoord != null)
                            OnConfirmCoord(bTypeCntRS232[i], bAddressSend, bCodeError, tCoord);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}


            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmCoord != null)
                                OnConfirmCoord(bTypeCntTcpServer[i], bAddressSend, bCodeError, tCoord);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmReconFWS(object sender, byte bAddressSend, byte bCodeError, TReconFWS[] tReconFWS)
        {
            bool blReceive = false;
            
            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;
                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmReconFWS != null)
                            OnConfirmReconFWS(bTypeCntRS232[i], bAddressSend, bCodeError, tReconFWS);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmReconFWS != null)
                                OnConfirmReconFWS(bTypeCntTcpServer[i], bAddressSend, bCodeError, tReconFWS);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmReconFHSS(object sender, byte bAddressSend, byte bCodeError, TReconFHSS[] tReconFHSS)
        {
            bool blReceive = false;

            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;
                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmReconFHSS != null)
                            OnConfirmReconFHSS(bTypeCntRS232[i], bAddressSend, bCodeError, tReconFHSS);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmReconFHSS != null)
                                OnConfirmReconFHSS(bTypeCntTcpServer[i], bAddressSend, bCodeError, tReconFHSS);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmExecBear(object sender, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing)
        {
            bool blReceive = false;

            try
            {
            RS232PC tempRS232PC = (RS232PC)sender;
            
            int i = 0;
            while (i < objRS232.Length)
            {
                if (objRS232[i].Equals(tempRS232PC))
                {
                    if (OnConfirmExecBear != null)
                        OnConfirmExecBear(bTypeCntRS232[i], bAddressSend, bCodeError, iID, iFreq, sOwnBearing);

                    blReceive = true;
                    i = objRS232.Length;
                }
                i++;
            }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmExecBear != null)
                                OnConfirmExecBear(bTypeCntTcpServer[i], bAddressSend, bCodeError, iID, iFreq, sOwnBearing);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmSimulBear(object sender, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing, short sLinkedBearing)
        {
            bool blReceive = false;
            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;

                
                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmSimulBear != null)
                            OnConfirmSimulBear(bTypeCntRS232[i], bAddressSend, bCodeError, iID, iFreq, sOwnBearing, sLinkedBearing);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmSimulBear != null)
                                OnConfirmSimulBear(bTypeCntTcpServer[i], bAddressSend, bCodeError, iID, iFreq, sOwnBearing, sLinkedBearing);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmStateSupprFWS(object sender, byte bAddressSend, byte bCodeError, TResSupprFWS[] tResSupprFWS)
        {
            bool blReceive = false;

            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;

                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmStateSupprFWS != null)
                            OnConfirmStateSupprFWS(bTypeCntRS232[i], bAddressSend, bCodeError, tResSupprFWS);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmStateSupprFWS != null)
                                OnConfirmStateSupprFWS(bTypeCntTcpServer[i], bAddressSend, bCodeError, tResSupprFWS);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }

        private void Receive_ConfirmStateSupprFHSS(object sender, byte bAddressSend, byte bCodeError,  TResSupprFHSS[] tResSupprFHSS)
        {
            bool blReceive = false;

            try
            {
                RS232PC tempRS232PC = (RS232PC)sender;

                int i = 0;
                while (i < objRS232.Length)
                {
                    if (objRS232[i].Equals(tempRS232PC))
                    {
                        if (OnConfirmStateSupprFHSS != null)
                            OnConfirmStateSupprFHSS(bTypeCntRS232[i], bAddressSend, bCodeError, tResSupprFHSS);

                        blReceive = true;
                        i = objRS232.Length;
                    }
                    i++;
                }
            }
            catch(SystemException)
            {}

            if (blReceive == false)
            {
                try
                {
                    TcpServerPC tempTcpServerPC = (TcpServerPC)sender;

                    int i = 0;
                    while (i < objTcpServer.Length)
                    {
                        if (objTcpServer[i].Equals(tempTcpServerPC))
                        {
                            if (OnConfirmStateSupprFHSS != null)
                                OnConfirmStateSupprFHSS(bTypeCntTcpServer[i], bAddressSend, bCodeError, tResSupprFHSS);

                            blReceive = true;
                            i = objTcpServer.Length;
                        }
                        i++;

                    }
                }
                catch (SystemException)
                { }
            }
        }


        #endregion


    }
}
