﻿using Func;
using StructTypeGR_BRZ;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GrozaBerezinaDLL
{
    public class IExBRZ_GRZ : DeCodeCmd
    {
        private Thread thrRead;

        private SerialPort _port;

        public NetworkStream streamClient;
        private TcpClient tcpClient;

        byte bAddressSend;
       
        byte bCounter;


        public IExBRZ_GRZ(byte bAddress)
        {
            bAddressSend = bAddress;
        }

        #region Delegates

        public delegate void ConnectEventHandler(object sender);
        public delegate void ByteEventHandler(object sender, byte[] bByte);
        public delegate void CmdEventHandler(object sender, object obj);


        public delegate void CmdTextEventHandler(object sender, byte bAddressSend, string strText);
        public delegate void ConfirmTextEventHandler(object sender, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmSynchTimeEventHandler(object sender, byte bAddressSend, byte bCodeError, TTimeMy tTime);
        public delegate void ConfirmRegimeEventHandler(object sender, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmRangeSectorEventHandler(object sender, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmRangeSpecEventHandler(object sender, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmSupprFWSEventHandler(object sender, byte bAddressSend, byte bCodeError);
        public delegate void ConfirmSupprFHSSEventHandler(object sender, byte bAddressSend, byte bCodeError);

        public delegate void ConfirmStateEventHandler(object sender, byte bAddressSend, byte bCodeError, byte bRegime, byte[] bLetter);
        public delegate void ConfirmCoordEventHandler(object sender, byte bAddressSend, byte bCodeError, TCoord tCoord);
        public delegate void ConfirmReconFWSEventHandler(object sender, byte bAddressSend, byte bCodeError, TReconFWS[] tReconFWS);
        public delegate void ConfirmReconFHSSEventHandler(object sender, byte bAddressSend, byte bCodeError, TReconFHSS[] tReconFHSS);
        public delegate void ConfirmExecBearEventHandler(object sender, byte bAddressSend, byte bCodeError, short sOwnBearing);
        public delegate void ConfirmSimulBearEventHandler(object sender, byte bAddressSend, byte bCodeError, short sOwnBearing, short sLinkedBearing);
        public delegate void ConfirmStateSupprFWSEventHandler(object sender, byte bAddressSend, byte bCodeError, TResSupprFWS[] tResSupprFWS);
        public delegate void ConfirmStateSupprFHSSEventHandler(object sender, byte bAddressSend, byte bCodeError, TResSupprFHSS[] tResSupprFHSS);



        #endregion

        #region Events
        public event ConnectEventHandler OnConnect;
        public event ConnectEventHandler OnDisconnect;

        public event ByteEventHandler OnReadByte;
        public event ByteEventHandler OnWriteByte;

        public event CmdTextEventHandler OnTextCmd;
        public event ConfirmTextEventHandler OnConfirmTextCmd;
        public event ConfirmSynchTimeEventHandler OnConfirmSynchTime;
        public event ConfirmRegimeEventHandler OnConfirmRegime;
        public event ConfirmRangeSectorEventHandler OnConfirmRangeSector;
        public event ConfirmRangeSpecEventHandler OnConfirmRangeSpec;
        public event ConfirmSupprFWSEventHandler OnConfirmSupprFWS;
        public event ConfirmSupprFHSSEventHandler OnConfirmSupprFHSS;

        public event ConfirmStateEventHandler OnConfirmState;
        public event ConfirmCoordEventHandler OnConfirmCoord;
        public event ConfirmReconFWSEventHandler OnConfirmReconFWS;
        public event ConfirmReconFHSSEventHandler OnConfirmReconFHSS;
        public event ConfirmExecBearEventHandler OnConfirmExecBear;
        public event ConfirmSimulBearEventHandler OnConfirmSimulBear;
        public event ConfirmStateSupprFWSEventHandler OnConfirmStateSupprFWS;
        public event ConfirmStateSupprFHSSEventHandler OnConfirmStateSupprFHSS;

        #endregion



        // connect to server
        public bool Connect(string strIPServer, int iPortServer)
        {
            // if there is client
            if (tcpClient != null)
                tcpClient = null;

            // create client
            tcpClient = new TcpClient();

            try
            {
                // begin connect 
                var result = tcpClient.BeginConnect(strIPServer, iPortServer, null, null);

                // try connect
                var success = result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(1));

                if (!success)
                {
                    throw new Exception("Failed to connect.");
                }

                // we have connected
                tcpClient.EndConnect(result);
            }
            catch (System.Exception )
            {                
                //generate event
                if (OnDisconnect != null)
                    OnDisconnect(this);
                return false;
            }


            if (tcpClient.Connected)
            {
                // create stream for client
                if (streamClient != null)
                    streamClient = null;
                streamClient = tcpClient.GetStream();

                // destroy thread for reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // create thread for reading
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }

                catch (System.Exception )
                {                    
                    // generate event
                    if (OnDisconnect != null)
                        OnDisconnect(this);
                    return false;

                }
            }

            if (OnConnect != null)
                OnConnect(this);
            return true;
        }



        public void Connect(string portName, Int32 baudRate, System.IO.Ports.Parity parity, Int32 dataBits, System.IO.Ports.StopBits stopBits)
        {
            // Open COM port

            if (_port == null)
                _port = new SerialPort();

            // if port is open
            if (_port.IsOpen)

                // close it
                DisconnectCom();

            // try to open 
            try
            {
                // set parameters of port
                _port.PortName = portName;
                _port.BaudRate = baudRate;

                _port.Parity = parity;
                _port.DataBits = dataBits;
                _port.StopBits = stopBits;

                _port.RtsEnable = true;
                _port.DtrEnable = true;

                _port.ReceivedBytesThreshold = 1000;

                // open it
                _port.Open();

                // create the thread for reading data from the port
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // load function of the thread for reading data from the port
                try
                {
                    thrRead = new Thread(new ThreadStart(ReadData));
                    thrRead.IsBackground = true;
                    thrRead.Start();
                }
                catch (System.Exception)
                {

                }

                if (OnConnect != null)
                    OnConnect(this);

            }
            catch (System.Exception)
            {
                if (OnDisconnect != null)
                    OnDisconnect(this);

                //return false;
            }
        }


        public void Disconnect()
        {
            DisconnectCom();

            DisconnectEth();
        }

        private void DisconnectCom()
        {
            // clear in buffer
            try
            {
                _port.DiscardInBuffer();
            }
            catch (System.Exception)
            {


            }

            // clear in buffer
            try
            {
                _port.DiscardOutBuffer();
            }
            catch (System.Exception)
            {

            }

            try
            {
                // close port
                _port.Close();

                // destroy thread of reading
                if (thrRead != null)
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }

                // raise event
                if (OnDisconnect != null)
                    OnDisconnect(this);
            }

            catch (System.Exception)
            {

            }

        }

        // disconnet from server
        private void DisconnectEth()
        {
            // if there is client
            if (streamClient != null)
            {
                // close client stream
                try
                {
                    streamClient.Close();
                    streamClient = null;
                }
                catch (System.Exception)
                {
                    
                }

            }

            // if there is client
            if (tcpClient != null)
            {
                // close client connection
                try
                {
                    tcpClient.Close();
                    tcpClient = null;
                }
                catch (System.Exception )
                {
                    
                }

            }

            // if there is client
            if (thrRead != null)
            {
                // destroy thread for reading
                try
                {
                    thrRead.Abort();
                    thrRead.Join(500);
                    thrRead = null;
                }
                catch (System.Exception )
                {
                    
                }

            }

            // generate event
            if (OnDisconnect != null)
                OnDisconnect(this);
        }


        /*private bool WriteData(byte[] bSend)
        {
            try
            {
                streamClient.Write(bSend, 0, bSend.Length);

                if (OnWriteByte != null)
                    OnWriteByte(bSend);

                return true;
            }
            catch (System.Exception )
            {
                return false;
            }

        }*/

        private bool WriteData(byte[] bSend)
        {
            try
            {
                _port.Write(bSend, 0, bSend.Length);

                if (OnWriteByte != null)
                    OnWriteByte(this, bSend);

                return true;
            }
            catch (System.Exception)
            {
                return false;
            }

        }

        #region SendFunction

        public void SendText(byte bAddressReceive, string strText)
        {
            byte[] bSend = FormSendText(bAddressReceive, strText);

            WriteData(bSend);
        }

        public void SendSynchTime(byte bAddressReceive, TTimeMy tTime)
        {
            byte[] bSend = FormSendSynchTime(bAddressReceive, tTime);

            WriteData(bSend);
        }

        public void SendRegime(byte bAddressReceive, byte bRegime)
        {
            byte[] bSend = FormSendRegime(bAddressReceive, bRegime);

            WriteData(bSend);
        }

        public void SendRangeSector(byte bAddressReceive, byte bSign, TRangeSector[] tRangeSector)
        {
            byte[] bSend = FormSendRangeSector(bAddressReceive, bSign, tRangeSector);

            WriteData(bSend);
        }

        public void SendRangeSpec(byte bAddressReceive, byte bSign, TRangeSpec[] tRangeSpec)
        {
            byte[] bSend = FormSendRangeSpec(bAddressReceive, bSign, tRangeSpec);

            WriteData(bSend);
        }

        public void SendSupprFWS(byte bAddressReceive, int iDuration, TSupprFWS[] tSupprFWS)
        {
            byte[] bSend = FormSendSupprFWS(bAddressReceive, iDuration, tSupprFWS);

            WriteData(bSend);
        }

        public void SendSupprFHSS(byte bAddressReceive, int iDuration, TSupprFHSS[] tSupprFHSS)
        {
            byte[] bSend = FormSendSupprFHSS(bAddressReceive, iDuration, tSupprFHSS);

            WriteData(bSend);
        }

        public void SendState(byte bAddressReceive)
        {
            byte[] bSend = FormSendState(bAddressReceive);

            WriteData(bSend);
        }

        public void SendCoord(byte bAddressReceive)
        {
            byte[] bSend = FormSendCoord(bAddressReceive);

            WriteData(bSend);
        }

        public void SendReconFWS(byte bAddressReceive)
        {
            byte[] bSend = FormSendReconFWS(bAddressReceive);

            WriteData(bSend);
        }

        public void SendReconFHSS(byte bAddressReceive)
        {
            byte[] bSend = FormSendReconFHSS(bAddressReceive);

            WriteData(bSend);
        }

        public void SendExecBear(byte bAddressReceive, int iID, int iFreq)
        {
            byte[] bSend = FormSendExecBear(bAddressReceive, iID, iFreq);

            WriteData(bSend);
        }

        public void SendSimulBear(byte bAddressReceive, int iID, int iFreq)
        {
            byte[] bSend = FormSendSimulBear(bAddressReceive, iID, iFreq);

            WriteData(bSend);
        }

        public void SendStateSupprFWS(byte bAddressReceive)
        {
            byte[] bSend = FormSendStateSupprFWS(bAddressReceive);

            WriteData(bSend);
        }

        public void SendStateSupprFHSS(byte bAddressReceive, byte bLetter)
        {
            byte[] bSend = FormSendStateSupprFHSS(bAddressReceive);

            WriteData(bSend);
        }

        public void SendAudio(byte bAddressReceive, int iFreq)
        {
            byte[] bSend = FormSendAudio(bAddressReceive, iFreq);

            WriteData(bSend);
        }

        #endregion

/*
        #region FormSendFunction

        public byte[] SetServicePart(byte bAddressReceive, byte bCode, int iLength)
        {
            byte[] bHead = null;

            TServicePart tServicePart = new TServicePart();
            tServicePart.bAdrSend = bAddressSend;
            tServicePart.bAdrReceive = bAddressReceive;
            tServicePart.bCode = bCode;
            if (bCounter == 255)
                bCounter = 0;
            tServicePart.bCounter = bCounter++;
            tServicePart.iLength = iLength;

            bHead = StrArr.StructToByteArray(tServicePart);


            return bHead;
        }


        public byte[] FormSendText(byte bAddressReceive, string strText)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = Encoding.Default.GetBytes(strText);

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, TEXT, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch(SystemException)
            {}

            return bSend;
        }

        public byte[] FormSendSynchTime(byte bAddressReceive, TTimeMy tTime)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[3];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SYNCH_TIME, iLengthData);

            bData = StrArr.StructToByteArray(tTime);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch(SystemException)
            {}

            return bSend;
        }

        public byte[] FormSendRegime(byte bAddressReceive, byte bRegime)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[1];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, REGIME, iLengthData);

            bData[0] = bRegime;

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendRangeSector(byte bAddressReceive, byte bSign, TRangeSector[] tRangeSector)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tRangeSector != null)
            {
                int iLengthPack = 12;

                bData = new byte[1 + tRangeSector.Length * iLengthPack];
                bData[0] = bSign;

                for (int i = 0; i < tRangeSector.Length; i++)
                    Array.Copy(StrArr.StructToByteArray(tRangeSector[i]), 0, bData, 1 + i * iLengthPack, iLengthPack);
            }
            else
            {
                bData = new byte[1];
                bData[0] = bSign;
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RANGE_SECTOR, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendRangeSpec(byte bAddressReceive, byte bSign, TRangeSpec[] tRangeSpec)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tRangeSpec != null)
            {
                int iLengthPack = 8;

                bData = new byte[1 + tRangeSpec.Length * iLengthPack];
                bData[0] = bSign;

                for (int i = 0; i < tRangeSpec.Length; i++)
                    Array.Copy(StrArr.StructToByteArray(tRangeSpec[i]), 0, bData, 1 + i * iLengthPack, iLengthPack);
            }
            else
            {
                bData = new byte[1];
                bData[0] = bSign;
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RANGE_SPEC, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendSupprFWS(byte bAddressReceive, int iDuration, TSupprFWS[] tSupprFWS)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tSupprFWS != null)
            {
                int iLengthPack = 11;

                bData = new byte[4 + tSupprFWS.Length * iLengthPack];
                Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 4);

                for (int i = 0; i < tSupprFWS.Length; i++)
                    Array.Copy(StrArr.StructToByteArray(tSupprFWS[i]), 0, bData, 4 + i * iLengthPack, iLengthPack);
            }
            else
            {
                bData = new byte[4];
                Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 4);
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SUPPR_FWS, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendSupprFHSS(byte bAddressReceive, int iDuration, TSupprFHSS[] tSupprFHSS)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            if (tSupprFHSS != null)
            {
                int iLengthPack = 8;

                bData = new byte[4 + tSupprFHSS.Length * iLengthPack];
                Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 4);

                for (int i = 0; i < tSupprFHSS.Length; i++)
                    Array.Copy(StrArr.StructToByteArray(tSupprFHSS[i]), 0, bData, 4 + i * iLengthPack, iLengthPack);
            }
            else
            {
                bData = new byte[4];
                Array.Copy(BitConverter.GetBytes(iDuration), 0, bData, 0, 4);
            }

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE_SUPPR_FHSS, iLengthData);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendState(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array           
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE, iLengthData);


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendCoord(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array           
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, COORD, iLengthData);


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendReconFWS(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array           
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RECON_FWS, iLengthData);


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData,0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendReconFHSS(byte bAddressReceive)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array           
            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, RECON_FHSS, iLengthData);


            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendExecBear(byte bAddressReceive, int iFreq)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[4];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, EXEC_BEAR, iLengthData);

            Array.Copy(BitConverter.GetBytes(iFreq), 0, bData, 0, 4);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendSimulBear(byte bAddressReceive, int iFreq)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[4];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, SIMUL_BEAR, iLengthData);

            Array.Copy(BitConverter.GetBytes(iFreq), 0, bData, 0, 4);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        public byte[] FormSendStateSupprFWS(byte bAddressReceive, byte bLetter)
        {
            int iLengthData = 0;
            byte[] bData = null;
            

            // create data array
            bData = new byte[1];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE_SUPPR_FWS, iLengthData);

            bData[0] = bLetter;

            
           // create send array
           byte[] bSend = new byte[LEN_HEAD + iLengthData];

           try
           {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            {
            }

            return bSend;
        }

        public byte[] FormSendStateSupprFHSS(byte bAddressReceive, byte bLetter)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[1];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, STATE_SUPPR_FHSS, iLengthData);

            bData[0] = bLetter;

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            try
            {
                Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

                if (bData != null)
                    Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);
            }
            catch (SystemException)
            {
            }

            return bSend;
        }

        public byte[] FormSendAudio(byte bAddressReceive, int iFreq)
        {
            int iLengthData = 0;
            byte[] bData = null;

            // create data array
            bData = new byte[4];

            if (bData != null)
                iLengthData = bData.Length;

            // create head array
            byte[] bHead = SetServicePart(bAddressReceive, AUDIO, iLengthData);

            Array.Copy(BitConverter.GetBytes(iFreq), 0, bData, 0, 4);

            // create send array
            byte[] bSend = new byte[LEN_HEAD + iLengthData];

            Array.Copy(bHead, 0, bSend, 0, LEN_HEAD);

            if (bData != null)
                Array.Copy(bData, 0, bSend, LEN_HEAD, iLengthData);

            return bSend;
        }

        #endregion


        #region ReceiveFunction


        public void DecodeCommand(TServicePart tServicePart, byte[] bData)
        {
            if (tServicePart.bAdrReceive == bAddressSend)
            {
                switch (tServicePart.bCode)
                {
                    case TEXT:


                        break;

                    case SYNCH_TIME:

                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            TTimeMy tTime = new TTimeMy();
                            object obj = tTime;
                            StrArr.ByteArrayToStructure(bData, ref obj);
                            tTime = (TTimeMy)obj;

                            if (OnConfirmSynchTime != null)
                                OnConfirmSynchTime(this, tServicePart.bAdrSend, bCode, tTime);

                        }
                        catch (Exception)
                        { }
                        break;

                    case REGIME:
                        try
                        {
                            byte bCode = bData[0];

                            if (OnConfirmRegime != null)
                                OnConfirmRegime(this, tServicePart.bAdrSend, bCode);
                        }
                        catch (Exception)
                        { }
                        break;

                    case RANGE_SECTOR:
                        try
                        {
                            byte bCode = bData[0];

                            if (OnConfirmRangeSector != null)
                                OnConfirmRangeSector(this, tServicePart.bAdrSend, bCode);
                        }
                        catch (Exception)
                        { }
                        break;

                    case RANGE_SPEC:
                        try
                        {
                            byte bCode = bData[0];

                            if (OnConfirmRangeSpec != null)
                                OnConfirmRangeSpec(this, tServicePart.bAdrSend, bCode);
                        }
                        catch (Exception)
                        { }
                        break;

                    case SUPPR_FWS:
                        try
                        {
                            byte bCode = bData[0];

                            if (OnConfirmSupprFWS != null)
                                OnConfirmSupprFWS(this, tServicePart.bAdrSend, bCode);
                        }
                        catch (Exception)
                        { }
                        break;

                    case SUPPR_FHSS:
                        try
                        {
                            byte bCode = bData[0];

                            if (OnConfirmSupprFHSS != null)
                                OnConfirmSupprFHSS(this, tServicePart.bAdrSend, bCode);
                        }
                        catch (Exception)
                        { }
                        break;

                    case STATE:
                        try
                        {
                            byte bCode = bData[0];
                            byte bRegime = bData[1];

                            byte[] bLetter = new byte[bData.Length - 2];
                            Array.Copy(bData, 2, bLetter, 0, bLetter.Length);

                            if (OnConfirmState != null)
                                OnConfirmState(this, tServicePart.bAdrSend, bCode, bRegime, bLetter);

                        }
                        catch (Exception)
                        { }
                        break;

                    case COORD:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            TCoord tCoord = new TCoord();
                            object obj = tCoord;
                            StrArr.ByteArrayToStructure(bData, ref obj);
                            tCoord = (TCoord)obj;

                            if (OnConfirmCoord != null)
                                OnConfirmCoord(this, tServicePart.bAdrSend, bCode, tCoord);

                        }
                        catch (Exception)
                        { }
                        break;

                    case RECON_FWS:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 24;
                            int iCountPack = bData.Length / iLengthPack;

                            TReconFWS[] tReconFWS = null;

                            if (iCountPack > 0)
                            {
                                tReconFWS = new TReconFWS[iCountPack];
                                byte[] bPack = new byte[iLengthPack];
                                for (int i = 0; i < iCountPack; i++)
                                {
                                    Array.Clear(bPack, 0, bPack.Length);
                                    Array.Copy(bData, i * iLengthPack + 1, bPack, 0, iLengthPack);

                                    TReconFWS tReconTempFWS = new TReconFWS();
                                    object obj = tReconTempFWS;
                                    StrArr.ByteArrayToStructure(bPack, ref obj);
                                    tReconTempFWS = (TReconFWS)obj;

                                    tReconFWS[i] = tReconTempFWS;
                                }
                            }

                            if (OnConfirmReconFWS != null)
                                OnConfirmReconFWS(this, tServicePart.bAdrSend, bCode, tReconFWS);

                        }
                        catch (Exception)
                        { }
                        break;

                    case RECON_FHSS:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 33;
                            int iCountPack = bData.Length / iLengthPack;

                            TReconFHSS[] tReconFHSS = null;

                            if (iCountPack > 0)
                            {
                                tReconFHSS = new TReconFHSS[iCountPack];
                                byte[] bPack = new byte[iLengthPack];
                                for (int i = 0; i < iCountPack; i++)
                                {
                                    Array.Clear(bPack, 0, bPack.Length);
                                    Array.Copy(bData, i * iLengthPack + 1, bPack, 0, iLengthPack);

                                    TReconFHSS tReconTempFHSS = new TReconFHSS();
                                    object obj = tReconTempFHSS;
                                    StrArr.ByteArrayToStructure(bPack, ref obj);
                                    tReconTempFHSS = (TReconFHSS)obj;

                                    tReconFHSS[i] = tReconTempFHSS;
                                }
                            }

                            if (OnConfirmReconFHSS != null)
                                OnConfirmReconFHSS(this, tServicePart.bAdrSend, bCode, tReconFHSS);

                        }
                        catch (Exception)
                        { }
                        break;

                    case EXEC_BEAR:
                        try
                        {
                            byte bCode = bData[0];

                            short sOwnBearing = BitConverter.ToInt16(bData, 1);

                            if (OnConfirmExecBear != null)
                                OnConfirmExecBear(this, tServicePart.bAdrSend, bCode, sOwnBearing);

                        }
                        catch (Exception)
                        { }

                        break;

                    case SIMUL_BEAR:
                        try
                        {
                            byte bCode = bData[0];

                            short sOwnBearing = BitConverter.ToInt16(bData, 1);
                            short sLinkedBearing = BitConverter.ToInt16(bData, 3);

                            if (OnConfirmSimulBear != null)
                                OnConfirmSimulBear(this, tServicePart.bAdrSend, bCode, sOwnBearing, sLinkedBearing);

                        }
                        catch (Exception)
                        { }

                        break;

                    case STATE_SUPPR_FWS:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 2;
                            int iCountPack = bData.Length / iLengthPack;

                            TResSuppr[] tResSuppr = null;

                            if (iCountPack > 0)
                            {
                                tResSuppr = new TResSuppr[iCountPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    tResSuppr[i].bResCtrl = bData[i * iLengthPack + 1];
                                    tResSuppr[i].bResCtrl = bData[i * iLengthPack + 2];
                                }
                            }

                            if (OnConfirmStateSupprFWS != null)
                                OnConfirmStateSupprFWS(this, tServicePart.bAdrSend, bCode, tResSuppr);

                        }
                        catch (Exception)
                        { }
                        break;

                    case STATE_SUPPR_FHSS:
                        try
                        {
                            byte bCode = bData[0];

                            Array.Reverse(bData);
                            Array.Resize(ref bData, bData.Length - 1);
                            Array.Reverse(bData);

                            int iLengthPack = 2;
                            int iCountPack = bData.Length / iLengthPack;

                            TResSuppr[] tResSuppr = null;

                            if (iCountPack > 0)
                            {
                                tResSuppr = new TResSuppr[iCountPack];

                                for (int i = 0; i < iCountPack; i++)
                                {
                                    tResSuppr[i].bResCtrl = bData[i * iLengthPack + 1];
                                    tResSuppr[i].bResCtrl = bData[i * iLengthPack + 2];
                                }
                            }

                            if (OnConfirmStateSupprFHSS != null)
                                OnConfirmStateSupprFHSS(this, tServicePart.bAdrSend, bCode, tResSuppr);

                        }
                        catch (Exception)
                        { }
                        break;

                    default:
                        break;


                }
            }
        }
        #endregion*/

        // Read data (byte) (thread)
        public void ReadData()
        {
            byte[] bBufRead = new byte[LEN_ARRAY];
            byte[] bBufSave = new byte[LEN_ARRAY];

            int iReadByte = -1;
            int iTempLength = 0;

            bool blExistCmd = false;

            while (true)
            {
                try
                {
                    Array.Clear(bBufRead, 0, bBufRead.Length);

                    iReadByte = streamClient.Read(bBufRead, 0, bBufRead.Length);

                    if (iReadByte > 0)
                    {
                        Array.Resize(ref bBufRead, iReadByte);

                        if (OnReadByte != null)
                            OnReadByte(this, bBufRead);


                        Array.Copy(bBufRead, 0, bBufSave, iTempLength, iReadByte);

                        iTempLength += iReadByte;

                        while (iTempLength >= LEN_HEAD && blExistCmd)
                        {

                            byte[] bBufHead = new byte[LEN_HEAD];
                            Array.Copy(bBufSave, 0, bBufHead, 0, LEN_HEAD);

                            TServicePart tServicePart = new TServicePart();
                            object obj = tServicePart;
                            StrArr.ByteArrayToStructure(bBufHead, ref obj);
                            tServicePart = (TServicePart)obj;

                            int iLengthCmd = tServicePart.iLength;
                            if (iTempLength - LEN_HEAD >= iLengthCmd)
                            {
                                byte[] bBufDecode = new byte[iLengthCmd];
                                Array.Copy(bBufSave, LEN_HEAD, bBufDecode, 0, iLengthCmd);
                                DecodeCommand(tServicePart, bBufDecode);

                                Array.Reverse(bBufSave);
                                Array.Resize(ref bBufSave, bBufSave.Length - (LEN_HEAD + iLengthCmd));
                                Array.Reverse(bBufSave);

                                iTempLength -= (LEN_HEAD + iLengthCmd);

                            }

                            else

                                blExistCmd = false;

                        } // while (iTempLength > LEN_HEAD)
                    }
                    else 
                    {
                        if (OnDisconnect != null)
                            OnDisconnect(this);
                    }
                }

                catch (System.Exception )
                {

                }


            }
        }

        

    }
}
